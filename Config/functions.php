<?php

function reMonthesItems($items, $model, $field) {
    $results = array();
    foreach ($items as $item) {
        $month = $item[$model][$field];
        $results[date('Y-m-01', strtotime($month))][] = $item;
    }
    return $results;
}

function getSSecionsSettings($index = null) {
    $sections = array(
        SIDE_SOCIALFEED => array('title' => "Social Feed", 'element' => "socialfeed"),
        SIDE_IMAGES => array('title' => "Images", 'element' => "images"),
        SIDE_HINTS => array('title' => "Hints", 'element' => "hints"),
        SIDE_CRUMBS => array('title' => "Breadcrumbs", 'element' => "crumbs"),
    );
    if (!empty($index) && !empty($sections[$index])) {
        return $sections[$index];
    }
    return $sections;
}

function teams_element($index = null) {
    $index = empty($index) ? MANAGMENT_TEAM : $index;
    $teams = array(
        MANAGMENT_TEAM => "management_team",
        STRATA_MANAGERS => "strata_managers",
        SPECIALIST_PERSONAL => "specialist_personnel",
    );
    if (!empty($index) && !empty($teams[$index])) {
        return $teams[$index];
    }
    return $teams;
}

function getTeams($index = null) {
    $teams = array(
        MANAGMENT_TEAM => "Managment Team",
        STRATA_MANAGERS => "Strata Manager",
        SPECIALIST_PERSONAL => "Specialist Personnel",
    );
    if (!empty($index) && !empty($teams[$index])) {
        return $teams[$index];
    }
    return $teams;
}

function getStaticGalleries() {
    return array(SITE_PHOTO_GALLERY, MOBILE_PHOTO_GALLERY);
}

function getGalleryTypes($index = null) {
    $types = array(
        PHOTO_GALLERY => 'Phot Galley Page',
        BANNER_GALLERY => 'Banner Gallery',
        PAGECONTENT_GALLERY => 'Page Content Gallery',
        SIDEBAR_GALLERY => 'Side Bar Gallery',
        MOBILE_GALLERY => "Mobile Banner Gallery"
    );
    if (!empty($index) && !empty($types[$index])) {
        return $types[$index];
    }
    return $types;
}

function setIdsIndex($list, $model = '', $idkey = 'id', $subModel = false, $sub_id = 'id') {
    $result = array();
    foreach ($list as $value):
        $new_index = empty($model) ? $value[$idkey] : $value[$model][$idkey];
        $result[$new_index] = $value;
        if (!empty($subModel)) {
            $result[$new_index][$subModel] = setIdsIndex($value[$subModel], '', $sub_id);
        }
    endforeach;
    return $result;
}

function pageURL($page, $full = false) {
    $url = '';
    if (!empty($page['is_url'])) {
        $url = Router::url($page['url'], $full);
    } else {
        $url = Router::url('/content/' . $page['permalink'], $full);
    }
    return $url;
}

function checkCurrentMenu($menu) {
    $Pparams = $GLOBALS['Pparams'];
    $class = '';
    if ($menu == '/') {
        $class = ($Pparams['action'] == 'home') ? "current" : "";
    } elseif ($Pparams['controller'] == $menu) {
        $class = 'current';
    } elseif (!empty($Pparams['pass']) && $Pparams['pass'][0] == $menu) {

        $class = 'current';
    }
    return $class;
}

function generateCode($characters = 6) {
    /* list all possible characters, similar looking characters and vowels have been removed */
    $possible = '2345789ABCDEFGHKMNPQRSTUVWXZY';
    $code = '';
    $i = 0;
    while ($i < $characters) {
        $code .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
        $i++;
    }
    return $code;
}

function generatePerma($string) {
    return substr(Inflector::slug($string), 0, 70);
}

function strLimit($string, $limit = 150, $etc = '...') {
    if (strlen($string) < $limit)
        return $string;
    return substr($string, 0, max(array(strripos(substr($string, 0, $limit), ' '), strripos(substr($string, 0, $limit), "\n")))) . $etc;
}

function getVideoTypes() {
    return array('file' => "Video", 'url' => "YouTube URL");
}

/**
 * Escape carriage returns and single and double quotes for JavaScript segments.
 *
 * @param string $script string that might have javascript elements
 * @return string escaped string
 */
function escapeScript($script) {
    $script = str_replace(array("\r\n", "\n", "\r"), '\n', $script);
    $script = str_replace(array('"', "'"), array('\"', "\\'"), $script);
    return $script;
}

/**
 * Escape a string to be JavaScript friendly.
 *
 * List of escaped ellements:
 * 	+ "\r\n" => '\n'
 * 	+ "\r" => '\n'
 * 	+ "\n" => '\n'
 * 	+ '"' => '\"'
 * 	+ "'" => "\\'"
 *
 * @param  string $script String that needs to get escaped.
 * @return string Escaped string.
 */
function escapeString($string) {
    $escape = array("\r\n" => '\n', "\r" => '\n', "\n" => '\n', '"' => '\"', "'" => "\\'");
    return str_replace(array_keys($escape), array_values($escape), $string);
}

function strlow($str) {
    return trim(strtolower($str));
}

if (!function_exists('get_resized_image_url')) {

    function get_resized_image_url($image, $width = 0, $height = 0, $crop = false, $path = false) {
        $url = Router::url("/resize_image.php?image=" . urlencode(base64_encode($image)) . "&w=$width&h=$height", true);
        $url .= "&crop=$crop";
        if (!empty($path)) {
            $url .= "&path=" . urlencode($path);
        }
        return $url;
    }

}
?>