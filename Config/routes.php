<?php

Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
Router::connect('/home', array('controller' => 'pages', 'action' => 'home'));
Router::connect('/contact', array('controller' => 'contacts', 'action' => 'contactus'));
Router::connect('/search/*', array('controller' => 'pages', 'action' => 'search'));
Router::connect('/content/*', array('controller' => 'pages', 'action' => 'view'));

Router::connect('/admin', array('controller' => 'admins', 'action' => 'admin'));
Router::connect('/admin/loginAs/*', array('controller' => 'admins', 'action' => 'loginAs', 'prefix' => 'admin', 'admin' => true));
Router::connect('/admin/dealer_logout', array('controller' => 'admins', 'action' => 'dealer_logout', 'prefix' => 'admin', 'admin' => true));
Router::connect('/admin/logout', array('controller' => 'admins', 'action' => 'logout', 'prefix' => 'admin', 'admin' => true));

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';
