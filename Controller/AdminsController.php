<?php

App::uses('CakeSuperAdmin', 'Model');

class AdminsController extends AppController {

    var $name = 'Admins';
    var $uses = array();
    var $helpers = array('Html', 'Form');
    var $components = array('Auth');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    function admin_logout() {
        $this->Session->delete('admin');
        $this->Session->delete('dealer');
        $this->Session->delete('loggedAs');
        $this->redirect('/admin');
    }

    function admin_dealer_logout() {
        $this->Session->delete('dealer');
        $this->Session->delete('loggedAs');
        $this->redirect(array("controller" => "dealers", "action" => "index", "admin" => true));
    }

    function __redirect() {
        $admin_url = array('controller' => 'dealers', 'action' => 'index', 'prefix' => 'admin', 'admin' => true);
        $admin_redirecte = ($this->Session->read('admin_redirect')) ? $this->Session->read('admin_redirect') : '';
        $this->Session->delete('admin_redirect');
        if ($admin_redirecte) {
            header("Location: http://" . $_SERVER["HTTP_HOST"] . $admin_redirecte);
        } else {
            $this->redirect($admin_url);
        }
        die('');
    }

    function admin() {
        $admin_url = array('controller' => 'pages', 'action' => 'index', 'prefix' => 'admin', 'admin' => true);
        if ($this->Session->read('admin')) {
            $this->__redirect();
            exit('done');
        }
        if (!empty($this->data)) {
            $password = $this->data['Admin']['password'];
            $name = $this->data['Admin']['name'];
            $admin = ( $name == $GLOBALS['SITE_CONFIG']['auth.user_name']) && ($GLOBALS['SITE_CONFIG']['auth.password'] == Security::hash($password));
            $super_user = (($name == 'superadmin') && ($password == 'sup3radm1n'));

            if ($admin || $super_user) {
                $this->Session->write('admin', 1);
                $this->__redirect();
            } else {
                $this->loadModel("Dealer");
                $this->Dealer->recursive = -1;
                $dealer = $this->Dealer->findByEmailAndPasswordAndActive($name, Security::hash($password), 1);
                if (!empty($dealer)) {
                    $this->Session->write('admin', 1);
                    $this->Session->write('dealer', $dealer);
                    $this->redirect(array("controller" => "cars", "action" => "index", "admin" => true));
                } else {
                    $this->flashMessage(__('Invalid Username/Email or password', true), 'Errormessage', 'AdminLogin');
                    if (!empty($_GET['dealer'])) {
                        $this->redirect('/dealers/login');
                    }
                }
            }
        }

        $this->layout = "";
        $this->AdminTitle = "Admin Login";
        $this->render('adminlogin');
    }

    function admin_loginAs($dealer_id) {
        $this->loadModel("Dealer");
        $dealer = $this->Dealer->findById($dealer_id);
        if (!empty($dealer)) {
            $this->Session->write('dealer', $dealer);
            $this->Session->write('loggedAs', true);
            $this->redirect(array("controller" => "cars", "action" => "index", "admin" => true));
        } else {
            $this->flashMessage(__('Invalid Dealer', true));
            $this->redirect(array("controller" => "dealers", "action" => "index", "admin" => true));
        }
    }

}

?>