<?php

App::uses('Controller', 'Controller');
App::uses('Security', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = array('List', 'Number', 'Text');
    public $components = array('Session', 'Auth');
    public $pageTitle = null;
    public $metaDescription = null;
    public $metaKeywords = null;
    public $metaRobots = null;
    public $currentMenuID = null;
    public $crumbs = array();
    public $isMobile = null;
    public $loggedUser = false;

    public function beforeFilter() {
        parent::beforeFilter();
        Configure::write('Config.language', 'en');
        //Loading configurations
        $this->__load_config();
        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
//        debug($prefix);
        if (strtolower($prefix) == 'admin') {
            $this->Auth->allow();
            if (!defined('ADMIN_ZONE')) {
                define('ADMIN_ZONE', 1);
            }
            $this->__authenticate_admin();
            $this->layout = 'admin';
        } else {
            $user = $this->_is_user();
//            $this->_check_redirection($user);
//            debug($this->request->params);exit;

            $this->loggedUser = $user;
            $this->set('loggedUser', $user);
        }
        if (empty($this->titleAlias)) {
            $this->titleAlias = Inflector::humanize(Inflector::underscore($this->name));
        }
        $this->set('selectedMenu', $this->titleAlias);
        $this->set('titleAlias', $this->titleAlias);
    }

    function _check_redirection($user) {
        $login_action = Router::url($this->Auth->loginAction);
        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
        debug($prefix);
        $custom_urls = array(Router::url('/'), $login_action, Router::url(array('action' => 'delete_redirect')));
        if ($prefix != 'admin') {
            if ($this->Session->check('Auth.redirect') && !$user && !in_array($this->here, $custom_urls)) {
//            debug($this->Auth->loginRedirect);
//            debug($this->here);
//                $this->Session->delete('Auth.redirect');
                $this->redirect('/#UserLogin');
//                exit;
            }
        }
    }

    function __load_config() {
        Cache::config('default', array('engine' => 'File', 'prefix' => 'SiteCache_'));
        $this->loadModel('SiteConfig');
        $cached_config = Cache::read('site_config');
        if (empty($cached_config)) {
            $cached_config = $this->SiteConfig->get();
            Cache::write('site_config', $cached_config);
        }
        $GLOBALS['SITE_CONFIG'] = $cached_config;


        $this->config = $GLOBALS['SITE_CONFIG'];
        $config_for_layout = $this->config;
        unset($config_for_layout['auth.password']);
        unset($config_for_layout['auth.user_name']);
        $this->set('config', $config_for_layout);
    }

    public function beforeRender() {




        $this->__change_title();
        if (empty($this->params['prefix'])) {
            $this->_set_seo();

            if ($this->request->is('ajax')) {
                $this->layout = '';
            }
        }
        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName)) {
            $model = $this->modelName;
        }
        $this->set('modelName', $model);
        if (isset($this->pageTitle)) {
            $this->set('title_for_layout', $this->pageTitle);
        }
        $this->loadModel('TopBanner');
        $banners = $this->TopBanner->find('all', array('conditions' => array('TopBanner.active' => 1)));
        $this->set(compact('banners'));

        $this->loadModel('Link');
        $topLinks = $this->Link->find('all', array('conditions' => array('Link.active' => 1)));
        $this->set(compact('topLinks'));
        $this->load_topmenu();
        $this->set('footer_snippet', $this->get_snippet('footer'));
        $this->set('currentMenuID', $this->currentMenuID);
    }

    protected function __authenticate_admin($redirect = true) {
        if ($this->Session->read('admin')) {
            return true;
        } elseif (!empty($redirect)) {
            if (!$this->request->is('ajax')) {
                $this->Session->check('admin_redirect') or
                        $this->Session->write('admin_redirect', $_SERVER["REQUEST_URI"]) and
                        $this->flashMessage('Your session has ended, please re-login', 'Errormessage', 'AdminLogin');
            }
            ($this->request->is('ajax') and die(json_encode(array('status' => 'error', 'success' => false, 'message' => 'Your session has ended, Please re-login in admin panel and try again', 'error' => 'Your session has ended, Please re-login in admin panel and try again'))) ) or $this->redirect('/admin/');
            die('');
        } else {
            return false;
        }
    }

    protected function __authenticate_dealer() {
        if ($this->Session->read('dealer')) {
            return true;
        } else {
            return false;
        }
    }

    protected function __get_dealer_info() {
        return $this->Session->read('dealer');
    }

    protected function flashMessage($message, $class = 'Errormessage', $key = 'flash') {
        $this->Session->setFlash($message, 'default', array('class' => $class), $key);
    }

    public function admin_update_display_order() {
        //debug($this->params);
        $params = $this->params['data'];
        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName))
            $model = $this->modelName;
        foreach ($params as $field => $value) {
            if (strpos($field, 'display_order') === false) {
                continue;
            }
            $id = substr($field, strlen('display_order_'));
            $this->{$model}->create();
            $this->{$model}->id = $id;
            $this->{$model}->saveField('display_order', intval($value));
        }
        if ($this->request->is('ajax'))
            die(json_encode(array('status' => 'success', 'message' => 'Display Order Updated Successfully')));
        else
            $this->redirect($this->referer(array('action' => 'index'), true));
    }

    public function admin_update_active($field_name = 'active') {
        $params = $this->params['data'];

        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName))
            $model = $this->modelName;
        foreach ($params as $field => $value) {
            if (strpos($field, $field_name) === false) {
                continue;
            }
            $id = substr($field, strlen($field_name . '_'));
            $this->{$model}->create();
            $this->{$model}->id = $id;
            $this->{$model}->saveField($field_name, intval($value));
        }
        $this->redirect($this->referer(array('action' => 'index'), true));
    }

    private function _set_seo() {
        if (empty($this->metaDescription) && !empty($this->config['meta.description'])) {
            $this->metaDescription = $this->config['meta.description'];
        }
        if (empty($this->metaKeywords) && !empty($this->config['meta.keywords'])) {
            $this->metaKeywords = $this->config['meta.keywords'];
        }
        if (empty($this->metaRobots) && !empty($this->config['meta.robots'])) {
            $this->metaRobots = $this->config['meta.robots'];
        }

        $this->set('metaDescription', h(substr(preg_replace('/\\s+/', ' ', $this->metaDescription), 0, 500)));
        $this->set('metaKeywords', $this->metaKeywords);
        $this->set('metaRobots', $this->metaRobots);
    }

    private function __change_title() {
        if (empty($this->params['prefix']) || (isset($this->params['prefix']) && $this->params['prefix'] != 'admin')) {
            $this->loadModel('Seo');
            $record = $this->Seo->find('first', array('order' => 'INSTR("' . $this->here . '", `criteria`) desc , LENGTH(`criteria`) ', 'conditions' => array('("' . $this->here . '" LIKE CONCAT( \'%\' , `criteria`))')));
            if (!empty($record)) {
                $this->pageTitle = $record['Seo']['title'];
                $this->metaDescription = $record['Seo']['description'];
                $this->metaKeywords = $record['Seo']['keywords'];
                return true;
            }
        }


        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
        if (strtolower($prefix) == 'admin') {

            $prefix = empty($this->params['prefix']) ? false : $this->params['prefix'];
            $action = $this->params['action'];
            if ($prefix) {
                $titleArr[] = Inflector::humanize($this->params['prefix']);
                $action = substr($action, strlen("{$prefix}_"));
            }
            $titleArr[] = $this->titleAlias;
            if (strtolower($action) != 'index') {
                $titleArr[] = Inflector::humanize($action);
            }
        } elseif (!empty($this->pageTitle)) {
            $titleArr = array();
            $titleArr[] = $this->pageTitle;
        } else {
            $titleArr = array();
            $titleArr[] = Inflector::humanize(!empty($this->params['prefix']) ? substr($this->params['action'], strlen("{$this->params['prefix']}_")) : $this->params['action']);
        }


        $titleArr[] = $this->config['basic.site_name'];

        $this->pageTitle = implode(' - ', $titleArr);
    }

    public function reset_filter() {
        $this->autoRender = false;
        $modelName = Inflector::singularize($this->name);

        if (!empty($this->modelName)) {
            $modelName = $this->modelName;
        }

        $this->Session->delete("{$modelName}_Filter");
        echo "Success";
    }

    protected function _filter_params($params = false) {

        $modelName = Inflector::singularize($this->name);
        $conditions = array();
        if (!empty($this->modelName)) {
            $modelName = $this->modelName;
        }

        $filters = false;
        $ignore_session = false;

        if (!empty($this->{$modelName}->filters)) {
            $filters = $this->{$modelName}->filters;
        } else {
            $this->set(compact('filters', 'modelName'));
            return $conditions;
        }
        $this->set(compact('filters', 'modelName'));

        $url_params = $this->request->query;


        unset($url_params['url'], $url_params['page'], $url_params['sort'], $url_params['direction']);
        $params = empty($params) ? $url_params : $params;
        $sessionParams = $this->Session->read("{$modelName}_Filter");
        if (!$ignore_session && $sessionParams && empty($params))
            $params = $sessionParams;
        elseif (!$ignore_session && !empty($params)) {
            $this->Session->write("{$modelName}_Filter", $params);
        }

        foreach ($filters as $field => $filters) {
            if (is_numeric($field)) {
                $field = $filters;
                $type = '=';
                $param = $field;
            } elseif (is_string($filters)) {
                $type = $filters;
                $param = $field;
            } elseif (is_array($filters)) {
                $type = empty($filters['type']) ? '=' : $filters['type'];
                $param = $field;
            }
            switch (strtolower($type)) {
                case '=':
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field"] = $params[$param];
                    }
                    break;
                case 'like':
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field LIKE"] = "%{$params[$param]}%";
                    }
                    break;
                case 'date_range':
                    $from_param = empty($filters['from']) ? "from" : $filters['from'];
                    $to_param = empty($filters['from']) ? 'to' : $filters['to'];
                    if (!empty($params[$from_param])) {

                        $conditions["$modelName.$field >="] = date('Y-m-d 00:00:00', strtotime($params[$from_param]));
                    }
                    if (!empty($params[$to_param])) {

                        $conditions["$modelName.$field <="] = date('Y-m-d 23:59:59', strtotime($params[$to_param]));
                    }
                    break;
                case 'number_range':
                    $from_param = empty($filters['from']) ? 'from' : $filters['from'];
                    $to_param = empty($filters['from']) ? 'to' : $filters['to'];
                    if (!empty($params[$from_param]) || (isset($params[$param]) && strval($params[$from_param]) === '0')) {
                        $conditions["$modelName.$field >="] = $params[$from_param];
                    }
                    if (!empty($params[$to_param]) || (isset($params[$to_param]) && strval($params[$to_param]) === '0')) {
                        $conditions["$modelName.$field <="] = $params[$to_param];
                    }
                    break;
                default:
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field $type"] = $params[$param];
                    }
                    break;
            }
        }
        return $conditions;
    }

    protected function get_snippet($name) {
        $snippet = Cache::read('snippet.' . $name);

        if (empty($snippet)) {
            $this->loadModel('Snippet');
            $conditions['Snippet.permalink'] = $name;
            $snippet = $this->Snippet->find('first', array('conditions' => $conditions));
            if ($snippet) {
                Cache::write('snippet.' . $name, ($snippet['Snippet']['content']));
                return ($snippet['Snippet']['content']);
            } else {
                return '';
            }
        } else
            return ($snippet);
    }

    public function admin_clear_cache() {
        Cache::clear();
        foreach (array('persistent', 'models', 'views') as $type) {
            $files = glob(CACHE . $type . DS . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    @unlink($file);
            }
        }
        $this->flashMessage('Cache cleared successfully', 'Sucmessage');
        $this->redirect($this->referer(array('action' => 'index', 'controller' => 'pages')));
    }

    //--------------------------------------
    function admin_delete_field($type, $id, $field, $redirect = false) {
        if (!$redirect)
            $redirect = $this->referer();

        $type = ucfirst($type);

        if (!$id) {
            $this->flashMessage(__('Invalid Item ID', true));
            $this->redirect($redirect);
        } else if (!$field) {
            $this->flashMessage(__('Invalid Field Name', true));
            $this->redirect($redirect);
        } else {
            $modelName = Inflector::singularize($this->name);
            if (!empty($this->modelName))
                $modelName = $this->modelName;
            $this->{$modelName}->id = $id;
            $base_name = $this->{$modelName}->field($field);
            if (!$base_name) {
                $this->flashMessage(__('No ' . $type . ' is found', true));
                $this->redirect($redirect);
            }
            if (!$this->{$modelName}->deleteFile($field, $id, $base_name)) {

                $this->flashMessage(__($type . ' couldn\'t delete the ' . $type . ' from the hard disk ', true));
                $this->redirect($redirect);
            }

            if (!$this->{$modelName}->saveField($field, "", array('callbacks' => false))) {
                $this->flashMessage(__('Can\'t Update the item', true));
                $this->redirect($redirect);
            }

            $this->flashMessage(__('The ' . $type . ' has been removed', true), 'Sucmessage');
            $this->redirect($redirect);
        }
    }

    function admin_notfound() {
        $this->render('../notfound');
    }

    private function load_topmenu() {
        $topmenu = Cache::read('topmenu');
        if (!$topmenu) {
            $this->loadModel('Page');
            $conditions = array();
            $conditions['Menu.active'] = 1;
            $conditions['Menu.add_to_main_menu'] = 1;
            $conditions[] = "(Menu.menu_id = 0 OR Menu.menu_id IS NULL)";
            $this->Page->alias = "Menu";
            $topmenu = $this->Page->find('all', array('fields' => $this->Page->menuFields, 'conditions' => $conditions, 'order' => 'Menu.display_order'));
            Cache::write('topmenu', $topmenu);
            $this->Page->alias = "Page";
        }
        $this->set('topmenu', $topmenu);
    }

    public function _is_user() {
        return $this->Auth->user();
    }

    public function delete_redirect() {
        $this->Session->delete('Auth.redirect');
        exit;
    }

}
