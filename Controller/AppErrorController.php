<?php

App::uses('CakeErrorController', 'Controller');

class AppErrorController extends CakeErrorController {

	public function beforeRender() {
		$this->template = false;
	}

}

?>