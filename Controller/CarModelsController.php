<?php

App::uses('AppController', 'Controller');

/**
 * CarModels Controller
 *
 * @property CarModel $CarModel
 */
class CarModelsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        $this->CarModel->recursive = 0;
        $this->set('carModels', $this->paginate($conditions));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->CarModel->id = $id;
        if (!$this->CarModel->exists()) {
            throw new NotFoundException(__('Invalid car model'));
        }
        $this->set('carModel', $this->CarModel->read(null, $id));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = $this->_filter_params();
        $this->CarModel->recursive = 0;
        $makes = $this->CarModel->Make->find('list');
        $this->set(compact('makes'));
        $this->set('carModels', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->CarModel->id = $id;
        if (!$this->CarModel->exists()) {
            throw new NotFoundException(__('Invalid car model'));
        }
        $this->set('carModel', $this->CarModel->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->CarModel->create();
            if ($this->CarModel->save($this->request->data)) {
                $this->flashMessage(__('The car model has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The car model could not be saved. Please, try again.'));
            }
        }
        $makes = $this->CarModel->Make->find('list');
        $this->set(compact('makes'));
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->CarModel->id = $id;
        if (!$this->CarModel->exists()) {
            throw new NotFoundException(__('Invalid car model'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->CarModel->save($this->request->data)) {
                $this->flashMessage(__('The car model has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The car model could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->CarModel->read(null, $id);
        }
        $makes = $this->CarModel->Make->find('list');
        $this->set(compact('makes'));
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $carModels = $this->CarModel->find('all', array('conditions' => array('CarModel.id' => $id)));
            $this->set(compact('carModels'));
            return;
        }

        if (!empty($id)) {

            $this->CarModel->id = $id;
            if (!$this->CarModel->exists()) {
                throw new NotFoundException(__('Invalid car model'));
            }
            if ($this->CarModel->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Car model deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Car model was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->CarModel->deleteAll(array('CarModel.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Carmodels deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected carmodels'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
