<?php

App::uses('AppController', 'Controller');
App::import('Vendor', 'excel-xml');
App::import('Vendor', 'datasource-csv');

/**
 * Cars Controller
 *
 * @property Car $Car
 */
class CarsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    var $errors = array();
    var $logs = array();
    var $components = array('Paginator');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    function import_from_ftp() {
        ini_set("memory_limit", "1542M");
        set_time_limit(999999);

        $import_logs = array();
        $folder = FTP_IMPORT_PATH;

        $this->loadModel('FtpImportedCron');
        $accounts = $this->FtpImportedCron->find("all", array(
            "conditions" => array(
                "FtpImportedCron.active" => 1,
                "FtpImportedCron.is_ftp_created" => 1,
                "FtpImportedCron.is_ftp_created" => 1,
            //add condition for run time
            )
        ));

        if (!empty($accounts)) {
            foreach ($accounts as $account) {
                if (empty($account["FtpImportedCron"]["last_run"]))
                    $account["FtpImportedCron"]["last_run"] = date('Y-m-d', strtotime('- 2 days'));
                $last_run_date = date('Y-m-d', strtotime($account["FtpImportedCron"]["last_run"])) . ' ' . $account["FtpImportedCron"]["hour_to_run"] . ':00:00';
                $next_supposed_run = strtotime('+24 Hours', strtotime($last_run_date));
                if ($next_supposed_run < time()) {
                    $import_logs[] = date('Y-m-d H:i:s') . ' Start Importing data for FTP account "' . $account["FtpImportedCron"] ["ftp_username"] . '" Dealer #' . $account ["FtpImportedCron"]["dealer_id"] . '';
                    if (!empty($account["FtpImportedCron"]["file_name"]) && file_exists($file = $folder . $account["FtpImportedCron"] ["ftp_username"] . DS . $account["FtpImportedCron"]["file_name"])) {
                        $import_logs[] = ' Opening the file "' . $file . '" and start importing ...';
                        $result = $this->import_from_template($account["FtpImportedCron"] ["template_id"], $file);
                        if ($result) {
                            $this->import_images($account ["FtpImportedCron"]["dealer_id"], $folder . $account["FtpImportedCron"]["ftp_username"] . DS);
                        }
                        $import_logs = array_merge($import_logs, $this->errors, $this->logs);
                        if (!$result)
                            $import_logs [] = 'Failed To Import the File';
                    } else {
                        $import_logs[] = 'Couldn\'t find the file "' . $file . '" to import..exit!';
                    }

                    $import_logs[] = date('Y-m-d H:i:s') . ' End Importing for ' . $account["FtpImportedCron"]["ftp_username"];
                    $data[0]["id"] = $account["FtpImportedCron"]["id"];
                    $data[0] ["last_run"] = date('Y-m-d');
                    $this->FtpImportedCron->saveMany($data, array("validate" => false));
                    $import_logs[] = '---------------------------------------------';
                }
                $this->errors = array();
                $this->logs = array();
            }

            file_put_contents(
                    APP . 'webroot/logs/import_log_' . date('Y-m-d') . '.txt', implode("\r\n", $import_logs), FILE_APPEND);
            debug(($import_logs));
//            $log_lines = implode("\r\n", array_merge($this->errors, $this->logs));
        }

        die();
    }

    function admin_importing_data() {

//        $crumbs['controller'] = array('Step 1', 'admin/cars');
        //        $filename = "";
        $step = "step2";


        if (!empty($this->request->data)) {
            if ($this->request->data['Car']['step'] == "step2") {
                $step = $this->step2();
            } elseif ($this->request->data['Car']['step'] == "step3") {
                $step = $this->step3();
            }
//            $this->pageTitle = "Import Data";
        } else {
            if ($this->__authenticate_dealer()) {
                $dealer = $this->__get_dealer_info();
                $this->request->data['Car']['dealer_id'] = $dealer["Dealer"]["id"];
            }
        }

        $this->loadModel("Dealer");
        $this->set('dealers', $this->Dealer->find("list"));
        $this->loadModel("Template");
        if ($this->__authenticate_dealer()) {
            $dealer = $this->__get_dealer_info();
            $templates = $this->Template->find("list", array('conditions' => array('Dealer.id' => $dealer[
                    "Dealer"]["id"]), "fields" => array("Template.id", "Template.name"), "recursive" => 0));
        } else
            $templates = $this->Template->find("list", array("fields" => array("Template.id", "Template.name", "Dealer.name"), "recursive" => 0));


        $this->set(compact("templates"));

        $this->request->data['Car']['step'] = $step;
        $this->set("step", $step);
        if ($step == "step3") {
            $this->render("admin_step2");
        }
    }

    function step2() {
        $Error = "";
        $step = "step2";
        $file_extension = pathinfo($this->request->data['Car']['file']['name'], PATHINFO_EXTENSION);

        if (empty($this->request->data['Car']['file']["name"])) {
            $this->Car->validationErrors["file"] = __('No file selected. This field is required', true);
            $Error .= "Error while saving uploaded file" . "<br>";
        } /* elseif (!in_array($file_extension, array("csv"))) {
          $this->Car->validationErrors["file"] = __('Invalid file type. Types required CSV', true);
          $Error .= "Error while saving uploaded file" . "<br>";
          } */ elseif ($this->request->data['Car']['file']['error'] != 0) {
            $this->Car->validationErrors["file"] = __('Error while uploading file', true);
            $Error .= "Error while saving uploaded file" . "<br>";
        } elseif (empty($this->request->data[
                        'Car']['dealer_id'])) {
            $this->Car->validationErrors["dealer_id"] = __('No Dealer was selected. This field is required', true);
            $Error .= "Error while saving uploaded file" . "<br>";
        }





        $folder = WWW_ROOT . DS . "files" . DS . "excel" . DS;

        if (empty($Error)) {
            if (!empty($this->request->data["Car"]["filename"])) {
                $filename = $this->request->data["Car"]["filename"];
                $display_name = $this->request->data["Car"]["displayname"];
            } else {
                $filename = $this->request->data['Car']['user_time'] . "_" . str_replace(" ", "_", $this->request->data["Car"]["filename"]);
                $this->request->data["Car"]["filename"] = $filename;
                $display_name = $this->request->data['Car']['file']['name'];

                $this->request->data["Car"]["displayname"] = $display_name;
            }
        }

//         die();
        if (empty($Error)) {

            $path = $folder . $filename;
            $this->loadModel('Car');
            $fields_key = Car::$fields;
            $csv = new File_CSV_DataSource;
            $csv->settings(array('delimiter' => $this->request->data['Car']['delimiter']));
            $csv->load($this->request->data['Car']['file']['tmp_name']);
            $data_array = $csv->getrawArray();
            $columns = $data_array[0];
            $firstrow_cells = count($columns);
            $worng_rows = "";
            foreach ($columns as $key => &$value) {
                
            }
            $row = 0;

            if (count($columns) >= 1) {

                for ($i = 1; $i < count($data_array); $i++) {
                    $row++;
                    if (count($data_array[$i]) != $firstrow_cells) {

                        $worng_rows.= " " . $row + 1 . " ";
                    }
                }

                if (empty($worng_rows)) {

                    foreach ($columns as $j => $k) {
                        foreach ($fields_key as $i => $v) {
                            if (preg_replace('/[\s\-_]*/', ' ', strtolower(trim($k))) == preg_replace('/[\s\-_]*/', ' ', strtolower(trim($v)))) {
                                $this->request->data['Field'][$j] = $i;
                            }
                        }
                    }


                    if (!move_uploaded_file($this->request->data['Car']['file']['tmp_name'], $path)) {
                        $this->Car->validationErrors["file"] = __('Error while importing', true);
                        $Error = "Error while importing" . "<br>";
                    }
                } else {
                    $this->Car->validationErrors ["file"] = __('Records (' . $worng_rows . ') have different count columns from others', true);
                    $Error = "Error while importing" . "<br>";
                }
//                else if (count(explode(",", $worng_rows)) < 30)
//                    $Error = "These rows " . $worng_rows . " doesn't have the same number of cells of the first row";
//                else
                //                    $Error = "There are many rows doesn't have the same number of cells of the first row";
                if (empty($Error))
                    $step = "step3";
                $this->set("columns", $columns);
                $this->set("fields_key", $fields_key);
            } else {
                $Error = " There is no  columns in csv file" . "<br>";
            }
        }
        if (!empty($Error))
            $this->flashMessage($Error);


        $this->request->data['Car']['filename'] = $filename;

        $this->request->data['Car']['delimiter'] = $this->request->data['Car']['delimiter'];
        return $step;
    }

    function test() {
        $this->import_images(4, '/var/www/html/ozcars/webroot/ftp_folders/afs/');
        debug($this->errors);
        debug($this->logs);
        $this->autoRender = false;
        $this->layout = 'admin';
        $this->render('admin_index');
    }

    function import_images($dealer_id, $images_folder) {
        if (empty($dealer_id)) {
            $this->errors[] = 'FETAL EERRO: Please specify the dealer id';
            return false;
        }
        $dealer = $this->Car->Dealer->read(null, $dealer_id);
        if (empty($dealer)) {
            $this->errors[] = 'FETAL EERRO:  Deale r #' . $dealer_id . ' is not existed';
            return false;
        }
        $cars = $this->Car->find("all", array(
            "conditions" => array("Car.dealer_id" => $dealer_id,
                "Car.deleted <>" => 1,
            )
                )
        );
        $extensions_array = array('.png', '.PNG', '.jpg', '.JPG');
        $found_images = array();

        $images_to_remove = array();
        $pervious_imported_images = array();

        foreach ($cars as $car) {


            foreach ($car['CarImage'] as $image) {
                if ($image['automatic_imported']) {
                    $pervious_imported_images[$image['image']] = $image;
                    $pervious_imported_images[$image['image']]['path'] = IMAGES_FOLDER . $image['image'];
                    $remove_images[$image['image']] = $image['id'];
                }
            }

            for ($i = 0; $i <= 36; $i++) {
                foreach ($extensions_array as $ext) {

                    if (file_exists($file = $images_folder . $dealer_id . '-' . $car['Car'] ['stock_number'] . '-' . $i . $ext)) {
                        $found_images[basename($file)] = array(
                            'image' => basename($file),
                            'path' => $file,
                            'automatic_imported' => 1,
                            'car_id' => $car['Car']['id'],
                            'display_order' => $i * 10
                        );
                    }
                }
                if ($i - count($found_images) > 4)
                    break;
            }

//Compare both found images previulsy imported iamges
//Get Existed Images pathes for this car
        }

        $images_data = array();
        foreach ($found_images as $key => $found_image) {



            if (isset($pervious_imported_images[$found_image['image']]) &&
                    filesize($pervious_imported_images[$found_image['image']] ['path']) == filesize($found_image['path']) && md5_file($pervious_imported_images[$found_image['image']]['path']) == md5_file($found_image['path'])) {
                unset($found_images[$key]);
                unset($remove_images[$found_image['image']]);

                $this->logs[] = ' found existed image (' . $found_image['image'] . '), will not re-import';
            } else {
                $this->logs[] = 'found new image (' . $found_image['image'] . '), importing it...';
                if (copy($found_image['path'], IMAGES_FOLDER . $found_image['image'])) {

                    chmod(IMAGES_FOLDER . $found_image['image'], 0777);
                    chown(IMAGES_FOLDER . $found_image['image'], 'apche');
                    $images_data[] = $found_image;
                    $this->logs[] = 'Copied image "' . $found_image['image'] . '" into folder "' . IMAGES_FOLDER .
                            "'";
                } else {
                    $this->errors[] = ' Unable to copy image file ' . $found_image['image'] . ' into folder ' . IMAGES_FOLDER;
                    return false;
                    unset($found_images[$key]);
                }
            }
        }



        if (count($found_images) > 0) {
//debug($images_data);
            if ($this->Car->CarImage->saveMany($images_data)) {
                $this->logs[] = 'Inserted new found image into database ';
                $this->logs [] = 'imported new ' . count($found_images) . ' images succesfully';
                return true;
            } else {
                $this->errors[] = 'FETAL ERROR: Couldn\'t insert images into database ';
                return false;
            }
        } else {
            $this->logs[] = 'No new images have been found to import';
            return true;
        }
    }

    function admin_delete_images($ids, $redirect = false) {

        if ($redirect == '1')
            $redirect = $this->referer();

        if (!empty($ids)) {
            if ($this->Car->CarImage->deleteAll(array('CarImage.id' => $ids))) {
                $this->flashMessage(__('Image(s) has been removed', true), 'Sucmessa ge');
            } else {
                $this->flashMessage(__('Error during deleting the image', true));
            }
        } else {
            $this->flashMessage(__('invalid image ids', true));
        }

        if ($redirect) {
//$this->render('index');
            $this->redirect($redirect);
        } else
            return true;
    }

    function delete_car_images($ids, $redirect = false) {
        if (!$redirect)
            $redirect = $this->referer();

        if (!empty($ids)) {
            if ($this->Car->CarImage->deleteAll(array('conditions' => array('CarImage .id' => $ids)))) {
                $this->flashMessage(__('Image(s) has been removed', true), 'Sucmessage');
            } else {
                $this->flashMessage(__('Error during deleting the image', true));
            }
        } else {
            $this->flashMessage(__('invalid image ids', true));
        }

        if ($redirect) {
            $this->redirect($redirect);
        } else
            return true;
    }

    function import_cars($filename, $dealer_id, $cars_type, $delimiter, $import_first_row, $mapping_fields) {
        $this->errors = array();
        $this->loadModel('Car');

        if (!in_array("stock_number", $mapping_fields)) {
            $this->errors[] = "You must specify the 'Stock Number' field ";
            return false;
        }
        if (!file_exists(
                        $filename)) {
            $this->errors[] = "FETAL ERROR: File is not existed '$filename' ";
            return false;
        }
        if (!is_readable($filename)) {
            $this->errors[] = "FETAL ERROR: File is not redable '$filename' ";
            return false;
        }
        $csv = new File_CSV_DataSource;
        $csv->settings(array('delimiter' => $delimiter));

        $csv->load($filename);

        $data_array = $csv->getrawArray();

        $columns = $data_array[0];
        $row = 0;

        $deleted_cars_count = 0;
        $updated_cars_count = 0;
        $inserted_cars_count = 0;


        if (count($columns) != count($mapping_fields)) {
            $this->errors[] = "FETAL ERROR: Number of columns in the CSV and Template are not equal ";
            $this->errors[] = "FILE COLUMNS: (" . implode(',', $columns) . ")";
            $this->errors[] = "TEMPLATE COLUMNS: (\"" . implode('","', $mapping_fields) . "\")";
            return false;
        }

        if (count($columns) >= 1) {


            if (count($data_array) > 1) {

                $this->loadModel("DealerLocation");
                $locations = $this->DealerLocation->find("list", array(
                    "conditions" => array(
                        "DealerLocation.dealer_id" => $dealer_id
                    ),
                    "fields" => array("DealerLocation.code", "DealerLocation.id"
                    )
                        )
                );


                $ls = array();
                foreach ($locations as $key => $location)
                    $ls[trim(strtolower($key))] = $location;
                $locations = $ls;

                $this->loadModel("Make");
                $this->Make->virtualFields = array('name_lowered' => "LOWER(Make.name)");
                $makes = $this->Make->find("list", array(
                    "fields" => array(
                        "Make.name_lowered", "Make.id"
                    )
                        )
                );
                $this->loadModel("CarModel");
                $this->CarModel->virtualFields = array('name_lowered' => "LOWER(CarModel.name)");
                $models = $this->CarModel->find("list", array(
                    "fields" => array(
                        "CarModel.name_lowered", "CarModel.id")
                        )
                );
            }
        }

        $all_data = array();


        $x = $import_first_row ? 0 : 1;

        for ($i = $x; $i < count($data_array); $i++) {
            foreach ($mapping_fields as $key => $field) {
                if (!empty($field)) {
                    if ($field == "dealer_location_code") {
                        $data_array[$i][$key] = strtolower(trim($data_array[$i][$key]));
                        if (!empty($locations) && !empty($locations[$data_array[$i][$key]])) {
                            $all_data[$i]["dealer_location_id"] = $locations[$data_array[$i][$key]];
                        } else
                            $this->logs[] = 'WARNING: Dealer Location (' . $data_array[$i][$key] . ') in record (' . ($i + 1) .
                                    ') does not exist in the system models directory. Please add it and re-import the file ';
                    } elseif ($field == "make") {
                        if (!empty($makes[strtolower(trim($data_array[$i][$key]))])) {
                            $all_data[$i]["make"] = $makes[strtolower(trim($data_array[$i][$key]))];
                        } else {
                            $all_data[$i]["make"] = trim($data_array[$i][$key]);
                            $this->logs[] = 'WARNING: Make (' . $data_array[$i][$key] . ') in record (' . ($i + 1) . ') does not exist in the system models directory. Please add it and re-import the file ';
                        }
                    } elseif ($field == "model") {
                        if (!empty($models[strtolower(trim($data_array[$i][$key]))])) {
                            $all_data[$i]["model"] = $models[strtolower(trim($data_array[$i][$key]))];
                        } else {
                            $all_data[$i]["model"
                                    ] = trim($data_array[$i][
                                    $key]);
                            $this->logs[] = 'WARNING: Model (' . $data_array[$i][$key] . ') in record (' . ($i + 1) . ') does not exist in the system models directory. Please add it and re-import the file ';
                        }
                    } else {
                        $all_data[$i][$field] = trim($data_array[$i][$key]);
                    }
                }
            }
        }

        if (!empty($all_data)) {
            $row = count($all_data);
            $current_cars = $this->Car->find("list", array(
                "conditions" => array(
                    "Car.dealer_id" => $dealer_id,
                    'Car.deleted <> ' => '1'
                ),
                "fields" => array(
                    "Car.stock_number", "Car.id"
                )
            ));

            $existed_stocks = array();
            $none_existed_stocks = array();
            $file_stocks = array();
            foreach ($all_data as $ii => &$data_row) {
                if (empty(
                                $data_row["stock_number"])) {

                    $this->errors[] = "FETAL ERROR: Records No.   $ii  has no stock number";
                    return false;
                }
                if (in_array($data_row["stock_number"], $file_stocks)) {
                    $this->errors[] = "FETAL ERROR: Records No. $ii has a duplicated stock number ( {$data_row["stock_number"]})";
                    return false;
                }

                $file_stocks[] = $data_row["stock_number"];
                if (isset($current_cars[$data_row["stock_number"]])) {
                    $data_row["id"] = $current_cars[$data_row["stock_number"]];

                    $existed_stocks[] = $data_row["stock_number"];
                } else {
                    $none_existed_stocks[] = $data_row["stock_number"];
                }
                $data_row["dealer_id"] = $dealer_id;
                $data_row["deleted"] = 0;
                $data_row["type_id"] = $cars_type;
            } $new_ids = Hash::combine($all_data, '{n}.stock_number', '{n}.stock_number');
            if (!$this->Car->UpdateAll(
                            array('Car.deleted' => true), array("Car.dealer_id" => $dealer_id, "NOT" => array("Car.stock_number" => $new_ids)))) {
                $this->errors[] = "FETAL ERROR: Unable to remove non-existed cars";
                return false;
            }
            $deleted_cars_count = $this->Car->getAffectedRows();
            $this->logs [] = 'Removed ' . $deleted_cars_count . ' old cars which are not longer existed in the new imported CSV file: ';







//                debug($all_data);
//                die();
            $this->Car->create();
            if (!$this->Car->saveMany($all_data, array("validate" => false))) {
                $this->errors[] = "FETAL ERROR: Records could not be imported. Please try again";
                return false;
            }
            if (count($existed_stocks) > 0) {
                $this->logs[] = 'Updated ' . count($existed_stocks) . ' Existed Cars:';
                $this->logs[] = '(' . implode(', ', $existed_stocks) . ')';
            } else
                $this->logs[] = 'No Existed Stock To Update';

            if (count($none_existed_stocks) > 0) {
                $this->logs[] = 'Inserted New ' . count($none_existed_stocks) . ' Cars:';
                $this->logs[] = '(' . implode(', ', $none_existed_stocks) . ')';
            } else
                $this->logs[] = 'No new cars to insert';

            $this->logs[] = ("Total $row records has been imported successfully");
            return true;
        } else {
            $this->errors[] = ("No da ta has been imported");
            return
                    false;
        }
    }

    function step3() {
        $Error = "";
        $all_data = array();
        $folder = WWW_ROOT . DS . "files" . DS . "excel" . DS;
        $this->loadModel('Car');





        if (!empty($this->request->data['Car']['template_id'])) {
            $filename = $this->request->data['Car']['file']['tmp_name'];

            $result = $this->import_from_template($this->request->data['Car']['template_id'], $filename);
        } else {

            $filename = $folder . $this->request->data['Car']['filename'];

            $result = $this->import_cars($filename, $this->request->data['Car']["dealer_id"], $this->request->data['Car']["type_id"], $this->request->data['Car']['delimiter'], $this->request->data['Car']['import_first_row'], $this->request->data['Field']);
        }


        if ($result) {
            if (!empty($this->request->data['save_as_template'])) {
                if (empty($this->request->data['Car']['template_name']))
                    $this->request->data['Car']['template_name'] = 'Untitled Template_' . $this->request->data['Car']['displayn ame'];

                $this->loadModel('Template');
                $data['Template'] = array(
                    'name' => $this->request->data['Car']['template_name'],
                    'dealer_id' => $this->request->data['Car']["dealer_id"],
                    'type_id' => $this->request->data['Car']["type_id"],
                    'delimiter' => $this->request->data['Car']['delimiter'],
                    'import_first_row' => $this->request->data['Car']['import_first_row']
                );
                $data['MapField'] = array();

                foreach ($this->request->data['Field'] as $key => $field) {
                    $data['MapField'][] = array('csv_field_order' => $key, 'field' => $field);
                }


                $this->Template->saveAll($data);
            }

            $this->flashMessage(implode("<br/>", $this->logs), 'Sucmessage');

//$this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(implode("<br/>", $this->errors));
            return "step2";
        }
    }

    function import_from_template($template_id, $filename) {
        $this->loadModel("Template");
        $template_data = $this->Template->find("first", array(
            "conditions" => array("Template.id" => $template_id),
                )
        );
        $fields = Hash::combine($template_data, 'MapField.{n}.csv_field_order', 'MapField.{n}.field');

        return $this->import_cars($filename, $template_data["Template"]["dealer_id"], $template_data["Template"]["type_id"], $template_data["Template"]["delimiter"], $template_data["Template"]['import_first_row'], $fields);
    }

    function _template_proccessing() {


        $folder = WWW_ROOT . DS . "files" . DS . "excel" . DS;
        $file_extension = pathinfo($this->request->data['Car']['file']['name'], PATHINFO_EXTENSION);

        if (empty($this->request->data['Car']['file']["name"])) {
            $this->Car->validationErrors["file"] = __('No file selected. This field is required', true);
            $Error .= "Error while saving uploaded file" . "<br>";
        } /* elseif (!in_array($file_extension, array("csv"))) {
          $this->Car->validationErrors["file"] = __('Invalid file type. Types required CSV', true);
          $Error .= "Error while saving uploaded file" . "<br>";
          } */ elseif ($this->request->data['Car']['file']['error'] != 0) {
            $this->Car->validationErrors["file"] = __('Error while uploading file', true);
            $Error .= "Error while saving uploaded file" . "<br>";
        }

        $filename = $this->request->data['Car']['file']['tmp_name'];
        $csv = new File_CSV_DataSource;
        $csv->settings(array('delimiter' => $this->request->data['Car']['delimiter']));
        $csv->load($filename);

        $data_array = $csv->getrawArray();
        $columns = $data_array[0];

        if (count($columns) != count($template_data["MapField"])) {
            $this->Car->validationErrors["file"] = __('Error while uploading file', true);
            $Error = "Number of columns is not equal" . "<br>";
        }
        $has_stock_number = false;
        foreach ($template_data["MapField"] as $field_row) {
            if ($field_row["field"] == "stock_number") {
                $has_stock_number = true;
                break;
            }
        }
        if (!$has_stock_number) {
            $this->Car->validationErrors["file"] = __('Error while uploading file', true);
            $Error = "The Selected Template has no Stock Number field" . "<br>";
        }


        if (!empty($Error)) {
            $this->flashMessage($Error);
            $this->redirect(array("action" => "importing_data"));
        }

        if (empty($Error)) {
            if (!empty($this->request->data["Car"]["filename"])) {
                $filename = $this->request->data["Car"]["filename"];
                $display_name = $this->request->data["Car"]["displayname"];
            } else {
                $filename = $this->request->data['Car']['user_time'] . "_" . str_replace(" ", "_", $this->request->data["Car"]["filename"]);
                $this->request->data["Car"]["filename"] = $filename;
                $display_name = $this->request->data['Car']['file']['name'];

                $this->request->data["Car"]["displayname"] = $display_name;
            }
            if (!move_uploaded_file($this->request->data['Car']['file']['tmp_name'], $folder . $filename)) {
                $this->Car->validationErrors["file"] = __('Error while saving uploaded file', true);
                $Error = "Error while saving uploaded file" . "<br>";
            }
        }



        $this->request->data['Field'] = Hash::combine($template_data, 'MapField.{n}.csv_field_order', 'MapField.{n}.field');

        $this->request->data['Car']['dealer_id'] = $template_data["Template"]["dealer_id"];
        $this->request->data['Car']['delimiter'] = $template_data["Template"]["delimiter"];
        $this->request->data['Car']['type_id'] = $template_data["Template"]["type_id"];
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->__form_common();
        $this->set("types", Car::$types);
        $conditions = array();
        if ($this->__authenticate_dealer()) {
            unset($this->Car->filters["dealer_id"]);
            $dealer = $this->__get_dealer_info();
            $conditions["Car.dealer_id"] = $dealer["Dealer"]["id"];
        }
        $conditions["Car.deleted <> "] = 1;
        $conditions[] = $this->_filter_params();
        $this->Car->recursive = 1;
        $this->set('cars', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Car->id = $id;
        if (!$this->Car->exists()) {
            throw new NotFoundException(__('Invalid car'));
        }
        $this->set('car', $this->Car->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {

        $dealer_id = false;
        if ($this->__authenticate_dealer()) {
            $dealer = $this->__get_dealer_info();
            $dealer_id = $dealer["Dealer"]["id"];
            $this->request->data["Car"]["dealer_id"] = $dealer_id;
        }
        if ($this->request->is('post')) {
            $this->Car->create();
            if ($this->Car->save($this->request->data)) {
                $this->__submit_common();
                $this->flashMessage(__('The car has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'edit', $this->Car->id));
            } else {
                $this->flashMessage(__('The car could not be saved. Please, try again.'));
            }
        }
        $this->__form_common($dealer_id);
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Car->id = $id;
        if (!$this->Car->exists()) {
            throw new NotFoundException(__('Invalid car'));
        }
        $dealer_id = false;
        if ($this->__authenticate_dealer()) {
            $dealer = $this->__get_dealer_info();
            $dealer_id = $dealer["Dealer"]["id"];
            $this->request->data["Car"]["dealer_id"] = $dealer_id;
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Car->save($this->request->data)) {
                $this->__submit_common();
                $this->flashMessage(__('The car has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->flashMessage(__('The car could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Car->read(null, $id);
        }

        $this->__form_common($dealer_id);
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common($dealer_id = false) {
        $makes = $this->Car->Make->find('list');
        $models = $this->Car->CarModel->find('list');
        $dealers = $this->Car->Dealer->find('list');
        if (!empty($dealer_id)) {
            $dealerLocations = $this->Car->DealerLocation->find('list', array("conditions" => array("DealerLocation.dealer_id" => $dealer_id)));
        } else {
            $dealerLocations = $this->Car->DealerLocation->find('list');
        }
        $this->set(compact('makes', 'models', 'dealers', 'dealerLocations'));
        $car_file_settings = $this->Car->CarImage->getFileSettings();
        $this->set(compact('car_file_settings'));
        return true;
    }

    /**
     * common method for add an d edit s ubmissions
     *
     * @return boolean 
     */
    function __submit_common() {
        $this->Car->save_related_images($this->request->data);
        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->
                    request->data('ids');
            $cars = $this->Car->find('all', array('conditions' => array('Car.id' => $id)));
            $this->set(compact('cars'));
            return;
        }

        if (!empty($id)) {

            $this->Car->id = $id;
            if (!$this->Car->exists()) {
                throw new NotFoundException(__('Invalid car'));
            }
            if ($this->Car->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Car deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Car was not deleted'));
                $this->redirect(array('action' => 'index'))
                ;
            }
        } elseif (is_array($this
                        ->request->data('ids'))) {
            if ($this->Car->deleteAll(array('Car.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Cars deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error'
                    )));
                } else {
                    $this->flashMessage(__('Error deleting selected cars'));
                    $this->redirect(array('action'
                        => 'index'));
                }
            }
        }
    }

    public function search($more = "") {
        $data = $this->params->query;
//        debug($data);exit;
        $limit = 20;
        if (!empty($this->request->params['named']['limit'])) {
            $limit = $this->request->params['named']['limit'];
        }

        if (!isset($data['search_any'])) {
            $options = $this->Car->search_params($data, $limit);
        } else {
            $options = $this->Car->search_params($data, $limit, true);
        }

        if (!empty($this->request->params['named']['save'])) {
            $user = $this->loggedUser;
            if (!empty($user)) {
                $this->loadModel('UserSearch');
                $this->UserSearch->create();
                if ($this->UserSearch->save(array('UserSearch' => array('user_id' => $user['id'], 'search_parameters' => $data)))) {
                    $this->flashMessage(__('Your search has been saved successfully', true), 'Notemessage');
                }
            } else {
                $url = array('controller' => 'cars', 'action' => 'search', 'save' => 1, '?' => $data);
                $this->Session->write('Auth.redirect', $url);
                $this->redirect($this->Auth->loginAction);
            }
        }

        $this->request->params['named']['limit'] = $options['limit'];
        $options['limit'] = 1; 
        $this->Paginator->settings = $options;
        $cars = $this->Paginator->paginate('Car');

        if (!empty($options['conditions']['Car.type_id']) && count($options['conditions']) == 1) {
            $count_conditions = $options['conditions'];
            if ($options['conditions']['Car.type_id'] == 1) {
                $new_conditions = $options['conditions'];
                $demo_conditions = array_merge($count_conditions, array('Car.type_id !=' => 1, 'Car.type_id != ' => 2));
                $used_conditions = array_merge($count_conditions, array('Car.type_id !=' => 1, 'Car.type_id != ' => 3));
            } elseif ($options['conditions']['Car.type_id'] == 2) {
                $new_conditions = array_merge($count_conditions, array('Car.type_id !=' => 2, 'Car.type_id != ' => 3));
                $demo_conditions = array_merge($count_conditions, array('Car.type_id !=' => 2, 'Car.type_id != ' => 3));
                $used_conditions = $options['conditions'];
            } elseif ($options['conditions']['Car.type_id'] == 3) {
                $new_conditions = array_merge($count_conditions, array('Car.type_id !=' => 2, 'Car.type_id != ' => 3));
                $used_conditions = array_merge($count_conditions, array('Car.type_id !=' => 1, 'Car.type_id != ' => 3));
                $demo_conditions = $options['conditions'];
            }
        } else {
            unset($options['conditions']['Car.type_id']);
            $count_conditions = $options['conditions'];
            $new_conditions = array_merge($count_conditions, array('Car.type_id' => 1));
            $used_conditions = array_merge($count_conditions, array('Car.type_id' => 2));
            $demo_conditions = array_merge($count_conditions, array('Car.type_id' => 3));
        }

        $contain = array('CarImage', 'Dealer', 'DealerLocation');

        $total_cars = $this->Car->find('count', array('conditions' => $count_conditions, 'contain' => $contain));
        $new_cars = $this->Car->find('count', array('conditions' => $new_conditions, 'contain' => $contain));
        $demo_cars = $this->Car->find('count', array('conditions' => $demo_conditions, 'contain' => $contain));
        $used_cars = $this->Car->find('count', array('conditions' => $used_conditions, 'contain' => $contain));


        $this->set($this->Car->getAllFields2());
        $this->set('params', $data);
        $this->set(compact('cars', 'total_cars', 'new_cars', 'used_cars', 'demo_cars'));

        if ($this->request->is('ajax')) {
            $view = new View();
            if ($more == "") {
                die($view->element('cars/search-results', compact('cars', 'total_cars', 'new_cars', 'used_cars')));
            } elseif ($more == "map_search") {
                if (count($cars) == 0) {
                    die();
                }

                die($view->element('cars/search_results_map_search', compact('cars', 'total_cars', 'new_cars', 'used_cars')));
            } else {
                die($view->element('cars/search-results', compact('cars', 'total_cars', 'new_cars', 'used_cars')));
            }
            exit;
        }


//        exit;
    }

    function advanced_search() {
        $this->set($this->Car->getLocationFields());
        $dealers = $this->Car->Dealer->find('list');
        $this->set($this->Car->getAllFields2());
        $this->set(compact('dealers'));
    }

    function map_search() {
        $data = $this->params->query;
//        debug($data);
        $conditions = array();
        $fields = array('count(`Car`.`id`) as CarsCount',
            'DealerLocation.longitude',
            'DealerLocation.id',
            'DealerLocation.latitude', 'DealerLocation.zoom', 'DealerLocation.name'
        );

        if (!empty($data)) {
            $options = $this->Car->search_params($data);
            $conditions = $options['conditions'];
        }
        $dealerss = $this->Car->find('all', array('group' => 'DealerLocation.id', 'conditions' => $conditions, 'fields' => $fields, 'contain' => array('DealerLocation', 'Dealer')));

        $locationss = array();
        if (!empty($dealerss)) {
            foreach ($dealerss as $dealer) {
                $locationss[$dealer['DealerLocation']['id']]['dealer_id'] = $dealer['DealerLocation']['id'];
                $locationss[$dealer['DealerLocation']['id']]['lat'] = $dealer['DealerLocation']['latitude'];
                $locationss[$dealer['DealerLocation']['id']]['long'] = $dealer['DealerLocation']['longitude'];
                $locationss[$dealer['DealerLocation']['id']]['count'] = $dealer[0]['CarsCount'];
            }
        }
        if ($this->request->is('ajax')) {
            echo json_encode($locationss);
            exit;
        }
        $this->set($this->Car->getLocationFields());
        $dealers = $this->Car->Dealer->find('list');
        $this->set($this->Car->getAllFields2());
        $this->set('params', $data);

        $this->set(compact('dealers', 'locationss'));
    }

    function get_map_snippet($dealer_id) {
        $view = new View();
        $fields = array(
            'count(`Car`.`id`) as CarsCount',
            'DealerLocation.longitude',
            'DealerLocation.id',
            'DealerLocation.latitude', 'DealerLocation.zoom', 'DealerLocation.name', 'DealerLocation.address1',
            'DealerLocation.address2',
            'DealerLocation.suburb', 'DealerLocation.state', 'DealerLocation.city', 'DealerLocation.logo'
        );
        $data = $this->params->query;
        $conditions = array();
        if (!empty($data)) {
            $options = $this->Car->search_params($data);
            $conditions = $options['conditions'];
        }
        $conditions['DealerLocation.id'] = $dealer_id;

//        $group = array('Car.year', 'Car.make', 'Car.model', 'Car.series');
        $this->Car->hasMany['CarImage']['limit'] = 1;
        $new_data = array_merge($data, array('dealer_location' => $dealer_id));
        $dealer = $this->Car->find('first', array('group' => 'DealerLocation.id', 'conditions' => $conditions, 'fields' => $fields, 'contain' => array('DealerLocation', 'Dealer')));
        $cars = $this->Car->find('all', array('conditions' => $conditions, 'limit' => 2, 'contain' => array('DealerLocation', 'Dealer', 'CarImage')));
//        debug($this->Car->getDataSource()->getLog());
        die($view->element('map-dealership-snippet', compact('dealer', 'cars', 'new_data')));
    }

    public function view($id = false) {
        $this->__form_common();
        if (!$id) {
            throw new NotFoundException(__('Invalid car'));
        }
        $options = $this->Car->search_params();
        $car = $this->Car->find('first', array('conditions' => array('Car.id' => $id), 'fields' => $options['fields']
            , 'contain' => array('DealerLocation', 'Dealer', 'CarImage')));
        $dealer_location = $this->Car->DealerLocation->find('first', array('conditions' => array('DealerLocation.id' => $car['Car']['dealer_location_id'])));
//        debug($dealer_location);
        $this->set(compact('car', 'dealer_location'));
//        $this->set('enquiry_states', Car::$states);
        $data = $this->params->query;
        $this->set('params', $data);
        $this->set($this->Car->getAllFields2());
    }

    function add_to_compare($car_id = false) {

//        $this->Session->delete('compare_cars');
        $cars_id = (array) $this->Session->read('compare_cars');
        if (!in_array($car_id, $cars_id)) {
            array_push($cars_id, $car_id);
            $status = 'success';
            $message = __('This car has been added to your comparison list', true);
        } else {
            $status = 'information';
            $message = __('The car is already in your comparison list', true);
        }
        $this->Session->write('compare_cars', $cars_id);
        if ($this->request->is('ajax')) {
            echo json_encode(array('message' => $message, 'status' => $status));
            exit;
        } else {
            if ($status == 'error') {
                $status = 'Errormessage';
            } elseif ($status == 'information') {
                $status = 'Notemessage';
            }
            $this->flashMessage($message, $status);
            $this->redirect(array('action' => 'cars_compare'));
        }
    }

    public function cars_compare() {
//        $this->Session->delete('compare_cars');
        $cars_id = $this->Session->read('compare_cars');
        if (empty($cars_id)) {
            $this->flashMessage('There are not any cars to compare');
        }
        $cars = $this->Car->find('all', array('conditions' => array('Car.id' => $cars_id)));

        $this->set(compact('cars', 'cars_id'));
    }

    function dealer_search() {
        $data = $this->params->query;
//        $this->loadModel('Car');
        $conditions = array();
        $fields = array(
            'count(`Car`.`id`) as CarsCount',
            'DealerLocation.dealer_id',
            'DealerLocation.logo',
            'DealerLocation.longitude',
            'DealerLocation.name',
            'DealerLocation.email',
            'DealerLocation.phone1',
            'DealerLocation.phone2',
            'DealerLocation.address1',
            'DealerLocation.city',
            'DealerLocation.state',
            'DealerLocation.suburb',
            'DealerLocation.suburb',
            'DealerLocation.id',
            'DealerLocation.latitude', 'DealerLocation.zoom',
        );
        if (!empty($data['location'])) {
            $location = urlencode($data['location']);
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$location}&sensor=false";

            $output = $this->Car->Request_GoogalMap_URL($url);
//            debug($output);exit;
            $status = $output->status;

            if ($status == "OK") {
                $formated_address = $output->results[0]->formatted_address;
                $lat = $output->results[0]->geometry->location->lat;
                $lng = $output->results[0]->geometry->location->lng;
            }
            $fields[] = "DISTANCE( DealerLocation.latitude ,DealerLocation.longitude  , $lat, $lng ) AS dist";
        }

        if (!empty($data)) {
            $options = $this->Car->search_params($data);
            $conditions = $options['conditions'];
        }


        $dealerss = $this->Car->find('all', array('group' => 'DealerLocation.id', 'conditions' => $conditions, 'fields' => $fields, 'contain' => array('DealerLocation', 'Dealer')));

        $this->Paginator->settings = array('limit' => 3, 'group' => 'DealerLocation.id', 'conditions' => $conditions, 'fields' => $fields, 'contain' => array('DealerLocation', 'Dealer'));
        $dealerss = $this->Paginator->paginate('Car');

//        $dealerss = $this->Pagfind('all', array('group' => 'DealerLocation.id', 'conditions' => $conditions, 'fields' => $fields, 'contain' => array('DealerLocation', 'Dealer')));
//        debug($dealerss);
        $locationss = array();
        if (!empty($dealerss)) {
            foreach ($dealerss as $dealer) {
                $locationss[$dealer['DealerLocation']['id']]['dealer_id'] = $dealer['DealerLocation']['id'];
                $locationss[$dealer['DealerLocation']['id']]['lat'] = $dealer['DealerLocation']['latitude'];
                $locationss[$dealer['DealerLocation']['id']]['long'] = $dealer['DealerLocation']['longitude'];
                $locationss[$dealer['DealerLocation']['id']]['count'] = $dealer[0]['CarsCount'];
            }
        }
        if ($this->request->is('ajax')) {
            $view = new View();
            $element = $view->element('cars/dealer-search-snippet', array('dealers' => $dealerss));
            echo json_encode(array('locations' => $locationss, 'dealers' => $element));
            exit;
        }


        $this->set('params', $data);
        $this->set(compact('locationss', 'dealerss'));
    }

}
