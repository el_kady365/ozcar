<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Contacts Controller
 *
 * @property Contact $Contact
 */
class ContactsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Components->disable('Auth');
        $this->Auth->allow('ajax_enquire','contactus');        
    }

    function ajax_enquire() {
        Configure::write('debug', 2);
        $status = false;
        $validationErrors = array();
        if ($this->request->is('post')) {
            $this->Contact->create();
            $this->request->data('Contact.ip_address', $_SERVER['REMOTE_ADDR']);
            if (!empty($this->request->data['Contact']['to'])) {
                $to = $this->request->data['Contact']['to'];
            } else {
                $to = $this->config['basic.admin_email'];
            }
            $this->request->data['Contact']['to'] = $to;
            if ($this->Contact->save($this->request->data)) {
                $email = new CakeEmail();
                $email->template('contact', 'contact')
                        ->emailFormat('html')
                        ->subject("{$this->config['basic.site_name']}: {$this->request->data('Contact.subject')}")
                        ->to($to)
                        ->from(array($this->request->data('Contact.email') => $this->request->data('Contact.name')))
                        ->viewVars(array('config' => $this->config, 'contact_data' => $this->request->data['Contact']));

                if ($email->send()) {
                    $message = __('Your comments has been sent.', true);
                    $status = true;
                } else {
                    $message = __('can\'t send email', true);
                }
            } else {
                $validationErrors = $this->Contact->validationErrors;
                $message = __('Could not send your message. Please, try again.');
            }
        }
        $securityCode = generateCode();
        $_SESSION['security_code'] = $securityCode;
        $json_data = array('message' => $message, 'status' => $status, 'token_security' => $securityCode, 'validationErrors' => $validationErrors);
        echo json_encode($json_data);
        exit();
    }

    /**
     * contact_us method
     *
     * @return void
     */
    public function contactus($home = false) {
        if ($this->request->is('post')) {
            $this->Contact->create();
            $this->request->data['Contact']['name'] = $this->request->data['Contact']['fname'] . ' ' . $this->request->data['Contact']['lname'];
            $this->request->data('Contact.ip_address', $_SERVER['REMOTE_ADDR']);
            $subject = 'New Contact us';
            if ($home) {
                $subject = 'New SEO Inquery';
            }
            if ($this->Contact->save($this->request->data)) {
                $email = new CakeEmail();
                $email->template('contact', 'contact')
                        ->emailFormat('html')
                        ->subject("{$this->config['basic.site_name']}: " . $subject)
                        ->to($this->config['basic.admin_email'])
                        ->from(array($this->request->data('Contact.email') => $this->request->data('Contact.name')))
                        ->viewVars(array('config' => $this->config, 'contact_data' => $this->request->data['Contact']));


                if ($email->send()) {
                    $this->flashMessage(__('Your message has been sent.', true), 'flashMessage Sucmessage');
                    if ($home) {
                        $this->redirect(array('controller' => '/'));
                    }
                    $this->redirect(array('action' => 'contactus'));
                } else {
                    $this->flashMessage(__('can\'t send email', true), 'flashMessage Errormessage');
                    if ($home) {
                        $this->redirect(array('controller' => '/'));
                    }
                }
            } else {
                $this->flashMessage(__('Could not send your message. Please, try again.'));
                if ($home) {
                    $this->redirect(array('controller' => '/'));
                }
            }
        }
        $this->loadModel('Slider');
        $sliders = $this->Slider->find('all', array('conditions' => array('Slider.active' => 1), 'limit' => 4, 'order' => 'RAND()'));
        $this->set(compact('sliders'));

        $this->set('contactus_address', $this->get_snippet('contactus-address'));
        $this->set('contact_header', $this->get_snippet('contact-header'));
        $this->set('contact_footer', $this->get_snippet('contact-footer'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = $this->_filter_params();


        $this->Contact->recursive = 0;
        $this->paginate = array('order' => array('created' => 'desc'));
        $this->set('contacts', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
            throw new NotFoundException(__('Invalid contact'));
        }
        $this->set('contact', $this->Contact->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->Contact->create();
            if ($this->Contact->save($this->request->data)) {
                $this->flashMessage(__('The contact has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The contact could not be saved. Please, try again.'));
            }
        }
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
            throw new NotFoundException(__('Invalid contact'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->Contact->save($this->request->data)) {
                $this->flashMessage(__('The contact has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The contact could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Contact->read(null, $id);
        }
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $contacts = $this->Contact->find('all', array('conditions' => array('Contact.id' => $id)));
            $this->set(compact('contacts'));
            return;
        }

        if (!empty($id)) {
            $this->Contact->id = $id;
            if (!$this->Contact->exists()) {
                throw new NotFoundException(__('Invalid contact'));
            }
            if ($this->Contact->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Contact deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Contact was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Contact->deleteAll(array('Contact.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Contacts deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected contacts'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
