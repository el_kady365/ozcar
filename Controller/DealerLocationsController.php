<?php

App::uses('AppController', 'Controller');

/**
 * DealerLocations Controller
 *
 * @property DealerLocation $DealerLocation
 */
class DealerLocationsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {

        $conditions = array();
        $this->__form_common();
        if ($this->__authenticate_dealer()) {
            unset($this->DealerLocation->filters["dealer_id"]);
            $dealer = $this->__get_dealer_info();
            $conditions["DealerLocation.dealer_id"] = $dealer["Dealer"]["id"];
        }
        $conditions[] = $this->_filter_params();
        $this->DealerLocation->recursive = 0;
        $this->set('dealers', $this->DealerLocation->Dealer->find("list"));
        $this->set('dealerLocations', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->DealerLocation->id = $id;
        if (!$this->DealerLocation->exists()) {
            throw new NotFoundException(__('Invalid dealer location'));
        }
        $this->set('dealerLocation', $this->DealerLocation->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add($dealer_id = false) {
        if (empty($dealer_id)) {
            $this->flashMessage(__('Invalid Dealer'));
            $this->redirect(array("controller" => "dealers", 'action' => 'index'));
        }
        $this->request->data["DealerLocation"]["dealer_id"] = $dealer_id;
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->DealerLocation->create();
            if ($this->DealerLocation->save($this->request->data)) {
                $count_locations = $this->DealerLocation->find('count', array("conditions" => array("dealer_id" => $dealer_id)));
                if ($count_locations == 1) {
                    $this->DealerLocation->Dealer->id = $dealer_id;
                    $this->DealerLocation->Dealer->saveField("main_location_id", $this->DealerLocation->id);
                }
                $this->flashMessage(__('The dealer location has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index', "?" => array("dealer_id" => $this->request->data["DealerLocation"]["dealer_id"])));
            } else {
                $this->flashMessage(__('The dealer location could not be saved. Please, try again.'));
            }
        }
        $dealers = $this->DealerLocation->Dealer->find('list');
        $this->set(compact('dealers'));
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->DealerLocation->id = $id;
        if (!$this->DealerLocation->exists()) {
            throw new NotFoundException(__('Invalid dealer location'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->DealerLocation->save($this->request->data)) {
                $this->flashMessage(__('The dealer location has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index', "?" => array("dealer_id" => $this->request->data["DealerLocation"]["dealer_id"])));
            } else {
                $this->flashMessage(__('The dealer location could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->DealerLocation->read(null, $id);
        }
        $dealers = $this->DealerLocation->Dealer->find('list');
        $this->set(compact('dealers'));
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
        $this->loadModel("Contact");
        $file_settings = $this->DealerLocation->getFileSettings();
        $this->set(compact('file_settings'));
        $this->set("states", Contact::$states);
        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $dealerLocations = $this->DealerLocation->find('all', array('conditions' => array('DealerLocation.id' => $id)));
            $this->set(compact('dealerLocations'));
            return;
        }

        if (!empty($id)) {

            $this->DealerLocation->id = $id;
            if (!$this->DealerLocation->exists()) {
                throw new NotFoundException(__('Invalid dealer location'));
            }
            if ($this->DealerLocation->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Dealer location deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Dealer location was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->DealerLocation->deleteAll(array('DealerLocation.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Dealerlocations deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected dealerlocations'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
