<?php

App::uses('AppController', 'Controller');

/**
 * Dealers Controller
 *
 * @property Dealer $Dealer
 */
class DealersController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    public function admin_getLocations($dealer_id = false) {
        if (!empty($dealer_id)) {
            $locations = $this->Dealer->DealerLocation->find("list", array("conditions" => array("dealer_id" => $dealer_id)));
        } else {
            $locations = $this->Dealer->DealerLocation->find("list");
        }
        die(json_encode($locations));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = $this->_filter_params();
        $this->Dealer->recursive = 0;
        $this->set('dealers', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Dealer->id = $id;
        if (!$this->Dealer->exists()) {
            throw new NotFoundException(__('Invalid dealer'));
        }
        $this->set('dealer', $this->Dealer->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();

            $this->Dealer->create();
            if ($this->Dealer->save($this->request->data)) {
                $this->Dealer->save_related_locations($this->request->data);
                $this->flashMessage(__('The dealer has been saved. Please add Location'), 'Sucmessage');
                $this->redirect(array("controller" => "dealers", 'action' => 'index'));
            } else {
                $this->flashMessage(__('The dealer could not be saved. Please, try again.'));
            }
        }
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Dealer->id = $id;
        if (!$this->Dealer->exists()) {
            throw new NotFoundException(__('Invalid dealer'));
        }
        $dealer = false;
        if ($this->__authenticate_dealer()) {
            $dealer = $this->__get_dealer_info();
            if ($this->Dealer->id != $dealer["Dealer"]["id"]) {
                $this->flashMessage(__('The dealer could not be saved.'));
                $this->redirect(array('action' => 'edit', $dealer["Dealer"]["id"]));
            }
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->Dealer->save($this->request->data)) {
                $this->Dealer->save_related_locations($this->request->data);
                $this->flashMessage(__('The dealer has been saved'), 'Sucmessage');
                if (empty($dealer)) {
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->flashMessage(__('The dealer could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Dealer->read(null, $id);
        }
        $mainLocations = $this->Dealer->DealerLocation->find('list', array("conditions" => array("dealer_id" => $id)));
        $this->set(compact('mainLocations'));
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
        $file_settings = $this->Dealer->getFileSettings();
        $dealerlocation_file_settings = $this->Dealer->DealerLocation->getFileSettings();
        $this->set(compact('file_settings', 'dealerlocation_file_settings'));
        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $dealers = $this->Dealer->find('all', array('conditions' => array('Dealer.id' => $id)));
            $this->set(compact('dealers'));
            return;
        }

        if (!empty($id)) {

            $this->Dealer->id = $id;
            if (!$this->Dealer->exists()) {
                throw new NotFoundException(__('Invalid dealer'));
            }
            if ($this->Dealer->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Dealer deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Dealer was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Dealer->deleteAll(array('Dealer.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Dealers deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected dealers'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

    function login() {
        $this->loadModel('Snippet');
    }

}
