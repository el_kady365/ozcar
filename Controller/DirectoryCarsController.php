<?php
App::uses('AppController', 'Controller');
/**
 * DirectoryCars Controller
 *
 * @property DirectoryCar $DirectoryCar
 */
class DirectoryCarsController extends AppController {
    var $titleAlias='Cars Directory';

/**
    * admin_index method
    *
    * @return void
    */
    public function admin_index() {
            $conditions = $this->_filter_params() ;
            $this->DirectoryCar->recursive = 0;
                $this->set('directoryCars', $this->paginate($conditions));
    }

    /**
    * admin_view method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_view($id = null) {
            $this->DirectoryCar->id = $id;
            if (!$this->DirectoryCar->exists()) {
                    throw new NotFoundException(__('Invalid directory car'));
            }
            $this->set('directoryCar', $this->DirectoryCar->read(null, $id));
    }
    
    
    /**
    * admin_add method
    *
    * @return void
    */
    public function admin_add() {
            if ($this->request->is('post')) {
                    $this->__submit_common();
                    $this->DirectoryCar->create();
                    if ($this->DirectoryCar->save($this->request->data)) {
                            $this->flashMessage(__('The directory car has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                            $this->flashMessage(__('The directory car could not be saved. Please, try again.'));
                    }
            }
		$this->__form_common();
    }
    


    /**
    * admin_edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_edit($id = null) {
            $this->DirectoryCar->id = $id;
            if (!$this->DirectoryCar->exists()) {
                    throw new NotFoundException(__('Invalid directory car'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                    $this->__submit_common();
                    if ($this->DirectoryCar->save($this->request->data)) {
                            $this->flashMessage(__('The directory car has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                    $this->flashMessage(__('The directory car could not be saved. Please, try again.'));
			}
            } else {
                    $this->request->data = $this->DirectoryCar->read(null, $id);
            }
		$this->__form_common();
		$this->render('admin_add') ;
    }
    
        /**
    * common method for add and edit
    *
    * @return boolean 
    */

    
    function __form_common(){ 

        return true;
    }
    
    

        /**
    * common method for add and edit submissions
    *
    * @return boolean 
    */

    
    function __submit_common(){ 

        return true;
    }
    
        
    
    /**
    * admin_delete method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void 
    */
    public function admin_delete($id = null) {
            if( $this->request->data('submit_btn') === 'no'){
              $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
              $this->redirect( array('action' => 'index')); 

            } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) &&  $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax')  )) {
                $id = !empty($id) ? array($id) : $this->request->data('ids') ;
                $directoryCars = $this->DirectoryCar->find('all', array('conditions' => array('DirectoryCar.id' => $id))) ;
                $this->set(compact('directoryCars'));
                return; 
            }

            if (!empty($id)) {

                $this->DirectoryCar->id = $id;
                if (!$this->DirectoryCar->exists()) {
                    throw new NotFoundException(__('Invalid directory car'));
                }
                if ($this->DirectoryCar->delete()) {
                    if ($this->request->is('ajax')) {
                            die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Directory car deleted'), 'Sucmessage' );
                        $this->redirect(array('action' => 'index'));
                    }
               }
               if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
               } else {
                    $this->flashMessage(__('Directory car was not deleted'));
                    $this->redirect(array('action' => 'index'));
               } 
            } elseif(is_array($this->request->data('ids'))){
                if($this->DirectoryCar->deleteAll(array( 'DirectoryCar.id' => $this->request->data('ids')), true , true)){
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Directorycars deleted successfully'), 'Sucmessage');
                        $this->redirect(array('action' => 'index'));
                    }      
                } else {
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'error')));
                    } else {
                        $this->flashMessage(__('Error deleting selected directorycars'));
                        $this->redirect(array('action' => 'index'));
                    }
                }            
            }
    }
}
