<?php

App::uses('AppController', 'Controller');

/**
 * Enquiries Controller
 *
 * @property Enquiry $Enquiry
 */
class EnquiriesController extends AppController {
    /**
     * index method
     *
     * @return void
     */

    /**
     * admin_index method
     *
     * @return void
     */
     function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }
    
    public function admin_index() {


        $conditions = array();
        $location_condition = array();
        if ($this->__authenticate_dealer()) {
            unset($this->Enquiry->filters["dealer_id"]);
            $dealer = $this->__get_dealer_info();
            $conditions["Enquiry.dealer_id"] = $dealer["Dealer"]["id"];
            $location_condition["DealerLocation.dealer_id"] = $dealer["Dealer"]["id"];
        }
        $dealerLocations = $this->Enquiry->DealerLocation->find('list', array("conditions" => $location_condition));
        $conditions[] = $this->_filter_params();

        $this->loadModel("Contact");
        $states = Contact::$states;
        $dealers = $this->Enquiry->Dealer->find('list');
        $cars = $this->Enquiry->Car->find('list');
        $types = Enquiry::$types;
        $this->set(compact('dealers', 'dealerLocations', 'cars', 'types', 'states'));

        $this->Enquiry->recursive = 0;
        $this->set('enquiries', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Enquiry->id = $id;
        $this->loadModel('CarImage');

        if (!$this->Enquiry->exists()) {
            throw new NotFoundException(__('Invalid enquiry'));
        }
        $enquiry = $this->Enquiry->read(null, $id);
        $types = Enquiry::$types;
        $this->set('types', $types);
        $carImage = $this->CarImage->find('first', array('conditions' => array('CarImage.car_id' => $enquiry['Enquiry']['car_id'])));
        $this->set(compact('carImage', 'enquiry'));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->Enquiry->create();
            if ($this->Enquiry->save($this->request->data)) {
                $this->flashMessage(__('The enquiry has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The enquiry could not be saved. Please, try again.'));
            }
        }
        $dealers = $this->Enquiry->Dealer->find('list');
        $dealerLocations = $this->Enquiry->DealerLocation->find('list');
        $cars = $this->Enquiry->Car->find('list');
        $types = Enquiry::$types;
        $this->set(compact('dealers', 'dealerLocations', 'cars', 'types'));
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Enquiry->id = $id;
        if (!$this->Enquiry->exists()) {
            throw new NotFoundException(__('Invalid enquiry'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->Enquiry->save($this->request->data)) {
                $this->flashMessage(__('The enquiry has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The enquiry could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Enquiry->read(null, $id);
        }
        $dealers = $this->Enquiry->Dealer->find('list');
        $dealerLocations = $this->Enquiry->DealerLocation->find('list');
        $cars = $this->Enquiry->Car->find('list');
        $this->loadModel("Contact");
        $states = Contact::$states;
        $types = Enquiry::$types;
        $this->set(compact('dealers', 'dealerLocations', 'cars', 'types', 'states'));
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $enquiries = $this->Enquiry->find('all', array('conditions' => array('Enquiry.id' => $id)));
            $this->set(compact('enquiries'));
            return;
        }

        if (!empty($id)) {

            $this->Enquiry->id = $id;
            if (!$this->Enquiry->exists()) {
                throw new NotFoundException(__('Invalid enquiry'));
            }
            if ($this->Enquiry->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Enquiry deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Enquiry was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Enquiry->deleteAll(array('Enquiry.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Enquiries deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected enquiries'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

    function add() {
        if (!empty($this->request->data)) {
            $this->Enquiry->create();

            if ($this->Enquiry->save($this->request->data)) {
                $message = __('Your enquiry has been sent');
                $status = true;
                $validationErrors = '';
            } else {
                $status = false;
                $message = __('Can\'t send your enquiry');
                $validationErrors = $this->Enquiry->validationErrors;
            }
        }
        $json_data = array('message' => $message, 'status' => $status, 'validationErrors' => $validationErrors);
        echo json_encode($json_data);
        exit();
    }

}
