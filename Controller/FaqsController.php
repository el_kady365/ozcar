<?php
App::uses('AppController', 'Controller');
/**
 * Faqs Controller
 *
 * @property Faq $Faq
 */
class FaqsController extends AppController {

/**
    * admin_index method
    *
    * @return void
    */
    public function admin_index() {
            $conditions = $this->_filter_params() ;
            $this->Faq->recursive = 0;
                $this->set('faqs', $this->paginate($conditions));
    }

    /**
    * admin_view method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_view($id = null) {
            $this->Faq->id = $id;
            if (!$this->Faq->exists()) {
                    throw new NotFoundException(__('Invalid faq'));
            }
            $this->set('faq', $this->Faq->read(null, $id));
    }
    
    
    /**
    * admin_add method
    *
    * @return void
    */
    public function admin_add() {
            if ($this->request->is('post')) {
                    $this->__submit_common();
                    $this->Faq->create();
                    if ($this->Faq->save($this->request->data)) {
                            $this->flashMessage(__('The faq has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                            $this->flashMessage(__('The faq could not be saved. Please, try again.'));
                    }
            }
		$this->__form_common();
    }
    


    /**
    * admin_edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_edit($id = null) {
            $this->Faq->id = $id;
            if (!$this->Faq->exists()) {
                    throw new NotFoundException(__('Invalid faq'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                    $this->__submit_common();
                    if ($this->Faq->save($this->request->data)) {
                            $this->flashMessage(__('The faq has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                    $this->flashMessage(__('The faq could not be saved. Please, try again.'));
			}
            } else {
                    $this->request->data = $this->Faq->read(null, $id);
            }
		$this->__form_common();
		$this->render('admin_add') ;
    }
    
        /**
    * common method for add and edit
    *
    * @return boolean 
    */

    
    function __form_common(){ 

		$this->set('file_settings',$this->Faq->getFileSettings());
        return true;
    }
    
    

        /**
    * common method for add and edit submissions
    *
    * @return boolean 
    */

    
    function __submit_common(){ 

        return true;
    }
    
        
    
    /**
    * admin_delete method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void 
    */
    public function admin_delete($id = null) {
            if( $this->request->data('submit_btn') === 'no'){
              $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
              $this->redirect( array('action' => 'index')); 

            } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) &&  $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax')  )) {
                $id = !empty($id) ? array($id) : $this->request->data('ids') ;
                $faqs = $this->Faq->find('all', array('conditions' => array('Faq.id' => $id))) ;
                $this->set(compact('faqs'));
                return; 
            }

            if (!empty($id)) {

                $this->Faq->id = $id;
                if (!$this->Faq->exists()) {
                    throw new NotFoundException(__('Invalid faq'));
                }
                if ($this->Faq->delete()) {
                    if ($this->request->is('ajax')) {
                            die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Faq deleted'), 'Sucmessage' );
                        $this->redirect(array('action' => 'index'));
                    }
               }
               if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
               } else {
                    $this->flashMessage(__('Faq was not deleted'));
                    $this->redirect(array('action' => 'index'));
               } 
            } elseif(is_array($this->request->data('ids'))){
                if($this->Faq->deleteAll(array( 'Faq.id' => $this->request->data('ids')), true , true)){
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Faqs deleted successfully'), 'Sucmessage');
                        $this->redirect(array('action' => 'index'));
                    }      
                } else {
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'error')));
                    } else {
                        $this->flashMessage(__('Error deleting selected faqs'));
                        $this->redirect(array('action' => 'index'));
                    }
                }            
            }
    }
}
