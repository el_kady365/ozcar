<?php

class FreeHtmlsController extends AppController {

    var $name = 'FreeHtmls';
    var $helpers = array('Html', 'Form', 'Fck');

    function admin_index() {
        $conditions = $this->_filter_params();
        $this->FreeHtml->recursive = 0;
        $this->paginate["limit"] = 150;
        $this->paginate["order"] = "id desc";
        $this->set('freeHtmls', $this->paginate('FreeHtml',$conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid FreeHtml.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('freeHtml', $this->FreeHtml->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->FreeHtml->create();
            if ($this->FreeHtml->save($this->data)) {
                $this->flashMessage(__('The FreeHtml has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The FreeHtml could not be saved. Please, try again.', true));
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid FreeHtml', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->FreeHtml->save($this->data)) {
                $this->flashMessage(__('The FreeHtml has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The FreeHtml could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->FreeHtml->read(null, $id);
        }
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid id for FreeHtml', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FreeHtml->del($id)) {
            $this->flashMessage(__('FreeHtml deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        }
    }

    function admin_delete_multi() {
        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for Free html', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FreeHtml->deleteAll(array('FreeHtml.id' => $_POST['ids']))) {
            $this->flashMessage(__('Free html items deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action' => 'index'));
        }
    }

}

?>