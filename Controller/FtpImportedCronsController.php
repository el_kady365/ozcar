<?php

App::uses('AppController', 'Controller');

/**
 * FtpImportedCrons Controller
 *
 * @property FtpImportedCron $FtpImportedCron
 */
class FtpImportedCronsController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = $this->_filter_params();
        $this->FtpImportedCron->recursive = 0;
        $this->set('ftpImportedCrons', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->FtpImportedCron->id = $id;
        if (!$this->FtpImportedCron->exists()) {
            throw new NotFoundException(__('Invalid ftp imported cron'));
        }
        $this->set('ftpImportedCron', $this->FtpImportedCron->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {

        if ($this->request->is('post')) {
            if (!empty($this->request->data["FtpImportedCron"]["template_id"])) {
                $selected_template = $this->FtpImportedCron->Template->findById($this->request->data["FtpImportedCron"]["template_id"]);
                $this->request->data["FtpImportedCron"]["dealer_id"] = $selected_template["Template"]["dealer_id"];
            }
            $this->FtpImportedCron->create();
            $this->__submit_common();
            if ($this->FtpImportedCron->save($this->request->data)) {
                $this->flashMessage(__('The ftp imported cron has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The ftp imported cron could not be saved. Please, try again.'));
            }
        }
//        $dealers = $this->FtpImportedCron->Dealer->find('list');
//        $this->set(compact('dealers'));
        $this->__form_common();
    }

    public function create_ftp_accounts() {
        $ftp_folders_path = FTP_IMPORT_PATH;
        $group = "apache";
        $accounts = $this->FtpImportedCron->find("all", array(
            "recursive" => -1
            , "conditions" => array(
                "FtpImportedCron.is_ftp_created" =>0,
				//'FtpImportedCron.id'=>11,
        )));
        if (!empty($accounts)) {
            $data = array();
            foreach ($accounts as $key => $account) {
                //echo "adduser --quiet --disabled-password -shell /bin/bash --home $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']} --gecos \"User\" {$account['FtpImportedCron']['ftp_username']} apache";
                //echo "echo \"{$account['FtpImportedCron']['ftp_username']}:{$account['FtpImportedCron']['ftp_password']}\" | chpasswd";
                //echo("mkdir $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']}");
               // echo("chmod -R 775 $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']}");
              // echo("chown -R {$account['FtpImportedCron']['ftp_username']}.apache  $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']}");

                exec("adduser --home $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']}  {$account['FtpImportedCron']['ftp_username']}");
                exec("adduser {$account['FtpImportedCron']['ftp_username']} $group");
                exec("gpasswd -d $group {$account['FtpImportedCron']['ftp_username']}");
                exec("echo \"{$account['FtpImportedCron']['ftp_username']}:{$account['FtpImportedCron']['ftp_password']}\" | chpasswd");
                exec("chmod -R 0775 $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']}");
                exec("chown -R {$account['FtpImportedCron']['ftp_username']}.$group  $ftp_folders_path/{$account['FtpImportedCron']['ftp_username']}");
      
                $data[$key]["id"] = $account["FtpImportedCron"]["id"];
                $data[$key]["is_ftp_created"] = 1;
            }
            $this->FtpImportedCron->saveMany($data, array("validate" => false));
  //          
//            
        }

        die();
    }
    
    
    public function change_ftp_accounts_passwords() {
        $accounts = $this->FtpImportedCron->find("all", array(
            "recursive" => -1
            , "conditions" => array(
                "FtpImportedCron.is_password_changed" => 1,
        )));
        if (!empty($accounts)) {
            $data = array();
            foreach ($accounts as $key => $account) {
                exec("echo \"{$account['FtpImportedCron']['ftp_username']}:{$account['FtpImportedCron']['ftp_password']}\" | chpasswd");
                $data[$key]["id"] = $account["FtpImportedCron"]["id"];
                $data[$key]["is_password_changed"] = 0;
            }
            $this->FtpImportedCron->saveMany($data, array("validate" => false));
  //          
//            debug($data);
//            debug($accounts);
        }

        die();
    }
    
    
    
    
    

    

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->FtpImportedCron->id = $id;
        if (!$this->FtpImportedCron->exists()) {
            throw new NotFoundException(__('Invalid ftp imported cron'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();

            if ($this->request->data["FtpImportedCron"]["ftp_password"] != $this->FtpImportedCron->field("ftp_password")) {
                $this->request->data["FtpImportedCron"]["is_password_changed"] = 1;
                        
            }
            if ($this->FtpImportedCron->save($this->request->data)) {
                $this->flashMessage(__('The ftp imported cron has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The ftp imported cron could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->FtpImportedCron->read(null, $id);
        }
//        $dealers = $this->FtpImportedCron->Dealer->find('list');
//        $templates = $this->FtpImportedCron->Template->find('list');
//        $this->set(compact('dealers', 'templates'));
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
        $templates = $this->FtpImportedCron->Template->find("list", array("fields" => array("Template.id", "Template.name", "Dealer.name"), "recursive" => 0));
        $this->set(compact("templates"));
        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $ftpImportedCrons = $this->FtpImportedCron->find('all', array('conditions' => array('FtpImportedCron.id' => $id)));
            $this->set(compact('ftpImportedCrons'));
            return;
        }

        if (!empty($id)) {

            $this->FtpImportedCron->id = $id;
            if (!$this->FtpImportedCron->exists()) {
                throw new NotFoundException(__('Invalid ftp imported cron'));
            }
            if ($this->FtpImportedCron->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Ftp imported cron deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Ftp imported cron was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->FtpImportedCron->deleteAll(array('FtpImportedCron.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Ftpimportedcrons deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected ftpimportedcrons'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
