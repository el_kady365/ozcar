<?php

class FullWidthBannersController extends AppController {

    var $name = 'FullWidthBanners';
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $conditions = $this->_filter_params();
        $this->FullWidthBanner->recursive = 0;
        $this->set('fullWidthBanners', $this->paginate('FullWidthBanner', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid FullWidthBanner.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('fullWidthBanner', $this->FullWidthBanner->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->FullWidthBanner->create();
            if ($this->FullWidthBanner->save($this->data)) {
                $this->flashMessage(__('The FullWidthBanner has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The FullWidthBanner could not be saved. Please, try again.', true));
            }
        }
        $this->set('image_settings', $this->FullWidthBanner->getImageSettings());
        $this->loadModel('Dealer');
        $this->set('dealerships', $this->Dealer->find('list'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid FullWidthBanner', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->FullWidthBanner->save($this->data)) {
                $this->flashMessage(__('The FullWidthBanner has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The FullWidthBanner could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->FullWidthBanner->read(null, $id);
        }
        $this->loadModel('Dealer');
        $this->set('dealerships', $this->Dealer->find('list'));
        $this->set('image_settings', $this->FullWidthBanner->getImageSettings());
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid id for FullWidthBanner', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FullWidthBanner->del($id)) {
            $this->flashMessage(__('FullWidthBanner deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        }
    }

    function admin_delete_multi() {
        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for Page Banners', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FullWidthBanner->deleteAll(array('FullWidthBanner.id' => $_POST['ids']))) {
            $this->flashMessage(__('Page Banner items deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action' => 'index'));
        }
    }

}

?>
