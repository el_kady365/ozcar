<?php

App::uses('AppController', 'Controller');

/**
 * Links Controller
 *
 * @property Link $Link
 */
class LinksController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = $this->_filter_params();
        $this->Link->recursive = 0;
       
        $this->set('links', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Link->id = $id;
        if (!$this->Link->exists()) {
            throw new NotFoundException(__('Invalid link'));
        }
        $this->set('link', $this->Link->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->Link->create();
            if ($this->Link->save($this->request->data)) {
                $this->flashMessage(__('The link has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The link could not be saved. Please, try again.'));
            }
        }
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Link->id = $id;
        if (!$this->Link->exists()) {
            throw new NotFoundException(__('Invalid link'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->Link->save($this->request->data)) {
                $this->flashMessage(__('The link has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The link could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Link->read(null, $id);
        }
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $links = $this->Link->find('all', array('conditions' => array('Link.id' => $id)));
            $this->set(compact('links'));
            return;
        }

        if (!empty($id)) {

            $this->Link->id = $id;
            if (!$this->Link->exists()) {
                throw new NotFoundException(__('Invalid link'));
            }
            if ($this->Link->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Link deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Link was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Link->deleteAll(array('Link.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Links deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected links'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
