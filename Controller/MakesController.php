<?php

App::uses('AppController', 'Controller');

/**
 * Makes Controller
 *
 * @property Make $Make
 */
class MakesController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        $this->Make->recursive = 0;
        $this->set('makes', $this->paginate($conditions));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Make->id = $id;
        if (!$this->Make->exists()) {
            throw new NotFoundException(__('Invalid make'));
        }
        $this->set('make', $this->Make->read(null, $id));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function getModels($make_id) {
        $models = $this->Make->CarModel->find("list", array("conditions" => array("make_id" => $make_id)));
        die(json_encode($models));
    }

    public function admin_index() {
        $conditions = $this->_filter_params();
        $this->Make->recursive = 0;
        $this->set('makes', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Make->id = $id;
        if (!$this->Make->exists()) {
            throw new NotFoundException(__('Invalid make'));
        }
        $this->set('make', $this->Make->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->Make->create();
            if ($this->Make->save($this->request->data)) {
                $this->flashMessage(__('The make has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The make could not be saved. Please, try again.'));
            }
        }
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Make->id = $id;
        if (!$this->Make->exists()) {
            throw new NotFoundException(__('Invalid make'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->Make->save($this->request->data)) {
                $this->flashMessage(__('The make has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The make could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Make->read(null, $id);
        }
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $makes = $this->Make->find('all', array('conditions' => array('Make.id' => $id)));
            $this->set(compact('makes'));
            return;
        }

        if (!empty($id)) {

            $this->Make->id = $id;
            if (!$this->Make->exists()) {
                throw new NotFoundException(__('Invalid make'));
            }
            if ($this->Make->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Make deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Make was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Make->deleteAll(array('Make.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Makes deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected makes'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
