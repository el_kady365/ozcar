<?php

App::uses('AppController', 'Controller');

/**
 * MapFields Controller
 *
 * @property MapField $MapField
 */
class MapFieldsController extends AppController {

    /**
     * index method
     *
     * @return void
     */

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index($template_id) {
        $this->params->query["template_id"] = $template_id;
        if (empty($template_id)) {
            $this->flashMessage(__('Invalid Template'));
            $this->redirect(array("controller" => "templates",'action' => 'index'));
        }
        $this->__form_common();
        $conditions = $this->_filter_params();
        $this->MapField->recursive = 0;
        $this->set('mapFields', $this->paginate($conditions));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->MapField->id = $id;
        if (!$this->MapField->exists()) {
            throw new NotFoundException(__('Invalid map field'));
        }
        $this->set('mapField', $this->MapField->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add($template_id) {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->request->data["MapField"]["template_id"] = $template_id;
            $this->MapField->create();
            if ($this->MapField->save($this->request->data)) {
                $this->flashMessage(__('The map field has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index', $template_id));
            } else {
                $this->flashMessage(__('The map field could not be saved. Please, try again.'));
            }
        }
        
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->MapField->id = $id;
        if (!$this->MapField->exists()) {
            throw new NotFoundException(__('Invalid map field'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->MapField->save($this->request->data)) {
                $this->flashMessage(__('The map field has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index' , $this->MapField->field("template_id")));
            } else {
                $this->flashMessage(__('The map field could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->MapField->read(null, $id);
        }

        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
        $templates = $this->MapField->Template->find('list');
        $this->loadModel("Car");
        $fields = Car::$fields;
        asort($fields);
        $this->set(compact('templates' , 'fields'));
        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $mapFields = $this->MapField->find('all', array('conditions' => array('MapField.id' => $id)));
            $this->set(compact('mapFields'));
            return;
        }

        if (!empty($id)) {

            $this->MapField->id = $id;
            if (!$this->MapField->exists()) {
                throw new NotFoundException(__('Invalid map field'));
            }
            if ($this->MapField->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Map field deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Map field was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->MapField->deleteAll(array('MapField.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Mapfields deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected mapfields'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
