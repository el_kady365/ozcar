<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Messages Controller
 *
 */
class MessagesController extends AppController {

    /**
     * Scaffold
     *
     * @var mixed
     */
    function admin_inbox() {
        $conditions = array();
        $this->Message->recursive = 1;
        $location_condition = array();
        if ($this->__authenticate_dealer()) {
//            unset($this->Message->filters["dealer_id"]);
            $dealer = $this->__get_dealer_info();
            $conditions["Message.dealer_id"] = $dealer["Dealer"]["id"];
            $location_condition["DealerLocation.dealer_id"] = $dealer["Dealer"]["id"];
        }
        $conditions[] = $this->_filter_params();
        $this->loadModel('DealerLocation');
        $conditions[] = array('Message.dealer_id' => $dealer["Dealer"]["id"], 'Message.from_type' => 1, '(Message.receiver_deleted is Null Or Message.receiver_deleted=0)');
        $inbox = $this->paginate('Message', $conditions);

        $dealers = $this->DealerLocation->find('list');

        $this->set(compact('inbox', 'dealers'));
    }

    function admin_sent() {
        $conditions = array();
        $this->Message->recursive = 1;
        $location_condition = array();
        if ($this->__authenticate_dealer()) {

            $dealer = $this->__get_dealer_info();
            $conditions["Message.dealer_id"] = $dealer["Dealer"]["id"];
            $location_condition["DealerLocation.dealer_id"] = $dealer["Dealer"]["id"];
        }
        $conditions[] = $this->_filter_params();
        $this->loadModel('DealerLocation');
        $conditions[] = array('Message.dealer_id' => $dealer["Dealer"]["id"], 'Message.from_type' => 2, '(Message.sender_deleted is Null Or Message.sender_deleted=0)');
        $sent = $this->paginate('Message', $conditions);
        $dealers = $this->DealerLocation->find('list');

        $this->set(compact('sent', 'dealers'));
    }

    public function admin_view($id = null) {
        $this->Message->id = $id;


        if (!$this->Message->exists()) {
            throw new NotFoundException(__('Invalid Message'));
        }
        $message = $this->Message->read(null, $id);
        $this->set(compact('message'));
    }

    function admin_reply($id = false) {

        $message = $this->Message->read(null, $id);
        if (!empty($this->request->data)) {
            $this->request->data['Message']['sender_id'] = $message['Message']['receiver_id'];
            $this->request->data['Message']['receiver_id'] = $message['Message']['sender_id'];
            $this->request->data['Message']['dealer_id'] = $message['Message']['dealer_id'];
            $this->request->data['Message']['read'] = 0;
            $this->request->data['Message']['from_type'] = 2;
            $this->request->data['Message']['message_id'] = $id;
            $this->Message->create();
            if ($this->Message->save($this->request->data)) {
                $message = $this->Message->read(null, $this->Message->id);

                $email = new CakeEmail();
                $email->template('sent_message', 'contact')
                        ->emailFormat('html')
                        ->subject("{$this->config['basic.site_name']}: " . __('You received a new message from ', true) . $message['Sender']['name'])
                        ->to($message['Receiver']['email'])
                        ->from(array($this->config['basic.admin_email'] => $this->config['basic.site_name']))
                        ->viewVars(array('config' => $this->config, 'message' => $message));

                $email->send();

                $this->flashMessage(__('Your message has been sent.', true), 'Sucmessage');
                $this->redirect(array('action' => 'inbox'));
            } else {
                $this->setFlash(__('The message could not be sent. Please, try again.', true));
            }
        }
        $this->set('message', $message);
    }

    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'inbox'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $messages = $this->Message->find('all', array('conditions' => array('Message.id' => $id)));
            $this->set(compact('messages'));
            return;
        }

        if (!empty($id)) {

            $this->Message->id = $id;
            if (!$this->Message->exists()) {
                throw new NotFoundException(__('Invalid message'));
            }
            if ($this->_admin_delete($id)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Message deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'inbox'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Message was not deleted'));
                $this->redirect(array('action' => 'inbox'));
            }
        } elseif (is_array($this->request->data('ids'))) {

            foreach ($this->request->data('ids') as $id) {
                $this->_admin_delete($id);
            }

            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'success')));
            } else {
                $this->flashMessage(__('Messages deleted successfully'), 'Sucmessage');
                $this->redirect(array('action' => 'inbox'));
            }
        }
    }

    function inbox() {
        $user = $this->loggedUser;
        $this->loadModel('DealerLocation');
        $receiver_conditions = array('Message.receiver_id' => $user['id'], 'Message.from_type' => 2, '(Message.receiver_deleted is Null Or Message.receiver_deleted=0)');
        $inbox = $this->Message->find('all', array('conditions' => $receiver_conditions));
        $dealers = $this->DealerLocation->find('list');
        $this->pageTitle = __('Inbox', true);
        $this->set(compact('inbox', 'dealers'));
    }

    function sent() {
        $user = $this->loggedUser;
        $this->loadModel('DealerLocation');
        $receiver_conditions = array('Message.sender_id' => $user['id'], 'Message.from_type' => 1, '(Message.sender_deleted is Null Or Message.sender_deleted=0)');
        $sent = $this->Message->find('all', array('conditions' => $receiver_conditions));
        $dealers = $this->DealerLocation->find('list');
        $this->pageTitle = __('Sent', true);
        $this->set(compact('sent', 'dealers'));
    }

    function send() {
        $user = $this->loggedUser;
        $this->loadModel('DealerLocation');
        if (!empty($this->request->data)) {
            $this->request->data['Message']['sender_id'] = $user['id'];
            $this->DealerLocation->id = $this->request->data['Message']['receiver_id'];
            $this->request->data['Message']['dealer_id'] = $this->DealerLocation->field('dealer_id');
            $this->request->data['Message']['read'] = 0;
            $this->request->data['Message']['from_type'] = 1;
            $this->Message->create();
            if ($this->Message->save($this->request->data)) {
                $message = $this->Message->read(null, $this->Message->id);
                if ($message['Receiver']['email']) {
                    $email = new CakeEmail();
                    $email->template('sent_message', 'contact')
                            ->emailFormat('html')
                            ->subject("{$this->config['basic.site_name']}: " . __('You received a new message from ', true) . $message['Sender']['first_name'])
                            ->to($message['Receiver']['email'])
                            ->from(array($this->config['basic.admin_email'] => $this->config['basic.site_name']))
                            ->viewVars(array('config' => $this->config, 'message' => $message));

                    $email->send();
                }
                $this->flashMessage(__('Your message has been sent.', true), 'Sucmessage');
                $this->redirect(array('action' => 'inbox'));
            } else {
                $this->setFlash(__('The message could not be sent. Please, try again.', true));
            }
        }
    }

    function read($id = false) {

        $this->loadModel('Dealer');
        $message = $this->Message->read(null, $id);
        $sent = isset($_GET['sent']) ? $_GET['sent'] : '';
        if ($sent) {
            $dealer_id = $message['Receiver']['dealer_id'];
        } else {
            $dealer_id = $message['Sender']['dealer_id'];
        }
        $dealer = $this->Dealer->find('first', array('recursive' => -1, 'conditions' => array('Dealer.id' => $dealer_id)));
        $view = new View();
        echo $view->element('messages/read_message', compact('message', 'dealer', 'sent'));
        exit;
    }

    function reply($id = false) {
        $user = $this->loggedUser;
        $message = $this->Message->read(null, $id);
        if (!empty($this->request->data)) {
            $this->request->data['Message']['sender_id'] = $user['id'];
            $this->request->data['Message']['receiver_id'] = $message['Message']['sender_id'];
            $this->request->data['Message']['dealer_id'] = $message['Message']['dealer_id'];
            $this->request->data['Message']['read'] = 0;
            $this->request->data['Message']['from_type'] = 1;
            $this->request->data['Message']['message_id'] = $id;
            $this->Message->create();
            if ($this->Message->save($this->request->data)) {
                $message = $this->Message->read(null, $this->Message->id);
                $email = new CakeEmail();
                $email->template('sent_message', 'contact')
                        ->emailFormat('html')
                        ->subject("{$this->config['basic.site_name']}: " . __('You received a new message from ', true) . $message['Sender']['first_name'])
                        ->to($message['Receiver']['email'])
                        ->from(array($this->config['basic.admin_email'] => $this->config['basic.site_name']))
                        ->viewVars(array('config' => $this->config, 'message' => $message));

                $email->send();



                $this->flashMessage(__('Your message has been sent.', true), 'Sucmessage');
                $this->redirect(array('action' => 'inbox'));
            } else {
                $this->setFlash(__('The message could not be sent. Please, try again.', true));
            }
        }
        $this->loadModel('Dealer');

        $dealer = $this->Dealer->find('first', array('recursive' => -1, 'conditions' => array('Dealer.id' => $message['Sender']['dealer_id'])));
        $view = new View();
        echo $view->element('messages/reply_message', compact('message', 'dealer'));
        exit;
    }

    function _admin_delete($id) {
        $message = $this->Message->read(null, $id);
        $dealer = $this->__get_dealer_info();

        if ($message['Message']['dealer_id'] == $dealer["Dealer"]["id"] && $message['Message']['from_type'] == 1) {
            if ($message['Message']['sender_deleted'] == 1) {
                $this->Message->delete($id);
            } else {
                $this->Message->create();
                $this->Message->id = $id;
                $this->Message->saveField('receiver_deleted', 1);
            }
        } elseif ($message['Message']['dealer_id'] == $dealer["Dealer"]["id"] && $message['Message']['from_type'] == 2) {
            if ($message['Message']['receiver_deleted'] == 1) {
                $this->Message->delete($id);
            } else {
                $this->Message->create();
                $this->Message->id = $id;
                $this->Message->saveField('sender_deleted', 1);
            }
        }
        return true;
    }

    function _delete($id) {
        $message = $this->Message->read(null, $id);
        $user = $this->loggedUser;
        if ($message['Message']['receiver_id'] == $user['id']) {
            if ($message['Message']['sender_deleted'] == 1) {
                $this->Message->delete($id);
            } else {
                $this->Message->create();
                $this->Message->id = $id;
                $this->Message->saveField('receiver_deleted', 1);
            }
        } elseif ($message['Message']['sender_id'] == $user['id']) {
            if ($message['Message']['receiver_deleted'] == 1) {
                $this->Message->delete($id);
            } else {
                $this->Message->create();
                $this->Message->id = $id;
                $this->Message->saveField('sender_deleted', 1);
            }
        }

        $this->flashMessage('Message(s) deleted successfully', 'Sucmessage');
    }

    function do_action($type, $id) {
        if ($type == 'delete') {
            $this->_delete($id);
        } elseif ($type == 'mark_read') {
            if ($this->Message->updateAll(array('Message.read' => 1), array('Message.id' => $id))) {
                $this->flashMessage('Messages marked as read successfully', 'Sucmessage');
            } else {
                $this->flashMessage('Messages cannot be marked as read');
            }
        } elseif ($type == 'mark_unread') {
            if ($this->Message->updateAll(array('Message.read' => 0), array('Message.id' => $id))) {
                $this->flashMessage('Messages marked as unread successfully', 'Sucmessage');
            } else {
                $this->flashMessage('Messages cannot marked as unread', 'Sucmessage');
            }
        }
        $this->redirect($this->referer());
    }

    function do_operation() {
        $ids = $this->request->data['chk'];
        $operation = $this->request->query['action'];

        if ($operation == 'delete') {
            foreach ($ids as $id) {
                $this->_delete($id);
            }
        } elseif ($operation == 'mark_read') {
            if ($this->Message->updateAll(array('Message.read' => 1), array('Message.id' => $ids))) {
                $this->flashMessage('Messages marked as read successfully', 'Sucmessage');
            } else {
                $this->flashMessage('Messages cannot be marked as read');
            }
        } elseif ($operation == 'mark_unread') {
            if ($this->Message->updateAll(array('Message.read' => 0), array('Message.id' => $ids))) {
                $this->flashMessage('Messages marked as unread successfully', 'Sucmessage');
            } else {
                $this->flashMessage('Messages cannot marked as unread', 'Sucmessage');
            }
        }
        $this->redirect($this->referer());
    }

}
