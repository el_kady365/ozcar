<?php

/**
 * @property Page Page
 *
 */
class PagesController extends AppController {

    public $name = 'Pages';
    public $helpers = array('Html', 'Form');
    public $paginate = array();

    
    
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }
    function home() {
        $this->loadModel('Car');
        $this->set($this->Car->getAllFields2());

        $dealers = $this->Car->Dealer->find('list');
        $this->set(compact('dealers', 'makes', 'models'));
        $this->pageTitle = "Homepage";
    }

    function index($permalink = null) {
        $menuID = $this->getTopMenuID($permalink);
        $this->currentMenuID = $menuID;
        $page_permalink = $this->MenuItems[$menuID]['MenuPages'][0]['Page']['permalink'];
        if (empty($page_permalink)) {
            $this->redirect(array('action' => 'home'));
        }

        $this->render('view');
    }

//    function search() {
//        $search = trim($_GET['query']);
//        $options = array(
//            'recursive' => -1,
//            'limit' => 50,
//            //'conditions' => array("OR"=>array("`title` like \"%$search%\" ", " `content` like \"%$search%\" ")),
//            'conditions' => array("`title` like \"%$search%\" "),
//            'fields' => array('id', 'title', 'permalink', 'is_url', 'url')
//        );
//        $pages = $this->Page->find('all', $options);
//        //$pages = $this->Page->find('all', array('recursive' => -1, 'fields' => array('id', 'title', 'permalink', 'is_url', 'url')));
//        $titles = $urls = array();
//        foreach ($pages as &$page) {
//            $pageURL = pageURL($page['Page'], true);
//            $page['Page']['page_url'] = $pageURL;
//            $titles[] = $page['Page']['title'];
//            $urls[] = $pageURL;
//        }
//        $results = array(
//            'query' => $_GET['query'],
//            'suggestions' => $titles,
//            'data' => $urls
//        );
//        echo json_encode($results);
//        //exit;
//    }
 function _processEmbeded($page) {
        $matches = array();

        preg_match_all('/\\[\\{slideshow:\s?(\d+)[^\\}]*\\}\\]/i', $page['content'], $matches);
        if (!empty($matches[1])) {
            $this->loadModel('Slideshow');
            $this->set('slideshows', $this->Slideshow->find('all', array('conditions' => array('Slideshow.id' => $matches[1]))));
        }

        preg_match_all('/\\[\\{VIDEO:\s?(\d+)[^\\}]*\\}\\]/i', $page['content'], $matches);
        if (!empty($matches[1])) {
            $this->loadModel('Video');
            $this->set('videos', $this->Video->find('all', array('conditions' => array('Video.id' => $matches[1]))));
        }
    }
    function view($permalink) {
       
        //$this->layout = 'pdf';
      //  $data = Cache::read('Page.' . $permalink);
      //  if (empty($data)) {
            $data = $this->Page->find('first', array('conditions' => array('Page.permalink' => $permalink, 'Page.active' => 1)));
            $page=$data['Page'];
       //     Cache::write('Page.' . $data['Page']['permalink'], $data);
   //     }
      //  if (empty($data)) {
      //      echo 1;
     //        die();
    //        $this->redirect('/');
     //  }


//        $this->__is_item_authenticated($data['Page']);

        $contents = $data['Page']['content'];
        $this->_processEmbeded($page);
        if ($page['url']) {
            $this->redirect($page['url']);
        }

//        if (!empty($data['Page']['attachments']['links'])) {
//            $this->get_links($data['Page']['attachments']['links']);
//        }
//        if (!empty($data['Page']['attachments']['documents'])) {
//            $this->get_resources($data['Page']['attachments']['documents']);
//        }
//        if (!empty($data['Page']['attachments']['videos'])) {
//            $this->get_videos($data['Page']['attachments']['videos']);
//        }

        $this->pageTitle = $data[$this->Page->name]['title'];
        $this->set('contents', $contents);
        $this->set('page_title', $data[$this->Page->name]['title']);
        $this->set('page_id', $data['Page']['id']);
        $this->set('page', $data['Page']);
        $this->metaDescription = $data['Page']['description'];
        $this->metaKeywords = $data['Page']['keywords'];
    }

    function admin_index() {
        $conditions = $this->_filter_params();
        $this->Page->recursive = 0;
        $this->paginate['Page']['order'] = 'Page.id desc';
        $this->set('pages', $this->paginate('Page', $conditions));
    }

    function admin_add($category_id = false) {

        if (!empty($this->data)) {
            $this->Page->create();
            if ($this->Page->save($this->data)) {

                if ($this->data['Page']['url'] == '/') {
                    $this->request->data['Page']['is_url'] = 1;
                }

                if (!empty($this->data['Page']['as_template'])) {
                    $this->createTemplate($this->Page->id);
                }
                $this->flashMessage(__('The Page has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Page could not be saved. Please, try again.', true));
            }
        }

        $this->__common();
    }
    function admin_sections() {
        $sections = array();
        foreach (glob(APP . 'View/Elements/pageSections/*.*') as $file) {
            $sections[substr(basename($file), 0, -4)] = preg_replace('/([a-z])([A-Z]+)/', '$1 $2', substr(basename($file), 0, -4));
        }


//        unset($sections['CarFinder']);
//        unset($sections['Map']);
        $this->set('selectedMenu', 'page sections');
        $this->set(compact('sections'));
    }
    function admin_edit($id = null) {
        $this->loadModel('SidebarSection');
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Page', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if (!empty($this->data['Page']['attachments'])) {
                $attachments = array_map('array_filter', $this->data['Page']['attachments']);
                $this->data['Page']['attachments'] = json_encode($attachments);
            }
            if ($this->data['Page']['url'] == '/') {
                $this->request->data['Page']['is_url'] = 1;
            }
            if ($this->Page->save($this->data)) {

                if (!empty($this->data['Page']['as_template'])) {
                    $this->createTemplate($this->Page->id);
                }
                $this->flashMessage(__('The Page has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Page could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Page->read(null, $id);
        }




        $this->__common();
        $this->render('admin_add');
    }

    function __common() {
        $menus = $this->Page->find('list', array('conditions' => array('(Page.menu_id = 0 OR Page.menu_id IS NULL)', 'Page.add_to_main_menu' => 1)));
        $this->loadModel('SectionLayout');
        $sections = array();
        foreach (glob(APP . 'View/Elements/pageSections/*.*') as $file) {
            $sections[substr(basename($file), 0, -4)] = preg_replace('/([a-z])([A-Z]+)/', '$1 $2', substr(basename($file), 0, -4));
        }
        debug($sections);
        $this->set('sections', $sections);
      //  $this->set('sections', $this->SectionLayout->get_templates_list());
        $this->set(compact('menus'));
    }

    //-----------------------

    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $pages = $this->Page->find('all', array('conditions' => array('Page.id' => $id)));
            $this->set(compact('pages'));
            return;
        }

        if (!empty($id)) {

            $this->Page->id = $id;
            if (!$this->Page->exists()) {
                throw new NotFoundException(__('Invalid Page'));
            }
            if ($this->Page->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Page deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Page was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Page->deleteAll(array('Page.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Pages deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected Pages'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

    function admin_get_submenus($menu_id = null, $submenu = null) {
        Configure::write('debug', 0);
        $this->autoRender = false;
        if (empty($menu_id)) {
            json_encode(array("submenus" => array()));
            exit();
        }
        if (!empty($submenu)) {
            $conditions[] = "Page.id<>$submenu";
        }
        $conditions['Page.menu_id'] = $menu_id;
        $conditions['Page.parent_id IS NULL'];
        $this->Page->recursive = -1;
        $submenus = array();
        $submenus = $this->Page->find('all', array('conditions' => $conditions, 'fields' => array('id', 'title')));
        echo json_encode(array("submenus" => $submenus));
        exit();
    }

    function createTemplate($id) {
        set_time_limit(9999999);
        ini_set("memory_limit", "2542M");
        $page = $this->Page->findById($id);
        $this->set('page', $page);
        $this->set('contents', $page['Page']['content']);

        $view = new View($this, true);
        $report_content = $view->render('view', 'pdf');
        $report_content = preg_replace('/' . str_replace('/', '\/', Router::url('/')) . '/', WWW_ROOT, $report_content);
        $report_content = preg_replace('/webroot\/webroot/', 'webroot', $report_content);


        if (!empty($_GET['html'])) {
            file_put_contents(WWW_ROOT . 'tmpfiles/test.html', $report_content);
            echo $report_content;
            exit;
        }

        $imagePath = $this->createImage($report_content);
        $filename = basename($imagePath);
        $folder = WWW_ROOT . 'img/uploads/templates/';
        $this->loadModel('PagesTemplate');
        $this->PagesTemplate->smart_resize_image("$imagePath", 400, 10000, true, 'file', "$folder/$filename", 0);
        $this->PagesTemplate->smart_resize_image("$imagePath", 150, 10000, true, 'file', "$folder/thumb1_$filename", 0);
        $templateData = array(
            'PagesTemplate' => array(
                'title' => $page['Page']['title'], 'html' => $page['Page']['content'], 'image' => $filename,
            )
        );
        $this->PagesTemplate->create();
        $this->PagesTemplate->set($templateData);
        $this->PagesTemplate->save($templateData);
        unlink($imagePath);
        //exit;
    }

    function createImage($content) {
        $unique_id = uniqid();
        $output_image_file = WWW_ROOT . "tmpfiles/" . $unique_id . "_image.png";
        $output_html_file = WWW_ROOT . "tmpfiles/" . $unique_id . "_htm.html";
        file_put_contents($output_html_file, $content);
        $command = "/usr/local/bin/wkhtmltoimage --crop-w 700 $output_html_file $output_image_file";
        exec($command);
        //unlink($output_html_file);
        return $output_image_file;
    }

    function menus() {
        $this->loadModel('Page');
        $conditions['Page.active'] = 1;
        $conditions[] = "(Page.menu_id = 0 OR Page.menu_id IS NULL)";
        $conditions['Page.add_to_main_menu'] = 1;
        $pages = array();
        $menus = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order', 'recursive' => -1));

        foreach ($menus as $i => $menu) {

            $pages[$i]['Menu'][$i] = $menu['Page'];
            $conditions = array();
            $conditions['Page.active'] = 1;
            $conditions['Page.menu_id'] = $menu['Page']['id'];
            $menus2 = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order', 'recursive' => -1));

            foreach ($menus2 as $j => $menu2) {

                $pages[$i]['Page'][$j] = $menu2['Page'];
                $conditions = array();
                $conditions['Page.active'] = 1;
                $conditions['Page.submenu_id'] = $menu2['Page']['id'];
                $menus3 = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order', 'recursive' => -1));

                foreach ($menus3 as $k => $menu3) {
                    $pages[$i]['submenu'][$k] = $menu3['Page'];
                }
            }
        }
        exit();
    }

    function test($name = 'test') {
        $this->render($name);
    }

    function admin_render($name = 'test') {
        $this->render($name);
    }

    function static_page($page) {
        $this->render($page);
    }

    function search() {
        if (isset($_GET['s']) && !empty($_GET['s'])) {
            $conditions = array();
            $keyword = $_GET['s'];
            $conditions[] = "MATCH(title, content) AGAINST('{$keyword}')";
            $this->paginate = array('order' => "MATCH(title, content) AGAINST('{$keyword}') asc", 'limit' => 1);
            $results = $this->paginate('Page', $conditions);
            $this->set(compact('results', 'keyword'));
        }
    }
    
     function admin_pageSection($section) {
        if ($this->request->is('ajax')) {
            if (!empty($section)) {
                $model = $section;
                $result = AppModel::getData($model);
                $data_array = array();
                foreach ($result as $i => $record) {
                    $data_array[$i]['id'] = $record[$model]['id'];
                    if ($model == 'Special') {
                        $title = $record[$model]['spc_fname'] . ' ' . $record[$model]['spc_lname'];
                    } else {
                        $title = (!empty($record[$model]['title'])) ? $record[$model]['title'] : $record[$model]['name'];
                    }
                    $data_array[$i]['name'] = $title;
                }
            }

            echo json_encode($data_array);
            exit;
        }
        $this->autoRender = false;
    }
    

}

?>