<?php

App::uses('AppController', 'Controller');

/**
 * sections Controller
 *
 * @property $SectionLayout
 */
class SectionLayoutsController extends AppController {

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$conditions = $this->_filter_params();
		$this->SectionLayout->recursive = 0;
		$this->set('sectionsLayouts', $this->paginate($conditions));
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null) {
		$this->SectionLayout->id = $id;
		if (!$this->SectionLayout->exists()) {
			throw new NotFoundException(__('Invalid sections layout'));
		}
		$this->set('sectionsLayout', $this->SectionLayout->read(null, $id));
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		if ($this->request->is('post')) {

			$this->SectionLayout->create();
			if ($this->SectionLayout->save($this->request->data)) {
				$this->__submit_common();
				$this->flashMessage(__('The sections layout has been saved'), 'Sucmessage');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMessage(__('The sections layout could not be saved. Please, try again.'));
			}
		}
		$this->__form_common();
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		$this->SectionLayout->id = $id;
		if (!$this->SectionLayout->exists()) {
			throw new NotFoundException(__('Invalid sections layout'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->SectionLayout->save($this->request->data)) {
				$this->__submit_common();
				$this->flashMessage(__('The sections layout has been saved'), 'Sucmessage');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMessage(__('The sections layout could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SectionLayout->read(null, $id);
		}
		$this->__form_common();
		$this->render('admin_add');
	}

	/**
	 * common method for add and edit
	 *
	 * @return boolean 
	 */
	function __form_common() {

		$this->set('file_settings', $this->SectionLayout->getFileSettings());
		$this->set('types', $this->SectionLayout->types);
		return true;
	}

	/**
	 * common method for add and edit submissions
	 *
	 * @return boolean 
	 */
	function __submit_common() {
//        debug($this->request->data['SectionLayout']['id']);exit;
//        $this->createTemplate($this->SectionLayout->id);
		return true;
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void 
	 */
	public function admin_delete($id = null) {
		if ($this->request->data('submit_btn') === 'no') {
			$this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
			$this->redirect(array('action' => 'index'));
		} elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
			$id = !empty($id) ? array($id) : $this->request->data('ids');
			$sectionLayouts = $this->SectionLayout->find('all', array('conditions' => array('SectionLayout.id' => $id)));
			$this->set(compact('sectionLayouts'));
			return;
		}

		if (!empty($id)) {

			$this->SectionLayout->id = $id;
			if (!$this->SectionLayout->exists()) {
				throw new NotFoundException(__('Invalid sections layout'));
			}
			if ($this->SectionLayout->delete()) {
				if ($this->request->is('ajax')) {
					die(json_encode(array('status' => 'success')));
				} else {
					$this->flashMessage(__('Sections layout deleted'), 'Sucmessage');
					$this->redirect(array('action' => 'index'));
				}
			}
			if ($this->request->is('ajax')) {
				die(json_encode(array('status' => 'error')));
			} else {
				$this->flashMessage(__('Sections layout was not deleted'));
				$this->redirect(array('action' => 'index'));
			}
		} elseif (is_array($this->request->data('ids'))) {
			if ($this->SectionLayout->deleteAll(array('SectionLayout.id' => $this->request->data('ids')), true, true)) {
				if ($this->request->is('ajax')) {
					die(json_encode(array('status' => 'success')));
				} else {
					$this->flashMessage(__('Sectionslayouts deleted successfully'), 'Sucmessage');
					$this->redirect(array('action' => 'index'));
				}
			} else {
				if ($this->request->is('ajax')) {
					die(json_encode(array('status' => 'error')));
				} else {
					$this->flashMessage(__('Error deleting selected sectionslayouts'));
					$this->redirect(array('action' => 'index'));
				}
			}
		}
	}

}
