<?php
App::uses('AppController', 'Controller');
/**
 * Seos Controller
 *
 * @property Seo $Seo
 */
class SeosController extends AppController {

/**
    * index method
    *
    * @return void
    */
    public function index() {
            $conditions = array() ; 
        
            $this->Seo->recursive = 0;
        
        $this->set('seos', $this->paginate($conditions));
    }

    /**
    * view method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function view($id = null) {
            $this->Seo->id = $id;
            if (!$this->Seo->exists()) {
                    throw new NotFoundException(__('Invalid SEO rule'));
            }
            $this->set('seo', $this->Seo->read(null, $id));
    }
    
    

    /**
    * admin_index method
    *
    * @return void
    */
    public function admin_index() {
            $conditions = $this->_filter_params() ;
            $this->Seo->recursive = 0;
        
        $this->set('seos', $this->paginate($conditions));
    }

    /**
    * admin_view method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_view($id = null) {
            $this->Seo->id = $id;
            if (!$this->Seo->exists()) {
                    throw new NotFoundException(__('Invalid SEO rule'));
            }
            $this->set('seo', $this->Seo->read(null, $id));
    }
    
    
    /**
    * admin_add method
    *
    * @return void
    */
    public function admin_add() {
            if ($this->request->is('post')) {
                    $this->__submit_common();
                    $this->Seo->create();
                    if ($this->Seo->save($this->request->data)) {
                            $this->flashMessage(__('The SEO rule has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                            $this->flashMessage(__('The SEO rule could not be saved. Please, try again.'));
                    }
            }
		$this->__form_common();
    }
    


    /**
    * admin_edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_edit($id = null) {
            $this->Seo->id = $id;
            if (!$this->Seo->exists()) {
                    throw new NotFoundException(__('Invalid SEO rule'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                    $this->__submit_common();
                    if ($this->Seo->save($this->request->data)) {
                            $this->flashMessage(__('The SEO rule has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                    $this->flashMessage(__('The SEO rule could not be saved. Please, try again.'));
			}
            } else {
                    $this->request->data = $this->Seo->read(null, $id);
            }
		$this->__form_common();
		$this->render('admin_add') ;
    }
    
        /**
    * common method for add and edit
    *
    * @return boolean 
    */

    
    function __form_common(){ 

        return true;
    }
    
    

        /**
    * common method for add and edit submissions
    *
    * @return boolean 
    */

    
    function __submit_common(){ 

        return true;
    }
    
        
    
    /**
    * admin_delete method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void 
    */
    public function admin_delete($id = null) {
            if( $this->request->data('submit_btn') === 'no'){
              $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
              $this->redirect( array('action' => 'index')); 

            } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) &&  $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax')  )) {
                $id = !empty($id) ? array($id) : $this->request->data('ids') ;
                $seos = $this->Seo->find('all', array('conditions' => array('Seo.id' => $id))) ;
                $this->set(compact('seos'));
                return; 
            }

            if (!empty($id)) {

                $this->Seo->id = $id;
                if (!$this->Seo->exists()) {
                    throw new NotFoundException(__('Invalid SEO rule'));
                }
                if ($this->Seo->delete()) {
                    if ($this->request->is('ajax')) {
                            die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('SEO rule deleted'), 'Sucmessage' );
                        $this->redirect(array('action' => 'index'));
                    }
               }
               if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
               } else {
                    $this->flashMessage(__('SEO rule was not deleted'));
                    $this->redirect(array('action' => 'index'));
               } 
            } elseif(is_array($this->request->data('ids'))){
                if($this->Seo->deleteAll(array( 'Seo.id' => $this->request->data('ids')), true , true)){
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Selected SEO rules deleted successfully'), 'Sucmessage');
                        $this->redirect(array('action' => 'index'));
                    }      
                } else {
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'error')));
                    } else {
                        $this->flashMessage(__('Error deleting selected SEO rules'));
                        $this->redirect(array('action' => 'index'));
                    }
                }            
            }
    }
}
