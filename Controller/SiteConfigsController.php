<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * SiteConfigs Controller
 *
 * @property SiteConfig $SiteConfig
 */
class SiteConfigsController extends AppController {

    public function forgot() {
        if ($this->__authenticate_admin(false)) {
            $this->redirect('/');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $ok = $this->SiteConfig->validateForgot($this->data);
            if (!$ok) {
                $this->flashMessage(__('Please fix errors ,try again', true));
            } else {
                $str = $this->config['basic.site_name'] + $this->config['basic.admin_email'] + $this->config['auth.user_name'] + $this->config['auth.password'] + date('Ymd');
                $confirm_code = substr(base64_encode(md5($str)), 8, 8);
                $email = new CakeEmail();
                $email->template('forgot')
                        ->emailFormat('html')
                        ->subject("{$this->config['basic.site_name']}: " . __('Password Reset Confirmation', true))
                        ->to($this->config['basic.admin_email'])
                        ->from(array($this->config['basic.send_email_from'] => $this->config['basic.site_name']))
                        ->viewVars(array('config' => $this->config, 'confirm_code' => $confirm_code));

                $email->send();

                $this->Session->write('confirm_code', $confirm_code);

                $this->redirect(array('action' => 'reset'));
            }
        }
        $this->pageTitle = "Forgot Password";
    }

    //---------------------------------------
    public function reset() {
        if ($this->__authenticate_admin(false)) {
            $this->redirect('/');
        }
        if (!$this->Session->check('confirm_code')) {
            // $this->redirect(array('action' => 'forgot'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->data['SiteConfig']['code'] == $this->Session->read('confirm_code')) {
                $this->redirect(array('action' => 'confirm_password'));
            } else {
                $this->SiteConfig->validationErrors['code'] = __('The code you entered is invalid.', true);
            }
        }
        $this->set('email', $this->config['basic.admin_email']);
        $this->pageTitle = "Reset Password";
    }

    //---------------------------------------
    public function confirm_password() {

        if ($this->__authenticate_admin(false)) {
            $this->redirect('/');
        }
        if (!$this->SiteConfig->check_code_confirmation($this->params['url'])) {
            // $this->redirect(array('action' => 'forgot'));
        }
        if (!empty($this->data['SiteConfig'])) {
            if ($this->SiteConfig->saveConfirmPassword($this->data)) {
                $this->flashMessage(__('New Password has been saved', true), 'Sucmessage');
                $this->redirect('/');
            } else {
                $this->flashMessage(__('Faild to save new password ,try again', true));
            }
        }
        $this->pageTitle = "Reset Password";
    }

    function admin_getFileSettings($varid = null) {
        die(json_encode(array('current_value' => SiteConfig::getCached($varid), 'download_url' => $this->SiteConfig->uploadedFilePath($varid), 'delete_url' => Router::url(array('action' => 'upload', $varid, '?' => array('delete' => true)))) + array_intersect_key($this->SiteConfig->uploadedFileSettings($varid), array_combine(array('width', 'height', 'max_file_size', 'extensions'), range(0, 3)))));
    }

    function admin_clear_cache() {
        $this->SiteConfig->clearCache();
        parent::admin_clear_cache();
    }

    function admin_upload($varid = null) {
        Configure::write('debug', 0);

        ob_clean();
        if (empty($varid))
            json_encode(array('suceess' => false, 'error' => 'Error in upload URL'));
        $var = $this->SiteConfig->decode_var($varid);
        if (empty($var))
            die(json_encode(array('success' => false, 'error' => 'Permission Denied')));


        $var_details = $this->SiteConfig->getStructure($varid);

        if (empty($var_details['actsAs'])) {
            die(json_encode(array('success' => false, 'error' => 'Permission Denied')));
        }
        foreach ($var_details['actsAs'] as $bhv => $opts) {
            $this->SiteConfig->Behaviors->attach($bhv, array('value' => $opts));
        }


        if (!empty($this->request->query['delete'])) {
            $rec = $this->SiteConfig->findByGroupAndSetting($var->group, $var->setting);
            if (isset($rec['SiteConfig']['id']) && $this->SiteConfig->delete($rec['SiteConfig']['id']) && SiteConfig::clearCache()) {
                die(json_encode(array('success' => true)));
            } else {
                die(json_encode(array('success' => false, 'error' => 'Could not delete the file')));
            }
        } elseif (!empty($this->data) || $_SERVER['REQUEST_METHOD'] === 'POST') {

            $record = $this->SiteConfig->find('first', array('conditions' => array('group' => $var->group, 'setting' => $var->setting)));
            $id = !empty($record['SiteConfig']['id']) ? $record['SiteConfig']['id'] : null;
            $error = array();
            $modelname = 'SiteConfig';



            if (!isset($_SERVER['CONTENT_TYPE'])) { // 
; // do nothing
            } else if (strpos(strtolower($_SERVER['CONTENT_TYPE']), 'multipart/') === 0) { // hidden iframe form submission
                foreach ($this->data[$modelname] as $i => $file) {

                    if (!$this->{$modelname}->save(array($modelname => array('id' => $id, $i => $file, 'group' => $var->group, 'setting' => $var->setting)))) {
                        $error[] = reset($this->{$modelname}->validationErrors);
                    }
                    break;
                }
            } else { // ajax upload
                $input = fopen("php://input", "r");
                $temp = tmpfile();
                $realSize = stream_copy_to_stream($input, $temp);
                fclose($input);

                $meta_data = stream_get_meta_data($temp);

                $tmp_name = $meta_data["uri"];


                $this->data = array($modelname => array('value' => array(
                            'name' => $this->params['url']['data'][$modelname]['value'],
                            'tmp_name' => $tmp_name,
                            'size' => filesize($tmp_name),
                            'type' => 'application/octet-stream',
                            'error' => 0
                        ),
                        'id' => $id, 'group' => $var->group, 'setting' => $var->setting,
                ));
                if (!$this->{$modelname}->save($this->data)) {
                    //die(json_encode($this->data));
                    $error[] = reset($this->{$modelname}->validationErrors);
                }
            }
            header('Content-Type: text/plain'); // respond as text/plain so the f*n ie can parse json correctly
            if (empty($error)) {
                //  $this->flashMessage('Success');
                Cache::write('logo_path', false);

                die(json_encode(array('success' => true, 'url' => $this->SiteConfig->uploadedFilePath($varid))));
            } else {
                //   $this->flashMessage(count($error) . 'Error(s) occured <br/>' . implode('<br/>', ($error)));
                die(json_encode(array('success' => false, 'error' => implode('<br/>', $error))));
            }
        }
        $this->redirect(array('action' => 'edit'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $valid = $this->__submit_common();

            if ($valid && $this->SiteConfig->saveAll($this->request->data['SiteConfig'], array('validate' => true))) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'suceess', 'message' => 'The site configuration has been saved successfully.. ')));
                } else {
                    $this->flashMessage(__('The site configuration has been saved'), 'Sucmessage');
                    $this->redirect(array('action' => 'edit'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error', 'message' => 'Error saving selected colours..')));
                } else {
                    $this->flashMessage(__('The site configuration could not be saved. Please, try again.'));
                }
            }
        } else {
            $data = $this->SiteConfig->find('all');
            $this->request->data['SiteConfig'] = Set::extract('/SiteConfig/.', $data);
        }
        $this->__form_common();
        $this->pageTitle = 'Edit Site Configuration';
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
        $map = array();
        if (!empty($this->request->data)) {
            $map = Set::combine($this->request->data['SiteConfig'], array('{0}.{1}', '{n}.group', '{n}.setting'), '{n}.id');
            $map = array_combine(array_keys($map), array_keys($this->request->data['SiteConfig']));
            $dbSettings = array_diff(array_keys($map), array_keys($this->SiteConfig->getStructure()));

            foreach ($dbSettings as $setting) {
                $ex = explode('.', $setting);
                if (count($ex) == 2 && !empty($ex[0])) {
                    $gid = $ex[0];
                    $varid = $ex[1];
                } else {
                    $gid = 'misc';
                    $varid = $ex[0];
                }
                $this->SiteConfig->vars[$gid]['vars'][$varid] = array('title' => Inflector::humanize($varid), 'input' => array('class' => 'from-db'));
            }
        }
        $this->set('config_schema', $this->SiteConfig->vars);

        $this->set('map', $map);

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {
        if (!empty($this->request->data)) {
            //  debug($this->request->data);
            $map = Set::combine($this->request->data['SiteConfig'], array('{0}.{1}', '{n}.group', '{n}.setting'), '{n}.id');
            $map = array_combine(array_keys($map), array_keys($this->request->data['SiteConfig']));
            foreach ($map as $key => $id) {
                $structure = $this->SiteConfig->getStructure($key);

                if (!empty($structure['compare'])) {
                    if ($this->request->data['SiteConfig'][$id]['value'] != $this->request->data['SiteConfig'][$map[$structure['compare']]]['value']) {
                        $this->SiteConfig->validationErrors = array($id => array('value' => array("Values do not match")), $map[$structure['compare']] => array('value' => array("Values do not match")));
                        return false;
                    }
                }
                if (!empty($structure['nosave'])) {
                    unset($this->request->data['SiteConfig'][$id]);
                }
            }
        }

        return true;
    }

}
