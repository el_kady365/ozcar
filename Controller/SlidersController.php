<?php

App::uses('AppController', 'Controller');

/**
 * Sliders Controller
 *
 * @property Slider $Slider
 */
class SlidersController extends AppController {

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$conditions = $this->_filter_params();
		$this->Slider->recursive = 0;
		$this->set('sliders', $this->paginate($conditions));
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null) {
		$this->Slider->id = $id;
		if (!$this->Slider->exists()) {
			throw new NotFoundException(__('Invalid slider'));
		}
		$this->set('slider', $this->Slider->read(null, $id));
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->__submit_common();
			$this->Slider->create();
			if ($this->Slider->save($this->request->data)) {
				$this->flashMessage(__('The slider has been saved'), 'Sucmessage');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMessage(__('The slider could not be saved. Please, try again.'));
			}
		}
		$this->__form_common();
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		$this->Slider->id = $id;
		if (!$this->Slider->exists()) {
			throw new NotFoundException(__('Invalid slider'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->__submit_common();
			if ($this->Slider->save($this->request->data)) {
				$this->flashMessage(__('The slider has been saved'), 'Sucmessage');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMessage(__('The slider could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Slider->read(null, $id);
		}
		$this->__form_common();
		$this->render('admin_add');
	}

	/**
	 * common method for add and edit
	 *
	 * @return boolean 
	 */
	function __form_common() {

		$this->set('file_settings', $this->Slider->getFileSettings());
		return true;
	}

	/**
	 * common method for add and edit submissions
	 *
	 * @return boolean 
	 */
	function __submit_common() {

		return true;
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void 
	 */
	public function admin_delete($id = null) {
		if ($this->request->data('submit_btn') === 'no') {
			$this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
			$this->redirect(array('action' => 'index'));
		} elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
			$id = !empty($id) ? array($id) : $this->request->data('ids');
			$sliders = $this->Slider->find('all', array('conditions' => array('Slider.id' => $id)));
			$this->set(compact('sliders'));
			return;
		}

		if (!empty($id)) {

			$this->Slider->id = $id;
			if (!$this->Slider->exists()) {
				throw new NotFoundException(__('Invalid slider'));
			}
			if ($this->Slider->delete()) {
				if ($this->request->is('ajax')) {
					die(json_encode(array('status' => 'success')));
				} else {
					$this->flashMessage(__('Slider deleted'), 'Sucmessage');
					$this->redirect(array('action' => 'index'));
				}
			}
			if ($this->request->is('ajax')) {
				die(json_encode(array('status' => 'error')));
			} else {
				$this->flashMessage(__('Slider was not deleted'));
				$this->redirect(array('action' => 'index'));
			}
		} elseif (is_array($this->request->data('ids'))) {
			if ($this->Slider->deleteAll(array('Slider.id' => $this->request->data('ids')), true, true)) {
				if ($this->request->is('ajax')) {
					die(json_encode(array('status' => 'success')));
				} else {
					$this->flashMessage(__('Sliders deleted successfully'), 'Sucmessage');
					$this->redirect(array('action' => 'index'));
				}
			} else {
				if ($this->request->is('ajax')) {
					die(json_encode(array('status' => 'error')));
				} else {
					$this->flashMessage(__('Error deleting selected sliders'));
					$this->redirect(array('action' => 'index'));
				}
			}
		}
	}

}
