<?php

App::uses('AppController', 'Controller');

/**
 * Snippets Controller
 *
 * @property Snippet $Snippet
 */
class SnippetsController extends AppController {

	public function admin_index() {
		$this->set('snippets', $this->Snippet->snippets);
	}

	public function admin_edit($permalink = false) {
		$permalink = strtolower($permalink);
		if (!isset($this->Snippet->snippets[$permalink])) {
			$this->flashMessage(__('Snippet not found'));
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
                        $this->request->data['Snippet']['permalink'] = $permalink;
			if ($this->Snippet->save($this->request->data)) {
				$this->flashMessage(__('Snippet has been saved'), 'Sucmessage');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->flashMessage('Could not save snippet');
			}
		} else {
			$snippet = $this->Snippet->find('first', array('conditions' => array('Snippet.permalink' => $permalink)));
			if (!$snippet) {
				$this->Snippet->create(array('title' => $this->Snippet->snippets[$permalink], 'permalink' => $permalink, 'content' => ''));
				$snippet = $this->Snippet->save();
			}

			$this->request->data = $snippet;
		}
	}
}
