<?php

App::uses('AppController', 'Controller');

/**
 * Templates Controller
 *
 * @property Template $Template
 */
class TemplatesController extends AppController {
    /**
     * index method
     *
     * @return void
     */

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        if ($this->__authenticate_dealer()) {
            unset($this->Template->filters["dealer_id"]);
            $dealer = $this->__get_dealer_info();
            $conditions["Template.dealer_id"] = $dealer["Dealer"]["id"];
        }
        $conditions[] = $this->_filter_params();
        $this->Template->recursive = 0;
        $this->set('templates', $this->paginate($conditions));
        
        $this->set('dealers', $this->Template->Dealer->find("list"));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Template->id = $id;
        if (!$this->Template->exists()) {
            throw new NotFoundException(__('Invalid template'));
        }
        $this->set('template', $this->Template->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->__submit_common();
            $this->Template->create();
            if ($this->Template->save($this->request->data)) {
                $this->flashMessage(__('The template has been saved'), 'Sucmessage');
                $this->redirect(array("controller" => "map_fields", 'action' => 'add', $this->Template->id));
            } else {
                $this->flashMessage(__('The template could not be saved. Please, try again.'));
            }
        }
        $this->__form_common();
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Template->id = $id;
        if (!$this->Template->exists()) {
            throw new NotFoundException(__('Invalid template'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->__submit_common();
            if ($this->Template->save($this->request->data)) {
                $this->flashMessage(__('The template has been saved'), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The template could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Template->read(null, $id);
        }
        $this->__form_common();
        $this->render('admin_add');
    }

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
        if (empty($_SESSION["dealer"])) {

            $this->set('dealers', $this->Template->Dealer->find("list"));
        }
        $this->loadModel("Car");
        $this->set("types", Car::$types);
        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {

        return true;
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $templates = $this->Template->find('all', array('conditions' => array('Template.id' => $id)));
            $this->set(compact('templates'));
            return;
        }

        if (!empty($id)) {

            $this->Template->id = $id;
            if (!$this->Template->exists()) {
                throw new NotFoundException(__('Invalid template'));
            }
            if ($this->Template->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Template deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('Template was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->Template->deleteAll(array('Template.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Templates deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected templates'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
