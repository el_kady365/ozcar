<?php

class ThreeImageTextsController extends AppController {

    var $name = 'ThreeImageTexts';
    var $helpers = array('Html', 'Form', 'Fck');

    function admin_index() {
        $conditions = $this->_filter_params();
        $this->ThreeImageText->recursive = 0;
        $this->set('threeImageTexts', $this->paginate('ThreeImageText',$conditions));
        
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid ThreeImageText.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('threeImageText', $this->ThreeImageText->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->ThreeImageText->create();
            if ($this->ThreeImageText->save($this->data)) {
                $this->flashMessage(__('The ThreeImageText has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The ThreeImageText could not be saved. Please, try again.', true));
            }
        }
        $this->loadModel('Dealer');
        $this->set('dealerships', $this->Dealer->getList());
        $this->set('image_settings', $this->ThreeImageText->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid ThreeImageText', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->ThreeImageText->save($this->data)) {
                $this->flashMessage(__('The ThreeImageText has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The ThreeImageText could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->ThreeImageText->read(null, $id);
        }
        $this->loadModel('Dealer');
        $this->set('dealerships', $this->Dealer->getList());
        $this->set('image_settings', $this->ThreeImageText->getImageSettings());
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid id for ThreeImageText', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->ThreeImageText->del($id)) {
            $this->flashMessage(__('ThreeImageText deleted', true));
            $this->redirect(array('action' => 'index'));
        }
    }
    
    function admin_delete_multi() {
        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for ThreeImageText', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->ThreeImageText->deleteAll(array('ThreeImageText.id' => $_POST['ids']))) {
            $this->flashMessage(__('ThreeImageText items deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action' => 'index'));
        }
    }

}

?>
