<?php
App::uses('AppController', 'Controller');
/**
 * TopBanners Controller
 *
 * @property TopBanner $TopBanner
 */
class TopBannersController extends AppController {



    /**
    * admin_index method
    *
    * @return void
    */
    public function admin_index() {
            $conditions = $this->_filter_params() ;
            $this->TopBanner->recursive = 0;
        
        $this->set('topBanners', $this->paginate($conditions));
    }

    /**
    * admin_view method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_view($id = null) {
            $this->TopBanner->id = $id;
            if (!$this->TopBanner->exists()) {
                    throw new NotFoundException(__('Invalid top banner'));
            }
            $this->set('topBanner', $this->TopBanner->read(null, $id));
    }
    
    
    /**
    * admin_add method
    *
    * @return void
    */
    public function admin_add() {
            if ($this->request->is('post')) {
                    $this->__submit_common();
                    $this->TopBanner->create();
                    if ($this->TopBanner->save($this->request->data)) {
                            $this->flashMessage(__('The top banner has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                            $this->flashMessage(__('The top banner could not be saved. Please, try again.'));
                    }
            }
		$this->__form_common();
    }
    


    /**
    * admin_edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_edit($id = null) {
            $this->TopBanner->id = $id;
            if (!$this->TopBanner->exists()) {
                    throw new NotFoundException(__('Invalid top banner'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                    $this->__submit_common();
                    if ($this->TopBanner->save($this->request->data)) {
                            $this->flashMessage(__('The top banner has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                    $this->flashMessage(__('The top banner could not be saved. Please, try again.'));
			}
            } else {
                    $this->request->data = $this->TopBanner->read(null, $id);
            }
		$this->__form_common();
		$this->render('admin_add') ;
    }
    
        /**
    * common method for add and edit
    *
    * @return boolean 
    */

    
    function __form_common(){ 

		$this->set('file_settings',$this->TopBanner->getFileSettings());
        return true;
    }
    
    

        /**
    * common method for add and edit submissions
    *
    * @return boolean 
    */

    
    function __submit_common(){ 

        return true;
    }
    
        
    
    /**
    * admin_delete method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void 
    */
    public function admin_delete($id = null) {
            if( $this->request->data('submit_btn') === 'no'){
              $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
              $this->redirect( array('action' => 'index')); 

            } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) &&  $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax')  )) {
                $id = !empty($id) ? array($id) : $this->request->data('ids') ;
                $topBanners = $this->TopBanner->find('all', array('conditions' => array('TopBanner.id' => $id))) ;
                $this->set(compact('topBanners'));
                return; 
            }

            if (!empty($id)) {

                $this->TopBanner->id = $id;
                if (!$this->TopBanner->exists()) {
                    throw new NotFoundException(__('Invalid top banner'));
                }
                if ($this->TopBanner->delete()) {
                    if ($this->request->is('ajax')) {
                            die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Top banner deleted'), 'Sucmessage' );
                        $this->redirect(array('action' => 'index'));
                    }
               }
               if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
               } else {
                    $this->flashMessage(__('Top banner was not deleted'));
                    $this->redirect(array('action' => 'index'));
               } 
            } elseif(is_array($this->request->data('ids'))){
                if($this->TopBanner->deleteAll(array( 'TopBanner.id' => $this->request->data('ids')), true , true)){
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'success')));
                    } else {
                        $this->flashMessage(__('Topbanners deleted successfully'), 'Sucmessage');
                        $this->redirect(array('action' => 'index'));
                    }      
                } else {
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'error')));
                    } else {
                        $this->flashMessage(__('Error deleting selected topbanners'));
                        $this->redirect(array('action' => 'index'));
                    }
                }            
            }
    }
}
