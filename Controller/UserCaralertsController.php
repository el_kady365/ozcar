<?php

App::uses('AppController', 'Controller');

/**
 * UserCaralerts Controller
 *
 */
class UserCaralertsController extends AppController {

    /**
     * 
     *
     * @var mixed
     */
    public function add($car_id = false) {
        if (!$car_id) {
            throw new NotFoundException(__('Invalid car'));
        }

        $user = $this->loggedUser;
        $this->loadModel('UserCaralert');
        $userCar = $this->UserCaralert->find('count', array('conditions' => array('UserCaralert.user_id' => $user['id'], 'UserCaralert.car_id' => $car_id)));
        if (!$userCar) {
            $this->UserCaralert->create();
            if ($this->UserCaralert->save(array('UserCaralert' => array('user_id' => $user['id'], 'car_id' => $car_id)))) {
                $this->flashMessage('The car saved successfully to your saved cars', 'Sucmessage');
                $this->redirect($this->referer());
            } else {
                $this->flashMessage('Error! the car has not been saved to your saved cars');
                $this->redirect($this->referer());
            }
        }else{
                $this->flashMessage('The car saved successfully to your saved cars', 'Sucmessage');
                $this->redirect($this->referer());            
        }
    }

    function index() {
        $user_id = $this->Auth->user('id');
        $this->UserCaralert->recursive = 2;
        $saved_cars = $this->UserCaralert->find('all', array('conditions' => array('UserCaralert.user_id' => $user_id), 'contain' => array('Car', 'Car.CarImage'), 'limit' => 6));
        $this->set(compact('saved_cars'));
    }

    function do_operation() {
        $ids = $this->request->data['chk'];
        $operation = $this->request->query['action'];

        if ($operation == 'delete') {
            if ($this->UserCaralert->deleteAll(array('UserCaralert.id' => $ids))) {
                $this->flashMessage('Your saved car deleted successfully', 'Sucmessage');
            } else {
                $this->setFlash('UserCaralert can not be deleted');
            }
        } elseif ($operation == 'enquire') {
            $car = $this->UserCaralert->find('first', array('conditions' => array('UserCaralert.id' => $ids[0]), 'recursive' => -1));
            $this->redirect(array('controller' => 'cars', 'action' => 'view', $car['UserCaralert']['car_id'], '#'=>'carEnquiry'));
            exit;
        }
        $this->redirect(array('action' => 'index'));
    }

}
