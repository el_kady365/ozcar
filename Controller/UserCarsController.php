<?php

App::uses('AppController', 'Controller');

/**
 * UserCars Controller
 *
 */
class UserCarsController extends AppController {

    /**
     * 
     *
     * @var mixed
     */
    public function add($car_id = false) {
        if (!$car_id) {
            throw new NotFoundException(__('Invalid car'));
        }

        $user = $this->loggedUser;
        $this->loadModel('UserCar');
        $userCar = $this->UserCar->find('count', array('conditions' => array('UserCar.user_id' => $user['id'], 'UserCar.car_id' => $car_id)));
        if (!$userCar) {
            $this->UserCar->create();
            if ($this->UserCar->save(array('UserCar' => array('user_id' => $user['id'], 'car_id' => $car_id)))) {
                $this->flashMessage('The car saved successfully to your saved cars', 'Sucmessage');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->flashMessage('Error! the car has not been saved to your saved cars');
                $this->redirect(array('action'=>'index'));
            }
        }else{
                $this->flashMessage('The car saved successfully to your saved cars', 'Sucmessage');
                $this->redirect(array('action'=>'index'));            
        }
    }

    function index() {
        $user_id = $this->Auth->user('id');
        $this->UserCar->recursive = 2;
        $saved_cars = $this->UserCar->find('all', array('conditions' => array('UserCar.user_id' => $user_id), 'contain' => array('Car', 'Car.CarImage'), 'limit' => 6));
        $this->set(compact('saved_cars'));
    }

    function do_operation() {
        $ids = $this->request->data['chk'];
        $operation = $this->request->query['action'];

        if ($operation == 'delete') {
            if ($this->UserCar->deleteAll(array('UserCar.id' => $ids))) {
                $this->flashMessage('Your saved car deleted successfully', 'Sucmessage');
            } else {
                $this->setFlash('UserCar can not be deleted');
            }
        } elseif ($operation == 'enquire') {
            $car = $this->UserCar->find('first', array('conditions' => array('UserCar.id' => $ids[0]), 'recursive' => -1));
            $this->redirect(array('controller' => 'cars', 'action' => 'view', $car['UserCar']['car_id'], '#'=>'carEnquiry'));
            exit;
        }
        $this->redirect($this->referer());
    }

}
