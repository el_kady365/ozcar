<?php

App::uses('AppController', 'Controller');

/**
 * UserDealerLocations Controller
 *
 */
class UserDealerLocationsController extends AppController {

    /**
     * 
     *
     * @var mixed
     */
    public function add($dealer_location_id = false) {
        if (!$dealer_location_id) {
            throw new NotFoundException(__('Invalid dealer location'));
        }

        $user = $this->loggedUser;
        $this->loadModel('UserDealerLocation');
        $userDealership = $this->UserDealerLocation->find('count', array('conditions' => array('UserDealerLocation.user_id' => $user['id'], 'UserDealerLocation.dealer_location_id' => $dealer_location_id)));
        $message = '';
        $status = '';
        if (!$userDealership) {
            $this->UserDealerLocation->create();
            if ($this->UserDealerLocation->save(array('UserDealerLocation' => array('user_id' => $user['id'], 'dealer_location_id' => $dealer_location_id)))) {
                $message = 'The dealer location saved successfully to your saved dealer locations';
                $status = 'success';
            } else {
                $message = 'Error! the dealer location has not been saved to your saved dealer locations';
                $status = 'error';
            }
        } else {
            $message = 'You have already added this dealership';
            $status = 'information';
        }
        if ($this->request->is('ajax')) {
            echo json_encode(array('message' => $message, 'status' => $status));
            exit;
        } else {
            if ($status == 'error') {
                $status = 'Errormessage';
            } elseif ($status == 'information') {
                $status = 'Notemessage';
            }
            $this->flashMessage($message, $status);
            $this->redirect(array('action' => 'index'));
        }
    }

    function index() {
        $user = $this->loggedUser;
        $this->loadModel('Car');

        $saved_dealerships = $this->UserDealerLocation->getSavedDealerLocations($user);

        $this->set(compact('saved_dealerships'));
    }

    function do_operation() {
        $ids = $this->request->data['chk'];
        $operation = $this->request->query['action'];

        if ($operation == 'delete') {
            if ($this->UserDealerLocation->deleteAll(array('UserDealerLocation.id' => $ids))) {
                $this->flashMessage('Your saved dealer location deleted successfully', 'Sucmessage');
            } else {
                $this->setFlash('UserDealerLocation can not be deleted');
            }
        }
        $this->redirect($this->referer());
    }

}
