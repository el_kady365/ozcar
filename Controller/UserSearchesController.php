<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * SavedSearches Controller
 * @property Car $Car
 * @property UserSearch $UserSearch
 *
 */
class UserSearchesController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('cron_car_alerts');
    }

    function index($alerts = false) {
        $user = $this->Auth->user();
        $conditions = array();
        $conditions['UserSearch.user_id'] = $user['id'];
        if ($alerts) {
            $conditions['UserSearch.send_alert'] = 1;
        } else {
            $conditions[] = '(UserSearch.send_alert IS NULL OR UserSearch.send_alert=0)';
        }
        $searches = $this->UserSearch->find('all', array('conditions' => $conditions));
        $this->set('searches', $searches);
        $this->set('alerts', $alerts);
    }

    public function car_alerts($id = false) {
        $user = $this->Auth->user();
        if (empty($user['id'])) {
            $this->redirect('/users/login');
            return;
        }
        $params = $this->params->query;
        $this->loadModel('Car');
        if (!empty($params)) {
            $this->Car->validate = array('make' => array('rule' => 'notEmpty', 'message' => 'Required'));
            $this->Car->set(array('Car' => $params));
            if ($this->Car->validates()) {
                if (!$id) {
                    $this->UserSearch->create();
                } else {
                    $this->UserSearch->id = $id;
                }
                $data = array();
                $data['UserSearch']['user_id'] = $user['id'];
                $data['UserSearch']['send_alert'] = 1;
                $data['UserSearch']['notify_period'] = $params['frequency'];
                $data['UserSearch']['search_parameters'] = $params;

                if ($this->UserSearch->save($data)) {
                    $this->flashMessage(__('Your search results has been saved successfully', true), 'Sucmessage');
                    $this->redirect(array('action' => 'car_alerts', $this->UserSearch->id));
                }
            } else {
                $this->flashMessage(__('Please complete the form data and continue', true));
            }
        } elseif ($id && empty($data)) {
            $search = $this->UserSearch->read(null, $id);
            $params = json_decode($search['UserSearch']['search_parameters'], 1);
        }

        $this->set('params', $params);
        $this->set('id', $id);
        $this->set($this->Car->getAllFields2());
    }

    public function delete($id) {
        $search = $this->UserSearch->read(null, $id);
        if (empty($search)) {
            throw new NotFoundException(__('Invalid Search'));
        }
        if ($this->UserSearch->delete()) {
            $this->flashMessage(__('Search deleted'), 'Sucmessage');
            $this->redirect($this->referer());
        }
    }

    public function cron_car_alerts() {
//        die('passed');
        $this->loadModel('Car');
//exit;
        $conditions['UserSearch.send_alert'] = 1;
        $conditions[] = "(DATE_ADD(UserSearch.last_notified,INTERVAL UserSearch.notify_period DAY) <= now() OR (UserSearch.last_notified is NULL OR UserSearch.last_notified='' )) ";
        $this->UserSearch->recursive = -1;
        $users = $this->UserSearch->find('all', array('conditions' => $conditions));
//        debug($users);exit;
        $this->loadModel('Users.User');
        if (!empty($users)) {
            foreach ($users as $user) {

                $foundUser = $this->User->find('first', array('conditions' => array('User.id' => $user['UserSearch']['user_id'])));
//                debug($foundUser);exit;
                $options = $this->Car->search_params(json_decode($user['UserSearch']['search_parameters'], 1));
                $cars = $this->Car->find('all', array('conditions' => $options['conditions'], 'order' => 'Car.created desc',
                    'contain' => false));
//                debug($this->config);exit;

                $email = new CakeEmail();
//                $email->config(array('host' => 'ssl://smtp.gmail.com',
//                    'port' => 465,
//                    'timeout' => 30,
//                    'username' => 'my@gmail.com',
//                    'password' => 'secret',
//                    'transport' => 'Smtp')
//                );
                $email->template('car_alert', 'contact')
                        ->emailFormat('html')
                        ->subject("{$this->config['basic.site_name']}: {Car alerts}")
                        ->to($foundUser['User']['email'])
                        ->from(array($this->config['basic.admin_email'] => $this->config['basic.site_name']))
                        ->viewVars(array('config' => $this->config, 'cars' => $cars, 'user' => $foundUser));

                $email->send();
            }
        }
        exit;
    }

    function do_operation() {
        $ids = $this->request->data['chk'];
        $operation = $this->request->query['action'];

        if ($operation == 'delete') {
            if ($this->UserSearch->deleteAll(array('UserSearch.id' => $ids))) {
                $this->flashMessage('UserSearch deleted successfully', 'Sucmessage');
            } else {
                $this->flashMessage('UserSearch can not be deleted');
            }
        } elseif ($operation == 'edit') {
            $this->redirect(array('action' => 'car_alerts', $ids[0]));
            exit;
        }
        $this->redirect($this->referer());
    }

}
