<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    public $clearCache = array();
    public $unsafeFields = array();
    public STATIC $ResizesOptions = array();
    public static $states = array(
        'ACT' => 'Australian Capital Territory',
        'NSW' => 'New South Wales',
        'NT' => 'Northern Territory',
        'Queensland' => 'Queensland',
        'SA' => 'South Australia',
        'Tasmania' => 'Tasmania',
        'Victoria' => 'Victoria',
        'WA' => 'Western Australia'
    );

    public static function parseTime($timestr) {
        $time = floatval($timestr);
        if (stripos($timestr, 'h')) {
            $time = (float) $timestr;
        } elseif (stripos($timestr, 'm')) {
            $time = (float) ($timestr / 60);
        } elseif (strpos($timestr, ':')) {
            $time = self::timeFromString($timestr);
        } elseif (empty($timestr)) {
            $time = 0;
        }
        return $time;
    }

    public function afterSave($created) {
        if ($this->hasField('display_order')) {
            if (empty($this->data[$this->alias]['display_order'])) {
                $this->saveField('display_order', $this->id);
            }
        }


        foreach ($this->clearCache as $cacheitem) {
            Cache::write($cacheitem, false);
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        foreach ($this->clearCache as $cacheitem) {
            Cache::clearGroup($cacheitem);
        }
    }

    public static function getTime($floatTime) {
        $time = array('hours' => 0, 'minutes' => 0, 'seconds' => 0);
        $time['hours'] = floor($floatTime);

        $floatTime -= $time['hours'];
        $floatTime *= 60;
        $time['minutes'] = floor($floatTime);

        if ($time['minutes'] >= 60) {
            $time['hours'] += 1;
            $time['minutes'] = 0;
        }
        $floatTime -= $time['minutes'];
        $floatTime *= 60;
        $time['seconds'] = round($floatTime);

        if ($time['seconds'] == 60) {
            $time['minutes'] += 1;
            if ($time['minutes'] >= 60) {
                $time['hours'] += 1;
                $time['minutes'] = 0;
            }
            $time['seconds'] = 0;
        }

        $time['string'] = str_pad($time['hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($time['minutes'], 2, '0', STR_PAD_LEFT);
        ;
        return $time;
    }

    public static function getTimeObj($floattime) {
        $time = self::getTime($floattime);
        $obj = new stdClass();
        $obj->hours = $time['hours'];
        $obj->minutes = $time['minutes'];
        $obj->seconds = $time['seconds'];
        $obj->string = $time['string'];
        return $obj;
    }

    public static function timeFromString($strTime) {
        $time = explode(':', $strTime);

        if (empty($time[1]))
            $time[1] = 0;
        if (empty($time[2]))
            $time[2] = 0;
        $ret = (float) ((int) $time[0] + (float) $time[1] / 60 + (float) $time[2] / 3600);

        return ($ret);
    }

    function safeSave($data) {
        return $this->save($data, true, array_diff(array_keys($this->schema()), $this->unsafeFields));
    }

    public static function getData($modelName, $ids = array(), $conditions = array()) {
        $modelObject = ClassRegistry::init($modelName);
        if ($modelObject) {
            $in_conditions = array();

            if (!empty($ids)) {
                $in_conditions[$modelName . '.id'] = $ids;
            }
            if (!empty($conditions)) {
                $in_conditions += $conditions;
            }
            $result = $modelObject->find('all', array(
                'conditions' => $in_conditions,
            ));
            return $result;
        }
        return false;
    }

    function smart_resize_image($file, $width = 0, $height = 0, $proportional = true, $output = 'file', $file_name = false, $crop = false) {

        if ($height == 0 && $width == 0)
            return true;

        if ($height <= 0 && $width <= 0) {
            return false;
        }

        $info = getimagesize($file);
        $image = '';


        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;



        switch ($info[2]) {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($file);
                break;
            default:
                return false;
                break;
        }

        if ($proportional == 1) {
            if ($width == 0)
                $factor = $height / $height_old;
            elseif ($height == 0)
                $factor = $width / $width_old;
            else
                $factor = min($width / $width_old, $height / $height_old);

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        } else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
        }

        $image_resized = imagecreatetruecolor($final_width, $final_height);

        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $trnprt_indx = imagecolortransparent($image);
            // If we have a specific transparent color
            if ($trnprt_indx >= 0) {

                // Get the original image's transparent color's RGB values
                $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

                // Allocate the same color in the new image resource
                $trnprt_indx = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

                // Completely fill the background of the new image with allocated color.
                imagefill($image_resized, 0, 0, $trnprt_indx);

                // Set the background color for new image to transparent
                imagecolortransparent($image_resized, $trnprt_indx);
            }
            // Always make a transparent background color for PNGs that don't have one allocated already
            elseif ($info[2] == IMAGETYPE_PNG) {

                // Turn off transparency blending (temporarily)
                imagealphablending($image_resized, false);

                // Create a new transparent color for image
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);

                // Completely fill the background of the new image with allocated color.
                imagefill($image_resized, 0, 0, $color);

                // Restore transparency blending
                imagesavealpha($image_resized, true);
            }
        }


        if ($crop) {
            $orig_aspect = $width_old / $height_old;
            $new_aspect = $width / $height;
            if ($orig_aspect != $new_aspect) {
                $w_ratio = $width_old / $width;
                $h_ratio = $height_old / $height;

                $mid_w = $width;
                $mid_h = $height;

                if ($h_ratio < $w_ratio) {
                    $mid_w = $width_old * $height / $height_old;
                } else {
                    $mid_h = $height_old * $width / $width_old;
                }





                $image_mid = imagecreatetruecolor($mid_w, $mid_h);
                imagecopyresampled($image_mid, $image, 0, 0, 0, 0, $mid_w, $mid_h, $width_old, $height_old);


                $image_resized = imagecreatetruecolor($width, $height);
                $crop_w = abs($mid_w - $width) / 2;
                $crop_h = abs($mid_h - $height) / 2;

                imagecopy($image_resized, $image_mid, 0, 0, $crop_w, $crop_h, $width, $height);
            } else {
                $image_resized = $image;
            }
        } else {

            imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
        }

        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = ($file_name ? $file_name : $file);
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        switch ($info[2]) {
            case IMAGETYPE_GIF:
                imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($image_resized, $output, 100);
                break;
            case IMAGETYPE_PNG:
                imagepng($image_resized, $output);
                break;
            default:
                return false;
                break;
        }

        return true;
    }

}
