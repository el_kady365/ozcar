<?php

App::uses('AppModel', 'Model');

/**
 * Car Model
 *
 * @property Make $Make
 * @property CarModel $CarModel
 * @property Dealer $Dealer
 * @property DealerLocation $DealerLocation
 */
class Car extends AppModel {

    var $filters = array("make", "model", "dealer_id" => array("title" => "Dealer"), "stock_number" => array("type" => "like", "title" => "Stock Number"));
    public $actsAs = array('Containable');
    public static $types = array(
        1 => "New",
        2 => "Used",
        3 => "Demo",
    );
    public static $carRatings = array(
        '>= 3.5' => '3.5 stars or more',
        '>= 4' => '4 stars or more',
        '>= 4.5' => '4.5 stars or more',
        '= 5' => '5 stars only',
        '<= 3.5' => 'less than 3.5 stars',
    );
    public static $Distances = array(
        10 => "10 Kilometres",
        20 => "20 Kilometres",
        50 => "50 Kilometres",
        100 => "100 Kilometres",
    );
    public static $FuelTypes = array(
        "Diesel" => "Diesel",
        "Electric" => "Electric",
        "LPG only" => "LPG only",
        "Petrol" => "Petrol",
        "Petrol - Premium ULP" => "Petrol - Premium ULP",
        "Petrol - Unleaded ULP" => "Petrol - Unleaded ULP",
        "Petrol or LPG (Dual)" => "Petrol or LPG (Dual)"
    );
    public static $fields = array(
//        'id' => 'Id',
        'make' => 'Make',
        'model' => 'Model',
//        'type_id' => 'Type Id',
        'year' => 'Year',
        'badge' => 'Badge',
        'series' => 'Series',
        'nvic' => 'NVIC',
        'body' => 'Body',
        'engine_capacity' => 'Engine Capacity',
        'cylinders' => 'Cylinders',
        'fuel_type' => 'Fuel Type',
        'doors' => 'Doors',
        'stock_number' => 'Stock Number',
        'rego' => 'Rego',
        'vin_number' => 'Vin Number',
//        'dealer_id' => 'Dealer Id',
        'odometer' => 'Odometer',
        'colour' => 'Colour',
        'trim' => 'Trim',
        'feature_code' => 'Feature Code',
        'selling_price' => 'Selling Price',
        'comments' => 'Comments',
        'dealer_location_code' => 'Dealer Location code',
//        'dealer_location_id' => 'Dealer Location Id',
        'compliance_date' => 'Compliance Date',
        'redbook_code' => 'Redbook Code',
        'created' => 'Created',
        'modified' => 'Modified',
        'transmission' => 'Transmission',
        'size' => 'Size',
        'month' => 'Month',
        'newPr' => 'NewPr',
        'width' => 'Width',
        'height' => 'Height',
        'length' => 'Length',
        'seats' => 'Seats',
        'turncir' => 'Turncir',
        'kerb_weight' => 'Kerb Weight',
        'steering' => 'Steering',
        'front_tyres' => 'Front Tyres',
        'rear_tyres' => 'Rear Tyres',
        'wheelbase' => 'Wheelbase',
        'front_brake' => 'Front Brake',
        'rear_brake' => 'Rear Brake',
        'towing_capacity_braked' => 'Towing Capacity Braked',
        'towing_capacity_unbraked' => 'Towing Capacity Unbraked',
        'drive_type' => 'Drive Type',
        'power' => 'Power',
        'torque' => 'Torque',
        'boreStroke' => 'BoreStroke',
        'compression_ratio' => 'Compression Ratio',
        'service_months' => 'Service Months',
        'service_kms' => 'Service Kms',
        'warranty' => 'Warranty',
        'warranty_kms' => 'Warranty Kms',
        'trim_colour' => 'Trim Colour',
        'registration_expiry' => 'Registration Expiry',
        'build_date' => 'Build Date',
        'price_excl_govt' => 'Price Excl Govt',
        'ancap_rating' => 'Ancap Rating',
        'green_star_rating' => 'Green Star Rating',
        'roadworthy_cert' => 'Roadworthy Cert'
    );
    public $validate = array(
        'dealer_id' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
        'dealer_location_id' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
        'type_id' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
        'make' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
        'model' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
        'stock_number' => array(
            "NotEmpty" => array(
                'rule' => 'notempty', "required" => true, 'message' => 'Required'
            ), "Unique" => array(
                'rule' => array('checkUnique', array('dealer_id', 'stock_number'), false),
                'message' => 'This number is already existed for another car'
            )
        ),
        'year' => array('rule' => array('maxLength', 4), 'message' => 'Enter valid four digits of the year'),
    );
    public $hasMany = array(
        'CarImage' => array(
            'className' => 'CarImage',
            'foreignKey' => 'car_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );
    public $belongsTo = array(
        'Make' => array(
            'className' => 'Make',
            'foreignKey' => 'make',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CarModel' => array(
            'className' => 'CarModel',
            'foreignKey' => 'model',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Dealer' => array(
            'className' => 'Dealer',
            'foreignKey' => 'dealer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DealerLocation' => array(
            'className' => 'DealerLocation',
            'foreignKey' => 'dealer_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public static $BodyTypes = array(
        'SMALL' => array('image' => 'small', 'title' => 'Small cars', 'keywords' => array(
                '3D HATCHBACK',
                '5D HATCHBACK',
                '2D HATCHBACK',
                '2D LIFTBACK',
                '5D AEROBACK'
            )
        ),
        'SEDAN' => array(
            'image' => 'sedan', 'title' => 'Sedans',
            'keywords' => array(
                '4D SEDAN',
                '4D SALOON',
                '4D HARDTOP',
                '4D SOFTTOP',
                '5D SEDAN'
            )
        ),
        'SUV' => array('image' => 'suv', 'title' => 'SUV\'s', 'keywords' => array(
                '5D LIFTBACK'
            )),
        'HATCHBACK' => array('image' => 'hatch', 'title' => 'Hatchbacks', 'keywords' => array('5D VAN',
                'VAN'
            )),
        'UTILITY' => array('image' => 'utilites', 'title' => 'Utilites', 'keywords' => array(
                'C/CHAS',
                'CREW CAB P/UP',
                'P/UP',
                'CREW C/CHAS',
                'SPACE C/CHAS',
                'SUPER C/CHAS',
                'SUPER CAB P/UP',
                'UTILITY',
                'DUAL CAB P/UP',
                'DUAL C/CHAS',
                'CARGO TRAY',
                'DUMP TRUCK',
                'SPACE CAB P/UP',
                'CREW CAB UTILITY',
                'DUAL CAB UTILITY',
                'TRAY',
                'CAB PLUS P/UP',
                'FREESTYLE C/CHAS',
                'FREESTYLE P/UP',
                'DOUBLE CAB UTILITY',
                'CLUB CAB UTILITY',
                'CLUB C/CHAS',
                'COIL C/CHAS',
                'LEAF C/CHAS',
                'KING C/CHAS',
                'KING CAB P/UP',
                'X CAB P/UP',
                'X CAB C/CHAS'
            )),
        'VAN' => array('image' => 'large', 'title' => 'Large cars', 'keywords' => array(
                '2D PANELVAN',
                'BUS',
                '4D VAN',
                '2D VAN',
                '2D WAGON',
                'WALK-THRU VAN',
                'WINDOW VAN',
                '3D VAN',
                '4D LONG VAN',
                '4D BLIND VAN',
                'TROOPCARRIER',
            )),
        'WAGON' => array('image' => 'wagons', 'title' => 'Wagons', 'keywords' => array(
                '4D WAGON',
                '4D SPORTWAGON',
                '5D WAGON'
            )),
        'COUPE' => array('image' => 'coupes', 'title' => 'Coupes', 'keywords' => array(
                '2D COUPE',
                '2D CONVERTIBLE',
                '2D CABRIOLET',
                '2D ROADSTER',
                '4D COUPE',
                '2D HARDTOP',
                '3D COUPE',
                '2D SOFTTOP',
                '2D SOFTBACK',
                '2D HARDBACK',
            ))
    );
    public static $Transmissions = array(
        'Auto' => 'Automatic',
        'Manual' => 'Manual',
        'Other' => 'Other',
    );

    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }

    public static function getBodyTypesDropDown() {
        $bodytpes = array();
        foreach (self::$BodyTypes as $key => $body) {
            $bodytpes[$key] = $body['title'];
        }
        return $bodytpes;
    }

    public static function getExistedMakes($dealer_id = false) {
        $mdl = ClassRegistry::init('Car');
        $conditions = array();
        if (!empty($dealer_id)) {
            $conditions = array("Car.dealer_id" => $dealer_id);
        }
        $result = $mdl->find("all", array("fields" => array("Make.id", "Make.name"), "conditions" => $conditions, "group" => "Car.make_id"));
        return Hash::combine($result, '{n}.Make.id', '{n}.Make.name');
    }

    public static function getExistedModels($dealer_id = false, $make_id = false) {
        $mdl = ClassRegistry::init('Car');
        $conditions = array();
        if (!empty($dealer_id)) {
            $conditions = array("Car.dealer_id" => $dealer_id);
        }
        if (!empty($make_id)) {
            $conditions = array("Car.make_id" => $make_id);
        }
        $result = $mdl->find("all", array("fields" => array("CarModel.id", "CarModel.name"), "conditions" => $conditions, "group" => "Car.model"));
        return Hash::combine($result, '{n}.CarModel.id', '{n}.CarModel.name');
    }

    function save_related_images($data) {
        $id = $this->id;
        if (!empty($data['CarImage'])) {
            debug($data['CarImage']);
            $layout_texts = $this->CarImage->find('list', array('conditions' => array('CarImage.car_id' => $id), 'fields' => 'CarImage.id'));
            $texts_posted_ids = array();
            foreach ($data['CarImage'] as $i => $item) {
                $texts_posted_ids[] = $item['id'];
            }
            $delete_text_array = array_diff($layout_texts, $texts_posted_ids);
            $this->CarImage->deleteAll(array('CarImage.id' => $delete_text_array));


            foreach ($data['CarImage'] as $i => $item) {
                if (!empty($data['CarImage'][$i]['image']) && $data['CarImage'][$i]['image']['size'] > 0 || $data['CarImage'][$i]['display_order'] != '') {
                    $data['CarImage'][$i]['car_id'] = $id;
                    if (empty($data['CarImage'][$i]['display_order'])) {
                        $data['CarImage'][$i]['display_order'] = $i;
                    }



                    $data2['CarImage'] = $data['CarImage'][$i];
                    $this->CarImage->create();
                    if (!$this->CarImage->save($data2['CarImage'], false))
                        $Errors.="Can't Upload for " . $data['CarImage']['name'] . (is_array($data['CarImage']['image']) ? $data['CarImage']['image']['name'] : $data['CarImage']['image']['name']) . ":<br/>" . implode("<br/>", $this->CarImage->validationErrors) . "<br/><br/>";
                    $data['CarImage'][$i]['display_order'] = $i;
                }



                $data2['CarImage'] = $data['CarImage'][$i];
                $this->CarImage->create();
                if (!$this->CarImage->save($data2['CarImage'], false))
                    $Errors.=implode("<br/>", $this->CarImage->validationErrors) . "<br/><br/>";
                else
                    unset($data['CarImage'][$i]);
            }
        }
        return true;
    }

    function getAllFields2() {
        $fields = Cache::read('carFields');
        if (!$fields) {
            $fields = $this->query('SELECT make, model, apl, body FROM directory_cars ORDER BY make, model, apl, body');
            Cache::write('carFields', $fields);
        }
        $makes = $models = $serieses = $bodies = $transmissions = $colours = array();

        foreach ($fields as $key => $columns) {
            foreach ($columns as $column) {
                $makes[$column['make']] = $column['make'];

                $models[$column['make']][$column['model']] = $column['model'];

                if (!empty($column['apl'])) {
                    $serieses[$column['model']][$column['apl']] = $column['apl'];
                }

                if (!empty($column['body'])) {
                    $bodies[$column['body']] = $column['body'];
                    $jsBodies[$column['make']][$column['model']][$column['apl']][$column['body']] = $column['body'];
                }
            }
        }

        $field_colours = $this->query('select distinct colour from cars');
//        debug($field_colours);
        if (!empty($field_colours)) {
            foreach ($field_colours as $color) {
                $colours[$color['cars']['colour']] = $color['cars']['colour'];
            }
        }

        return compact('makes', 'models', 'serieses', 'bodies', 'jsBodies', 'colours');
    }

    function getLocationFields() {
        $fields = $this->query('SELECT state,suburb FROM dealer_locations ');
        $states = $suburbs = array();

        foreach ($fields as $key => $columns) {
            foreach ($columns as $column) {
                if (!empty($column['state'])) {
                    $states[$column['state']] = $column['state'];
                }

                if (!empty($column['suburb'])) {
                    $suburbs[$column['state']][$column['suburb']] = $column['suburb'];
                }
            }
        }
        return compact('states', 'suburbs');
    }

    public static function getYears() {
        $years = array();
        for ($i = date('Y') - 30; $i <= date('Y'); $i++) {
            $years[$i] = $i;
        }
        return $years;
    }

    public function search_params($data = array(), $limit = 20, $any_search = false) {
        $conditions = array();
        $options = array();
///Equals 
//        if(isset())
//        unset($data['debug']);
//        debug($data);
        if ($any_search == false) {
            if (!empty($data['type_id']) && $data['type_id'] != 'all') {
                $conditions['Car.type_id'] = $data['type_id'];
            }

            if (!empty($data['make'])) {
                $conditions['Car.make'] = $data['make'];
            }
            if (!empty($data['model'])) {
                $conditions['Car.model'] = $data['model'];
            }
            if (!empty($data['dealer_id'])) {
                $conditions['Car.dealer_id'] = $data['dealer_id'];
            }
            if (!empty($data['dealer_location'])) {
                $conditions['Car.dealer_location_id'] = $data['dealer_location'];
            }

            if (!empty($data['series'])) {
                $conditions['Car.series'] = $data['series'];
            }
            if (!empty($data['body'])) {
                $conditions['Or'] = array();
                foreach ($data['body'] as $body) {
                    $keywords = self::$BodyTypes[$body]['keywords'];
                    foreach ($keywords as $keyword) {
                        $conditions['Or'][] = array("REPLACE (Car.body, '-', ' ') Like " => '%' . $keyword . '%');
                    }
                }
            }




            if (!empty($data['keywords'])) {

                $data['keywords'] = str_replace(array("'", '"', '+', '\\', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_'), ' ', $data['keywords']);
                $data['keywords'] = preg_replace('/\s+/', ' +', $data['keywords']);
                $data['keywords'] = '+' . $data['keywords'];
                $conditions[] = 'MATCH (`Car`.`make`,
			`Car`.`model`,
			`Car`.`nvic`,
			`Car`.`badge`,
			`Car`.`series`,
			`Car`.`body`,
			`Car`.`rego`, 
			`Car`.`stock_number`,
			`Car`.`vin_number`,
			`Car`.`colour`,
			`Car`.`comments`,
			`Car`.`transmission`,
			`Car`.`power`,
			`Car`.`redbook_code`,
			`Car`.`year`) AGAINST (\'' . $data['keywords'] . '\' IN BOOLEAN MODE)';
            }

            if (!empty($data['cylinders'])) {
                $conditions['Car.cylinders'] = $data['cylinders'];
            }

            if (!empty($data['seats'])) {
                $conditions['Car.seats'] = $data['seats'];
            }

            if (!empty($data['doors'])) {
                $conditions['Car.doors'] = $data['doors'];
            }
            if (!empty($data['colour'])) {
                $conditions['Car.colour'] = $data['colour'];
            }

            if (!empty($data['transmission']) && $data['transmission'] != 'Other') {
                $conditions['Car.transmission like '] = "%{$data['transmission']}%";
            } else if (!empty($data['transmission']) && $data['transmission'] == 'Other') {
                $transmissions = self::$Transmissions;
                array_pop($transmissions);
                foreach ($transmissions as $trans) {
                    $conditions['Or'][] = array('Car.transmission Not Like' => '%' . $trans . '%');
                }
            }
            if (!empty($data['fuel_type'])) {
                $conditions['Car.fuel_type like '] = "%{$data['fuel_type']}%";
            }

//Greater than and lower than

            if (!empty($data['price_from'])) {
                $conditions['Car.selling_price >= '] = $data['price_from'];
            }
            if (!empty($data['price_to'])) {
                $conditions['Car.selling_price <= '] = $data['price_to'];
            }

            if (!empty($data['year_from'])) {
                $conditions['Car.year >= '] = $data['year_from'];
            }
            if (!empty($data['year_to'])) {
                $conditions['Car.year <= '] = $data['year_to'];
            }

            if (!empty($data['kms_from'])) {
                $conditions['Car.odometer >= '] = $data['kms_from'];
            }

            if (!empty($data['kms_to'])) {
                $conditions['Car.odometer <= '] = $data['kms_to'];
            }

            if (!empty($data['power_from'])) {
                $conditions['Car.power >= '] = $data['power_from'];
            }

            if (!empty($data['power_to'])) {
                $conditions['Car.power <= '] = $data['power_to'];
            }

            if (!empty($data['engine_from'])) {
                $conditions['substr(Car.engine_capacity,1,3) >= '] = $data['engine_from'];
            }

            if (!empty($data['engine_to'])) {
                $conditions['substr(Car.engine_capacity,1,3) <= '] = $data['engine_to'];
            }



            if (!empty($data['fuel_type'])) {
                $conditions['Car.fuel_type'] = $data['fuel_type'];
            }

            if (!empty($data['cylinders'])) {
                $conditions['Car.cylinders'] = $data['cylinders'];
            }
            if (!empty($data['green_star_rating'])) {
                $conditions[] = 'Car.green_star_rating ' . $data['green_star_rating'];
            }
            if (!empty($data['ancap_rating'])) {
                $conditions[] = 'Car.ancap_rating ' . $data['ancap_rating'];
            }

            if (!empty($data['added_after'])) {
                $conditions['Car.created > '] = date('Y-m-d H:i:s', strtotime($data['added_after']));
            }
        } else {
            $search_any = explode(" ", $data['search_any']);
            foreach ($search_any as $key => $search) {
                $conditions[$key]['Or']['Car.make'] = $search;
                $conditions[$key]['Or']['Car.model'] = $search;
                $conditions[$key]['Or']['Car.colour'] = $search;
                $conditions[$key]['Or']['Car.series'] = $search;
            }
        }

//OR
//        if (!empty($data['location'])) {
//            $conditions['Or'][] = "DealerLocation.suburb Like '%{$data['location']}%";
//            $conditions['Or'][] = "DealerLocation.city Like '%{$data['location']}%'";
//            $conditions['Or'][] = "DealerLocation.state Like '%{$data['location']}%'";
//        }
//paginate

        $options['fields'] = array(
            'Car.id', 'Car.make', 'Car.model', 'Car.series', 'Car.year', 'Car.body', 'Car.odometer', 'Car.engine_capacity', 'Car.dealer_location_id', 'Car.selling_price','Car.registration','Car.fuel_economy',
            'Car.transmission', 'Car.comments', 'Car.dealer_id', 'Car.ancap_rating', 'Car.green_star_rating', 'DealerLocation.city', 'DealerLocation.state', 'DealerLocation.city', 'DealerLocation.longitude',
            'DealerLocation.latitude', 'DealerLocation.zoom', 'DealerLocation.name', 'Dealer.logo', 'DealerLocation.id',
            '(select count(id) from cars where make =Car.make AND model=Car.model AND year = Car.year AND series = Car.series AND dealer_location_id=Car.dealer_location_id group by Car.dealer_location_id) as CarsCount'
        );

        if (!empty($data['location'])) {
            $dist = 100;
            if (!empty($data['distance'])) {
                $dist = $data['distance'];
            }
            $location = urlencode($data['location']);
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$location}&sensor=false";

            $output = $this->Request_GoogalMap_URL($url);
//            debug($output);exit;
            $status = $output->status;

            if ($status == "OK") {
                $formated_address = $output->results[0]->formatted_address;
                $lat = $output->results[0]->geometry->location->lat;
                $lng = $output->results[0]->geometry->location->lng;
//                $options['fields'] = "  AS dist";
                $conditions["DISTANCE( DealerLocation.latitude ,DealerLocation.longitude  , $lat, $lng ) <="] = $dist;
            }
        }
//        debug($options['fields']);
//        $options['group'] = array('Car.year', 'Car.make', 'Car.model', 'Car.series', 'Car.dealer_location_id');
        if ($limit == 'all') {
            $limit = $this->find('count', array('conditions' => $conditions));
            $options['maxLimit'] = $limit;
        }
        $options['limit'] = $limit;

        $options['conditions'] = $conditions;
        $options['contain'] = array('CarImage', 'Dealer', 'DealerLocation');


        return $options;
    }

    function Request_GoogalMap_URL($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $geocode = curl_exec($ch);
        curl_close($ch);
        return json_decode($geocode);
    }

    public static function getDealerLogo($dealer_id, $width = false, $height = false) {
        $Dealer = ClassRegistry::init('Dealer');
        $Dealer->recursive = -1;
        $logo = $Dealer->read(null, $dealer_id);
        if ($width) {
            return $logo['Dealer']['logo_thumb1_full_path'];
        }
        return $logo['Dealer']['logo_full_path'];
    }

}
