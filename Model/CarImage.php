<?php

App::uses('AppModel', 'Model');

/**
 * CarImage Model
 *
 * @property Car $Car
 */
class CarImage extends AppModel {

    /**
     * Behaviors
     *
     * @var array
     */
    public $actsAs = array(
        'ImageFile' => array(
            'image' => array("width" => 0, "height" => 0)
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
//	public $belongsTo = array(
//		'Car' => array(
//			'className' => 'Car',
//			'foreignKey' => 'car_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		)
//	);
    public function afterFind($results, $primary = false) {
        $results = parent::afterFind($results, $primary);

        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
        }

        return $results;
    }

}
