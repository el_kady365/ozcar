<?php

App::uses('AppModel', 'Model');

/**
 * Contact Model
 *
 */
class Contact extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
    public static $states = array(
        'ACT' => 'Australian Capital Territory',
        'NSW' => 'New South Wales',
        'NT' => 'Northern Territory',
        'Queensland' => 'Queensland',
        'SA' => 'South Australia',
        'Tasmania' => 'Tasmania',
        'Victoria' => 'Victoria',
        'WA' => 'Western Australia'
    );
    
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a your name ',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Invalid email!',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'message' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Message is required',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
	);

	function checkCaptcha($data) {
		
		return strtolower($_SESSION['security_code']) == strtolower($data['security_code']);
	}

}
