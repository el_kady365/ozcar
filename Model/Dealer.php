<?php

App::uses('AppModel', 'Model');

/**
 * Dealer Model
 *
 * @property MainLocation $MainLocation
 * @property Car $Car
 * @property DealerLocation $DealerLocation
 */
class Dealer extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    var $filters = array("name" => array("like"));
    public $actsAs = array(
        'Containable',
        'ImageFile' => array(
            'logo' => array(
                'create_thumbs' => true,
                'thumbs' => array(
                    array('prefix' => 'thumb1_', 'width' => '120', 'height' => 0, 'default' => false),
                )
                , "width" => 0, "height" => 0)
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $validate = array(
        'email' => array('rule' => 'notempty', 'message' => 'Required'),
        'password' => array('rule' => 'notempty', "required" => true, 'message' => 'Required', "on" => "create"),
    );
    public $belongsTo = array(
        'MainLocation' => array(
            'className' => 'DealerLocation',
            'foreignKey' => 'main_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Car' => array(
            'className' => 'Car',
            'foreignKey' => 'dealer_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'DealerLocation' => array(
            'className' => 'DealerLocation',
            'foreignKey' => 'dealer_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function beforeSave($options = array()) {
        $ret = parent::beforeSave($options);
        if (!empty($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password']);
        } else {
            unset($this->data[$this->alias]['password']);
        }

        return ($ret && true);
    }

    public function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
        }

        foreach ($results as &$result) {
            if (!empty($result[$this->alias]['password'])) {
                $result[$this->alias]['password'] = "";
            }
        }


        return $results;
    }

    public function save_related_locations($data) {
        $id = $this->id;
//        debug($id);exit;
        if (!empty($data['DealerLocation'])) {

            $layout_texts = $this->DealerLocation->find('list', array('conditions' => array('DealerLocation.dealer_id' => $id), 'fields' => 'DealerLocation.id'));
            $texts_posted_ids = array();
            foreach ($data['DealerLocation'] as $i => $item) {
                $texts_posted_ids[] = $item['id'];
            }
            $delete_text_array = array_diff($layout_texts, $texts_posted_ids);
            $this->DealerLocation->deleteAll(array('DealerLocation.id' => $delete_text_array));

            $Errors = '';
            foreach ($data['DealerLocation'] as $i => $item) {

                $data['DealerLocation'][$i]['dealer_id'] = $id;
                if (empty($data['DealerLocation'][$i]['display_order'])) {
                    $data['DealerLocation'][$i]['display_order'] = $i;
                }
                $data2['DealerLocation'] = $data['DealerLocation'][$i];
                $this->DealerLocation->create();
                if (!$this->DealerLocation->save($data2['DealerLocation'], false))
                    $Errors.="Can't Upload for " . $data['DealerLocation']['name'] . (is_array($data['DealerLocation']['image']) ? $data['DealerLocation']['image']['name'] : $data['DealerLocation']['image']['name']) . ":<br/>" . implode("<br/>", $this->DealerLocation->validationErrors) . "<br/><br/>";
                $data['DealerLocation'][$i]['display_order'] = $i;




                $data2['DealerLocation'] = $data['DealerLocation'][$i];
//                debug($data['DealerLocation'][$i]);exit;
                $this->DealerLocation->create();
                if (!$this->DealerLocation->save($data2['DealerLocation'], false))
                    $Errors.=implode("<br/>", $this->DealerLocation->validationErrors) . "<br/><br/>";
                else
                    unset($data['DealerLocation'][$i]);
            }
        }
        return true;
    }

    public function getList($by_code = false) {
        $list = $this->find('list');

        return $list;
    }

}
