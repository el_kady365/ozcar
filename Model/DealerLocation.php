<?php

App::uses('AppModel', 'Model');

/**
 * DealerLocation Model
 *
 * @property Dealer $Dealer
 * @property Car $Car
 */
class DealerLocation extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    var $filters = array("name" => "like", "city" => "like", "state", "dealer_id" => array("title" => "Dealer"));
    public $actsAs = array(
        'Containable',
        'ImageFile' => array(
            'logo' => array("width" => 0, "height" => 0)
        )
    );
    public $belongsTo = array(
        'Dealer' => array(
            'className' => 'Dealer',
            'foreignKey' => 'dealer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasMany = array(
        'Car' => array(
            'className' => 'Car',
            'foreignKey' => 'dealer_location_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public $validate = array(
        'code' => array(
            "NotEmpty" => array(
                'rule' => 'notempty', 'message' => 'Required'
            ),
            'Unique' => array(
                'rule' => array('checkUnique', array('dealer_id', 'code'), false),
                'message' => 'This code is already existed for another location'
            ),
    ));

    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data[$this->name]['coordinates'])) {
            $coords = explode(',', $this->data["DealerLocation"]["coordinates"]);
            $this->data[$this->name]['latitude'] = $coords[0];
            $this->data[$this->name]['longitude'] = $coords[1];
            $this->data[$this->name]['zoom'] = $coords[2];
        }
        return true;
    }

    function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
        }
        return $results;
    }

}
