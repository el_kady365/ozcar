<?php
App::uses('AppModel', 'Model');
/**
 * DirectoryCar Model
 *
 */
class DirectoryCar extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
        var $filters = array("make" => "like", "model" => "like", "body"=>'like', 'apl'=>'like');
	public $validate = array(
		'make' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'model' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
        
}
