<?php

App::uses('AppModel', 'Model');

/**
 * Enquiry Model
 *
 * @property Dealership $Dealership
 * @property DealerLocation $DealerLocation
 * @property Car $Car
 * @property Type $Type
 */
class Enquiry extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    public static $types = array(
        1 => "General Enquiry",
        2 => "Service Enquiry",
        3 => "Car Enquiry",
        4 => "Offer Enquiry",
    );
    public static $fields = array(
        4 => array(
            'title' => 'Offer',
            'input-options' => array('type' => 'text', 'label' => 'YOUR OFFER', 'div' => false)
        )
    );
    var $filters = array(
        "dealer_id" => array("title" => "Dealer"), "dealer_location_id" => array("title" => "Dealer Location"), "type_id", "state" => array("title" => "State", "state" => array("type" => "select"))
    );
    public $validate = array(
        'boot' => array('rule' => 'notEmpty', 'message' => 'Required', 'required' => true),
        'description' => array(
            array('rule' => 'notEmpty', 'message' => 'Required'),
        ),
        'field1' => array(
            array('rule' => 'notEmpty', 'message' => 'Required'),
        ),
        'first_name' => array('rule' => 'notEmpty', 'message' => 'Required'),
        'last_name' => array('rule' => 'notEmpty', 'message' => 'Required'),
        'telephone' => array(
            array('rule' => 'notEmpty', 'message' => 'Required'),
            array('rule' => 'numeric', 'message' => 'Please enter valid phone number')
        ),
        'email' => array(
            array('rule' => 'notEmpty', 'message' => 'Required'),
            array('rule' => 'email', 'message' => 'Please enter valid email')
        ),
        'post_code' => array('rule' => 'notEmpty', 'message' => 'Required'),
    );


//The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Dealer' => array(
            'className' => 'Dealer',
            'foreignKey' => 'dealer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DealerLocation' => array(
            'className' => 'DealerLocation',
            'foreignKey' => 'dealer_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Car' => array(
            'className' => 'Car',
            'foreignKey' => 'car_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
//		'Type' => array(
//			'className' => 'Type',
//			'foreignKey' => 'type_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		)
    );

}
