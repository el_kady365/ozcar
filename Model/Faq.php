<?php

App::uses('AppModel', 'Model');

/**
 * Faq Model
 *
 */
class Faq extends AppModel {

    /**
     * Behaviors
     *
     * @var array
     */
    public $actsAs = array(
        'File' => array(
            'file' => array('extensions' => array('mp4'))
        )
    );

}
