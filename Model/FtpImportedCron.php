<?php

App::uses('AppModel', 'Model');

/**
 * FtpImportedCron Model
 *
 * @property Dealer $Dealer
 * @property Template $Template
 */
class FtpImportedCron extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'ftp_user_name';
    public $validate = array(
        'template_id' => array(
            'rule' => 'notempty',
            'message' => 'Required',
        //'allowEmpty' => false,
        //'required' => false,
        //'last' => false, // Stop validation after this rule
        //'on' => 'create', // Limit validation to 'create' or 'update' operations
        ), 'ftp_username' => array(
            "Notempty" => array(
                'rule' => 'notempty',
                'message' => 'Required',
            ),
            "Unique" => array(
                'rule' => 'isUnique',
                'message' => 'This username is already existed',
            ),
            'Username' => array(
                'rule' => '/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/',
                'message' => 'Only letters, integers and underscore are allowed'
            )
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Dealer' => array(
            'className' => 'Dealer',
            'foreignKey' => 'dealer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Template' => array(
            'className' => 'Template',
            'foreignKey' => 'template_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
