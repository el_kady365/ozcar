<?php

class FullWidthBanner extends AppModel {

    var $name = 'FullWidthBanner';
    var $actsAs = array(
        'ImageFile' => array(
            'image' => array(
                'width' => 1170, 'height' => 350, 'folder' => 'files/images/'
            ),
        ),
    );
    
    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data[$this->name]['dealerships'])) {
            $this->data[$this->name]['dealerships'] = "," . implode(',', $this->data[$this->name]['dealerships']) . ",";
        }
        return true;
    }

    function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        foreach ($results as &$result) {
            if (!empty($result[$this->name]['dealerships'])) {
                $result[$this->name]['dealerships'] = explode(',', $result[$this->name]['dealerships']);
            }
        }
        return $results;
    }

}

?>