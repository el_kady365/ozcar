<?php

App::uses('AppModel', 'Model');

/**
 * Link Model
 *
 */
class Link extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'title';
	public $validate = array(
		'title' => array('rule' => 'notempty', 'message' => 'Required'),
		'url' => array(
			array('rule' => 'notempty', 'message' => 'Required'),
		),
		'color' => array('rule' => 'notempty', 'message' => 'Required'),
	);

}
