<?php

App::uses('AppModel', 'Model');

/**
 * MapField Model
 *
 * @property Template $Template
 */
class MapField extends AppModel {

    var $filters = array("template_id");

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'template_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'csv_field_order' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'Unique' => array(
                'rule' => array('checkUnique', array('template_id', 'csv_field_order'), false),
                'message' => 'This field order is already selected for another field'
            ),
        ),
       
    );

    public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }

    public $belongsTo = array(
        'Template' => array(
            'className' => 'Template',
            'foreignKey' => 'template_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
