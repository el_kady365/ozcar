<?php

App::uses('AppModel', 'Model');

/**
 * Message Model
 *
 * @property Sender $Sender
 * @property Receiver $Receiver
 * @property ParentMessage $ParentMessage
 * 
 */
class Message extends AppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
//        'Sender' => array(
//            'className' => 'DealerLocation',
//            'foreignKey' => 'sender_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        ),
//        'Receiver' => array(
//            'className' => 'User',
//            'foreignKey' => 'receiver_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        ),
        'ParentMessage' => array(
            'className' => 'Message',
            'foreignKey' => 'message_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        foreach ($results as &$result) {
            if (!empty($result[$this->name]['from_type']) && $result[$this->name]['from_type'] == 2) {
               
                $User = ClassRegistry::init('Users.User')->read(null, $result[$this->name]['receiver_id']);
               
                $DealerLocation = ClassRegistry::init('DealerLocation')->read(null, $result[$this->name]['sender_id']);
                $result['Sender'] = $DealerLocation['DealerLocation'];
                $result['Receiver'] = $User['User'];
                $result['Receiver']['first_name'] = $User['UserDetail']['first_name'];
                $result['Receiver']['last_name'] = $User['UserDetail']['last_name'];
            } elseif (!empty($result[$this->name]['from_type']) && $result[$this->name]['from_type'] == 1) {
                $User = ClassRegistry::init('Users.User')->read(null, $result[$this->name]['sender_id']);
                
                $DealerLocation = ClassRegistry::init('DealerLocation')->read(null, $result[$this->name]['receiver_id']);
                $result['Sender'] = $User['User'];
                $result['Sender']['first_name'] = $User['UserDetail']['first_name'];
                $result['Sender']['last_name'] = $User['UserDetail']['last_name'];
                $result['Receiver'] = $DealerLocation['DealerLocation'];
            }
        }

        return $results;
    }

}
