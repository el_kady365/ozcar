<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 * @property Site $Site
 */
class Page extends AppModel {

    public $menuFields = array('`Menu`.`id`', '`Menu`.`title`', '`Menu`.`permalink`', '`Menu`.`menu_id`', '`Menu`.`submenu_id`', '`Menu`.`add_to_main_menu`', '`Menu`.`url`', '`Menu`.`is_url`', '`Menu`.`display_order`', '`Menu`.`active`');
    public $filters = array('title' => 'like');
    public $validate = array('title' => array('rule' => 'notempty', 'message' => 'Required'));

    public function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);

        if ($this->alias == 'Menu') {
            foreach ($results as &$result) {
                
                 if (empty($result[$this->alias]['menu_id']) && !empty($result[$this->alias]['add_to_main_menu'])) {
                    $result[$this->alias]['Submenu'] = array();
                    $conditions = array();
                    $conditions[$this->alias . '.active'] = 1;
                    $conditions[$this->alias . '.menu_id'] = $result[$this->alias]['id'];
                    $conditions[] = "({$this->alias}.submenu_id = 0 OR {$this->alias}.submenu_id IS NULL)";
                    $submenus = $this->find('all', array('fields' => $this->menuFields, 'conditions' => $conditions, 'order' => $this->alias . '.display_order', 'recursive' => -1));
//                    if ($result[$this->alias]['url'] == '#' && !empty($submenus[0]))
//                        $result[$this->alias]['url'] = $submenus[0][$this->alias]['url'];
                    $result[$this->alias]['Submenu'] = $submenus;
                }

                if (!empty($result[$this->alias]['menu_id']) && empty($result[$this->alias]['submenu_id']) && !empty($result[$this->alias]['add_to_main_menu'])) {
                    $result[$this->alias]['SubSubmenu'] = array();
                    $conditions = array();
                    $conditions[$this->alias . '.active'] = 1;
                    $conditions[$this->alias . '.submenu_id'] = $result[$this->alias]['id'];
                    $submenus = $this->find('all', array('fields' => $this->menuFields, 'conditions' => $conditions, 'order' => $this->alias . '.display_order', 'recursive' => -1));
                    $result[$this->alias]['SubSubmenu'] = $submenus;
                }
                
                
                if (empty($result[$this->alias]['active']))
                    continue;
            }
        }
        return $results;
    }

    public function afterSave($created) {
        parent::afterSave($created);
        if (!$created && !empty($this->data[$this->alias]['permalink'])) {
            Cache::write('Page.' . $this->data[$this->alias]['permalink'], false);
        }
    }

   

}
