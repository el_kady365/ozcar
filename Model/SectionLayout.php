<?php

App::uses('AppModel', 'Model');

/**
 * SectionLayout Model
 *
 */
class SectionLayout extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';
	var $types = array(1 => 'Home', 2 => 'Inner');

	/**
	 * Behaviors
	 *
	 * @var array
	 */
	public $actsAs = array(
		'ImageFile' => array(
			'image' => array(
				'resize' => true,
				'width' => '400',
				'height' => 0,
				'folder' => 'img/uploads/',
				'create_thumbs' => true,
				'thumbs' => array(
					array('prefix' => 'thumb1_', 'width' => 0, 'height' => '50', 'default' => false),
					
				)
			)
		)
	);

	function get_templates_list() {
		$ret_templates = array();
		$templates = $this->find('all', array('order' => 'id'));

		foreach ($templates as $i => $template) {
			$ret_templates[$template[$this->name]['id']]['key'] = $template[$this->name]['id'];
			$ret_templates[$template[$this->name]['id']]['full_image'] = $template[$this->name]['image_full_path'];
			$ret_templates[$template[$this->name]['id']]['thumb_image'] = $template[$this->name]['image_thumb1_full_path'];
			$ret_templates[$template[$this->name]['id']]['html'] = $template[$this->name]['html'];
		}
		return $ret_templates;
	}

}
