<?php

App::uses('AppModel', 'Model');

/**
 * SiteConfig Model
 *
 * @property Site $Site
 * @property Site $Site
 */
class SiteConfig extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'value';
	public $virtualFields = array(
		'var' => 'CONCAT(SiteConfig.group, ".", SiteConfig.setting)'
	);
	public $supportedFileBehaviors = array('File', 'ImageFile', 'PDFFile');
	public $clearCache = array('site_config');
	public static $config = null;

    /**
     * Validation rules
     *
     * @var array
     * 
     */
    //<editor-fold defaultstate="collapsed" desc="Basic Validation Rules">
    public $validate = array(
        'setting' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Setting must not be empty',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'active' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                'message' => 'Invalid Value ',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //</editor-fold>


    /**
     * Configuration Vars
     *  @var array
     * 
     */
    
    //<editor-fold defaultstate="expanded" desc="Configuration Vars">
    public $vars = array(
        //<editor-fold defaultstate="collapsed" desc="Basic">
        'basic' => array(
            'title' => 'Basic Configuration',
            'vars' => array(
                //<editor-fold defaultstate="collapsed" desc="Site Name">
                'site_name' => array(
                    'title' => 'Site Name',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'notEmpty',
                            'required' => true
                        ),
                    ),
                ),
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Admin Email">
                'admin_email' => array(
                    'title' => 'Admin Email',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'email',
                            'required' => true
                        ),
                    ),
                ),
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Send Mail From">
                'send_email_from' => array(
                    'title' => 'Send Email From',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'email',
                            'required' => true,
                            'message' => 'invalid email',
                        ),
                    ),
                ),
                //</editor-fold>
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="File">
//                'manual' => array(
//                    'title' => 'Manual',
//                    'actsAs' => array('File' => array('extensions' => array('pdf'))),
//                    'input' => array(
//                        'type' => 'file',
//                    ),
//                    'validation' => array(
//                    ),
//                ),
            //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Google Analytics">                
//                'google_analytics' => array(
//                    'title' => 'Google Analytics',
//                    'input' => array(
//                        'type' => 'textarea',
//                    ),
//                    'validation' => array(
//                    ),
//                ),
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Checked">                
//                'checked' => array(
//                    'title' => 'Checked?',
//                    'input' => array(
//                        'type' => 'checkbox',
//                    ),
//                    'validation' => array(
//                        'boolean' => array(
//                            'rule' => 'boolean',
//                            'message' => 'invalid value!'
//                        ),
//                    ),
//                ),
            //</editor-fold>
            
            ),
        ),
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Contact">
//        'contact' => array(
//            'title' => 'Contact Details',
//            'vars' => array(
//                'map_coordinates' => array(
//                    'title' => 'Map Coordinates',
//                    'input' => array(
//                        'type' => 'text',
//                        'class' => 'map-selector',
//                        'default' => '0,0,1',
//                    ),
//                    'validation' => array(
//                    ),
//                ),
////                'mobile' => array(
////                    'title' => 'Mobile',
////                    'input' => array(
////                        'type' => 'text',
////                    ),
////                    'validation' => array(
////                    ),
////                ),
////                'fax' => array(
////                    'title' => 'Fax',
////                    'input' => array(
////                        'type' => 'text',
////                    ),
////                    'nosave' => false,
////                    'validation' => array(
////                    ),
////                ),
////                'email' => array(
////                    'title' => 'Email',
////                    'input' => array(
////                        'type' => 'text',
////                    ),
////                    'nosave' => false,
////                    'validation' => array(
////                    ),
////                ),
//                'phone' => array(
//                    'title' => 'Phone',
//                    'input' => array(
//                        'type' => 'text',
//                    ),
//                    'nosave' => false,
//                    'validation' => array(
//                    ),
//                ),
////                'phone2' => array(
////                    'title' => 'Phone (alternate line)',
////                    'input' => array(
////                        'type' => 'text',
////                    ),
////                    'nosave' => false,
////                    'validation' => array(
////                    ),
////                ),
//            )
//        ),
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Admin Authentication">
        'auth' => array(
            'title' => 'Admin Authentication',
            'vars' => array(
                'user_name' => array(
                    'title' => 'User Name',
                    'input' => array(
                        'type' => 'text',
                        'autocomplete' => 'off',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'notEmpty',
                            'required' => true
                        ),
                    ),
                ),
                'password' => array(
                    'title' => 'Password',
                    'input' => array(
                        'type' => 'password',
                        'value' => '',
                        'title' => 'Leave blank to use current password',
                        'placeholder' => 'Type in to set or change password...',
                        'autocomplete' => 'off',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'notEmpty',
                            'required' => 'create',
                            'allowEmpty' => true,
                            'on' => 'update'
                        ),
                        'size' => array(
                            'rule' => array('between', 6, 32),
                            'message' => 'Password should be at least 6 chars long'
                        ),
                    ),
                ),
                'repassword' => array(
                    'title' => 'Retype Password',
                    'input' => array(
                        'type' => 'password',
                        'value' => '',
                    ),
                    'nosave' => true,
                    'compare' => 'auth.password',
                ),
            )
        ),
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Meta">
        'meta' => array(
            'title' => 'Meta Tags',
            'vars' => array(
                'keywords' => array(
                    'title' => 'Keywords',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                    ),
                ),
                'description' => array(
                    'title' => 'Description',
                    'input' => array(
                        'type' => 'textarea',
                    ),
                    'validation' => array(
                    ),
                ),
                'robots' => array(
                    'title' => 'Robots',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                    ),
                ),
            )
        ),
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Links">
        'links' => array(
            'title' => 'Links',
            'vars' => array(
                'facebook' => array(
                    'title' => 'Facebook',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'url' => array(
                            'rule' => 'notEmpty',
                            'allowEmpty' => true,
                            'message' => 'Required',
                        )
                    ),
                ),
                'twitter' => array(
                    'title' => 'Twitter',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'url' => array(
                            'rule' => 'notEmpty',
                            'allowEmpty' => true,
                            'message' => 'Required',
                        )
                    ),
                ),
                'youtube' => array(
                    'title' => 'Youtube',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'notEmpty' => array(
                            'rule' => 'notEmpty',
                            'allowEmpty' => true,
                            'message' => 'Required',
                        )
                    ),
                ),
//                'google' => array(
//                    'title' => 'Google Plus',
//                    'input' => array(
//                        'type' => 'text',
//                    ),
//                    'validation' => array(
//                        'url' => array(
//                            'rule' => 'url',
//                            'allowEmpty' => true,
//                            'message' => 'Invalid URL',
//                        )
//                    ),
//                ),
//                'mailto' => array(
//                    'title' => 'Email',
//                    'input' => array(
//                        'type' => 'text',
//                    ),
//                    'validation' => array(
//                        'email' => array(
//                            'rule' => 'email',
//                            'allowEmpty' => true,
//                            'message' => 'Invalid Email',
//                        )
//                    ),
//                ),
//                'google' => array(
//                    'title' => 'Google+',
//                    'input' => array(
//                        'type' => 'text',
//                    ),
//                ),
            ),
        ),
            //</editor-fold>
    );

    //</editor-fold>
    
	public function beforeValidate($options = array()) {

		parent::beforeValidate($options);
		$this->validator()->remove('value');


		foreach ($this->vars as $gid => $configGroup) {
			if (!empty($configGroup['vars'][$this->data[$this->alias]['setting']]['validation'])) {
				$this->validator()->add('value', $configGroup['vars'][$this->data[$this->alias]['setting']]['validation']);
			}
		}
	}

	public function beforeSave($options = array()) {
		$ret = parent::beforeSave($options);
		if (in_array(strtolower($this->data[$this->alias]['setting']), array('password', 'passwd'))) {
			if (!empty($this->data[$this->alias]['value']) || (isset($this->data[$this->alias]['value']) && is_numeric($this->data[$this->alias]['value']))) {
				$this->data[$this->alias]['value'] = Security::hash($this->data[$this->alias]['value']);
			} else {
				unset($this->data[$this->alias]['value']);
			}
		}

		return ($ret && true);
	}

	public function getStructure($var = null) {
		$ret = array();

		if (!empty($var) && strpos($var, '.') !== false) {
			$ex = explode('.', $var);
			$gid = $ex[0];
			$varid = $ex[1];
			return isset($this->vars[$gid]['vars'][$varid]) ? $this->vars[$gid]['vars'][$varid] : null;
		}
		foreach ($this->vars as $gid => $group) {
			foreach ($group['vars'] as $varid => $var_details) {
				$ret[$gid . '.' . $varid] = $var_details;
			}
		}
		return $ret;
	}

	public static function decode_var($var) {
		if (strpos($var, '.') === false)
			return false;
		$object = new stdClass();
		$ex = explode('.', $var);
		$object->group = $ex[0];
		$object->setting = $ex[1];
		return $object;
	}

	public static function getCached($var = null, $defaultValue = false ) {
		$cached_config = self::$config;
		if (empty($cached_config)) {
			$cached_config = Cache::read('site_config');
			if (empty($cached_config)) {
				$cached_config = SiteConfig::get();
				Cache::write('site_config', $cached_config);
			}
			self::$config = $cached_config;
		}
		if ($var && isset($cached_config[$var]))
			return $cached_config[$var];
		elseif (!$var)
			return $cached_config;
		elseif($defaultValue){
			return $defaultValue;
		} else {
			return false;
		}
	}

	public static function get($var = null) {
		$conditions = array();
		if (!empty($var)) {
			$dot_var = $var;
			$var = self::decode_var($var);
			$gid = $var->group;
			$var = $var->setting;


			if (!empty($gid)) {
				$conditions['SiteConfig.group'] = $gid;
			}
			$conditions['SiteConfig.setting'] = $var;
		}

		$model = new self;
		$list = $model->find('list', array('conditions' => $conditions, 'fields' => array('var', 'value')));
		$model = null;
		if (!empty($var) && count($list) == 1) {
			return $list[$dot_var];
		} else {
			return $list;
		}
	}

	public static function setVar($var, $value) {
		$model = new self;
		$var = self::decode_var($var);
		$current = $model->find('first', array('conditions' => array('group' => $var->group, 'setting' => $var->setting)));
		if (!empty($current['SiteConfig']['id'])) {
			$model->id = $current['SiteConfig']['id'];
		} else {
			$model->create();
		}

		$model->set(array('group' => $var->group, 'setting' => $var->setting, 'value' => $value));
		if ($model->save()) {
			if (self::clearCache())
				return true;
		}
		return false;
	}

	public static function deleteVar($var) {
		$model = new self;
		$var = self::decode_var($var);
		$current = $model->find('first', array('conditions' => array('group' => $var->group, 'setting' => $var->setting)));
		if (!empty($current['SiteConfig']['id'])) {
			$model->id = $current['SiteConfig']['id'];
			if ($model->delete()) {
				if (self::clearCache())
					return true;
			}
		}
		return false;
	}

	public static function clearCache() {
		if (Cache::write('site_config', null)) {
			self::$config = null;
			return true;
		}
		return false;
	}

	public function uploadedFileSettings($var) {
		$varOpts = $this->getStructure($var);
		//$varComponents = $this->decode_var($var);
		$ok = false;
		if (!empty($varOpts['actsAs'])) {
			foreach ($varOpts['actsAs'] as $bhv => $opts) {
				if (in_array($bhv, $this->supportedFileBehaviors)) {
					$ok = true;
					$this->Behaviors->attach($bhv, array('value' => $opts));
				}
			}
		} else {
			return false;
		}

		$ret = false;
		if ($ok) {
			$ret = $this->getFileSettings('value');
		}
		foreach ($this->Behaviors->attached() as $bhv) {
			$this->Behaviors->unload($bhv);
		}

		return $ret;
	}

	public  function uploadedFilePath($var, $cached = true) {
		$settings = $this->uploadedFileSettings($var);
		$value = $cached ? $this->getCached($var) : $this->get($var);
		if (empty($value)) {
			//$varstruct = $this->getStructure($var);
			if (!empty($settings['default'])) {
				$value = $settings['default'];
			} else {
				return false;
			}
		}

		return Router::url('/' . $settings['folder'] . $value);
	}
	
	public static function instance(){
		return new self;
	}

	function validateForgot($data) {
		if (empty($data[$this->alias]['email'])) {
			$this->validationErrors['email'] = __('Email is required', true);
			return false;
		}
		if (isset($data[$this->alias]['security_code']) && ($data[$this->alias]['security_code'] == '' || !$this->chkSecurityCode($data[$this->name]) )) {
			$this->validationErrors['security_code'] = __('Invalid security code', true);
			return false;
		}
		if ($data[$this->alias]['email'] != SiteConfig::getCached('basic.admin_email')) {
			$this->validationErrors['email'] = __('The entered email is incorrect', true);
			return false;
		}
		return true;
	}

	//-----------------------
	function check_code_confirmation($data) {
		if (empty($_SESSION['confirm_code'])) {

			if (empty($data['email']) || empty($data['code'])) {
				return false;
			}
			$str = SiteConfig::getCached('basic.site_name') + SiteConfig::getCached('basic.admin_email') + SiteConfig::getCached('auth.user_name') + SiteConfig::getCached('auth.password') + date('Ymd');
			$new_code = substr(base64_encode(md5($str)), 8, 8);

			if ($new_code == $data['code']) {
				$_SESSION['confirm_code'] = $new_code;
				return true;
			}
		} else {
			return true;
		}
		return false;
	}

	//-----------------------------------
	function saveConfirmPassword($data) {
		if (empty($data[$this->name]['password'])) {
			$this->validationErrors['password'] = __('Required', true);
			return false;
		}
		if ($data[$this->name]['password'] != $data[$this->name]['paswrd']) {
			$this->validationErrors['paswrd'] = __('Passwords do not match', true);
			return false;
		}
		$current_password = $this->find('first', array('conditions' => array('group' => 'auth', 'setting' => 'password')));

		$new_password = array();
		$new_password[$this->name]['id'] = $current_password[$this->name]['id'];
		$new_password[$this->name]['group'] = 'auth';
		$new_password[$this->name]['setting'] = 'password';
		$new_password[$this->name]['value'] = ($data[$this->name]['password']);
		if ($this->save($new_password)) {
			unset($_SESSION['confirm_code']);
			return true;
		}
		if (!empty($this->validationErrors['value'])) {
			$this->validationErrors['password'] = $this->validationErrors['value'];
		}
		return false;
	}

	function chkSecurityCode($param) {
		return !empty($_SESSION['security_code']) && !empty($param['security_code']) && strtolower($_SESSION['security_code']) == strtolower($param['security_code']);
	}

}
