<?php

App::uses('AppModel', 'Model');

/**
 * Snippet Model
 *
 */
class Snippet extends AppModel {

    public $snippets = array(
        'car-enquiry-top' => "Car Enquiry Snippet",
        'offer-enquiry-top' => "Offer Enquiry Snippet",
        'dealers-login-register-snippet' => "Dealers login register snippet"
    );

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        asort($this->snippets);
    }

    public function afterSave($created) {
        parent::afterSave($created);
        Cache::write('snippet.' . $this->data[$this->alias]['permalink'], false);
    }

    public static function getSnippet($key) {
        $snippet = Cache::read('snippet.' . $key);
        $mdl = new self();
        if (empty($snippet)) {

            $conditions['Snippet.permalink'] = $key;
            $snippet = $mdl->find('first', array('conditions' => $conditions));
            if ($snippet) {
                Cache::write('snippet.' . $key, ($snippet['Snippet']['content']));
                return ($snippet['Snippet']['content']);
            } else {
                return '';
            }
        } else
            return ($snippet);
    }

}
