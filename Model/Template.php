<?php

App::uses('AppModel', 'Model');

/**
 * Template Model
 *
 * @property MapField $MapField
 */
class Template extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    public $validate = array(
        'name' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
        'dealer_id' => array('rule' => 'notempty', "required" => true, 'message' => 'Required'),
    );
    var $filters = array("name" => "like" ,"dealer_id" => array("title" => "Dealer"));
    public $hasMany = array(
        'MapField' => array(
            'className' => 'MapField',
            'foreignKey' => 'template_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => 'MapField.csv_field_order ASC',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public $belongsTo = array(
        'Dealer' => array(
            'className' => 'Dealer',
            'foreignKey' => 'dealer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

}
