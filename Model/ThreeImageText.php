<?php

class ThreeImageText extends AppModel {

    var $name = 'ThreeImageText';
    var $actsAs = array(
        'ImageFile' => array(
            'image1' => array(
                'width' => 370, 'height' => 250, 'folder' => 'files/images/'
            ),
            'image2' => array(
                'width' => 370, 'height' => 250, 'folder' => 'files/images/'
            ),
            'image3' => array(
                'width' => 370, 'height' => 250, 'folder' => 'files/images/'
            ),
        ),
    );
    
    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data[$this->name]['dealerships'])) {
            $this->data[$this->name]['dealerships'] = implode(',', $this->data[$this->name]['dealerships']);
        }
        return true;
    }

    function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        foreach ($results as &$result) {
            if (!empty($result[$this->name]['dealerships'])) {
                $result[$this->name]['dealerships'] = explode(',', $result[$this->name]['dealerships']);
            }
        }
        return $results;
    }

}

?>