<?php

App::uses('AppModel', 'Model');

/**
 * TopBanner Model
 *
 */
class TopBanner extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

    /**
     * Behaviors
     *
     * @var array
     */
    public $actsAs = array(
        'ImageFile' => array(
            'image' => array(
                'width' => 350,
                'height' => 290,
                'resize' => true,
                'crop' => false,
                'create_thumbs' => true,
                'thumbs' => array(
                    array(
                        'prefix' => 'thumb_',
                        'width' => 256,
                        'height' => 56,
                    )
                )
            )
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'title' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter a title',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'new_window' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                'message' => 'Invalid value',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'active' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                'message' => 'Invalid value',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

}
