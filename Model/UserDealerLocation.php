<?php

App::uses('AppModel', 'Model');
App::uses('Car', 'Model');
App::uses('User', 'Users.Model');

/**
 * UserDealerLocation Model
 *
 * @property DealerLocation $DealerLocation
 * @property User $User
 */
class UserDealerLocation extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $actsAs = array('Containable');
    public $validate = array(
        'dealer_location_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'DealerLocation' => array(
            'className' => 'DealerLocation',
            'foreignKey' => 'dealer_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function getSavedDealerLocations($user) {
        $fields = array(
            'UserDealerLocation.id',
            'UserDealerLocation.created',
            'UserDealerLocation.dealer_location_id',
            'DealerLocation.dealer_id',
            'DealerLocation.id',
            '(select count(`Car`.`id`) from cars as Car where dealer_location_id = DealerLocation.id) as CarsCount',
            'DealerLocation.logo',
            'DealerLocation.longitude',
            'DealerLocation.name',
            'DealerLocation.email',
            'DealerLocation.phone1',
            'DealerLocation.phone2',
            'DealerLocation.address1',
            'DealerLocation.city',
            'DealerLocation.state',
            'DealerLocation.suburb',
            'DealerLocation.suburb',
            'DealerLocation.id',
            'DealerLocation.latitude',
            'DealerLocation.zoom',
        );

        if (!empty($user['UserDetail']['address']) && !empty($user['UserDetail']['suburb']) && !empty($user['UserDetail']['postcode'])) {
            $location = urlencode($user['UserDetail']['address'] . ', ' . $user['UserDetail']['suburb'] . ', ' . $user['UserDetail']['postcode'] . ', Australia');
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$location}&sensor=false";
            $Car = new Car();
            $output = $Car->Request_GoogalMap_URL($url);
//            debug($output);exit;
            $status = $output->status;

            if ($status == "OK") {
                $formated_address = $output->results[0]->formatted_address;
                $lat = $output->results[0]->geometry->location->lat;
                $lng = $output->results[0]->geometry->location->lng;
            }
            $fields[] = "DISTANCE( DealerLocation.latitude ,DealerLocation.longitude  , $lat, $lng ) AS dist";
        }

        $saved_dealerships = $this->find('all', array('conditions' => array('UserDealerLocation.user_id' => $user['id']), 'fields' => $fields, 'limit' => 6));

        return $saved_dealerships;
    }

}
