<?php

App::uses('AppModel', 'Model');

/**
 * UserSearch Model
 *
 * @property User $User
 */
class UserSearch extends AppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        $fields = array('make', 'model', 'series', 'body', 'transmission');
        $names = array();
        if (!empty($this->data['UserSearch']['search_parameters'])) {
            foreach ($this->data['UserSearch']['search_parameters'] as $key => $param) {
                if (in_array($key, $fields) && !empty($param)) {
                    $names[] = $param;
                }
            }
            
            $this->data['UserSearch']['title'] = implode(', ', $names);
            $this->data['UserSearch']['search_parameters'] = json_encode($this->data['UserSearch']['search_parameters']);
        }
        return true;
    }

}
