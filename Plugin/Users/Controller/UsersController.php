<?php

/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('CakeEmail', 'Network/Email');
App::uses('UsersAppController', 'Users.Controller');
App::uses('Security', 'Utility');
App::uses('Hash', 'Utility');

/**
 * Users Users Controller
 *
 * @package       Users
 * @subpackage    Users.Controller
 * @property	  AuthComponent $Auth
 * @property	  CookieComponent $Cookie
 * @property	  PaginatorComponent $Paginator
 * @property	  SecurityComponent $Security
 * @property	  SessionComponent $Session
 * @property	  User $User
 * @property	  RememberMeComponent $RememberMe
 */
class UsersController extends UsersAppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Users';

    /**
     * If the controller is a plugin controller set the plugin name
     *
     * @var mixed
     */
    public $plugin = null;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Time',
        'Text');

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Auth',
        'Session',
        'Cookie',
        'Paginator',
        //     'Security',
        'Search.Prg',
        'Users.RememberMe'
    );

    /**
     * Preset vars
     *
     * @var array $presetVars
     * @link https://github.com/CakeDC/search
     */
    public $presetVars = true;

    /**
     * Constructor
     *
     * @param CakeRequest $request Request object for this controller. Can be null for testing,
     *  but expect that features that use the request parameters will not work.
     * @param CakeResponse $response Response object for this controller.
     */
    public function __construct($request, $response) {
        $this->_setupComponents();
        $this->_setupHelpers();
        parent::__construct($request, $response);
        $this->_reInitControllerName();
    }

    /**
     * Providing backward compatibility to a fix that was just made recently to the core
     * for users that want to upgrade the plugin but not the core
     *
     * @link http://cakephp.lighthouseapp.com/projects/42648-cakephp/tickets/3550-inherited-controllers-get-wrong-property-names
     * @return void
     */
    protected function _reInitControllerName() {
        $name = substr(get_class($this), 0, -10);
        if ($this->name === null) {
            $this->name = $name;
        } elseif ($name !== $this->name) {
            $this->name = $name;
        }
    }

    /**
     * Returns $this->plugin with a dot, used for plugin loading using the dot notation
     *
     * @return mixed string|null
     */
    protected function _pluginDot() {
        if (is_string($this->plugin)) {
            return $this->plugin . '.';
        }
        return $this->plugin;
    }

    /**
     * Setup components based on plugin availability
     *
     * @return void
     * @link https://github.com/CakeDC/search
     */
    protected function _setupComponents() {
        if (App::import('Component', 'Search.Prg')) {
            $this->components[] = 'Search.Prg';
        }
    }

    /**
     * Setup helpers based on plugin availability
     *
     * @return void
     */
    protected function _setupHelpers() {
        if (App::import('Helper', 'Goodies.Gravatar')) {
            $this->helpers[] = 'Goodies.Gravatar';
        }
    }

    /**
     * beforeFilter callback
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->_setupAuth();

        $this->set('model', $this->modelClass);

        if (!Configure::read('App.defaultEmail')) {
            Configure::write('App.defaultEmail', 'noreply@' . env('HTTP_HOST'));
        }
    }

    /**
     * Setup Authentication Component
     *
     * @return void
     */
    protected function _setupAuth() {
        $this->Components->disable('Auth');

        $this->Auth->allow('add', 'reset', 'verify', 'logout', 'view', 'reset_password', 'login', 'resend_verification');
        if (!is_null(Configure::read('Users.allowRegistration')) && !Configure::read('Users.allowRegistration')) {
            $this->Auth->deny('add');
        }
        if ($this->request->action == 'register') {
            $this->Components->disable('Auth');
        }

        $this->Auth->authenticate = array(
            'Form' => array(
                'fields' => array(
                    'username' => 'username',
                    'password' => 'password'
                ),
                'userModel' => $this->_pluginDot() . $this->modelClass,
                'scope' => array(
                    $this->modelClass . '.active' => 1,
                    $this->modelClass . '.email_verified' => 1
                ),
                'recursive' => 1,
                'contain' => array('UserDetail')
        ));

        $this->Auth->loginRedirect = array('plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'dashboard');
        $this->Auth->logoutRedirect = array('plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'login');
        $this->Auth->loginAction = array('admin' => false, 'plugin' => Inflector::underscore($this->plugin), 'controller' => 'users', 'action' => 'login');
    }

    /**
     * Simple listing of all users
     *
     * @return void
     */
    public function index() {
        $this->redirect('/');
        $this->paginate = array(
            'limit' => 12,
            'conditions' => array(
                $this->modelClass . '.active' => 1,
                $this->modelClass . '.email_verified' => 1));
        $this->set('users', $this->paginate($this->modelClass));
    }

    /**
     * The homepage of a users giving him an overview about everything
     *
     * @return void
     */
    ////////////********************/////Start User board functions/////************///////////
    public function dashboardx() {
        
    }

    public function dashboard() {
//        debug($this->Auth->user());
        if (is_null(AuthComponent::user('id'))) {
            $this->render('login');
            return;
        }

        $user_id = $this->Auth->user('id');
        $this->loadModel('UserSearch');
        $this->loadModel('Car');
        $this->loadModel('Message');
        $this->loadModel('UserCar');
        $this->loadModel('UserDealerLocation');
        $this->loadModel('DealerLocation');
        $this->UserCar->recursive = 2;
        $saved_cars = $this->UserCar->find('all', array('conditions' => array('UserCar.user_id' => $user_id), 'contain' => array('Car', 'Car.CarImage'), 'limit' => 6));
        $saved_dealerships = $this->UserDealerLocation->getSavedDealerLocations($this->Auth->user());
        $searches = $this->UserSearch->find('all', array('order' => 'UserSearch.created desc', 'limit' => 6, 'conditions' => array('UserSearch.user_id' => $user_id, '(UserSearch.send_alert IS NULL OR UserSearch.send_alert=0)')));

        $receiver_conditions = array('Message.receiver_id' => $user_id, 'Message.from_type' => 2, '(Message.receiver_deleted is Null Or Message.receiver_deleted=0)');
        $inbox = $this->Message->find('all', array('conditions' => $receiver_conditions));
        $dealers = $this->DealerLocation->find('list');

        $user = $this->{$this->modelClass}->findById($this->Auth->user('id'));
        $this->set(compact('user', 'searches', 'saved_dealerships', 'saved_cars', 'inbox', 'dealers'));

        $this->pageTitle = 'Dashboard';
    }

    public function address() {
        if (is_null(AuthComponent::user('id'))) {
            $this->render('login');
            return;
        }
        $this->loadModel('UserAddress');
        $this->UserAddress->recursive = -1;
        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {

                $this->request->data['UserAddress']['user_id'] = AuthComponent::user('id');
                if ($this->UserAddress->hasAny(array(
                            'UserAddress.user_id' => AuthComponent::user('id'),
                            'UserAddress.default' => 1,
                        ))) {

                    $this->request->data['UserAddress']['default'] = 0;
                } else {
                    $this->request->data['UserAddress']['default'] = 1;
                }

                if ($this->UserAddress->save($this->request->data)) {
                    $this->Session->setFlash(__d('users', 'Address has been saved'));
                    $this->redirect(array('controller' => 'users', 'action' => 'address'));
                }
            }
        }

        $addresses = $this->UserAddress->findAllByUserId(AuthComponent::user('id'));
        $this->set(compact('addresses'));

        $this->set('states', AppUser::$states);
    }

////////////********************/////Start User board/////************///////////
    /**
     * Shows a users profile
     *
     * @param string $slug User Slug
     * @return void
     */
    public function view($slug = null) {
        $this->redirect('/');
        try {
            $this->set('user', $this->{$this->modelClass}->view($slug));
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage());
            $this->redirect('/');
        }
    }

    /**
     * Edit
     *
     * @param string $id User ID
     * @return void
     */

    /**
     * Admin Index
     *
     * @return void
     */
    public function admin_index() {
        $this->Prg->commonProcess();
        unset($this->{$this->modelClass}->validate['username']);
        unset($this->{$this->modelClass}->validate['email']);
        $this->{$this->modelClass}->data[$this->modelClass] = $this->passedArgs;
        if ($this->{$this->modelClass}->Behaviors->attached('Searchable')) {
            $parsedConditions = $this->{$this->modelClass}->parseCriteria($this->passedArgs);
        } else {
            $parsedConditions = array();
        }
        $this->Paginator->settings[$this->modelClass]['conditions'] = $parsedConditions;
        $this->Paginator->settings[$this->modelClass]['order'] = array($this->modelClass . '.created' => 'desc');

        $this->{$this->modelClass}->recursive = 0;
        $this->set('users', $this->paginate());
    }

    /**
     * Admin view
     *
     * @param string $id User ID
     * @return void
     */
    public function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__d('users', 'Invalid User.'));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('user', $this->{$this->modelClass}->read(null, $id));
    }

    /**
     * Admin add
     *
     * @return void
     */
    public function admin_add() {
        if (!empty($this->request->data)) {
            $this->request->data[$this->modelClass]['tos'] = true;
            $this->request->data[$this->modelClass]['email_verified'] = true;

            if ($this->{$this->modelClass}->add($this->request->data)) {
                $this->Session->setFlash(__d('users', 'The User has been saved'));
                $this->redirect(array('action' => 'index'));
            }
        }
        //$this->set('roles', Configure::read('Users.roles'));
    }

    /**
     * Admin edit
     *
     * @param string $id User ID
     * @return void
     */
    public function admin_edit($userId = null) {
        try {
            $result = $this->{$this->modelClass}->edit($userId, $this->request->data);
            if ($result === true) {
                $this->Session->setFlash(__d('users', 'User saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->request->data = $result;
            }
        } catch (OutOfBoundsException $e) {
            $this->Session->setFlash($e->getMessage());
            $this->redirect(array('action' => 'index'));
        }

        if (empty($this->request->data)) {
            $this->request->data = $this->{$this->modelClass}->read(null, $userId);
        }
        //$this->set('roles', Configure::read('Users.roles'));
    }

    /**
     * Delete a user account
     *
     * @param string $userId User ID
     * @return void
     */
//    public function admin_delete($userId = null) {
//        if ($this->{$this->modelClass}->delete($userId)) {
//            $this->Session->setFlash(__d('users', 'User deleted'));
//        } else {
//            $this->Session->setFlash(__d('users', 'Invalid User'));
//        }
//
//        $this->redirect(array('action' => 'index'));
//    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void 
     */
    public function admin_delete($id = null) {
        if ($this->request->data('submit_btn') === 'no') {
            $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
            $this->redirect(array('action' => 'index'));
        } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) && $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax') )) {
            $id = !empty($id) ? array($id) : $this->request->data('ids');
            $productReviews = $this->{$this->modelClass}->find('all', array('conditions' => array('{$this->modelClass}.id' => $id)));
            $this->set(compact('productReviews'));
            return;
        }

        if (!empty($id)) {

            $this->{$this->modelClass}->id = $id;
            if (!$this->{$this->modelClass}->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->{$this->modelClass}->delete()) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('User deleted'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            }
            if ($this->request->is('ajax')) {
                die(json_encode(array('status' => 'error')));
            } else {
                $this->flashMessage(__('User was not deleted'));
                $this->redirect(array('action' => 'index'));
            }
        } elseif (is_array($this->request->data('ids'))) {
            if ($this->{$this->modelClass}->deleteAll(array($this->modelClass . '.id' => $this->request->data('ids')), true, true)) {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'success')));
                } else {
                    $this->flashMessage(__('Users deleted successfully'), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
                } else {
                    $this->flashMessage(__('Error deleting selected users'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

    /**
     * Search for a user
     *
     * @return void
     */
    public function admin_search() {
        $this->search();
    }

    /**
     * User register action
     *
     * @return void
     */
    public function register() {
        if ($this->Auth->user()) {
            $this->flashMessage(__d('users', 'You are already registered and logged in!'));
            $this->redirect('/users/dashboard');
        }

        if (!empty($this->request->data)) {
            $this->request->data['User']['username'] = $this->request->data['User']['email'];
            $user = $this->{$this->modelClass}->register($this->request->data);
            if ($user !== false) {
                //  $this->_sendVerificationEmail($this->{$this->modelClass}->data);
                $this->Session->setFlash(__d('users', 'Your account has been created. Thank you!.'));
                $this->Auth->login();
                $previous_page = $this->Session->read('user_redirect');
                if (empty($previous_page)) {
                    $data['return_to'] = array('action' => 'dashboard');
                } else {
                    $data['return_to'] = $previous_page;
                }
                $this->Session->delete('user_redirect');
                $this->redirect($data['return_to']);
            } else {

                unset($this->request->data[$this->modelClass]['password']);
                unset($this->request->data[$this->modelClass]['temppassword']);
                $this->Session->setFlash(__d('users', 'Your account could not be created. Please, try again.'), 'default', array('class' => 'message warning'));
            }
        }
        $this->render('login');
    }

    /**
     * Common login action
     *
     * @return void
     */
    public function login() {
        if ($this->Auth->user()) {
            $this->flashMessage(__d('users', 'You are already registered and logged in!'), 'Notemessage');
            $this->redirect('/users/dashboard');
        }
//        debug($this->Session->read('Auth.redirect'));
        if ($this->request->is('post')) {
            $conditions = array(
                'User.username' => $this->request->data['User']['username'],
                'User.password' => $this->User->hash($this->request->data['User']['password'], null, true),
            );
            $userdata = $this->User->find('first', array('conditions' => $conditions, 'recursive' => -1));
            if (!empty($userdata)) {
                $this->User->reAuth($userdata['User']['id']);
//                debug($this->request->data);
                $this->getEventManager()->dispatch(new CakeEvent('Users.afterLogin', $this, array(
                    'isFirstLogin' => !$this->Auth->user('last_login'))));

                $this->{$this->modelClass}->id = $this->Auth->user('id');
                $this->{$this->modelClass}->saveField('last_login', date('Y-m-d H:i:s'));

                if ($this->here == $this->Auth->loginRedirect) {
                    $this->Auth->loginRedirect = '/';
                }
                $this->flashmessage(sprintf(__d('users', 'Welcome back %s'), $this->Auth->user('username')), 'Sucmessage');
                if (!empty($this->request->data)) {
                    $data = $this->request->data[$this->modelClass];

                    if (empty($this->request->data[$this->modelClass]['remember_me'])) {
                        $this->RememberMe->destroyCookie();
                    } else {
                        $this->_setCookie();
                    }
                }

//                 debug($this->Session->read('Auth.redirect'));exit;
                $this->redirect($this->Auth->redirect());
            } else {
                //$this->Auth->flash(__d('users', 'Invalid e-mail / password combination.  Please try again'));
                $this->flashMessage('Invalid Username / password combination.  Please try again');
            }
        }

        $allowRegistration = Configure::read('Users.allowRegistration');
        $this->set('allowRegistration', (is_null($allowRegistration) ? true : $allowRegistration));
    }

    /**
     * Search - Requires the CakeDC Search plugin to work
     *
     * @throws MissingPluginException
     * @return void
     * @link https://github.com/CakeDC/search
     */
    public function search() {
        if (!App::import('Component', 'Search.Prg')) {
            throw new MissingPluginException(array('plugin' => 'Search'));
        }

        $searchTerm = '';
        $this->Prg->commonProcess($this->modelClass, $this->modelClass, 'search', false);

        $by = null;
        if (!empty($this->request->params['named']['search'])) {
            $searchTerm = $this->request->params['named']['search'];
            $by = 'any';
        }
        if (!empty($this->request->params['named']['username'])) {
            $searchTerm = $this->request->params['named']['username'];
            $by = 'username';
        }
        if (!empty($this->request->params['named']['email'])) {
            $searchTerm = $this->request->params['named']['email'];
            $by = 'email';
        }
        $this->request->data[$this->modelClass]['search'] = $searchTerm;

        $this->paginate = array(
            'search',
            'limit' => 12,
            'by' => $by,
            'search' => $searchTerm,
            'conditions' => array(
                'AND' => array(
                    $this->modelClass . '.active' => 1,
                    $this->modelClass . '.email_verified' => 1)));

        $this->set('users', $this->paginate($this->modelClass));
        $this->set('searchTerm', $searchTerm);
    }

    /**
     * Common logout action
     *
     * @return void
     */
    public function logout() {
        $user = $this->Auth->user();
        $this->Session->delete($this->Auth->sessionKey);
        if (isset($_COOKIE[$this->Cookie->name])) {
            $this->Cookie->destroy();
        }
        $this->RememberMe->destroyCookie();
        $this->flashMessage(__d('users', 'you have successfully logged out'), 'Sucmessage');
        $this->redirect($this->Auth->logout());
    }

    /**
     * Checks if an email is already verified and if not renews the expiration time
     *
     * @return void
     */
    public function resend_verification() {
        if ($this->request->is('post')) {
            try {
                if ($this->{$this->modelClass}->checkEmailVerification($this->request->data)) {
                    $this->_sendVerificationEmail($this->{$this->modelClass}->data);
                    $this->Session->setFlash(__d('users', 'The email was resent. Please check your inbox.'));
                    $this->redirect('login');
                } else {
                    $this->Session->setFlash(__d('users', 'The email could not be sent. Please check errors.'));
                }
            } catch (Exception $e) {
                $this->Session->setFlash($e->getMessage());
            }
        }
    }

    /**
     * Confirm email action
     *
     * @param string $type Type, deprecated, will be removed. Its just still there for a smooth transistion.
     * @param string $token Token
     * @return void
     */
    public function verify($type = 'email', $token = null) {
        if ($type == 'reset') {
            // Backward compatiblity
            $this->request_new_password($token);
        }

        try {
            $this->{$this->modelClass}->verifyEmail($token);
            $this->Session->setFlash(__d('users', 'Your e-mail has been validated!'));
            return $this->redirect(array('action' => 'login'));
        } catch (RuntimeException $e) {
            $this->Session->setFlash($e->getMessage());
            return $this->redirect('/');
        }
    }

    /**
     * This method will send a new password to the user
     *
     * @param string $token Token
     * @throws NotFoundException
     * @return void
     */
    public function request_new_password($token = null) {
        if (Configure::read('Users.sendPassword') !== true) {
            throw new NotFoundException();
        }

        $data = $this->{$this->modelClass}->validateToken($token, true);

        if (!$data) {
            $this->Session->setFlash(__d('users', 'The url you accessed is not longer valid'));
            return $this->redirect('/');
        }

        $email = $data[$this->modelClass]['email'];
        unset($data[$this->modelClass]['email']);

        if ($this->{$this->modelClass}->save($data, array('validate' => false))) {
            $this->_sendNewPassword($data);
            $this->Session->setFlash(__d('users', 'Your password was sent to your registered email account'));
            return $this->redirect(array('action' => 'login'));
        }

        $this->Session->setFlash(__d('users', 'There was an error verifying your account. Please check the email you were sent, and retry the verification link.'));
        $this->redirect('/');
    }

    /**
     * Sends the password reset email
     *
     * @param array
     * @return void
     */
    protected function _sendNewPassword($userData) {
        $Email = $this->_getMailInstance();
        $Email->from(Configure::read('App.defaultEmail'))
                ->to($userData[$this->modelClass]['email'])
                ->replyTo(Configure::read('App.defaultEmail'))
                ->return(Configure::read('App.defaultEmail'))
                ->subject(env('HTTP_HOST') . ' ' . __d('users', 'Password Reset'))
                ->template($this->_pluginDot() . 'new_password')
                ->viewVars(array(
                    'model' => $this->modelClass,
                    'userData' => $userData))
                ->send();
    }

    /**
     * Allows the user to enter a new password, it needs to be confirmed by entering the old password
     *
     * @return void
     */
    public function change_password() {
        if ($this->request->is('post')) {
            $this->request->data[$this->modelClass]['id'] = $this->Auth->user('id');
            if ($this->{$this->modelClass}->changePassword($this->request->data)) {
                $this->Session->setFlash(__d('users', 'Password changed.'));
                // we don't want to keep the cookie with the old password around
                $this->RememberMe->destroyCookie();
                $this->redirect('/');
            }
        }
    }

    /**
     * Reset Password Action
     *
     * Handles the trigger of the reset, also takes the token, validates it and let the user enter
     * a new password.
     *
     * @param string $token Token
     * @param string $user User Data
     * @return void
     */
    public function reset_password($token = null, $user = null) {
        if (empty($token)) {
            $admin = false;
            if ($user) {
                $this->request->data = $user;
                $admin = true;
            }
            $this->_sendPasswordReset($admin);
        } else {
            $this->_resetPassword($token);
        }
    }

    /**
     * Sets a list of languages to the view which can be used in selects
     *
     * @deprecated No fallback provided, use the Utils plugin in your app directly
     * @param string $viewVar View variable name, default is languages
     * @throws MissingPluginException
     * @return void
     * @link https://github.com/CakeDC/utils
     */
    protected function _setLanguages($viewVar = 'languages') {
        if (!App::import('Lib', 'Utils.Languages')) {
            throw new MissingPluginException(array('plugin' => 'Utils'));
        }
        $Languages = new Languages();
        $this->set($viewVar, $Languages->lists('locale'));
    }

    /**
     * Sends the verification email
     *
     * This method is protected and not private so that classes that inherit this
     * controller can override this method to change the varification mail sending
     * in any possible way.
     *
     * @param string $to Receiver email address
     * @param array $options EmailComponent options
     * @return boolean Success
     */
    protected function _sendVerificationEmail($userData, $options = array()) {
        $defaults = array(
            'from' => Configure::read('App.defaultEmail'),
            'subject' => __d('users', 'Account verification'),
            'template' => $this->_pluginDot() . 'account_verification',
            'layout' => 'default',
            'emailFormat' => CakeEmail::MESSAGE_TEXT
        );

        $options = array_merge($defaults, $options);

        $Email = $this->_getMailInstance();
        $Email->to($userData[$this->modelClass]['email'])
                ->from($options['from'])
                ->emailFormat($options['emailFormat'])
                ->subject($options['subject'])
                ->template($options['template'], $options['layout'])
                ->viewVars(array(
                    'model' => $this->modelClass,
                    'user' => $userData
                ))
                ->send();
    }

    /**
     * Checks if the email is in the system and authenticated, if yes create the token
     * save it and send the user an email
     *
     * @param boolean $admin Admin boolean
     * @param array $options Options
     * @return void
     */
    protected function _sendPasswordReset($admin = null, $options = array()) {
        $defaults = array(
            'from' => Configure::read('App.defaultEmail'),
            'subject' => __d('users', 'Password Reset'),
            'template' => $this->_pluginDot() . 'password_reset_request',
            'emailFormat' => CakeEmail::MESSAGE_TEXT,
            'layout' => 'default'
        );

        $options = array_merge($defaults, $options);

        if (!empty($this->request->data)) {
            $user = $this->{$this->modelClass}->passwordReset($this->request->data);

            if (!empty($user)) {

                $Email = $this->_getMailInstance();
                $Email->to($user[$this->modelClass]['email'])
                        ->from($options['from'])
                        ->emailFormat($options['emailFormat'])
                        ->subject($options['subject'])
                        ->template($options['template'], $options['layout'])
                        ->viewVars(array(
                            'model' => $this->modelClass,
                            'user' => $this->{$this->modelClass}->data,
                            'token' => $this->{$this->modelClass}->data[$this->modelClass]['password_token']))
                        ->send();

                if ($admin) {
                    $this->Session->setFlash(sprintf(
                                    __d('users', '%s has been sent an email with instruction to reset their password.'), $user[$this->modelClass]['email']));
                    $this->redirect(array('action' => 'index', 'admin' => true));
                } else {
                    $this->Session->setFlash(__d('users', 'You should receive an email with further instructions shortly'));
                    $this->redirect(array('action' => 'login'));
                }
            } else {
                $this->Session->setFlash(__d('users', 'No user was found with that email.'));
                $this->redirect($this->referer('/'));
            }
        }
        $this->render('request_password_change');
    }

    /**
     * Sets the cookie to remember the user
     *
     * @param array RememberMe (Cookie) component properties as array, like array('domain' => 'yourdomain.com')
     * @param string Cookie data keyname for the userdata, its default is "User". This is set to User and NOT using the model alias to make sure it works with different apps with different user models across different (sub)domains.
     * @return void
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/cookie.html
     * @deprecated Use the RememberMe Component
     */
    protected function _setCookie($options = array(), $cookieKey = 'rememberMe') {
        $this->RememberMe->settings['cookieKey'] = $cookieKey;
        $this->RememberMe->configureCookie($options);
        $this->RememberMe->setCookie();
    }

    /**
     * This method allows the user to change his password if the reset token is correct
     *
     * @param string $token Token
     * @return void
     */
    protected function _resetPassword($token) {
        $user = $this->{$this->modelClass}->checkPasswordToken($token);
        if (empty($user)) {
            $this->Session->setFlash(__d('users', 'Invalid password reset token, try again.'));
            $this->redirect(array('action' => 'reset_password'));
        }

        if (!empty($this->request->data) && $this->{$this->modelClass}->resetPassword(Set::merge($user, $this->request->data))) {
            $this->Session->setFlash(__d('users', 'Password changed, you can now login with your new password.'));
            $this->redirect($this->Auth->loginAction);
        }

        $this->set('token', $token);
    }

    /**
     * Returns a CakeEmail object
     *
     * @return object CakeEmail instance
     * @link http://book.cakephp.org/2.0/en/core-utility-libraries/email.html
     */
    protected function _getMailInstance() {
        $emailConfig = Configure::read('Users.emailConfig');
        if ($emailConfig) {
            return new CakeEmail($emailConfig);
        } else {
            return new CakeEmail('default');
        }
    }

    public function view_profile() {
        if (is_null(AuthComponent::user('id'))) {
            $this->render('login');
            return;
        }
        $user = $this->Auth->user();
        if (empty($this->request->data)) {
            $this->request->data['User'] = $user;
            if (!empty($user['UserDetail'])) {
                $this->request->data['UserDetail'] = $user['UserDetail'];
            }
        }
        $this->loadModel('Car');
        $this->set($this->Car->getAllFields2());
        
        $this->set('licenseTypes', $this->User->UserDetail->licenseTypes);
    }

    function profile() {

        if (is_null(AuthComponent::user('id'))) {
            $this->render('login');
            return;
        }
        $user = $this->Auth->user();
        if (!empty($this->request->data)) {

            if ($this->User->saveAll($this->request->data)) {

                $this->User->reAuth($user['id']);
                $this->flashMessage(__('Your Profile saved successfully'),'Sucmessage');
                $this->redirect('/users/view_profile');
            } else {
                
            }
//            debug($this->User->validationErrors);
//            debug($this->User->getDataSource()->getLog());
        }

        if (empty($this->request->data)) {
            $this->request->data['User'] = $user;
            if (!empty($user['UserDetail'])) {
                $this->request->data['UserDetail'] = $user['UserDetail'];
            }
        }
        $this->loadModel('Car');
        $this->set($this->Car->getAllFields2());
        $this->set('licenseTypes', $this->User->UserDetail->licenseTypes);
    }

    function social($soical) {

        $this->Session->write('soical_backurl', Router::url(array('action' => 'social_callback')));
        $this->Session->write('soical_network', $soical);
        $this->Session->write('Last_Link', $_SERVER['REQUEST_URI']);
//        debug($this->Session->read('soical_network'));

        $config = array('path' => Router::url('/users/social/'));

        include getcwd() . DS . "social" . DS . 'index.php';
    }

    function social_callback() {

        $config = array('path' => Router::url('/users/social/'));
        include getcwd() . DS . "social" . DS . 'callback.php';
//        debug($response);
//        exit;
        if ($error == 0) {
//            debug($response);exit;
            $conditions = array('User.social_id' => $response['auth']['uid'], 'User.social_name' => $response['auth']['provider']);
            $userdata = $this->User->find('first', array('conditions' => $conditions, 'recursive' => -1));

            if (!empty($userdata)) {
                $this->User->reAuth($userdata['User']['id']);
                $this->flashMessage(__('You have been logged in successfully', true), 'Sucmessage');
                $user['User']['id'] = $userdata['User']['id'];
                $user['User']['last_login'] = date("Y-m-d H:i:s");
                unset($userdata['User']['password']);
                $this->User->save($user);

                $this->redirect(array('action' => 'dashboard'));
                die();
            } else {
                $user['User']['social_id'] = $response['auth']['uid'];
                $user['User']['social_name'] = $response['auth']['provider'];
                if (!empty($response['auth']['info']['email']))
                    $user['User']['email'] = $response['auth']['info']['email'];

                $user['UserDetail']['first_name'] = $response['auth']['info']['name'];
                $this->User->create();
                if ($this->User->saveAll($user)) {
                    $user['User']['id'] = $this->User->id;
                    $this->User->reAuth($this->User->id);
                    $this->flashMessage(__('You have been logged in successfully', true), 'Sucmessage');
                    $this->redirect(array('action' => 'dashboard'));
                    die();
                }
            }
            $this->Session->write('SocalData', $user);
            $this->Session->write('SocalResponse', $response);
            $this->redirect(array('action' => 'login'));
            die();
        }
        $this->flashMessage(__('Faild login with ' . $this->Session->read('soical_network'), true));
        $this->redirect(array('action' => 'login?sfdsf'));
        die();
    }

    function add_to_compare($car_id = false) {
        if (is_null(AuthComponent::user('id'))) {
            $this->render('login');
            return;
        }
//        $this->Session->delete('compare_cars');
        $cars_id = (array) $this->Session->read('compare_cars');
        if (!in_array($car_id, $cars_id)) {
            array_push($cars_id, $car_id);
        }
        $this->Session->write('compare_cars', $cars_id);
        $this->flashMessage('The car is added successfully to the comparison list', 'Sucmessage');
        $this->redirect(array('controller' => 'cars', 'action' => 'cars_compare'));
    }

}
