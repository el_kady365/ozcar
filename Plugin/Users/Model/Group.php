<?php

class Group extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    const GROUP_ADMIN = 1;
    const GROUP_CLIENT = 2;
    const GROUP_STAFF = 3;

    //   public $actsAs = array('Acl' => array('type' => 'requester'));
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'group_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function parentNode() {
        return null;
    }

    public function getUsers($group_id = null) {
        if (empty($group_id))
            return false;
        $users = $this->User->findAllByGroupId($group_id);

        if (empty($users))
            return false;
        return $users;
    }

}
