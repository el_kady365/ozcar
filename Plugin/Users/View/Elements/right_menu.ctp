<div class="col-md-4"><?php if (!empty($loggedUser)) { ?>
    <div class="side-nav pull-right">
        <div class="customer-panel m-b-lg">
            <h3>Your Dashboard</h3>
                <?php if (!empty($loggedUser['UserDetail'])) { ?>
                    <p>Logged in as <a href="<?php echo Router::url('/users/dashboard') ?>" class="text-primary"><strong><?php echo $loggedUser['UserDetail']['first_name'] . ' ' . $loggedUser['UserDetail']['last_name'] ?></strong></a></p>
                <?php } ?>
                <?php echo $this->element('Users.user_links'); ?>
            </div>
            <!-- /customer-panel  -->
        <?php } ?>
        <div class="promo-widget"> <a href="#"><img src="<?php echo Router::url('/css/img/banners/ads.png') ?>" alt="" title="" /></a> </div>
    </div>

</div>