<div class="collapse-section saved_cars">
    <div class="collapse-bar">
        <h3 class="pull-left"><a href="#" class="ico-04"><span class="switcher inline-block pull-left"></span> <span class="ico"></span> Recently Saved Cars</a></h3>
        <ul class="pull-right actions-links">
            <span class="delete-link hide">
                <li><a href="#" rel="delete">Delete</a></li>
                <li>|</li>
            </span>
            <span class="edit-link hide">

                <li><a href="#" rel="enquire">Enquire</a></li>
                <li>|</li>
                <li><a href="#">Add to Compare</a></li>
                <li>|</li>
            </span>
            <?php if (isset($dashboard)) { ?>

                <li><a href="<?php echo Router::url('/user_cars/') ?>">View All</a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="collapse-content">
        <?php if (!empty($saved_cars)) { ?>
            <form action="<?php echo Router::url(array('controller' => 'user_cars', "action" => "do_operation")) ?>" id="forn" method="post">
                <div class="saved-list row">
                    <?php
                    foreach ($saved_cars as $car) {
                        $title = $car['Car']['year'] . ' ' . $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series'];
                        $view_url = Router::url(array('controller' => 'cars', 'action' => 'view', $car['Car']['id'], Inflector::slug($title, '-')));
                        ?>
                        <div class="col-md-4 m-b-lg">
                            <div class="input checkbox clearfix">
                                <input class="pull-left" type="checkbox" name="chk[]" value="<?php echo $car['UserCar']['id'] ?>" />

                                <label><?php echo $title ?></label>
                            </div>

                            <div class="car-thumb m-b-sm m-t-sm "> <a href="#"><img src="<?php echo get_resized_image_url($car['Car']['CarImage'][0]['image'], 300, 200) ?>" alt="" title="" /></a> </div>
                            <div class="row row-sm saved-list-actions">
                                <div class="col-md-6 text-center"><a href="#" class="btn-danger btn-block">$<?php echo $this->Number->format((float) $car['Car']['selling_price']) ?></a></div>
                                <div class="col-md-6 text-center"><a href="<?php echo $view_url . "#offerEnquiry" ?>" class="btn-dark bold btn-block">MAKE OFFER</a></div>
                            </div>
                            <div class="feature-content">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td><span class="features-ico ico-01"></span><?php echo $car['Car']['body'] ?></td>
                                        <td><span class="features-ico ico-02"></span><?php echo $car['Car']['engine_capacity'] ?></td>
                                    </tr>
                                    <tr>
                                        <td><span class="features-ico ico-05"></span><?php echo $this->Number->format((float) $car['Car']['odometer']) ?> kms</td>
                                        <td><span class="features-ico ico-06"></span><?php echo $car['Car']['transmission'] ?></td>
                                    </tr>
                                    <tr>
                                        <td><span class="features-ico ico-08"></span><a href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'add_to_compare', $car['Car']['id'])) ?>">Compare</a></td>
                                        <td><span class="features-ico ico-09"></span><a href="<?php echo $view_url . "#carEnquiry" ?>">Enquire</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center" height="60"><a href="#"><img src="<?php echo Car::getDealerLogo($car['Car']['dealer_id'], 120) ?>" height="22" alt="" title="" /></a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center" height="60"><p class="text-sm">Saved on <strong class = "text-primary"><?php echo date('d-m-Y', strtotime($car['UserCar']['created'])) ?></strong> at <strong class = "text-primary"><?php echo date('h:ia', strtotime($car['UserCar']['created'])) ?></strong></p></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    <?php } ?> 
                </div>
            </form>
        <?php } ?>
    </div>
</div>
<script>
    $(function () {
        if ($('.saved_cars input[name="chk[]"]:checked').length == 1) {
            $('.saved_cars span.edit-link').removeClass('hide').show();
        } else {
            $('.saved_cars span.edit-link').addClass('hide').hide();
        }
        if ($('input[name="chk[]"]:checked').length) {
            $('.saved_cars span.delete-link').removeClass('hide').show();
        } else {
            $('.saved_cars span.delete-link').addClass('hide').hide();
        }
        $('.saved_cars input[name="chk[]"]').on('change', function () {
            if ($('.saved_cars input[name="chk[]"]:checked').length == 1) {
                $('.saved_cars span.edit-link').removeClass('hide').show();
            } else {
                $('.saved_cars span.edit-link').addClass('hide').hide();
            }
            if ($('.saved_cars input[name="chk[]"]:checked').length) {
                $('.saved_cars span.delete-link').removeClass('hide').show();
            } else {
                $('.saved_cars span.delete-link').addClass('hide').hide();
            }
//            alert('hello');
        });
        $(".saved_cars .actions-links li a").on('click', function () {
            action = $(this).prop('rel');
            form = $(this).closest('.collapse-section').find('form');
//            console.log(form);
            if (action != "")
            {
                if ($('input[name="chk[]"]:checked').length == 0)
                {
                    alert("You must choose one element at least");
                } else {
                    del = confirm("Are you sure you want to perform this operation?");
                    if (del)
                    {
                        form_action = form.prop('action');
                        form.prop('action', form_action + '?action=' + action);
                        form.submit();
                    }
                }
                return false;
            }
        });

    });




</script>