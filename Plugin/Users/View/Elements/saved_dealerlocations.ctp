<div class="collapse-section saved-dealerships">
    <div class="collapse-bar<?php echo!isset($dashboard) ? ' head-bar m-b-md' : '' ?>">
        <h3 class="pull-left">
            <a href="#" class="ico-07"><i class="fa fa-minus-square-o"></i> <span class="ico"></span> Saved Dealerships</a>
        </h3>
        <ul class="pull-right actions-links">
            <span class="delete-link hide">
                <li><a href="#" rel="delete">Delete</a></li>
                <li>|</li>
            </span>
            <?php if (isset($dashboard)) { ?>
                
                <li><a href="<?php echo Router::url('/user_dealer_locations/') ?>">View All</a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="collapse-content">
        <form action="<?php echo Router::url(array('controller' => 'user_dealer_locations', "action" => "do_operation")) ?>" id="forn" method="post">
            <div class="saved-dealers row">
                <?php foreach ($saved_dealerships as $dealer) { ?>


                    <div class="col-md-6">
                        <div class="dealer-list-head clearfix">
                            <div class="input checkbox clearfix pull-left">
                                <input type="checkbox" name="chk[]" value="<?php echo $dealer['UserDealerLocation']['id'] ?>" class="pull-left">
                                <label> <span class="text-sm">Saved on 
                                        <strong class="text-primary"><?php echo date('d-m-Y', strtotime($dealer['UserDealerLocation']['created'])) ?></strong> at <strong class="text-primary"><?php echo date('h:ia', strtotime($dealer['UserDealerLocation']['created'])) ?></strong></span>
                                </label>
                            </div>

                        </div>
                        <?php if (!empty($dealer['DealerLocation']['logo_full_path'])) { ?>
                            <div class="car-thumb m-b-sm m-t-sm "> 
                                <a href="#">
                                    <img src="<?php echo get_resized_image_url($dealer['DealerLocation']['logo'], 350, 200, 1) ?>" alt="" title="">
                                </a>
                            </div>
                        <?php } ?>



                        <div class="dealer-details row">
                            <div class="col-md-8">
                                <h5 class="bold"><?php echo $dealer['DealerLocation']['name'] ?></h5>
                                <p class="m-b-md m-t-md"><?php echo $dealer['DealerLocation']['address1'] ?><br>
                                    <?php echo $dealer['DealerLocation']['suburb'] ?> <?php echo $dealer['DealerLocation']['city'] ?> <?php echo $dealer['DealerLocation']['state'] ?></p>
                                <p><?php echo $dealer['DealerLocation']['phone1'] ?><br>
                                    <?php echo $dealer['DealerLocation']['phone2'] ?><br>

                                    <?php echo $dealer['DealerLocation']['email'] ?></p>
                            </div>
                            <div class="col-md-4 text-center"> 
                                <img title="" alt="" src="<?php echo Car::getDealerLogo($dealer['DealerLocation']['dealer_id']) ?>" />
                                <div class="row m-t-md">
                                    <div class="col-md-12"><span class="features-ico ico-10"></span> <strong><?php echo $dealer[0]['CarsCount'] ?> Cars</strong></div>
                                    <?php if (!empty($dealer[0]['dist'])) { ?>
                                        <div class="col-md-12"><span class="features-ico ico-11"></span> <strong><?php echo round($dealer[0]['dist'], 1) ?> kms</strong></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row row-sm m-t-sm">
                                    <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-info" href="#">VIEW</a></div>
                                    <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-info" href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'inbox', '#' => 'newMsg-' . $dealer['DealerLocation']['id'])) ?>">ENQUIRE</a></div>
                                    <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-info get-direction" data-address="<?php echo $dealer['DealerLocation']['id']; ?>" href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'dealer_search', '#' => 'dealer-' . $dealer['DealerLocation']['id'])) ?>">Directions</a></div>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>


            </div>
        </form>
    </div>
</div>
<script>
    $(function () {
        if ($('.saved-dealerships input[name="chk[]"]:checked').length == 1) {
            $('.saved-dealerships span.edit-link.alerts').removeClass('hide').show();
        } else {
            $('.saved-dealerships span.edit-link.alerts').addClass('hide').hide();
        }
        if ($('input[name="chk[]"]:checked').length) {
            $('.saved-dealerships span.delete-link').removeClass('hide').show();
        } else {
            $('.saved-dealerships span.delete-link').addClass('hide').hide();
        }
        $('.saved-dealerships input[name="chk[]"]').on('change', function () {
            if ($('.saved-dealerships input[name="chk[]"]:checked').length == 1) {
                $('.saved-dealerships span.edit-link.alerts').removeClass('hide').show();
            } else {
                $('.saved-dealerships span.edit-link.alerts').addClass('hide').hide();
            }
            if ($('.saved-dealerships input[name="chk[]"]:checked').length) {
                $('.saved-dealerships span.delete-link').removeClass('hide').show();
            } else {
                $('.saved-dealerships span.delete-link').addClass('hide').hide();
            }
//            alert('hello');
        });
        $(".saved-dealerships .actions-links li a").on('click', function () {
            action = $(this).prop('rel');
            form = $(this).closest('.collapse-section').find('form');
//            console.log(form);
            if (action != "")
            {
                if ($('input[name="chk[]"]:checked').length == 0)
                {
                    alert("You must choose one element at least");
                } else {
                    del = confirm("Are you sure you want to perform this operation?");
                    if (del)
                    {
                        form_action = form.prop('action');
                        form.prop('action', form_action + '?action=' + action);
                        form.submit();
                    }
                }

            }
        });

    });




</script>
<!-- /collapse-section  -->