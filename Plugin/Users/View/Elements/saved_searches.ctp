
<div class="collapse-section user-searches">
    <div class="collapse-bar<?php echo!isset($dashboard) ? ' head-bar m-b-md' : '' ?>">
        <h3 class="pull-left">
            <a href="#" class="ico-06">
                <i class="fa fa-minus-square-o"></i>
                <span class="ico"></span> 
                <?php if (isset($alerts) && $alerts == 1) { ?>
                    Car Alerts
                <?php } else {
                    ?>
                    Saved Searches
                <?php } ?>
            </a>
        </h3>
        <ul class="pull-right actions-links">
            <?php if (isset($alerts) && $alerts == 1) { ?>
                <li><a href="<?php echo Router::url(array('controller' => 'user_searches', 'action' => 'car_alerts')) ?>" >Add</a></li>
                <li>|</li>
            <?php } ?>
            <span class="delete-link hide">
                <li><a href="#" rel="delete">Delete</a></li>
                <li>|</li>
            </span>
            <span class="edit-link <?php echo (isset($alerts) && $alerts == 1) ? "alerts" : ''; ?> hide">
                <li><a href="#" rel="edit">Edit</a></li>
                <li>|</li>
            </span>
            <?php if (isset($dashboard)) { ?>
                
                <li><a href="<?php echo Router::url('/user_searches/') ?>">View All</a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="collapse-content">
        <form action="<?php echo Router::url(array('controller' => 'user_searches', "action" => "do_operation")) ?>" id="forn" method="post">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
                <?php
                if (!empty($searches)) {
                    foreach ($searches as $search) {
                        ?>
                        <tr>
                            <td><input type="checkbox" name="chk[]" value="<?php echo $search['UserSearch']['id'] ?>" /></td>
                            <td>
                                <?php if ($search['UserSearch']['send_alert']) { ?>
                                    <a href="<?php echo Router::url(array('controller' => 'user_searches', 'action' => 'car_alerts', $search['UserSearch']['id'])); ?>">
                                    <?php } else { ?>
                                        <a href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'search', '?' => json_decode($search['UserSearch']['search_parameters'], 1))); ?>">
                                        <?php } ?>

                                        <?php echo $search['UserSearch']['title'] ?></a></td>
                            <td><p class="text-sm">Saved on <strong class = "text-primary"><?php echo date('d-m-Y', strtotime($search['UserSearch']['created'])) ?></strong> at <strong class = "text-primary"><?php echo date('h:ia', strtotime($search['UserSearch']['created'])) ?></strong></p></td>
                            <td><a href="<?php echo Router::url(array('controller' => 'user_searches', 'action' => 'delete', $search['UserSearch']['id'])); ?>"><img src="<?php echo Router::url('/css/img/ico/row_del.png') ?>" alt="" title=""/></a></td>
                        </tr>
                        <?php
                    }
                }
                ?>


            </table>
        </form>
    </div>
</div>
<!-- /collapse-section  -->
<script>
    $(function () {
        if ($('.user-searches input[name="chk[]"]:checked').length == 1) {
            $('.user-searches span.edit-link.alerts').removeClass('hide').show();
        } else {
            $('.user-searches span.edit-link.alerts').addClass('hide').hide();
        }

        if ($('input[name="chk[]"]:checked').length) {
            $('.user-searches span.delete-link ').removeClass('hide').show();
        } else {
            $('.user-searches span.delete-link').addClass('hide').hide();
        }

        $('.user-searches input[name="chk[]"]').on('change', function () {
            if ($('.user-searches input[name="chk[]"]:checked').length == 1) {
                $('.user-searches span.edit-link.alerts').removeClass('hide').show();
            } else {
                $('.user-searches span.edit-link.alerts').addClass('hide').hide();
            }
            if ($('.user-searches input[name="chk[]"]:checked').length) {
                $('.user-searches span.delete-link').removeClass('hide').show();
            } else {
                $('.user-searches span.delete-link').addClass('hide').hide();
            }
//            alert('hello');
        });
        $(".user-searches .actions-links li a").on('click', function () {
            action = $(this).prop('rel');
            form = $(this).closest('.collapse-section').find('form');
//            console.log(form);
            if (action != "")
            {
                if ($('input[name="chk[]"]:checked').length == 0)
                {
                    alert("You must choose one element at least");
                } else {
                    del = confirm("Are you sure you want to perform this operation?");
                    if (del)
                    {
                        form_action = form.prop('action');
                        form.prop('action', form_action + '?action=' + action);
                        form.submit();
                    }
                }

            }
        });

    });




</script>
