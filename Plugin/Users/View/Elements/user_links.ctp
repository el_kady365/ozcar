<ul>
    <li><a href="<?php echo Router::url('/users/view_profile') ?>" class="ico-01"><span class="ico"></span> My profile</a></li>
    <li><a href="<?php echo Router::url('/messages/inbox') ?>" class="ico-02"><span class="ico"></span> Messages</a></li>
    <li><a href="<?php echo Router::url('/user_searches/index/1') ?>" class="ico-03"><span class="ico"></span> Manage Alerts</a></li>
    <li><a href="<?php echo Router::url('/user_cars/') ?>" class="ico-04"><span class="ico"></span> Saved Cars</a></li>
    <li><a href="<?php echo Router::url('/cars/cars_compare') ?>" class="ico-05"><span class="ico"></span> Compare Cars</a></li>
    <li><a href="<?php echo Router::url('/user_searches/') ?>" class="ico-06"><span class="ico"></span> Saved Searches</a></li>
    <li><a href="<?php echo Router::url('/user_dealer_locations') ?>" class="ico-07"><span class="ico"></span> Saved Dealerships</a></li>
    <li><a href="#" class="ico-08"><span class="ico"></span> Saved Catalogues</a></li>
    <!--<li><a href="#" class="ico-09"><span class="ico"></span> Saved Classifieds</a></li>-->
    <li><a href="<?php echo Router::url('/content/specials') ?>" target="_blank" class="ico-10"><span class="ico"></span> Dealer Specials</a></li>
<!--    <li><a href="#" class="ico-11"><span class="ico"></span> Settings</a></li>-->
    <li><a href="<?php echo Router::url('/users/logout') ?>" class="ico-12"><span class="ico"></span> Sign out </a></li>
</ul>