
<div class="addresses">
    <div>

        <div id="addrNew" class="addrNew">
            <div class="column clearfix">
                <div class="acctHdr">
                    <h3 id="headAddress" class="highlight-a font-b">add a new address</h3>
                    <h4 id="desAddress" class="txtBlack font-b">Add any address you want to save for faster checkout!</h4>
                </div>
                <ul class="newAddrForm clearfix">
                    <?php echo $this->Form->create('UserAddress', array('id' => 'addressform')) ?>
                    <?php echo $this->Form->input('id'); ?>
                    <li>
                        <?php echo $this->Form->input('address_title', array( 'label' => array('class' => 'font-b reqd', 'text' => '*Address Title'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li class="clear"></li>
                    <li>
                        <?php echo $this->Form->input('first_name', array('label' => array('class' => 'font-b reqd', 'text' => '*first name'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li>
                        <?php echo $this->Form->input('last_name', array('label' => array('class' => 'font-b reqd', 'text' => '*last name'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li >
                        <?php echo $this->Form->input('street_address', array( 'class' => 'txtDouble', 'label' => array('class' => 'font-b reqd', 'text' => '*street address'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li >
                        <?php echo $this->Form->input('street_address2', array('class' => 'txtDouble', 'label' => array('class' => 'font-b reqd', 'text' => 'street address 2'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li class="clear">
                        <?php echo $this->Form->input('city', array('label' => array( 'class' => 'font-b', 'text' => '*City'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li>
                        <?php echo $this->Form->input('state', array('class' => 'addrState', 'options' => $states, 'empty' => 'Select state ...', 'label' => array('class' => 'font-b', 'text' => '*state'), 'div' => array('style' => 'margin: 0; ', 'class' => 'input select  oneform'))); ?>
                    </li>
                    <li class="clear">
                        <?php echo $this->Form->input('zip_code', array( 'label' => array('class' => 'font-b', 'text' => '*Post Code'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                    <li>
                        <?php echo $this->Form->input('phone', array( 'label' => array('class' => 'font-b', 'text' => '*PHONE NUMBER'), 'div' => array('class' => 'input text'))); ?>
                    </li>
                </ul>
                <div class="frmBtns">
                    <?php echo $this->Form->button('<span id="submitbtn" class="btnSave font-b" id="btnAdd">Add to Address Book</span>', array('type' => 'submit')); ?>
                    <?php echo $this->Form->button('<span class="btnCancel font-b" >CANCEL</span>', array('type' => 'reset', 'id'=> 'restbtn' , 'style' => 'display: none;')); ?>
                </div>
                <?php echo $this->Form->end() ?>
            </div>
            <!-- eo .column --> 
        </div>
        <!-- eo .addrNew -->
        <div id="addrExisting" class="addrExisting clearfix">
            <div class="column clearfix">
                <div class="font-b acctHdr">
                    <h3 class="highlight-a">EXISTING ADDRESSES</h3>
                    <h4 class="txtBlack font-b">Click 'primary' to use as your billing address!</h4>
                </div>
                <ul id="addressList" class="addressList">
                    <?php foreach ($addresses as $address): ?>
                        <li <?php if ($address['UserAddress']['default']): ?>class="primaryAddr" <?php else: ?> class="addressItem" <?php endif; ?> ><a title="Delete" class="lnkDelAddr" href="#"></a>
                            <div class="addressItem"><span class="lblAddrName txtBold"><?php echo $address['UserAddress']['address_title'] ?></span><br>
                                <span class="lblAddrFullName"><?php echo $address['UserAddress']['first_name'] ?> <?php echo $address['UserAddress']['last_name'] ?></span><br>
                                <span class="lblAddrStreet"><?php echo $address['UserAddress']['street_address'] ?></span><br>
                                <span class="lblAddrStreet2"><?php echo $address['UserAddress']['street_address2'] ?></span><br>
                                <span class="lblAddrCity"><?php echo $address['UserAddress']['city'] ?></span>, <span class="lblAddrState"><?php echo $address['UserAddress']['state'] ?></span> <span class="lblAddrZip"><?php echo $address['UserAddress']['zip_code'] ?></span><br>
                                Phone: <span class="lblAddrPhone"><?php echo $address['UserAddress']['phone'] ?></span>
                                <input type="hidden" value="<?php echo $address['UserAddress']['id'] ?>" name="address_id" class="address-id" />
                                <!-- the integer following "addressItem" increases by 1 every time through the repeater --> 
                            </div>
                            <div class="actnBtns font-b clearfix"><a title="Make Primary Address" class="btnPrimary" href="#">PRIMARY</a> <a title="Edit Address" class="btnEdit" href="#">EDIT</a> </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- eo .column --> 
        </div>
        <!-- eo .addrExisting -->
        </form>
    </div>


</div>
<script type="text/javascript">
    $('select').uniform();
    var addresses = <?php echo json_encode(Hash::combine($addresses, '{n}.UserAddress.id', '{n}')); ?>;

    console.log(addresses);
    $(document).ready(function() {
        $('#addrExisting a.btnEdit').click(function(e) {
            e.preventDefault();
            var $editBtn = $(this);
            var addressId = $editBtn.closest('li').find('.address-id').val();
            for (name in addresses[addressId].UserAddress) {
                $('#addressform').find(':input[name="data\\[UserAddress\\]\\[' + name + '\\]"]').val(addresses[addressId].UserAddress[name]);
            }
            $('#restbtn').show();
            $('#headAddress').text('EDIT EXISTING ADDRESSES');
            $('#desAddress').hide();
            $('#submitbtn').text('Save Address');
            $.uniform.update();
        });
        $('#restbtn').click(function(e) {
            $('#restbtn').hide();
            $('#submitbtn').text('Add to Address Book');
            $('#headAddress').text('add a new address');
            $('#desAddress').show();
            $('#UserAddressId').val(null)
            $.uniform.update();
        });
    });
</script>
<?php
$this->set(array(
    'subLayout' => 'left-navigation',
    'body_class' => 'zaccount zInnerPage',
    'body_id' => 'zaccount',
    'h1' => 'My Account'
));
?>