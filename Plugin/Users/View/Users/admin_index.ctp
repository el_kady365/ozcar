<div class="static-pages index">
    <h2><?php echo __('Users'); ?></h2>

    <?php
   // echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'User.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        
        'User.username' => array('edit_link' => array('plugin' => 'users','action' => 'edit', '%id%')),
        'User.email' => array('edit_link' => array('plugin' => 'users','action' => 'edit', '%id%')),
        'User.email_verified' => array('edit_link' => array('plugin' => 'users','action' => 'edit', '%id%')),
        'User.active' => array('edit_link' => array('plugin' => 'users','action' => 'edit', '%id%')),        
        'User.created' => array('edit_link' => array('plugin' => 'users','action' => 'edit', '%id%')),        
        'User.modified' => array('edit_link' => array('plugin' => 'users','action' => 'edit', '%id%')),        
    );
    $links = array(
        $this->Html->link(__('edit', true), array('plugin' => 'users', 'action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('delete', true), array('plugin' => 'users', 'action' => 'delete', '%id%'), array('class' => 'Delete')),// __('Are you sure?', true)),
    );


    //debug($pages);
    ?><?= $this->List->adminIndexList($fields, $users, $links, true, null,null) ?></div>
