<div class="contents">
    <div class="container">
        <div class="col-md-8">
            <h1>Dashboard</h1>
            <?php echo $this->element('Users.saved_cars', array('dashboard' => 1)) ?>
            <!-- /collapse-section  -->
            <?php echo $this->element('Users.saved_searches', array('dashboard' => 1)) ?>
            <?php echo $this->element('Users.saved_dealerlocations', array('dashboard' => 1)) ?>
            <?php echo $this->element('messages/inbox', array('dashboard' => 1)) ?>

            
            <!-- /collapse-section  -->

            
            <!-- /collapse-section  --> 


            <div class="collapse-section">
                <div class="collapse-bar">
                    <h3 class="pull-left"><a href="#" class="ico-09"><span class="switcher inline-block pull-left"></span> <span class="ico"></span> Saved Classifieds</a></h3>
                    <ul class="pull-right">
                        <li><a href="#">Delete   </a></li>
                        <li>|</li>
                        <li><a href="#">View All</a></li>
                    </ul>
                </div>
                <div class="collapse-content">
                    <div class="specials">
                        <div class="row m-b-lg">
                            <div class="col-md-4"><a href="#"><img alt="" src="<?php echo Router::url('/css/img/banners/b_01.jpg') ?>"></a></div>
                            <div class="col-md-4"><a href="#"><img alt="" src="<?php echo Router::url('/css/img/banners/b_02.jpg') ?>"></a></div>
                            <div class="col-md-4"><a href="#"><img alt="" src="<?php echo Router::url('/css/img/banners/b_03.jpg') ?>"></a></div>
                        </div>
                    </div>



                </div>
            </div>
            <!-- /collapse-section  --> 




        </div>
        <?php echo $this->element('Users.right_menu') ?>
    </div>
</div>