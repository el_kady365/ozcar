<div class="whitebox">
    <div class="heading-bg">
        <h1 class="font-b">MY ACCOUNT </h1>
    </div>
    <ul class="leftInnerNav font-b">
        <li><a href="#" class="active">Preferences</a></li>
        <li><a href="#">Address Book</a></li>
        <li><a href="#" >Order History</a></li>
        <li><a href="#">Wish List</a></li>
        <li><a href="#">Log Out </a></li>
    </ul>
    <div class="wholeContent"> 

        <!-- eo .modalWindow -->
        <div class="inner clearfix">




            <div id="userInfo" class="acctPrefs clearfix" style="display:none">
                <div class="prefGroup clearfix">
                    <div class="savedInfo clearfix">
                        <div class="font-b acctHdr">
                            <h3 class="highlight-a">Registered Email</h3>
                        </div>
                        <div class="currInfo clearfix txtBlack"><span id="currEmail" class="currentPref"> amrelsaqqa@hotmail.com</span></div>
                        <!-- eo .currInfo --> 
                    </div>
                    <!-- eo .savedInfo -->
                    <div class="changeInfo changeEmail clearfix">
                        <div class="font-b acctHdr">
                            <h3 class="highlight-a">Change Email</h3>
                        </div>
                        <div class="input text">
                            <input type="text" class="" id="txtEmail" name="txtEmail">
                        </div>
                        <div class="frmBtns clearfix"> <a href="#" data-val="currEmail" class="btnUpdatePref font-b" id="btnUpdateEmail">update &nbsp;email</a> </div>
                    </div>
                    <!-- eo .changeEmail --> 
                </div>
                <!-- eo .prefGroup -->
                <div class="prefGroup clearfix">
                    <div class="savedInfo savedPass clearfix">
                        <div class="font-b acctHdr">
                            <h3 class="highlight-a">Registered Password</h3>
                        </div>
                        <div class="currInfo clearfix txtBlack"><span id="currPass" class="currentPref">******** </span></div>
                        <!-- eo .currInfo --> 
                    </div>
                    <!-- eo .savedPass -->
                    <div class="changeInfo changePass clearfix">
                        <div class="font-b acctHdr">
                            <h3 class="highlight-a">Change Password</h3>
                        </div>
                        <div class="input text"><input type="password" autocomplete="off" class="" id="txtPassword" name="txtPassword"></div>
                        <div class="frmBtns clearfix"> <a href="#" data-val="currPass" class="btnUpdatePref font-b" id="btnUpdatePass">update &nbsp;password</a> </div>
                        &nbsp;&nbsp;&nbsp; </div>
                    <!-- eo .changePass --> 
                </div>
                <!-- eo .prefGroup -->
                <div style="border: none;" class="prefGroup clearfix">
                    <div class="savedInfo savedDOB clearfix">
                        <div class="font-b acctHdr">
                            <h3 class="highlight-a">Registered Birth Date</h3>
                        </div>
                        <div class="currInfo clearfix txtBlack"><span id="currDOB" class="currentPref"> 07/25/1988</span></div>
                        <!-- eo .currInfo --> 
                    </div>
                    <!-- eo .savedDOB -->
                    <div class="changeInfo changeDOB clearfix">
                        <div class="font-b acctHdr">
                            <h3 class="highlight-a">Change Birth Date</h3>
                        </div>
                        <div class="oneform" style="margin-left:0; width:400px">
                            <div class="input select left">
                                <select tabindex="14" class="selDOBMonth selDOB" id="selDOBMonth" name="selDOBMonth" style="width:40px" >
                                    <option value="">Month</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                            <div class="input select left">
                                <select tabindex="15" class="selDOBDate selDOB" id="selDOBDate" name="selDOBDate" style="width:20px">
                                    <option value="">Date</option>
                                    <option value="01">1</option>
                                </select>
                            </div>
                            <div class="input select left">
                                <select tabindex="16" class="selDOBYear selDOB" id="selDOBYear" name="selDOBYear" style="width:20px">
                                    <option value="">Year</option>
                                </select>
                            </div>
                            <div class="clear"></div>

                        </div>
                        <div class="frmBtns clearfix"> <a href="#" data-val="currDOB" class="btnUpdatePref font-b" id="btnUpdateDOB">update &nbsp;birth &nbsp;date</a> </div>
                    </div>
                    <!-- eo .changeDOB --> 
                </div>
                <!-- eo .prefGroup --> 
            </div>

            <!--/ tabe one end -->




            <div id="wishList" class="clearfix" style="display:none" >
                <div class="hdrWhole font-b highlight-a">My Wish List</div>
                <ul id="acctWishList" class="wishList clearfix">
                    <li><a class="lnkDelWishItem" href="#" id="wl_34908"></a><a title="Cedar Fire Starter Tin" href="#">
                            <img height="91" width="64" alt="Cedar Fire Starter Tin" src="https://www.zippo.com/images/products/1_1025617_TH.JPG"></a>
                        <h5 class="wListName highlight-a">Cedar Fire Starter Tin</h5>
                        <h5 class="wListId">44023-Z</h5>
                        <h5 class="wListPrice">$19.95</h5>
                        <a href="#" id="sku_1025618" class="lnkAddCart font-b">ADD TO Cart</a> </li>
                    <li><a class="lnkDelWishItem" href="#" id="wl_34946"></a><a title="NFL" href="#">
                            <img height="91" width="64" alt="NFL" src="https://www.zippo.com/images/products/1_1026279_TH.JPG"></a>
                        <h5 class="wListName highlight-a">NFL</h5>
                        <h5 class="wListId">28593-000002-Z</h5>
                        <h5 class="wListPrice">$25.95</h5>
                        <a href="#" id="sku_1026280" class="lnkAddCart font-b">ADD TO Cart</a> </li>
                </ul>
                <div class="wListShare font-b txtA7 clearfix">Share your wish list…
                    <ul class="wlShareOpts clearfix">
                        <li>
                            <a title="share via email" id="lnkWLMail" class="lnkWLMail" href="http://zippo.com">
                                <img height="43" width="43" alt="share via email" src="<?php echo Router::url('/css/img/iconShareMail.png'); ?>"></a> </li>
                        <li>
                            <a id="lnkWLFB" rel="nofollow" title="share on facebook" class="lnkWLFB"  href="#">
                                <img height="43" width="43" alt="share on facebook" src="<?php echo Router::url('/css/img/iconShareFB.png'); ?>"></a> </li>
                        <li>
                            <a title="share on twitter" class="lnkWLTW"  href="#">
                                <img height="43" width="43" alt="share on twitter" src="<?php echo Router::url('/css/img/iconShareTW.png'); ?>"></a> </li>
                        <li><a title="print this" class="lnkWLPrint"  href="#">
                                <img height="43" width="43" alt="print this" src="<?php echo Router::url('/css/img/iconShareP.png'); ?>"></a> </li>
                    </ul>
                </div>
                <!-- eo. wListShare -->
            </div>      

            <!--/ tabe two end -->



            <div class="orderHistory" style="display:none">
                <div class="hdrWhole font-b highlight-a">My Order History</div>
                <table cellspacing="0" cellpadding="0" width="100%" id="tblOrders" class="tblOrders">
                    <tbody>
                        <tr>
                            <th class="font-b">Order Date </th>
                            <th class="font-b">Order # </th>
                            <th class="font-b">Order Total </th>
                            <th class="font-b">Tracking # </th>
                            <th class="font-b">Status </th>
                        </tr>
                        <tr id="trNoOrders">
                            <td colspan="5">You have not placed any orders with this account. </td>
                        </tr>
                        <tr>
                            <td>28 SEP 2013 </td>
                            <td>#80 </td>
                            <td>$40 </td>
                            <td>2874177 </td>
                            <td >Pending </td>
                        </tr>              

                    </tbody>
                </table>
            </div>
            <!--/ tabe two end -->


<div class="addresses">
<div>
  <form id="frmMain" action="address.aspx" method="post" name="frmMain">

    <div id="addrNew" class="addrNew">
      <div class="column clearfix">
        <div class="acctHdr">
          <h3 class="highlight-a font-b">add a new address</h3>
          <h4 class="txtBlack font-b">Add any address you want to save for faster checkout!</h4>
        </div>
        <ul class="newAddrForm clearfix">
          <li>
          <div class="input text">
            <label class="font-b reqd" for="txtAlias">*Address Title</label>
             <input type="text" class="" tabindex="1" id="txtAlias" name="txtAlias">
            </div>
          </li>
          <li >
             <div class="input text">
            <label class="font-b reqd" for="txtFirstName">*First Name</label>
            <input type="text" class="" tabindex="2" id="txtFirstName" maxlength="30" name="txtFirstName">
            </div>
          </li>
          <li>
             <div class="input text">
            <label class="font-b reqd" for="txtLastName">*Last Name</label>
            <span class="frmError" id="lblLastNameErr"></span>
            <input type="text" class=" " tabindex="3" id="txtLastName" maxlength="30" name="txtLastName">
            </div>
          </li>
          <li class="width100">
                       <div class="input text">

            <label class="font-b reqd" for="txtAddress">*Street Address</label>
             <input type="text" class=" txtDouble" tabindex="4" id="txtAddress" maxlength="30" name="txtAddress">
            </div>
          </li>
          <li class="width100 clear">
             <div class="input text">
             <label class="font-b" for="txtAddress2">Street Address 2</label>
            <span class="frmError" id="lblAddress2Err"></span>
            <input type="text" class=" txtDouble" tabindex="5" id="txtAddress2" maxlength="30" name="txtAddress2">
            </div>
          </li>
          <li class="clear">
                       <div class="input text">
             <label class="font-b" for="txtCity">*City</label>
             <input type="text" class=" " tabindex="6" id="txtCity" maxlength="30" name="txtCity">
            </div>
          </li>
          <li>
                       <div class="input select">
             <label class="font-b" for="selState">*State</label>
             <select tabindex="7" class="addrState" id="selState" name="selState">
              <option value="" selected="selected">Select a State</option>
              <option value="AK">AK - Alaska</option>
              </select>
             </div>
          </li>
          <li class="clear">
           <div class="input text">
             <label class="font-b" for="txtZip">*Zip Code</label>
             <input type="text" class=" " tabindex="8" id="txtZip" maxlength="10" name="txtZip">
            </div>
          </li>
          <li>
 			<div class="input text">
             <label class="font-b" for="txtPhone">Phone Number</label>
             <input type="text" class="  txt3" tabindex="9" id="txtPhoneAC" maxlength="3" name="txtPhoneAC">
             </div>
          </li>
        </ul>
        <div class="frmBtns">
        <button><span class="btnCancel font-b" href="#">CANCEL</span></button>
         <button type="submit"><span class="btnSave font-b" id="btnAdd">Add to Address Book</span></button>
          </div>
      </div>
      <!-- eo .column --> 
    </div>
    <!-- eo .addrNew -->
    <div id="addrExisting" class="addrExisting clearfix">
      <div class="column clearfix">
        <div class="font-b acctHdr">
          <h3 class="highlight-a">EXISTING ADDRESSES</h3>
          <h4 class="txtBlack font-b">Click 'primary' to use as your billing address!</h4>
        </div>
        <ul id="addressList" class="addressList">
          <li class="primaryAddr"><a title="Delete" class="lnkDelAddr" href="#"></a>
            <div class="addressItem"><span class="lblAddrName txtBold">ad21</span><br>
              <span class="lblAddrFullName">amr elsaqqa</span><br>
              <span class="lblAddrStreet">45 sg st</span><br>
              <span class="lblAddrStreet2"></span><br>
              <span class="lblAddrCity">MND</span>, <span class="lblAddrState">IL</span> <span class="lblAddrZip">45354</span><br>
              Phone: <span class="lblAddrPhone">444-848-8445</span>
              <input type="hidden" value="addresItem1" name="addresItemNumber">
              <!-- the integer following "addressItem" increases by 1 every time through the repeater --> 
            </div>
            <div class="actnBtns font-b clearfix"><a title="Make Primary Address" class="btnPrimary" href="#">PRIMARY</a> <a title="Edit Address" class="btnEdit" href="#">EDIT</a> </div>
          </li>
          <li><a title="Delete" class="lnkDelAddr" href="#"></a>
            <div class="addressItem"><span class="lblAddrName txtBold">erew</span><br>
              <span class="lblAddrFullName">amr elsaqqa</span><br>
              <span class="lblAddrStreet">45 sg st3 34</span><br>
              <span class="lblAddrStreet2"></span><br>
              <span class="lblAddrCity">w33</span>, <span class="lblAddrState">AZ</span> <span class="lblAddrZip">54545</span><br>
              Phone: <span class="lblAddrPhone">545-455-6565</span>
              <input type="hidden" value="addresItem2" name="addresItemNumber">
              <!-- the integer following "addressItem" increases by 1 every time through the repeater --> 
            </div>
            <div class="actnBtns font-b clearfix"><a title="Make Primary Address" class="btnPrimary" href="#">PRIMARY</a> <a title="Edit Address" class="btnEdit" href="#">EDIT</a> </div>
          </li>
        </ul>
      </div>
      <!-- eo .column --> 
    </div>
    <!-- eo .addrExisting -->
  </form>
</div>


</div>














        </div>


    </div>
    <div class="clear"></div>
    <div class="btmPromo clear clearfix">
        <div class="promoInner clearfix">
            <h3 class="font-b">Zippo eGift Card</h3>
            <p>Give the perfect gift that is sure to please every time.  Let them choose <br>
                the gift they want with a Zippo eGift Card. </p>
            <img width="195" src="<?php echo Router::url('/css/img/data/1_270.jpg'); ?>" alt=""> <a href="#" title="" class="font-b">Shop Now</a> </div>
    </div>
    <div class="clear"></div>
</div>
<?php
$this->set(array(
    'subLayout' => 'full-width',
    'body_class' => 'zaccount zInnerPage',
    'body_id' => 'zaccount'
));
?>