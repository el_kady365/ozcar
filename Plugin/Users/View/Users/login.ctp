<div class="login-section">
  <div class="container">
    <div class="col-md-12">
      <h3 class="text-center">customer CONFIDENCE</h3>
    </div>
    <div class="col-md-push-1 col-md-10">
      <div class="login-box">
        <div class="row">
          <div class="col-md-5 text-center">
            <h5 class="text-primary">Log in</h5>
            <div class="social-login">
                <a href="<?php echo Router::url(array('action' => 'social', 'facebook')) ?>" class="fb-btn">Sign in with Facebook </a>
                <a class="tw-btn" href="<?php echo Router::url(array('action' => 'social', 'twitter')) ?>">Sign in with Twitter</a>
                <a href="<?php echo Router::url(array('action' => 'social', 'google')) ?>" class="g-btn">Sign in with Google Plus </a>
            </div>
          </div>
          <div class="col-md-1">
            <div class="vertical-divider"><span>OR</span></div>
          </div>
          <div class="col-md-6">
            <h5 class="text-primary text-center">Log into my account</h5>
            <?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login'))) ?>           
                <?php echo $this->Form->input('username', array('error' => false, 'label' => false, 'div' => array('class' => 'input text left'),'placeholder'=>'Username')); ?>
               
                <?php echo $this->Form->input('password', array('error' => false, 'label' => false, 'div' => array('class' => 'input text left'),'placeholder'=>'Password')); ?>
               
              <div class="row m-t-sm">
                <div class="col-md-6"><div class="m-l-sm"> <a href="#" class="btn btn-block text-left" >Forgot your login?</a> <a href="#" class="btn btn-block text-left" >Not Registered?</a> </div></div>
                <div class="col-md-6">
                  <button type="submit" class="btn btn-default btn-md bold pull-right">LOGIN</button>
                </div>
              </div>
            <?php echo $this->Form->end(); ?>
          </div>
        </div>
      </div>
      <!-- /login-box  --> 
    </div>
    <div class="col-md-12">
      <h2 class="text-center">Buy cars from dealers<br>
        you can trust</h2>
    </div>
  </div>
    <div class="specials-promo text-center"> <a href="#"><img src="<?php echo Router::url('/css/img/banners/leader_banner_02.jpg')?>" alt="" /></a> </div>
</div>