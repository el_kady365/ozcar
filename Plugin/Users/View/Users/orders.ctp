<div class="orderHistory">
                <div class="hdrWhole font-b highlight-a">My Order History</div>
                <table cellspacing="0" cellpadding="0" width="100%" id="tblOrders" class="tblOrders">
                    <tbody>
                        <tr>
                            <th class="font-b">Order Date </th>
                            <th class="font-b">Order # </th>
                            <th class="font-b">Order Total </th>
                            <th class="font-b">Status </th>
                        </tr>
                        <?php if(empty($orders)):?>
                        <tr id="trNoOrders">
                            <td colspan="5">You have not placed any orders with this account. </td>
                        </tr>
                        <?php else :?>
                        <?php foreach ($orders as $order):?>
                        <tr>
                            <td><?php echo date('d M Y' , strtotime($order['Order']['created']));?></td>
                            <td>#<?php echo $order['Order']['id'];?></td>
                            <td>$<?php echo $order['Order']['amount'];?></td>
                            <td ><?php echo $statuses[$order['Order']['status_id']];?></td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>

<?php
$this->set(array(
    'subLayout' => 'left-navigation',
    'body_class' => 'zaccount zInnerPage',
    'body_id' => 'zaccount',
    'h1' => 'My Account'
));
?>