<div class="contents">
    <div class="container">
        <div class="col-md-8">
            <h1>Dashboard</h1>
            <?php
            echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'profile')));
            echo $this->Form->input('id');
            echo $this->Form->hidden('UserDetail.own_car');
            echo $this->Form->input('UserDetail.id');
            ?>  
            <div class="collapse-section">
                <div class="collapse-bar head-bar m-b-md m-t-sm" data-id="Username">
                    <h3 class="pull-left">1. Username</h3>
                </div>
                <div class="collapse-content">
                    <div class="row m-b-md">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('username', array()); ?>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('password', array('autocomplete' => 'off', 'value' => '')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('temppassword', array('type' => 'password', 'label' => 'Confirm Password')); ?>
                        </div>
                    </div>
                </div>
                <div class="collapse-bar head-bar m-b-md m-t-sm" data-id="Profile">
                    <h3 class="pull-left">2. Your Profile</h3>
                </div>
                <div class="collapse-content">
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.first_name', array()); ?>

                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.last_name', array()); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.address', array('type' => 'text')); ?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('UserDetail.suburb', array()); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('UserDetail.postcode', array()); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('User.email'); ?>
                        </div>

                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.telephone', array('label' => 'Contact Number')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.birth_date', array('type' => 'text', 'class' => 'hasDate', 'label' => 'Birthdate')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.license_type', array('empty' => 'Select License type')); ?>
                        </div>
                    </div>
                </div>
                <!--                <div class="collapse-bar head-bar m-b-md m-t-sm">
                                    <h3 class="pull-left">3. Cars You're Interested In</h3>
                                </div>
                                <div class="collapse-content">
                                    <div class="snippet-box">
                                        <p>What cars are you interested in?<br>
                                            Fill in the form below with xxx separated by commas.</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                <?php echo $this->Form->input('UserDetail.interested_car.make', array('label' => false, 'empty' => 'Make')); ?>
                                        </div>
                                        <div class="col-md-6">
                <?php echo $this->Form->input('UserDetail.interested_car.model', array('label' => false, 'options' => false, 'empty' => 'Model')); ?>
                                        </div>
                
                                        <div class="col-md-6">
                <?php echo $this->Form->input('UserDetail.interested_car.series', array('label' => false, 'options' => false, 'empty' => 'Series')); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input text">
                                                <input placeholder="Fuel Type" type="text" name="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                <?php echo $this->Form->input('UserDetail.interested_car.transmission', array('label' => false, 'options' => Car::$Transmissions, 'label' => false, 'empty' => 'Transmission')) ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input text">
                                                <input placeholder="Engine Size" type="text" name="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input text">
                                                <input placeholder="Cylinders" type="text" name="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                <?php echo $this->Form->input('UserDetail.interested_car.body', array('label' => false, 'options' => Car::getBodyTypesDropDown(), 'empty' => 'Body')); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input text">
                                                <input placeholder="Doors" type="text" name="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input text">
                                                <input placeholder="Colour" type="text" name="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                <?php echo $this->Form->input('UserDetail.interested_car.year', array('label' => false, 'options' => range(date('Y') - 25, date('Y')), 'empty' => 'Year')); ?>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="col-md-12">
                                            <div class="input textarea">
                                                <label>Other</label>
                                                <textarea></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                <div class="collapse-bar head-bar m-b-md m-t-sm" data-id="YourCar">
                    <h3 class="pull-left">4. Your Current Car</h3>
                </div>
                <div class="collapse-content">
                    <p>Do you currently own a car?</p>
                    <div class="m-b-lg">
                        <div class="input radio pull-left">
                            <input type="radio" name="own_car" value="1" <?php echo!empty($this->request->data['UserDetail']['own_car']) ? 'checked="checked"' : "" ?> />
                            <label>Yes</label>
                        </div>
                        <div class="input radio pull-left">
                            <input type="radio" name="own_car" value="0" <?php echo!empty($this->request->data['UserDetail']['own_car']) ? '' : 'checked="checked"' ?> />
                            <label>No</label>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="own-car" <?php echo!empty($this->request->data['UserDetail']['own_car']) ? '' : 'style="display: none;"' ?> >
                        <p>Do you currently own a car?</p>
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.make', array('label' => false, 'empty' => 'Make')); ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.model', array('label' => false, 'options' => false, 'empty' => 'Model')); ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.series', array('label' => false, 'options' => false, 'empty' => 'Series')); ?>
                            </div>

                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.fuel_type', array('label' => false, 'placeholder' => 'Fuel type')) ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.transmission', array('label' => false, 'options' => Car::$Transmissions, 'label' => false, 'empty' => 'Transmission')) ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.engine_size', array('label' => false, 'placeholder' => 'Engine Size')) ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.cylinders', array('label' => false, 'placeholder' => 'Cylinders')) ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.body', array('label' => false, 'options' => Car::getBodyTypesDropDown(), 'empty' => 'Body')); ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.doors', array('label' => false, 'placeholder' => 'Doors')) ?>
                            </div>

                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.colour', array('label' => false, 'placeholder' => 'Colour')) ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.year', array('label' => false, 'options' => range(date('Y') - 25, date('Y')), 'empty' => 'Year')); ?>
                            </div>

                        </div>
                    </div>
                </div>

                <!--                <div class="collapse-bar head-bar m-b-md m-t-sm" data-id="LifeStyle">
                                    <h3 class="pull-left">5. Lifestyle</h3>
                                </div>
                                <div class="collapse-content">
                                    <div class="snippet-box m-b-md">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                                    </div>
                                    <ul class="row features-list">
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Camping</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Travel</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>4WD</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Luxury cars</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Camping</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Travel</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>4WD</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Luxury cars</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Camping</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Travel</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>4WD</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Luxury cars</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Camping</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Travel</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>4WD</label>
                                            </div>
                                        </li>
                                        <li class="col-md-3">
                                            <div class="input checkbox">
                                                <input type="checkbox" value="" />
                                                <label>Luxury cars</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>-->
                <!--                <div class="collapse-bar head-bar m-b-md m-t-sm" data-id="Subscription">
                                    <h3 class="pull-left">6. Subscription</h3>
                                </div>-->
                <!--                <div class="collapse-content">
                                    <div class="snippet-box m-b-md">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                                    </div>
                                    <div id="catalouge-items" class="owl-carousel catalouge-items">
                                        <a class="item link"><img src="css/img/banners/b_01.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_02.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_03.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_01.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_02.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_03.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_01.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_02.jpg"  alt="" title=""/></a>
                                        <a class="item link"><img src="css/img/banners/b_03.jpg"  alt="" title=""/></a>
                                    </div>
                
                                     / catalouge-items
                
                
                
                                </div>-->
                <div class="collapse-bar head-bar m-b-md m-t-sm"><h3 class="pull-left">7. Create account</h3></div>
                <div class="collapse-content">
                    <div class="row">
                        <!--                        <div class="col-md-6">
                                                    <div class="input checkbox">
                                                        <input type="checkbox">
                                                        <label>I want to receive offers by email</label>
                                                    </div>
                        
                                                    <div class="input checkbox">
                        
                                                        <input type="checkbox">
                                                        <label>Lorem ipsum dolor sit amet consectetur.</label>
                                                    </div>
                                                </div>-->
                        <div class="col-md-4 col-md-push-2">
                            <button class="m-t-lg btn btn-md btn-info btn-block" type="submit">CREATE MY ACCOUNT</button>
                        </div>
                        <div class="col-md-12 m-t-lg">
                            <!--                            <div class="m-t-xl text-sm">
                                                            <p>
                                                                <strong>Privacy statement</strong>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                                                        </div>-->
                        </div>
                    </div>
                </div>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- /collapse-section  --> 

        </div>
        <?php echo $this->element('Users.right_menu') ?>
    </div>
</div>
<?php
echo $this->Html->css(array('jquery-ui.datepicker'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui-1.9.2.core_picker.min', 'jquery.scrollTo'));
?>
<script type="text/javascript">
    var models = <?php echo json_encode($models) ?>;
    var serieses = <?php echo json_encode($serieses); ?>;
    var bodies = <?php echo json_encode($jsBodies); ?>;

//    var selInterestedModel = '<?php echo!empty($this->request->data['UserDetail']['interested_car']['model']) ? $this->request->data['UserDetail']['interested_car']['model'] : '' ?>';
//    var selInterestedSeries = '<?php echo!empty($this->request->data['UserDetail']['interested_car']['series']) ? $this->request->data['UserDetail']['interested_car']['series'] : '' ?>';

    var selYourModel = '<?php echo!empty($this->request->data['UserDetail']['your_car']['model']) ? $this->request->data['UserDetail']['your_car']['model'] : '' ?>';
    var selYourSeries = '<?php echo!empty($this->request->data['UserDetail']['your_car']['series']) ? $this->request->data['UserDetail']['your_car']['series'] : '' ?>';

    $(function () {
        if (self.location.hash != '') {
            hash = self.location.hash.substr(1, self.location.hash.length);
            console.log(hash);
            $.scrollTo($('div[data-id="' + hash + '"]'), {duration: 1000});
        }
        $('#UserDetailInterestedCarMake').on('change', function () {
            getChildList($('#UserDetailInterestedCarModel'), $(this).val(), 'models', 'interested')
        }).change();
        $('#UserDetailInterestedCarModel').on('change', function () {
            getChildList($('#UserDetailInterestedCarSeries'), $(this).val(), 'serieses', 'interested')
        }).change();

        $('#UserDetailYourCarMake').on('change', function () {
            getChildList($('#UserDetailYourCarModel'), $(this).val(), 'models', 'your_car')
        }).change();
        $('#UserDetailYourCarModel').on('change', function () {
            getChildList($('#UserDetailYourCarSeries'), $(this).val(), 'serieses', 'your_car')
        }).change();
        $('input[name="own_car"]').on('change', function () {
            $('#UserDetailOwnCar').val($(this).val());
            showOwnCarDiv($(this).val());
        });
        $('.hasDate').datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function showOwnCarDiv(val) {
        if (val == 1) {
            $('.own-car').slideDown();
        } else {
            $('.own-car').slideUp();
        }
    }

    function getChildList(element, data_id, type, type2) {
        if (data_id) {
            var data = window[type][data_id];

            element.html('<option value="">Model</option>');
            if (type == 'serieses') {
                element.html('<option value="">Series</option>');
            }

            if (data) {
                $.each(data, function (key, value) {
                    selected = '';
                    if (type2 == 'interested' && selInterestedSeries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                        selected = 'selected="selected"';
                    }
                    if (type2 == 'interested' && selInterestedModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                        selected = 'selected="selected"';
                    }

                    if (type2 == 'your_car' && selYourSeries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                        selected = 'selected="selected"';
                    }
                    if (type2 == 'your_car' && selYourModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                        selected = 'selected="selected"';
                    }

                    element.append('<option value="' + key + '" ' + selected + '>' + value + '</option>');
                });
            }
        }
    }
</script>