<div class="contents">
    <div class="container">
        <div class="col-md-8">
            <h1>Dashboard</h1>
            <?php
            echo $this->Form->create('User', array('id' => 'UserForm', 'url' => array('controller' => 'users', 'action' => 'profile')));
            echo $this->Form->input('id');
            echo $this->Form->hidden('UserDetail.own_car');
            echo $this->Form->input('UserDetail.id');
            ?> 
            <div class="collapse-section">
                <div class="collapse-bar head-bar m-b-md">
                    <h3 class="pull-left m-t-sm">1. Username</h3>
                    <a href="#Username" class="btn btn-xs pull-right btn-info m-b-xs editLink">EDIT</a> </div>
                <div class="collapse-content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table">
                        <tr>
                            <td width="35%">Username:</td>
                            <td><?php echo!empty($loggedUser['username']) ? $loggedUser['username'] : '' ?></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td>*******</td>
                        </tr>
                    </table>
                </div>
                <div class="collapse-content hide" id="Username">
                    <div class="row m-b-md">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('username', array()); ?>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('password', array('autocomplete' => 'off', 'value' => '')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('temppassword', array('type' => 'password', 'label' => 'Confirm Password')); ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-offset-8 col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-lg btn-block btn-info SaveLink">SAVE</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-lg btn-block btn-info cancelLink">CANCEL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /collapse-section  -->
            <div class="collapse-section">
                <div class="collapse-bar head-bar m-b-md">
                    <h3 class="pull-left m-t-sm">2. Your Profile</h3>
                    <a href="#Profile" class="btn btn-xs pull-right btn-info m-b-xs editLink">EDIT</a> </div>
                <div class="collapse-content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table">
                        <tr>
                            <td width="35%">First Name:</td>
                            <td><?php echo!empty($loggedUser['UserDetail']) ? $loggedUser['UserDetail']['first_name'] : '' ?></td>
                        </tr>
                        <tr>
                            <td>Last Name:</td>
                            <td><?php echo!empty($loggedUser['UserDetail']) ? $loggedUser['UserDetail']['last_name'] : '' ?></td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td><?php echo!empty($loggedUser['UserDetail']['address']) ? $loggedUser['UserDetail']['address'] : '' ?><br>
                                <?php echo!empty($loggedUser['UserDetail']) ? $loggedUser['UserDetail']['suburb'] : '' ?> <?php echo $loggedUser['UserDetail']['postcode'] ?></td>
                        </tr>
                        <tr>
                            <td>Email Address:</td>
                            <td><?php echo!empty($loggedUser['email']) ? $loggedUser['email'] : '' ?></td>
                        </tr>
                        <tr>
                            <td>Contact Number:</td>
                            <td><?php echo!empty($loggedUser['UserDetail']['telephone']) ? $loggedUser['UserDetail']['telephone'] : '' ?></td>
                        </tr>
                        <tr>
                            <td>Birthdate:</td>
                            <td><?php echo!empty($loggedUser['UserDetail']['birth_date']) ? date('d / m / Y', strtotime($loggedUser['UserDetail']['birth_date'])) : '' ?></td>
                        </tr>
                        <tr>
                            <td>License Type:</td>
                            <td><?php echo!empty($loggedUser['UserDetail']) ? $licenseTypes[$loggedUser['UserDetail']['license_type']] : '' ?></td>
                        </tr>
                    </table>
                </div>
                <div class="collapse-content hide" id="Profile">
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.first_name', array()); ?>

                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.last_name', array()); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.address', array('type' => 'text')); ?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('UserDetail.suburb', array()); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('UserDetail.postcode', array()); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('User.email'); ?>
                        </div>

                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.telephone', array('label' => 'Contact Number')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.birth_date', array('type' => 'text', 'class' => 'hasDate', 'label' => 'Birthdate')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('UserDetail.license_type', array('empty' => 'Select License type')); ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-offset-8 col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-lg btn-block btn-info SaveLink">SAVE</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-lg btn-block btn-info cancelLink">CANCEL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /collapse-section  -->

            <!--            <div class="collapse-section">
                            <div class="collapse-bar head-bar m-b-md">
                                <h3 class="pull-left m-t-sm">3. Cars You Are Interested In</h3>
                                <a href="#" class="btn btn-xs pull-right btn-info m-b-xs">EDIT</a> </div>
                            <div class="collapse-content">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table">
                                    <tr>
                                        <td width="35%">Cars you are interested in:</td>
                                        <td><p class="m-b-md">2014 Holden Cruze<br>
                                                JH series Equipe MY14<br>
                                                Sports Automatic<br>
            
                                            <p class="m-b-md">2014 Ford Territory<br>
                                                SZ TX <br>
                                                Squential Sports Shift<br>
                                            </p>
                                            <p class="m-b-md">2012 Nissan Skyline<br>
                                                CKV36 370GT<br>
                                                Sports Automatic</p></td>
                                    </tr>
                                </table>
                            </div>
                        </div>-->
            <!-- /collapse-section  -->
            <div class="collapse-section">
                <div class="collapse-bar head-bar m-b-md">
                    <h3 class="pull-left m-t-sm">4. Your Current Car</h3>
                    <a href="#YourCar" class="btn btn-xs pull-right btn-info m-b-xs editLink">EDIT</a> </div>
                <div class="collapse-content">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table">
                        <tr>
                            <td width="35%">Do you currently own a car?</td>
                            <td><?php echo!empty($loggedUser['UserDetail']['own_car']) ? 'Yes' : 'No' ?></td>
                        </tr>
                        <?php if (!empty($loggedUser['UserDetail']['own_car'])) { ?>
                            <tr>
                                <td>Your current car:</td>
                                <td>
                                    <?php echo isset($loggedUser['UserDetail']['your_car']['year']) ? $loggedUser['UserDetail']['your_car']['year'] : '' ?>
                                    <?php echo isset($loggedUser['UserDetail']['your_car']['make']) ? $loggedUser['UserDetail']['your_car']['make'] : '' ?>
                                    <?php echo isset($loggedUser['UserDetail']['your_car']['model']) ? $loggedUser['UserDetail']['your_car']['model'] : '' ?>
                                    <br>
                                    <?php echo isset($loggedUser['UserDetail']['your_car']['series']) ? $loggedUser['UserDetail']['your_car']['series'] : '' ?><br>
                                    <?php echo isset($loggedUser['UserDetail']['your_car']['body']) ? $loggedUser['UserDetail']['your_car']['body'] : '' ?> 
                                    <?php echo isset($loggedUser['UserDetail']['your_car']['transmission']) ? $loggedUser['UserDetail']['your_car']['transmission'] : '' ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <div class="collapse-content hide" id="YourCar">
                    <p>Do you currently own a car?</p>
                    <div class="m-b-lg">
                        <div class="input radio pull-left">
                            <input type="radio" name="own_car" value="1" <?php echo!empty($this->request->data['UserDetail']['own_car']) ? 'checked="checked"' : "" ?> />
                            <label>Yes</label>
                        </div>
                        <div class="input radio pull-left">
                            <input type="radio" name="own_car" value="0" <?php echo!empty($this->request->data['UserDetail']['own_car']) ? '' : 'checked="checked"' ?> />
                            <label>No</label>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="own-car" <?php echo!empty($this->request->data['UserDetail']['own_car']) ? '' : 'style="display: none;"' ?> >
                        <p>Do you currently own a car?</p>
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.make', array('label' => false, 'empty' => 'Make')); ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.model', array('label' => false, 'options' => false, 'empty' => 'Model')); ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.series', array('label' => false, 'options' => false, 'empty' => 'Series')); ?>
                            </div>

                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.fuel_type', array('label' => false, 'placeholder' => 'Fuel type')) ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.transmission', array('label' => false, 'options' => Car::$Transmissions, 'label' => false, 'empty' => 'Transmission')) ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.engine_size', array('label' => false, 'placeholder' => 'Engine Size')) ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.cylinders', array('label' => false, 'placeholder' => 'Cylinders')) ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.body', array('label' => false, 'options' => Car::getBodyTypesDropDown(), 'empty' => 'Body')); ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.doors', array('label' => false, 'placeholder' => 'Doors')) ?>
                            </div>

                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.colour', array('label' => false, 'placeholder' => 'Colour')) ?>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('UserDetail.your_car.year', array('label' => false, 'options' => range(date('Y') - 25, date('Y')), 'empty' => 'Year')); ?>
                            </div>

                        </div>
                        
                    </div>
                    <div class="row">
                            <div class="col-md-offset-8 col-md-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-lg btn-block btn-info SaveLink">SAVE</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-lg btn-block btn-info cancelLink">CANCEL</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- /collapse-section  -->

            <!--            <div class="collapse-section">
                            <div class="collapse-bar head-bar m-b-md">
                                <h3 class="pull-left m-t-sm">5. Lifestyle</h3>
                                <a href="<?php echo Router::url('/users/profile#LifeStyle') ?>" class="btn btn-xs pull-right btn-info m-b-xs">EDIT</a> </div>
                            <div class="collapse-content">
                                <div class="snippet-box m-b-md">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                                </div>
                                <ul class="row features-list">
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Camping</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Travel</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>4WD</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Luxury cars</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Camping</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Travel</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>4WD</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Luxury cars</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Camping</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Travel</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>4WD</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Luxury cars</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Camping</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Travel</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>4WD</label>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="input checkbox">
                                            <input type="checkbox" value="" />
                                            <label>Luxury cars</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>-->
            <!-- /collapse-section  -->

            <div class="collapse-section">
                <!--                <div class="collapse-bar head-bar m-b-md">
                                    <h3 class="pull-left m-t-sm">6. Subscriptions</h3>
                                    <a href="<?php echo Router::url('/users/profile#Subscription') ?>" class="btn btn-xs pull-right btn-info m-b-xs">EDIT</a> 
                                </div>
                                <div class="collapse-content">
                                    <div class="p-sm">
                                        <div class="row">
                                            <div class="col-md-4 m-b-md"><a href="#"><img src="css/img/banners/b_01.jpg" alt=""/></a></div>
                                            <div class="col-md-4 m-b-md"><a href="#"><img src="css/img/banners/b_02.jpg" alt=""/></a></div>
                                            <div class="col-md-4 m-b-md"><a href="#"><img src="css/img/banners/b_03.jpg" alt=""/></a></div>
                                            <div class="col-md-4 m-b-md"><a href="#"><img src="css/img/banners/b_01.jpg" alt=""/></a></div>
                                            <div class="col-md-4 m-b-md"><a href="#"><img src="css/img/banners/b_02.jpg" alt=""/></a></div>
                                            <div class="col-md-4 m-b-md"><a href="#"><img src="css/img/banners/b_03.jpg" alt=""/></a></div>
                                        </div>
                                    </div>-->
                <!--                    <div class="actions-bar">
                                        <div class="row">
                                            <div class="col-md-5"><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>" class="btn btn-lg btn-block btn-info">EDIT PROFILE</a></div>
                                            <div class="col-md-5"><a href="#" class="btn btn-lg btn-block btn-info">CANCEL</a></div>
                                        </div>
                                    </div>-->
                <!--                    <div class="m-b-lg m-t-lg">
                                        <p class="text-sm"><strong>Privacy statement</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                                    </div>-->
                <!--                </div>-->
            </div>
            <!-- /collapse-section  --> 
            <?php echo $this->Form->end(); ?>
        </div>
        <?php echo $this->element('Users.right_menu') ?>
    </div>
</div>
<?php
echo $this->Html->css(array('jquery-ui.datepicker'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui-1.9.2.core_picker.min', 'jquery.scrollTo'));
?>
<script type="text/javascript">
    var models = <?php echo json_encode($models) ?>;
    var serieses = <?php echo json_encode($serieses); ?>;
    var bodies = <?php echo json_encode($jsBodies); ?>;

//    var selInterestedModel = '<?php echo!empty($this->request->data['UserDetail']['interested_car']['model']) ? $this->request->data['UserDetail']['interested_car']['model'] : '' ?>';
    //    var selInterestedSeries = '<?php echo!empty($this->request->data['UserDetail']['interested_car']['series']) ? $this->request->data['UserDetail']['interested_car']['series'] : '' ?>';

    var selYourModel = '<?php echo!empty($this->request->data['UserDetail']['your_car']['model']) ? $this->request->data['UserDetail']['your_car']['model'] : '' ?>';
    var selYourSeries = '<?php echo!empty($this->request->data['UserDetail']['your_car']['series']) ? $this->request->data['UserDetail']['your_car']['series'] : '' ?>';

    $(function () {
        $('.editLink').on('click', function () {
            $id = $(this).attr('href');
            //            console.log($id);
            $(this).closest('.collapse-section').find('.collapse-content:first').hide();
            $.scrollTo($($(this).closest('.collapse-section').find('.collapse-bar')), {duration: 1000});
            $($id).removeClass('hide').show();
            return false;
        });
        $('.cancelLink').on('click', function () {
            $(this).closest('.collapse-section').find('.collapse-content:first').show();
            $($id).closest('.collapse-content').addClass('hide').slideUp();
            $.scrollTo($(this).closest('.collapse-section').find('.collapse-bar'), {duration: 1000});
             return false;
        });
        $('.SaveLink').on('click', function () {
            $('#UserForm').submit();
            return false;
        });
        
        if (self.location.hash != '') {
            hash = self.location.hash.substr(1, self.location.hash.length); //            console.log(hash);
            $.scrollTo($('div[data-id="' + hash + '"]'), {duration: 1000});
        }
        $('#UserDetailInterestedCarMake').on('change', function () {
            getChildList($('#UserDetailInterestedCarModel'), $(this).val(), 'models', 'interested')
        }).change();
        $('#UserDetailInterestedCarModel').on('change', function () {
            getChildList($('#UserDetailInterestedCarSeries'), $(this).val(), 'serieses', 'interested')
        }).change();

        $('#UserDetailYourCarMake').on('change', function () {
            getChildList($('#UserDetailYourCarModel'), $(this).val(), 'models', 'your_car')
        }).change();
        $('#UserDetailYourCarModel').on('change', function () {
            getChildList($('#UserDetailYourCarSeries'), $(this).val(), 'serieses', 'your_car')
        }).change();
        $('input[name="own_car"]').on('change', function () {
            $('#UserDetailOwnCar').val($(this).val());
            showOwnCarDiv($(this).val());
        });
        $('.hasDate').datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function showOwnCarDiv(val) {
        if (val == 1) {
            $('.own-car').slideDown();
        } else {
            $('.own-car').slideUp();
        }
    }

    function getChildList(element, data_id, type, type2) {
        if (data_id) {
            var data = window[type][data_id];

            element.html('<option value="">Model</option>');
            if (type == 'serieses') {
                element.html('<option value="">Series</option>');
            }

            if (data) {
                $.each(data, function (key, value) {
                    selected = '';
                    if (type2 == 'interested' && selInterestedSeries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                        selected = 'selected="selected"';
                    }
                    if (type2 == 'interested' && selInterestedModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                        selected = 'selected="selected"';
                    }

                    if (type2 == 'your_car' && selYourSeries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                        selected = 'selected="selected"';
                    }
                    if (type2 == 'your_car' && selYourModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                        selected = 'selected="selected"';
                    }

                    element.append('<option value="' + key + '" ' + selected + '>' + value + '</option>');
                });
            }
        }
    }
</script>