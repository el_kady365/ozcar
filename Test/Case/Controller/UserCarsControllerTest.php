<?php
require_once 'G:\xampp1\htdocs\cake224\lib\Cake\Core\App.php';
require_once 'G:\xampp1\htdocs\cake224\lib\Cake\TestSuite\CakeTestCase.php';
App::uses('UserCarsController', 'Controller');

/**
 * UserCarsController Test Case
 *
 */
class UserCarsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_car',
		'app.car',
		'app.make',
		'app.car_model',
		'app.dealer',
		'app.dealer_location',
		'app.car_image',
		'app.user'
	);

}
