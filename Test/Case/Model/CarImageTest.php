<?php
require_once '/var/www/html/cake224/lib/Cake/Core/App.php';
require_once '/var/www/html/cake224/lib/Cake/TestSuite/CakeTestCase.php';
App::uses('CarImage', 'Model');

/**
 * CarImage Test Case
 *
 */
class CarImageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.car_image',
		'app.car',
		'app.make',
		'app.car_model',
		'app.dealer',
		'app.dealer_location'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CarImage = ClassRegistry::init('CarImage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CarImage);

		parent::tearDown();
	}

}
