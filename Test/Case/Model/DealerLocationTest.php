<?php
require_once '/var/www/html/cake224/lib/Cake/Core/App.php';
require_once '/var/www/html/cake224/lib/Cake/TestSuite/CakeTestCase.php';
App::uses('DealerLocation', 'Model');

/**
 * DealerLocation Test Case
 *
 */
class DealerLocationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.dealer_location',
		'app.dealer',
		'app.car',
		'app.make',
		'app.model'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DealerLocation = ClassRegistry::init('DealerLocation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DealerLocation);

		parent::tearDown();
	}

}
