<?php
require_once '/var/www/html/cake224/lib/Cake/Core/App.php';
require_once '/var/www/html/cake224/lib/Cake/TestSuite/CakeTestCase.php';
App::uses('Dealer', 'Model');

/**
 * Dealer Test Case
 *
 */
class DealerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.dealer',
		'app.main_location',
		'app.car',
		'app.make',
		'app.model',
		'app.dealer_location'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Dealer = ClassRegistry::init('Dealer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Dealer);

		parent::tearDown();
	}

}
