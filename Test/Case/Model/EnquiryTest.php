<?php
require_once '/var/www/html/cake224/lib/Cake/Core/App.php';
require_once '/var/www/html/cake224/lib/Cake/TestSuite/CakeTestCase.php';
App::uses('Enquiry', 'Model');

/**
 * Enquiry Test Case
 *
 */
class EnquiryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.enquiry',
		'app.dealership',
		'app.dealer_location',
		'app.dealer',
		'app.car',
		'app.make',
		'app.car_model',
		'app.car_image',
		'app.type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Enquiry = ClassRegistry::init('Enquiry');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Enquiry);

		parent::tearDown();
	}

}
