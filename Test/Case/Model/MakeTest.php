<?php
require_once '/var/www/html/cake224/lib/Cake/Core/App.php';
require_once '/var/www/html/cake224/lib/Cake/TestSuite/CakeTestCase.php';
App::uses('Make', 'Model');

/**
 * Make Test Case
 *
 */
class MakeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.make',
		'app.car',
		'app.model',
		'app.dealer',
		'app.main_location',
		'app.dealer_location'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Make = ClassRegistry::init('Make');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Make);

		parent::tearDown();
	}

}
