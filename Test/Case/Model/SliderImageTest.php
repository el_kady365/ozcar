<?php
require_once '/var/www/html/cake224/lib/Cake/Core/App.php';
require_once '/var/www/html/cake224/lib/Cake/TestSuite/CakeTestCase.php';
App::uses('SliderImage', 'Model');

/**
 * SliderImage Test Case
 *
 */
class SliderImageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.slider_image',
		'app.slider'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SliderImage = ClassRegistry::init('SliderImage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SliderImage);

		parent::tearDown();
	}

}
