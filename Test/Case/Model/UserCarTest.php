<?php
require_once 'G:\xampp1\htdocs\cake224\lib\Cake\Core\App.php';
require_once 'G:\xampp1\htdocs\cake224\lib\Cake\TestSuite\CakeTestCase.php';
App::uses('UserCar', 'Model');

/**
 * UserCar Test Case
 *
 */
class UserCarTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_car',
		'app.car',
		'app.make',
		'app.car_model',
		'app.dealer',
		'app.dealer_location',
		'app.car_image',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserCar = ClassRegistry::init('UserCar');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserCar);

		parent::tearDown();
	}

}
