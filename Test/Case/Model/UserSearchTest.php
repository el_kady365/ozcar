<?php
require_once 'G:\xampp1\htdocs\cake224\lib\Cake\Core\App.php';
require_once 'G:\xampp1\htdocs\cake224\lib\Cake\TestSuite\CakeTestCase.php';
App::uses('UserSearch', 'Model');

/**
 * UserSearch Test Case
 *
 */
class UserSearchTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_search',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserSearch = ClassRegistry::init('UserSearch');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserSearch);

		parent::tearDown();
	}

}
