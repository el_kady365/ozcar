<?php
/**
 * CarFixture
 *
 */
class CarFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'make_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'car_model_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'year' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4),
		'badge' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'series' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'body' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'engine_capacity' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'cylinders' => array('type' => 'integer', 'null' => true, 'default' => null),
		'fuel_type' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'doors' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'stock_number' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'rego' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'vin_number' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dealer_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'odometer' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'colour' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'trim' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'feature_code' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'selling_price' => array('type' => 'float', 'null' => true, 'default' => null),
		'comments' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dealer_location_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'compliance_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'redbook_code' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'make_id' => 1,
			'car_model_id' => 1,
			'year' => 1,
			'badge' => 'Lorem ipsum dolor sit amet',
			'series' => 'Lorem ipsum dolor sit amet',
			'body' => 'Lorem ipsum dolor sit amet',
			'engine_capacity' => 'Lorem ipsum dolor sit amet',
			'cylinders' => 1,
			'fuel_type' => 'Lorem ipsum dolor sit amet',
			'doors' => 'Lorem ipsum dolor sit amet',
			'stock_number' => 'Lorem ipsum dolor sit amet',
			'rego' => 'Lorem ipsum dolor sit amet',
			'vin_number' => 'Lorem ipsum dolor sit amet',
			'dealer_id' => 1,
			'odometer' => 'Lorem ipsum dolor sit amet',
			'colour' => 'Lorem ipsum dolor sit amet',
			'trim' => 'Lorem ipsum dolor sit amet',
			'feature_code' => 'Lorem ipsum dolor sit amet',
			'selling_price' => 1,
			'comments' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'dealer_location_id' => 1,
			'compliance_date' => '2015-04-30',
			'redbook_code' => 'Lorem ipsum dolor sit amet',
			'created' => '2015-04-30 14:03:20',
			'modified' => '2015-04-30 14:03:20'
		),
	);

}
