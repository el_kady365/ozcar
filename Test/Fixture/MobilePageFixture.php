<?php
/**
 * MobilePageFixture
 *
 */
class MobilePageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'default' => null, 'key' => 'index', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'permalink' => array('type' => 'string', 'null' => false, 'default' => null, 'key' => 'unique', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'content' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'keywords' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 500, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'template_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'menu_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'main_nav' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'add_to_main_menu' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'is_url' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'new_window' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'access' => array('type' => 'integer', 'null' => true, 'default' => '0', 'comment' => '0=>Public,1=>members only,2=>Both'),
		'attachments' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'active' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'display_order' => array('type' => 'integer', 'null' => true, 'default' => null),
		'default' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'has_dynamic_children' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'dynamic_children' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'type' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'id_on_table' => array('type' => 'integer', 'null' => true, 'default' => null),
		'add_enquire_form' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'form_title' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'form_admin_email' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'banner_gallery' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'default_sidebar' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'add_forms' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'forms_description' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'add_resources' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'resources_description' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'add_break' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'perma_link' => array('column' => 'permalink', 'unique' => 1),
			'fk_gallery' => array('column' => 'banner_gallery', 'unique' => 0),
			'search' => array('column' => array('title', 'content'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'permalink' => 'Lorem ipsum dolor sit amet',
			'content' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'keywords' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'template_id' => 1,
			'menu_id' => 1,
			'parent_id' => 1,
			'main_nav' => 1,
			'add_to_main_menu' => 1,
			'url' => 'Lorem ipsum dolor sit amet',
			'is_url' => 1,
			'new_window' => 1,
			'access' => 1,
			'attachments' => 'Lorem ipsum dolor sit amet',
			'active' => 1,
			'display_order' => 1,
			'default' => 1,
			'has_dynamic_children' => 1,
			'dynamic_children' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => '2013-11-19 09:30:00',
			'modified' => '2013-11-19 09:30:00',
			'type' => 'Lorem ipsum dolor sit amet',
			'id_on_table' => 1,
			'add_enquire_form' => 1,
			'form_title' => 'Lorem ipsum dolor sit amet',
			'form_admin_email' => 'Lorem ipsum dolor sit amet',
			'banner_gallery' => 1,
			'default_sidebar' => 1,
			'add_forms' => 1,
			'forms_description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'add_resources' => 1,
			'resources_description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'add_break' => 1
		),
	);

}
