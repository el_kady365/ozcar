<?php
class Ftp{
    protected $conn;
    public $isConnected = false;
    
    public function __construct($url){
        ($this->conn = ftp_connect($url) ) and ( $this->isConnected = true ); 
    }
   
    public function __call($func,$a){
        if(function_exists('ftp_' . $func)){
            array_unshift($a,$this->conn);
            return call_user_func_array('ftp_' . $func,$a);
        }else{            
            return false ; //("$func is not a valid FTP function");
        }
    }
}
?>

