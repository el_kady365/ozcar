<?php
class SSH{
    protected $conn;
    public $isConnected = false;
    
    public function __construct($url){
        ($this->conn = ssh2_connect($url) ) and ( $this->isConnected = true ); 
    }
   
    public function __call($func,$a){
        if(function_exists('ssh2_' . $func)){
            array_unshift($a,$this->conn);
            return call_user_func_array('ssh2_' . $func,$a);
        }else{            
            return false ; //("$func is not a valid FTP function");
        }
    }
    
    public function chmod($perms, $file){
        return $this->exec('chmod ' . escapeshellarg($perms) . ' ' . escapeshellarg($file));
    }
    
    public function login($user, $password){
        return ssh2_auth_password($this->conn, $user, $password);
    }
}
?>

