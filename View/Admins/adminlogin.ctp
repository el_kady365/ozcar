<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Administrator Login</title>
        <meta name="keywords" content="keywords, keywords, keywords," />
        <meta name="description" content="Description." />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php echo $this->Html->css(array('admin_login/login', 'notifications')); ?>
        <?php echo $this->Html->script(array('admin_login/jquery', 'admin_login/infieldlabel.min')); ?>
    </head>
    <body style="background:#FFF;">
        <div class="st-login">
            <div class="st-logo"><a href="http://silvertrees.net/" target="_blank"><img src="<?= Router::url('/css/admin_login/login/silvertrees-logo.jpg') ?>" alt="" title="" /></a></div>
            <h2 class="sec-title">Administrator Login:</h2>
            <div class="st-globle">
                <div class="wa-ico">website admin</div>
            </div>
            <?php echo $this->Session->flash(); ?>
                                                <?php echo $this->Session->flash('AdminLogin'); ?>

            <div class="st-box">
                <div class="box-snippets">Please enter your administrator username below to login to your account and manage your site. </div>

                <div class="st-form">		
                    <?php
                    echo $this->Form->create('Admin', array('url' => '/admins/admin' ));
                    echo $this->Form->input('name', array('div' => array('autocomplete'=>'off','class' => 'input text field-labels'), 'class' => 'INPUT required', 'label' => 'Name'));
                    echo $this->Form->input('password', array('div' => array('autocomplete'=>'off','class' => 'input text field-labels'), 'class' => 'INPUT required', 'label' => 'Password'));
                    ?>
                     
                    </div>
                    <div class="submit">
                    <!--<a class="forget-pwd" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'forgot', 'admin')); ?>" >Forgot Password?</a>-->
                        <button class="right" type="submit"><span class="submit-btn"><span>Login</span></span></button>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>
