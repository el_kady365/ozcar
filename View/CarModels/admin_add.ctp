<div class="carModels form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'CarModel' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('name' , array('class' => 'input' ) );
		echo $this->Form->input('make_id' , array('class' => 'input' ) );
		echo $this->Form->input('active' , array('class' => 'input' ) );

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
