<div class="carModels index">
    <a style="float:right" href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-success btn-sm">
        <span class="st-ico st-icon-plus-2" aria-hidden="true"></span>+ Add New Model
    </a>
    <div class="clear"></div>
    <h2><?php echo __('Car Models'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    ?>
    <?php
    $fields = array(
        
        'CarModel.name' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Make.name' => array("title" => "Make") , 

//		'CarModel.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'CarModel.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
   

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $carModels, false, false, false);
    ?>
</div>
