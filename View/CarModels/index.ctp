<div class="carModels index">
	<h2><?php echo __('Car Models'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Name' ; ?></th>
	            <th><?php echo 'Make Id' ; ?></th>
	            <th><?php echo 'Active' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($carModels as $carModel): ?>
	<tr>
		<td><?php echo h($carModel['CarModel']['id']); ?>&nbsp;</td>
		<td><?php echo h($carModel['CarModel']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($carModel['Make']['name'], array('controller' => 'makes', 'action' => 'view', $carModel['Make']['id'])); ?>
		</td>
		<td><?php echo h($carModel['CarModel']['active']); ?>&nbsp;</td>
		<td><?php echo h($carModel['CarModel']['created']); ?>&nbsp;</td>
		<td><?php echo h($carModel['CarModel']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
