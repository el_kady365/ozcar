<?php
$modelClass = 'Car';
echo $this->Form->create($modelClass, array('type' => 'file'));
?>
<div class="cars form ExtendedForm">
    <div class="form-wrap">
        <?php echo $this->Html->script('many-add-delete', array('inline' => false)); ?>
        <div class="form-section">
            <h2 class="section-heading">1. Images</h2>
            <p>ADD IMAGES OF YOUR CAR | MAX SIZE EACH: <?php echo $car_file_settings['image']['max_file_size'] ?></p>

            <?php
            $car_images = !empty($this->data['CarImage']) ? $this->data['CarImage'] : array(array());
            ?>
            <div class="upload-boxs page_tabs many-container">
                <ul id="page_tabs" class="many upload-boxs inline-block">
                    <?php
                    foreach ($car_images as $i => $car_image) :
                        debug($car_image['image']);
                        ?>
                        <li class="record">
                            <?php echo $this->Form->input("CarImage.$i.id"); ?>
                            <div class="upload-box" <?php
                            if (!empty($car_image['image'])) {
                                $label = "";
                                ?>
                                     style="background-image:url('<?php echo get_resized_image_url($car_image['image'], 85, 85, 1) ?>');background-repeat: no-repeat;"
                                     <?php
                                 } else {
                                     $label = "+";
                                 }
                                 ?>>

                                <?php
                                echo $this->Form->input("CarImage.$i.image", array('div' => false, 'label' => $label, 'type' => 'file'));
                                ?>
                                <div class="uploadbox-btns">
                                    <a href="#" class="btn btn-default btn-xs delete-item" > <i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>


                        </li>
<?php endforeach; ?>



                </ul>
                <ul class="inline-block">
                    <li>
                        <div class="upload-box new-record">
                            <a href="#" class="add" title="Add New Car Image"><i class="fa fa-plus"></i></a>
                        </div>
                    </li>
                </ul>


            </div>



            <!--            <ul class="upload-boxs">
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio clip-radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                        </ul>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio "><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>
                            <li>
                                <div class="upload-box">+<input type="file" /></div>
                                <div class="input radio"><input type="radio" /><label>Main Image</label></div>
                            </li>-->
            <!--            </ul>-->
        </div>
        <!-- / form-section  -->
        <div class="form-section">
            <h2 class="section-heading">2. Car Details</h2>
            <div class="row">
                <div class="col-md-4"> <?php echo $this->Form->input('make', array("id" => "make_id", "empty" => "Select Make")); ?></div>
                <div class="col-md-4"> <?php echo $this->Form->input('model', array("options" => null, "id" => "model_id", "empty" => "Select Model")); ?></div>
                <div class="col-md-4"> <?php echo $this->Form->input('series'); ?></div>
            </div>

            <div class="row">
                <div class="col-md-2"> <?php echo $this->Form->input('year'); ?></div>
                <div class="col-md-2">
<?php echo $this->Form->input('odometer', array('label' => 'KILOMETERS')); ?>
                    <!--<div class="input text"><label>KILOMETERS</label><input type="text" /></div>-->
                </div>
                <div class="col-md-4"> <?php echo $this->Form->input('colour'); ?></div>
                <div class="col-md-4"> <?php echo $this->Form->input('transmission'); ?></div>
                <!--                <div class="col-md-4 col-md-push-8">
                                    <div class="input text"><a href="#" class="btn btn-block btn-md btn-primary text-center">+ ADD ADVANCED DETAILS</a></div>
                                </div>-->
            </div>
        </div>

        <!-- / form-section  -->
        <!--        <div class="form-section">
                    <h2 class="section-heading">3. Features</h2>
                    <div class="form-group clearfix">
                        <div class="input radio pull-left"><input type="radio" checked="checked" /><label>JUST ADD THEM FOR ME</label></div>
                        <div class="input radio pull-left"><input type="radio" /><label>LET ME DO IT MANUALLY</label></div>
                    </div>
                     / form-group  
        
        
                    <div class="dual-box-select">
                        <select id="source"  data-search="Search for options"  data-text="ADD FEATURES FROM LIST BELOW">
                            <option value="option_1">Audio - Input for iPod
                            <option value="option_2">Audio - MP3 Decoder</option>
                            <option value="option_3">Inbuilt Flash Drive</option>
                            <option value="option_4">Bluetooth System</option>
                            <option value="option_5">CD Player</option>
                            <option value="option_6">9 Speaker Stereo</option>
                            <option value="option_7">Data Logging</option>
                            <option value="option_8">Airbags - Driver & Passenger (Dual)</option>
                            <option value="option_9">Airbags - Head for 1st Row Seats (Front)</option>
                            <option value="option_11">Airbags - Head for 2nd Row Seats</option>
                            <option value="option_12">Airbags - Side for 1st Row Occupants (Front)</option>
                            <option value="option_13">Seatbelt - Pretensioners 1st Row (Front)</option>
                            <option value="option_14">Seatbelt - Load Limiters 1st Row (Front)</option>
                            <option value="option_15">9 Speaker Stereo</option>
                            <option value="option_16">Data Logging</option>
                        </select>
                        <select id="destination" data-text="YOUR CARS FEATURES"  data-search="Search for options">
                        </select>
                    </div>
                </div>-->
        <!-- / dual-box-select  --> 

        <!--        <div class="form-section">
                    <h2 class="section-heading">4. Description</h2>
                    <div class="form-group">
                        <div class="input textarea">
                            <label>ENTER A BRIEF DESCRIPTION ABOUT THE CAR</label>
                            <textarea></textarea>
                        </div>
                    </div>
                </div>-->
        <!-- / form-group  -->


        <!--        <div class="form-section">
                    <h2 class="section-heading">5. Extras</h2>
                    <div class="form-group clearfix">
                        <div class="input radio pull-left"><input type="radio" checked="checked" /><label>ADD EXTRAS</label></div>
                        <div class="input radio pull-left"><input type="radio" /><label>NO EXTRAS</label></div>
                    </div>
                     / form-group  
        
                    <div class="dual-box-select">
                        <select id="source2"  data-search="Search for options"  data-text="ADD FEATURES FROM LIST BELOW">
                            <option value="option_1">Audio - Input for iPod
                            <option value="option_2">Audio - MP3 Decoder</option>
                            <option value="option_3">Inbuilt Flash Drive</option>
                            <option value="option_4">Bluetooth System</option>
                            <option value="option_5">CD Player</option>
                            <option value="option_6">9 Speaker Stereo</option>
                            <option value="option_7">Data Logging</option>
                            <option value="option_8">Airbags - Driver & Passenger (Dual)</option>
                            <option value="option_9">Airbags - Head for 1st Row Seats (Front)</option>
                            <option value="option_11">Airbags - Head for 2nd Row Seats</option>
                            <option value="option_12">Airbags - Side for 1st Row Occupants (Front)</option>
                            <option value="option_13">Seatbelt - Pretensioners 1st Row (Front)</option>
                            <option value="option_14">Seatbelt - Load Limiters 1st Row (Front)</option>
                            <option value="option_15">9 Speaker Stereo</option>
                            <option value="option_16">Data Logging</option>
                        </select>
                        <select id="destination2" data-text="YOUR CARS FEATURES"  data-search="Search for options">
                        </select>
                    </div>
                </div>-->
        <!-- / dual-box-select  --> 

        <!--        <div class="form-actions text-right">
                    <button type="submit" class="btn btn-md btn-shape">SAVE AS TEMPLATE</button>
                    <button type="submit" class="btn btn-md btn-default">CREATE LISTING</button>
                </div>-->

<?php echo $this->Form->input('id', array('class' => 'input required')); ?>
        <div class="row">
            <div class="col-md-4"> <?php echo $this->Form->input('dealer_id', array("empty" => "Select Dealer", "id" => "dealer_id")); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('dealer_location_id', array("options" => null, "id" => "dealer_location_id", "empty" => "Select Dealer Location")); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('type_id', array("options" => Car::$types, "empty" => "Select Type")); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('badge'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('body'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('engine_capacity'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('cylinders'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('fuel_type'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('doors'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('stock_number'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('rego'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('vin_number'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('odometer'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('trim'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('feature_code'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('selling_price'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('compliance_date', array('class' => 'input calendar', "type" => "text")); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('redbook_code'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('size'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('month'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('newPr'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('width'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('height'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('length'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('seats'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('turncir'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('kerb_weight'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('steering'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('front_tyres'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('rear_tyres'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('wheelbase'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('front_brake'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('rear_brake'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('towing_capacity_braked'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('towing_capacity_unbraked'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('drive_type'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('power'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('torque'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('boreStroke'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('compression_ratio'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('service_months'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('service_kms'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('warranty'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('warranty_kms'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('registration_expiry'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('registration',array('label'=>'Registration plate')); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('trim_colour'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('build_date'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('price_excl_govt'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('ancap_rating'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('green_star_rating'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('roadworthy_cert'); ?></div>
            <div class="col-md-4"> <?php echo $this->Form->input('fuel_economy'); ?></div>
            <div class="col-md-12"> <?php echo $this->Form->input('comments', array('class' => 'input editor')); ?></div>

        </div>
        <?
// 		echo $this->Form->enableAjaxUploads();
//        echo $this->Form->enableEditors('textarea.editor');

        echo $this->Form->end(__('Submit'));
        ?>
    </div>

</div>
<?php
$this->Html->css(array('jquery-ui.datepicker'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui-1.9.2.core_picker.min'));
?>
<script type="text/javascript" >
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).closest('.upload-box').css({
                    'background-image': 'url(' + e.target.result + ')',
                    'background-repeat': 'no-repeat',
                    'background-position': 'center'
                });
                $(input).closest('.upload-box').find('label').text('');
//                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', 'input[type="file"]', function () {
        readURL(this);
    });

    function getLocations(obj) {


        var $this = obj;
        if ($this.val()) {
            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: '<?php echo $this->Html->url(array("controller" => "dealers", "action" => "getLocations")) ?>/' + $this.val(),
                success: function (data) {
                    console.log(data);
                    $('#dealer_location_id').html("<option>Select Dealer Location</option>");
                    $.each(data, function (key, value) {
                        $('#dealer_location_id')
                                .append($("<option></option>")
                                        .attr("value", key)
                                        .text(value));
                    });


                }});
        } else {
            $('#dealer_location_id').val(null);
        }
    }

    function getModels(obj) {


        var $this = obj;
        if ($this.val()) {
            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: '<?php echo $this->Html->url(array("controller" => "makes", "action" => "getModels", 'admin' => false)) ?>/' + $this.val(),
                success: function (data) {
                    console.log(data);
                    $('#model_id').html("<option>Select Model</option>");
                    $.each(data, function (key, value) {
                        $('#model_id')
                                .append($("<option></option>")
                                        .attr("value", key)
                                        .text(value));
                    });


                }});
        } else {
            $('#model_id').val(null);
        }
    }
    $(document).ready(function () {
<?php if (!empty($_SESSION["dealer"])): ?>

    <?php if ($this->action != "admin_add"): ?>
                getLocations($('#dealer_id'));
                getModels($('#make_id'));
    <?php endif; ?>
<?php endif; ?>
        if ($('#dealer_location_id').val() == null || $('#dealer_location_id').val() == '') {

        }
        if ($('#model_id').val() == null || $('#model_id').val() == '') {

        }

        $('.calendar').datepicker({
            dateFormat: "yy-mm-dd"
        });
        $('#dealer_id').on("change", function () {
            $('#dealer_location_id').val(null);

            getLocations($('#dealer_id'));

        });

        $('#make_id').on("change", function () {
            $('#model_id').val(null);

            getModels($('#make_id'));

        });
    });
</script>
