
<script type="text/javascript">
    $(document).ready(function () {
        checkTemplate();
    });
    function checkTemplate() {
        if ($("#CarTemplateId").val() == null || $("#CarTemplateId").val() == '') {
            $('#FileDetails').fadeIn(500);
            $('#CarFile').parent().find('label').text(function (name, value) {
                return value.replace(/\d+/, 4);
            });
            $("#CarStep").val("step2");
        } else {
            $('#FileDetails').fadeOut(500);
            $('#CarFile').parent().find('label').text(function (name, value) {
                return value.replace(/\d+/, 2);
            });
            $("#CarStep").val("step3");
        }

    }

</script>
<div class="cars form ExtendedForm">
    <div class="form-wrap">
        <div class="row">
            <div class="col-md-6"><h2><?php echo __("Upload Batch Vehicle"); ?></h2></div>
            <div class="col-md-6">
                <div class="upload-progress pull-right">
                    <p>Vehicle upload progress</p>
                    <div class="progress-bar">
                        <div class="percent" style="width:0%"></div>
                    </div>
                    <p class="text-center" >0% Complete</p>
                </div>
                <!-- -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <?php
                echo $this->Form->create('Car', array('type' => 'file', "action" => "importing_data"));
                // dealer
                ?>
                <div class="input select">
                    <h3 class="m-t-md m-b-md">
                        <label>1. Select Template</label>
                    </h3>
                    <?php
                    echo $this->Form->input("template_id", array('label' => false, 'div' => false, 'class' => 'INPUT', "onchange" => "checkTemplate()", "empty" => "No template", "type" => "select"));
                    ?>
                </div>
                <div id="FileDetails">
                    <div class="input select">
                        <h3 class="m-t-md m-b-md">
                            <label>2. Select Dealer</label>
                        </h3>
                        <?php
                        echo $this->Form->input("dealer_id", array('label' => false, 'div' => false, 'class' => 'INPUT', "empty" => "Select Dealer", "value" => !empty($_SESSION["dealer"]) ? $_SESSION["dealer"]["Dealer"]["id"] : "", "type" => !empty($_SESSION["dealer"]) ? "hidden" : "select"));
                        ?>
                    </div>

                    <div class="input select">
                        <h3 class="m-t-md m-b-md">
                            <label>3. Setup File Import details</label>
                        </h3>

                        <?php
                        echo $this->Form->input("delimiter", array('label' => false, 'div' => false, 'class' => 'INPUT', "type" => "select", "options" => array("," => "Comma Separated ( , ) ", ";" => "Semi Column Separated ( ; )", "\t" => "Tab Delimited")));
                        ?>
                    </div>
                    <?php
                    echo $this->Form->input("import_first_row", array('type' => 'checkbox', 'class' => 'INPUT', 'div' => array('class' => 'input radio m-t-m\'s')));
                    echo $this->Form->input("type_id", array('class' => 'INPUT', 'label' => 'Cars Type', "type" => "select", "options" => Car::$types));
                    ?>
                </div>
                <?php
                echo $this->Form->input("step", array('class' => 'INPUT', "type" => "hidden"));
                echo $this->Form->input("displayname", array('class' => 'INPUT', "type" => "hidden"));
                echo $this->Form->input("user_time", array('class' => 'INPUT', "type" => "hidden", "value" => time() . "_" . rand("100000", "1000000")));
                echo $this->Form->input("filename", array('class' => 'INPUT', "type" => "hidden"));
                ?>
                <div class="input select">
                    <h3 class="m-t-md m-b-md">
                        <label>4. Upload File</label>
                    </h3>
                    <?php
                    echo $this->Form->input("file", array('label' => false, 'div' => false, 'class' => 'INPUT', "multiple" => "multiple", "type" => "file"));
                    ?>
                </div>
                <?php
                echo $this->Form->submit(__('Next Step', true), array('class' => 'Submit'));
                echo $this->Form->end();
                ?>

            </div>
        </div>

    </div>
</div>
