
<script>
    $(document).ready(function(){
        checkUni();
    });
    function checkUni(){
        
        if($("#SheetFileType").val()=="pdf"){
        $("#SheetDelimiter").parent().fadeOut(500);
        }else{
            $("#SheetDelimiter").parent().fadeIn(500); 
            
        }
 
    }
   
</script>   
<div class="FormExtended">
    <h1><?php __("Import Data"); ?></h1>
    <?php
    echo $form->create('Sheet', array('type' => 'file', "action" => "step1"));
    echo $form->input("file_type", array('class' => 'INPUT', "type" => "select","onchange"=>"checkUni()" ,"options" => array("csv" => "CSV File ", "pdf" => "PDF (Time Slots for StoraEnso)")));
    echo $form->input("template", array('class' => 'INPUT',"empty"=>"Please Select"));
    echo $form->input("delimiter", array('class' => 'INPUT', "type" => "select", "options" => array("," => "Comma Separated ( , ) ", "\t" => "Tab Delimited")));
    echo $form->input("step", array('class' => 'INPUT', "type" => "hidden"));
    echo $form->input("displayname", array('class' => 'INPUT', "type" => "hidden"));
    echo $form->input("user_time", array('class' => 'INPUT', "type" => "hidden","value"=>  time()));
    echo $form->input("filename", array('class' => 'INPUT', "type" => "hidden"));
    echo $form->input("file", array('class' => 'INPUT', "type" => "file"));
    echo $form->submit(__('Next Step', true), array('class' => 'Submit'));
    echo $form->end();
    ?>


    <div id="xx"></div>
</div>
