
<script>
    function checkUni_() {
        var values = new Array();
        var flag = true;
        $("select").each(function() {
            if (values[$(this).val()] && $(this).val() != "") {
                $(this).parent().append('<div class="error-message">Selected before</div>');
                flag = false;
            }
            if ($(this).val() == "") {
               // $(this).parent().append('<div class="error-message">You didn\'t select this field</div>');
                
            }
            else
            values[$(this).val()] = 1;
        });
        if (flag) {
            document.getElementById("CarImportingDataForm").submit();
        }
    }
    
    
    $(function(){
        $('#save_template').click(function(){
             if($(this).is(':checked'))
        {
            $('#template_name').show();
        }
        else
            
            $('#template_name').hide();
        })
    })
   
    function checkUni() {
        if ($(".error-message").length) {
            $(".error-message").fadeOut(500, function() {
                $(".error-message").remove();
                checkUni_();
            });
        } else {
            $(".error-message").remove();
            checkUni_();
        }
    }

</script>
<div class="FormExtended">
    <div class="row">
    <div class="col-md-6"><h2><?php echo __("Upload Batch Vehicle"); ?></h2></div>
	<div class="col-md-6">
    	<div class="upload-progress pull-right">
        <p>Vehicle upload progress</p>
        <div class="progress-bar">
        	<div class="percent" style="width:50%"></div>
         </div>
        <p class="text-center" >50% Complete</p>
        </div>
        <!-- -->
    </div>
    </div>
    <?php
    echo $this->Form->create('Car', array("action" => "importing_data"));
    ?>
    <table border="1" cellpadding="0" cellspacing="0" class="listing-table import-table" width="100%">
        <tr>
            <td>
                <h3><?php echo $this->data["Car"]["displayname"] ?></h3>
            </td>
            <td>
                <h3>Car Data</h3>
            </td>
        </tr>

        <? foreach ($columns as $key => $value) : ?>
            <tr>
                <td>
                    <b><?php echo $value ?></b>
                </td>
                <td>
                    <?php echo $this->Form->input("Field.".$key, array('class' => 'INPUT', 'empty' => true, "type" => "select", "options" => $fields_key, "label" => "")); ?>
                </td>
            </tr>

        <?php endforeach; ?>
            
        <?php
        echo $this->Form->input("displayname", array('class' => 'INPUT', "type" => "hidden"));
        echo $this->Form->input("step", array('class' => 'INPUT', "type" => "hidden"));
        echo $this->Form->input("filename", array('class' => 'INPUT', "type" => "hidden"));
        echo $this->Form->input("delimiter", array('class' => 'INPUT', "type" => "hidden"));
        echo $this->Form->input("dealer_id", array('class' => 'INPUT', "type" => "hidden"));
        echo $this->Form->input("import_first_row", array('class' => 'INPUT', "type" => "hidden"));
        echo $this->Form->input("type_id", array('class' => 'INPUT', "type" => "hidden"));
        ?>
            <tr>
            <td colspan="2">
                
                    <div class="form-group">
                            <input name="save_as_template"  type="checkbox" id="save_template"    value="1"  />
                            <label for="save_template" >Save As Template</label>
                    </div>
                 
                    
                    <div style="display:none;"   id="template_name">
                    <?php echo $this->Form->input("template_name", array('class' => 'INPUT',  "type" => "text")); ?>
                    </div>
              

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="submit">
                    <div class="actions">
                        <input type="button" onclick="checkUni();" value="Import"  class="btn btn-lg btn-success" />
                    </div>
                </div>
               

            </td>
        </tr>
    </table>
    <?php
    echo $this->Form->end();
    ?>
</div>



