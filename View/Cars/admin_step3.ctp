

<div class="FormExtended">
	<div class="row">
    <div class="col-md-6"><h2><?php echo __("Upload Batch Vehicle"); ?></h2></div>
	<div class="col-md-6">
    	<div class="upload-progress pull-right">
        <p>Vehicle upload progress</p>
        <div class="progress-bar">
        	<div class="percent" style="width:100%"></div>
         </div>
        <p class="text-center" >100% Complete</p>
        </div>
        <!-- -->
    </div>
    </div>
</div>
