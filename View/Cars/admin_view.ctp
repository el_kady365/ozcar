<div class="cars view">
<h2><?php  echo __('Car'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($car['Car']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Make'); ?></dt>
		<dd>
			<?php echo $this->Html->link($car['Make']['name'], array('controller' => 'makes', 'action' => 'view', $car['Make']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Car Model'); ?></dt>
		<dd>
			<?php echo $this->Html->link($car['CarModel']['name'], array('controller' => 'car_models', 'action' => 'view', $car['CarModel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Year'); ?></dt>
		<dd>
			<?php echo h($car['Car']['year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Badge'); ?></dt>
		<dd>
			<?php echo h($car['Car']['badge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Series'); ?></dt>
		<dd>
			<?php echo h($car['Car']['series']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($car['Car']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Engine Capacity'); ?></dt>
		<dd>
			<?php echo h($car['Car']['engine_capacity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cylinders'); ?></dt>
		<dd>
			<?php echo h($car['Car']['cylinders']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Type'); ?></dt>
		<dd>
			<?php echo h($car['Car']['fuel_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Doors'); ?></dt>
		<dd>
			<?php echo h($car['Car']['doors']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Stock Number'); ?></dt>
		<dd>
			<?php echo h($car['Car']['stock_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rego'); ?></dt>
		<dd>
			<?php echo h($car['Car']['rego']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vin Number'); ?></dt>
		<dd>
			<?php echo h($car['Car']['vin_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dealer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($car['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $car['Dealer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Odometer'); ?></dt>
		<dd>
			<?php echo h($car['Car']['odometer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Colour'); ?></dt>
		<dd>
			<?php echo h($car['Car']['colour']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Trim'); ?></dt>
		<dd>
			<?php echo h($car['Car']['trim']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Feature Code'); ?></dt>
		<dd>
			<?php echo h($car['Car']['feature_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Selling Price'); ?></dt>
		<dd>
			<?php echo h($car['Car']['selling_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comments'); ?></dt>
		<dd>
			<?php echo h($car['Car']['comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dealer Location'); ?></dt>
		<dd>
			<?php echo $this->Html->link($car['DealerLocation']['name'], array('controller' => 'dealer_locations', 'action' => 'view', $car['DealerLocation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Compliance Date'); ?></dt>
		<dd>
			<?php echo h($car['Car']['compliance_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Redbook Code'); ?></dt>
		<dd>
			<?php echo h($car['Car']['redbook_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($car['Car']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($car['Car']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Car'), array('action' => 'edit', $car['Car']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Car'), array('action' => 'delete', $car['Car']['id']), null, __('Are you sure you want to delete # %s?', $car['Car']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cars'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Car'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Makes'), array('controller' => 'makes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Make'), array('controller' => 'makes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Car Models'), array('controller' => 'car_models', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Car Model'), array('controller' => 'car_models', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealers'), array('controller' => 'dealers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer'), array('controller' => 'dealers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealer Locations'), array('controller' => 'dealer_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer Location'), array('controller' => 'dealer_locations', 'action' => 'add')); ?> </li>
	</ul>
</div>
