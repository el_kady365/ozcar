<div class="pages">
    <div class="section-search">
        <div class="container">
            <div class="col-md-8-f">
                <div class="tabs  default-tabs">
                    <ul class="tab-buttons all-search-tabs clearfix">
                        <li class="tab-btn active" data-id="#AllCars"><a rel="all" href="#">ALL CARS</a></li>
                        <li class="tab-btn" data-id="#NewCars"><a rel="1" href="#">NEW CARS</a></li>
                        <li class="tab-btn" data-id="#UsedCars"><a rel="2" href="#">USED CARS</a></li>
                        <li class="tab-btn" data-id="#DemoCars"><a rel="3" href="#">DEMO CARS</a></li>
                    </ul>
                    <div class="tabs-box">
                        <?php echo $this->element('cars/car-advancedsearch'); ?>
                    </div>
                    <!-- / tabs-box --> 
                </div>
                <!-- / tabs --> 
            </div>
            <div class="col-md-4-f">
                <?php echo $this->element('cars/small-search') ?>
                <!-- / locations-box --> 

            </div>
        </div>
    </div>
    <!-- /section-search  -->

    <!--    <div class="random-cars m-t-xl">
            <div class="container">
                <div class="col-md-12">
    
    
                    <h5 class="bold highlight m-b-md">guaranteed quality cars <span class="text-primary">all around the country in one place</span></h5>
                </div>
            </div>
            <div class="random-cars-slider"> <a href="#" class="zoom-effect"><img src="<?php // echo Router::url('/css/img/thumbs/1.png')  ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php // echo Router::url('/css/img/thumbs/3.png')  ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php // echo Router::url('/css/img/thumbs/1.png')  ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/3.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/1.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/3.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/1.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php // echo Router::url('/css/img/thumbs/3.png')  ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/1.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/3.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php // echo Router::url('/css/img/thumbs/1.png')  ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/3.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/1.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/3.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php echo Router::url('/css/img/thumbs/1.png') ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="<?php // echo Router::url('/css/img/thumbs/3.png')  ?>"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> </div>
        </div>-->
    <!-- /content-section  --> 

</div>
<!-- /contents  -->

<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel'), array('inline' => false)) ?>