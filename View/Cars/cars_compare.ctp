<div class="contents">
    <div class="container">

        <div class="col-md-8">
            <?php if (!empty($cars)) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <h1>Comparison List <? echo count($cars) ?></h1>
                    </div>
                    <div class="col-md-6">
                        <p class="pull-right m-t-sm">Showing <a href="#" class="bold text-primary"><?php echo count($cars_id) ?></a> Cars to compare</p>
                    </div>
                </div>
                <div class="collapse-bar head-bar m-b-md">
                    <!--<h3 class="pull-left m-t-sm">Lorem Ipsum</h3>-->
                </div>
                <?php if (!empty($cars) and count($cars) > 2) { ?>
                    <div class="comparison-items clearfix">
                        <div class="comparison-slider pull-right m-t-md">
                            <?php foreach ($cars as $key => $car) { ?>
                                <div class="item">
                                    <a href="#">
                                        <?php if (!empty($car['CarImage'][0]['image'])) { ?>    
                                            <img title="<? echo $car['Car']['make'] . ' ' . $car['Car']['model'] ?>" src="<?php echo get_resized_image_url($car['CarImage'][0]['image'], 155, 110) ?>">
                                        <?php } else { ?>
                                            <img title="<? echo $car['Car']['make'] . ' ' . $car['Car']['model'] ?>" src="<?php echo get_resized_image_url('default-car.png', 155, 110) ?>">
                                        <?php }
                                        ?>
                                    </a>
                                    <!--                        <div class="input checkbox">
                                                                <input type="checkbox">
                                                                <label><strong>Lock</strong></label>
                                                            </div>-->
                                </div>
                            <?php } ?>

                        </div>

                    <?php } elseif (!empty($cars) and count($cars) <= 2) { ?>
                        <div class="comparison-items clearfix">
                            <div class="xcomparison-slider pull-right m-t-md">

                                <div class="item">
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <a href="#">
                                            <?php if (!empty($car['CarImage'][0]['image'])) { ?>    
                                                <img title="<? echo $car['Car']['make'] . ' ' . $car['Car']['model'] ?>" src="<?php echo get_resized_image_url($car['CarImage'][0]['image'], 155, 110) ?>">
                                            <?php } else { ?>
                                                <img title="<? echo $car['Car']['make'] . ' ' . $car['Car']['model'] ?>" src="<?php echo get_resized_image_url('default-car.png', 155, 110) ?>">
                                            <?php }
                                            ?>
                                        </a>
                                        <!--                        <div class="input checkbox">
                                                                    <input type="checkbox">
                                                                    <label><strong>Lock</strong></label>
                                                                </div>-->
                                    <?php } ?>
                                </div>


                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <!-- / comparion items -->

                    <div class="collapse-section">
                        <div class="collapse-bar">
                            <h3><a class="ico-04" href="#"><i class="fa fa-minus-square-o"></i> Vehicle Details</a></h3>
                        </div>
                        <div class="collapse-content">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="comparison-table">
                                <tr>
                                    <th>Make/Model</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><a href="#" class="bold"><?php echo $title = $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series']; ?></a></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <th>Year</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><a href="#" class="bold"><?php echo $car['Car']['year'] ?></a></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <th>Price</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><a href="#" class="bold">$<?php echo $this->Number->format((float) $car['Car']['selling_price']) ?></a></td>
                                    <?php } ?>

                                </tr>
                                <tr>
                                    <th>Dealership</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><a href="#" class="bold"><img src="<?php echo Car::getDealerLogo($car['Car']['dealer_id'], 120) ?>" alt="" title="" width="100"/></a></td>
                                    <?php } ?>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- / collapse-section -->

                    <div class="collapse-section">
                        <div class="collapse-bar">
                            <h3><a class="ico-04" href="#"><i class="fa fa-minus-square-o"></i> Overview</a></h3>
                        </div>
                        <div class="collapse-content">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="comparison-table">
                                <tr>
                                    <th>Body</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><?php echo $car['Car']['body'] ?></td>
                                    <?php } ?>

                                </tr>
                                <tr>
                                    <th>Engine</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><?php echo $car['Car']['engine_capacity'] ?></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <th>Transmission</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><?php echo $car['Car']['transmission'] ?></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <th>Kilometres</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><?php echo $this->Number->format((float) $car['Car']['odometer']) ?> kms</td>
                                    <?php } ?>

                                </tr>

                                <tr>
                                    <th>Registration Expiry</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><?php echo date('d/m/Y', strtotime($car['Car']['registration_expiry'])) ?> </td>
                                    <?php } ?>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- / collapse-section -->

                    <div class="collapse-section">
                        <div class="collapse-bar">
                            <h3><a class="ico-04" href="#"><i class="fa fa-minus-square-o"></i> Standard Features</a></h3>
                        </div>
                        <div class="collapse-content">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="comparison-table">
                                <tr>
                                    <th>&nbsp;</th>
                                    <?php
                                    foreach ($cars as $key => $car) {
                                        $features = explode(',', $car['Car']['comments']);
                                        ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>">
                                            <?php if (!empty($features) && !empty($car['Car']['comments'])) { ?>
                                                <ul class="standard-features">
                                                    <?php foreach ($features as $feature) {
                                                        ?>
                                                        <li><span><?php echo $feature ?></span></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </td>
                                    <?php } ?>

                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- / collapse-section --> 




                    <div class="collapse-section">
                        <div class="collapse-bar">
                            <h3><a class="ico-04" href="#"><i class="fa fa-minus-square-o"></i> Photos</a></h3>
                        </div>
                        <div class="collapse-content">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="comparison-table">
                                <tr>
                                    <th>Body</th>
                                    <?php foreach ($cars as $key => $car) { ?>
                                        <td class="xcol-<?php echo ($key + 1) ?>"><?php echo $car['Car']['body'] ?></td>
                                    <?php } ?>
                                </tr>
                                <?php
                                $count = 0;
                                foreach ($cars as $key => $car) {
                                    if (count($car['CarImage']) > $count) {
                                        $count = count($car['CarImage']);
                                    }
                                }

                                for ($i = 0; $i < $count; $i++) {
                                    ?>
                                    <tr>
                                        <th></th>

                                        <?php
                                        foreach ($cars as $key => $car) {
                                            ?>
                                            <td class="xcol-<?php echo ($key + 1) ?>">
                                                <?php if (isset($car['CarImage'][$i])) { ?>
                                                    <a href="#">
                                                        <img src="<?php echo get_resized_image_url($car['CarImage'][$i]['image'], 100, 80) ?>">
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?>



                            </table>
                        </div>
                    </div>      
                    <!-- / collapse-section --> 


                    <div class="snippets m-t-md text-sm">
                        <p>* Price shown is a price guide only based on information provided to us by the manufacturer. When purchasing a car, always confirm the single figure price with the seller of an actual vehicle. Click here for further information about our Terms & Conditions. </p>
                    </div>      
                <?php } else {
                    ?>
                    <div class="flashMessage Notemessage">There are no cars to compare </div>
                <?php }
                ?>

            </div>
            <?php echo $this->element('Users.right_menu'); ?>
        </div>
    </div>
    <?php echo $this->Html->script(array('owl.carousel'), array('inline' => false)) ?>
    <?php echo $this->append('script') ?>
    <script>
        var per_page = 3;
        var xcur = 1;
        var xcount = <?php echo count($cars); ?>;

        $('.owl-next').click(function () {

            if (xcur < xcount)
            {
                $('.xcol-' + (i + 1)).show();
                $('.xcol-' + (i - 3)).hide();
                xcur++;
            }
        })





        function move_to(index)
        {
            if (index >= 1 && index <= xcount - per_page + 1)
                for (i = 1; i <= xcount; i++) {
                    if (i >= index && i < index + per_page)
                        $('.xcol-' + i).show();
                    else
                        $('.xcol-' + i).hide();
                }
        }

        function owlMoved(element, info) {
            move_to(info.currentPosition);
        }

        if ($('.comparison-slider').length) {
            $(".comparison-slider").owlCarousel({
                slideSpeed: 300,
                paginationSpeed: 400,
                navigation: true,
                items: 3,
                paginationNumbers: false,
                afterMove: function (elem) {
                    var current = this.currentItem;
                    move_to(current + 1);

                }
            });

        }

        move_to(1);
    </script>
    <?php echo $this->end(); ?>