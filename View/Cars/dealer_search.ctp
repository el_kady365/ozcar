<?php
if (!empty($params['location']) && !strpos(strtolower($params['location']), 'australia'))
    $params['location'].=' ,Australia';
?>
<div id="skillform"  class="reveal-modal"> <a class="close-reveal-modal">&#215;</a>
    <div class="row">
        <div class="col-md-12">
            <div class="input text">
                <label> Enter Your Location</label>
                <input type="hidden" id="dealerId" />
                <input type="input" class="autocompleteAddress" name="to_location" id="toLocation" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-block btn-danger btn-md text-center" id="getDirection">Get Directions</button>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div class="pages">
    <div class="cars-map">
        <a href="#" class="show-search-overlay" style="display: none" ><i class="fa fa-angle-right"></i></a>
        <div class="container-fluid no-padding">
            <div class="col-md-7 no-padding"> 
                <div class="search-overlay">
                    <a href="#" class="hide-search-overlay"><i class="fa fa-angle-left"></i></a>
                    <div class="search-options">
                        <div class="search-option-head"> <i class="fa fa-search"></i>
                            <h1>location search</h1>
                            <h3>Looking to buy a car? Find one at a dealership near you!</h3>
                        </div>
                        <h3>fill in one or more of the following</h3>
                        <div class="location-form">
                            <?php
                            echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarMapSearch', 'url' => array('controller' => 'cars', 'action' => 'dealer_search')));
                            ?>

                            <div class="row">
                                <div class="col-md-5">
                                    <?php echo $this->Form->input('location', array('value' => !empty($params['location']) ? $params['location'] : '', 'placeholder' => 'Location/Postcode', 'required' => 'required', 'class' => 'autocompleteAddress', 'label' => false)) ?>
                                </div>
                                <div class="col-md-2">
                                    <div class="m-t-md"><strong>WITHIN</strong></div>
                                </div>
                                <div class="col-md-5">

                                    <?php echo $this->Form->input('distance', array('selected' => !empty($params['distance']) ? $params['distance'] : '', 'options' => Car::$Distances, 'label' => false, 'empty' => 'Kilometres radius ')) ?>
                                </div>

                                <div class="col-md-12">
                                    <div class="input submit">
                                        <button type="submit" class="btn btn-block text-center btn-md btn-danger m-b-sm bold">SEARCH</button>
                                    </div>
                                </div>
                                <div class="col-md-12"> <a href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'map_search', '?' => $this->request->query)) ?>" class="btn btn-block text-center btn-md  m-b-sm bold">ADVANCED SEARCH</a> </div>
                            </div>
                            <div class="clear"></div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                    <div id="dealer-results">
                        <?php echo $this->element('cars/dealer-search-snippet', array('dealers' => $dealerss)); ?>
                    </div> 
                </div>
            </div>
        </div>
        <div class="tabs-popup" style="display:none " data-id=""></div>
        <div class="google-maps-container" id="map-canvas"></div>
    </div>
</div>
<?php echo $this->Html->css(array('reveal'), null, array('inline' => false)) ?>
<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel', 'jquery.nimble.loader', 'reveal', 'scroll', 'jquery.scrollTo'), array('inline' => false)) ?>
<?php echo $this->Html->script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places', array('inline' => false)) ?>
<?php echo $this->Html->script('http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js', array('inline' => false)) ?>
<?php $this->append('script') ?>
<script>

    $(".scroll").niceScroll({zindex: 500, cursorwidth: "8px"});


    var map;
    var locationss =<?php echo json_encode($locationss) ?>;
    var overlays = [];
    var datas;
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();


    $(function () {

        $('#CarMapSearch').on('submit', function () {
            var action = $(this).attr('action');
            datas = $(this).serialize();
            $(this).find('input');
            $('body').nimbleLoader("show", {
                position: "fixed",
                loaderClass: "loading_bar_1",
                debug: true,
                speed: 'fast',
                hasBackground: true,
                zIndex: 999,
                backgroundColor: "#fff",
                backgroundOpacity: 0.9
            });
            $.ajax({
                url: action,
                data: datas,
                method: 'GET',
                dataType: 'json'
            }).done(function (data) {
                $('body').nimbleLoader("hide");
                console.log(data);
                locationss = data.locations;
                setmarkers(data.locations);
                $('#dealer-results').html(data.dealers);
                setAddress($('#CarLocation').val());
                $.scrollTo($('div#dealer-results'), {duration: 1000});
                setTimeout(function () {
                    resize_map();
                }, 1000);



            });
            return false;
        });
    });

    function CustomMarker(latlng, content, dealer, map) {
        this.latlng_ = latlng;
        this.content = content;
        this.dealer = dealer;

        // Once the LatLng and text are set, add the overlay to the map.  This will
        // trigger a call to panes_changed which should in turn call draw.
        this.setMap(map);
    }


    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function () {
        var me = this;

        // Check if the div has been created.
        var div = this.div_;
        if (!div) {
            // Create a overlay text DIV
            div = this.div_ = document.createElement('DIV');
            // Create the DIV representing our CustomMarker
            div.style.border = "none";
            div.style.position = "absolute";
            div.style.paddingLeft = "0px";
            div.style.cursor = 'pointer';

            var marker = document.createElement("span");
            marker.className = 'marker';
            marker.innerHTML = this.content;
            div.appendChild(marker);
            google.maps.event.addDomListener(div, "click", function (event) {
                google.maps.event.trigger(me, "click");
            });

            // Then add the overlay to the DOM
            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        // Position the overlay 
        var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
//        console.log(point);

        if (point) {

            div.style.left = (parseInt(point.x) - 30) + 'px';
            div.style.top = (parseInt(point.y) - 36) + 'px';
            if (this.dealer == $('.tabs-popup').data('id')) {
                $('.tabs-popup').css({left: div.style.left, top: div.style.top});
            }
        }



    };


    CustomMarker.prototype.remove = function () {
        // Check if the overlay was on the map and needs to be removed.
        if (this.div_) {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        }
    };

    CustomMarker.prototype.getPosition = function () {
        return this.latlng_;
    };





    function setmarkers(locations) {
        console.log(locations)
        for (var overlay in overlays) {
            over = overlays[overlay];
            over.setMap(null);
        }

        for (var dealer in locations)
        {
            var over = new CustomMarker(new google.maps.LatLng(locations[dealer]['lat'], locations[dealer]['long']), locations[dealer]['count'], locations[dealer]['dealer_id'], map);
            google.maps.event.addListener(over, 'click', (function (over, dealer) {
                var ib = new InfoBox();
                var dealer_id = locations[dealer]['dealer_id'];
                return function () {

                    point = over.getProjection().fromLatLngToDivPixel(over.latlng_);
                    map.panTo(over.getPosition());
                    $('.hide-search-overlay').click();
                    ib.open(map, over);
                    ib.setContent('<div class="tab_loading" id="loading_' + dealer_id + '"></div>');
                    setTimeout(function () {
                        $('html, body').animate({
                            scrollTop: $('#loading_' + dealer_id).offset().top - 50
                        }, 500);
                    }, 300);

                    $.ajax({
                        url: '<?php echo Router::url(array('action' => 'get_map_snippet')) ?>/' + locations[dealer]['dealer_id'],
                        method: 'GET',
                        data: datas
                    }).done(function (data) {
                        ib.setContent(data);
                        setTimeout(function () {
                            $('#tabs_' + dealer_id + ' .tab-btn').click(function () {
                                $('#tabs_' + dealer_id + ' .tabs-box .tab').hide();
                                $('#tabs_' + dealer_id + ' li').removeClass('active');
                                $('#tabs_' + dealer_id + ' ' + $(this).data('id')).show();
                                $(this).closest('li').addClass('active');
                                return false;
                            });

                        }, 1000);

                    });


                }
            })(over, dealer));



            overlays['overlay-' + locations[dealer]['dealer_id']] = over;
        }



    }


    $(function () {
        $('#getDirection').on('click', function () {
            dealer_id = $('#dealerId').val();
            origin = $('#toLocation').val();
            var request = {
                origin: origin,
                destination: new google.maps.LatLng(locationss[dealer_id]['lat'], locationss[dealer_id]['long']),
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else {
                    alert('Directions request failed due to ' + status);
                }

            });
            $('.tabs-popup').hide();
            $('#skillform').trigger('reveal:close');
        });
        $('#skillform').bind('reveal:close', function () {
            $('#skillform').find('input').html('');
        });
        $('.hide-search-overlay').on('click', function () {
            $('.search-overlay').fadeOut('fast');
            $('.show-search-overlay').show();
        });

        $('.show-search-overlay').on('click', function () {
            $('.search-overlay').fadeIn('fast');
            $(this).hide();
        });
    });


    function address_initialize() {
        $(".autocompleteAddress").each(function (index) {
            var txt_Obj = $(this)[0];
            autocomplete = new google.maps.places.Autocomplete((txt_Obj), {types: ['geocode'], componentRestrictions: {country: "Aus"}});
        });
    }

    function initialize() {
<?php ?>
        address_initialize();
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
            center: new google.maps.LatLng(-24.60706913770968, 134.208984375),
            zoom: 5,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(mapCanvas, mapOptions);
        map.addListener('click', function () {
            $('.tabs-popup').hide();
        });
        map.addListener('drag', function () {
            $('.tabs-popup').hide();
        });
        setmarkers(locationss);
        directionsDisplay.setMap(map);
<?php if (!empty($params['location'])) { ?>
            setAddress('<?php echo str_replace("'", "\\'", $params['location']) ?>');
<?php } ?>
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    if (self.location.hash.match(/dealer-/)) {
        hash = self.location.hash.match(/\d+/);
        console.log(hash);
        $('#dealerId').val(hash);
        $('#skillform').reveal();
    }

    $(document).on('click', '.get-direction', function () {
        dealer_id = $(this).data('address');
        $('#dealerId').val(dealer_id);
        $('#skillform').reveal();

        return false;
    });

    function resize_map()
    {

        var win_h = $(window).height();
        var footer_end = $('.copyrights').offset().top + $('.copyrights').height();
        var overlay_end = $('.search-overlay').offset().top + $('.search-overlay').height();
        var page_end = overlay_end;
        if (footer_end > overlay_end)
            page_end = footer_end;
        var map_end = $('#map-canvas').offset().top + $('#map-canvas').height();

        console.log("Map End: " + map_end);
        console.log("Wind H: " + win_h);
        console.log("Footer End: " + footer_end);
        console.log("OverLay End: " + overlay_end);
        console.log("Page End: " + page_end);


        if (page_end > map_end && ($('.search-overlay').height() > $('#map-canvas').height() || win_h > map_end))
            $('#map-canvas').height($('#map-canvas').height() + (page_end - map_end) + 250);
        if ($('#map-canvas').height() - 350 > $('.search-overlay').height() && $('#map-canvas').height() - 350 > win_h)
            $('#map-canvas').height($('#map-canvas').height() - (map_end - overlay_end) + 250);

        google.maps.event.trigger(map, "resize");
    }



    function setAddress(address) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                map.setZoom(11);


            }
        });
    }

    setTimeout(function () {
        resize_map();
    }, 1000);

</script>
<?php $this->end(); ?>
