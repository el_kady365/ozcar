<div class="cars index">
	<h2><?php echo __('Cars'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Make Id' ; ?></th>
	            <th><?php echo 'Car Model Id' ; ?></th>
	            <th><?php echo 'Type Id' ; ?></th>
	            <th><?php echo 'Year' ; ?></th>
	            <th><?php echo 'Badge' ; ?></th>
	            <th><?php echo 'Series' ; ?></th>
	            <th><?php echo 'Body' ; ?></th>
	            <th><?php echo 'Engine Capacity' ; ?></th>
	            <th><?php echo 'Cylinders' ; ?></th>
	            <th><?php echo 'Fuel Type' ; ?></th>
	            <th><?php echo 'Doors' ; ?></th>
	            <th><?php echo 'Stock Number' ; ?></th>
	            <th><?php echo 'Rego' ; ?></th>
	            <th><?php echo 'Vin Number' ; ?></th>
	            <th><?php echo 'Dealer Id' ; ?></th>
	            <th><?php echo 'Odometer' ; ?></th>
	            <th><?php echo 'Colour' ; ?></th>
	            <th><?php echo 'Trim' ; ?></th>
	            <th><?php echo 'Feature Code' ; ?></th>
	            <th><?php echo 'Selling Price' ; ?></th>
	            <th><?php echo 'Comments' ; ?></th>
	            <th><?php echo 'Dealer Location Id' ; ?></th>
	            <th><?php echo 'Compliance Date' ; ?></th>
	            <th><?php echo 'Redbook Code' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
	            <th><?php echo 'Transmission' ; ?></th>
	            <th><?php echo 'Size' ; ?></th>
	            <th><?php echo 'Month' ; ?></th>
	            <th><?php echo 'NewPr' ; ?></th>
	            <th><?php echo 'Width' ; ?></th>
	            <th><?php echo 'Height' ; ?></th>
	            <th><?php echo 'Length' ; ?></th>
	            <th><?php echo 'Seats' ; ?></th>
	            <th><?php echo 'Turncir' ; ?></th>
	            <th><?php echo 'Kerb Weight' ; ?></th>
	            <th><?php echo 'Steering' ; ?></th>
	            <th><?php echo 'Front Tyres' ; ?></th>
	            <th><?php echo 'Rear Tyres' ; ?></th>
	            <th><?php echo 'Wheelbase' ; ?></th>
	            <th><?php echo 'Front Brake' ; ?></th>
	            <th><?php echo 'Rear Brake' ; ?></th>
	            <th><?php echo 'Towing Capacity Braked' ; ?></th>
	            <th><?php echo 'Towing Capacity Unbraked' ; ?></th>
	            <th><?php echo 'Drive Type' ; ?></th>
	            <th><?php echo 'Power' ; ?></th>
	            <th><?php echo 'Torque' ; ?></th>
	            <th><?php echo 'BoreStroke' ; ?></th>
	            <th><?php echo 'Compression Ratio' ; ?></th>
	            <th><?php echo 'Service Months' ; ?></th>
	            <th><?php echo 'Service Kms' ; ?></th>
	            <th><?php echo 'Warranty' ; ?></th>
	            <th><?php echo 'Warranty Kms' ; ?></th>
	            <th><?php echo 'Trim Colour' ; ?></th>
	            <th><?php echo 'Registration Expiry' ; ?></th>
	            <th><?php echo 'Build Date' ; ?></th>
	            <th><?php echo 'Price Excl Govt' ; ?></th>
	            <th><?php echo 'Ancap Rating' ; ?></th>
	            <th><?php echo 'Green Star Rating' ; ?></th>
	            <th><?php echo 'Roadworthy Cert' ; ?></th>
		</tr>
	<?php
	foreach ($cars as $car): ?>
	<tr>
		<td><?php echo h($car['Car']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($car['Make']['name'], array('controller' => 'makes', 'action' => 'view', $car['Make']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($car['CarModel']['name'], array('controller' => 'car_models', 'action' => 'view', $car['CarModel']['id'])); ?>
		</td>
		<td><?php echo h($car['Car']['type_id']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['year']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['badge']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['series']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['body']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['engine_capacity']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['cylinders']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['fuel_type']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['doors']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['stock_number']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['rego']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['vin_number']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($car['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $car['Dealer']['id'])); ?>
		</td>
		<td><?php echo h($car['Car']['odometer']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['colour']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['trim']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['feature_code']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['selling_price']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['comments']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($car['DealerLocation']['name'], array('controller' => 'dealer_locations', 'action' => 'view', $car['DealerLocation']['id'])); ?>
		</td>
		<td><?php echo h($car['Car']['compliance_date']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['redbook_code']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['created']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['modified']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['transmission']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['size']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['month']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['newPr']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['width']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['height']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['length']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['seats']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['turncir']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['kerb_weight']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['steering']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['front_tyres']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['rear_tyres']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['wheelbase']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['front_brake']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['rear_brake']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['towing_capacity_braked']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['towing_capacity_unbraked']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['drive_type']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['power']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['torque']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['boreStroke']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['compression_ratio']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['service_months']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['service_kms']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['warranty']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['warranty_kms']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['trim_colour']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['registration_expiry']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['build_date']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['price_excl_govt']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['ancap_rating']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['green_star_rating']); ?>&nbsp;</td>
		<td><?php echo h($car['Car']['roadworthy_cert']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
