<div id="skillform"  class="reveal-modal">
    <a class="close-reveal-modal">&#215;</a>
    <div class="row">
        <div class="col-md-12">
            <div class="input text">
                <label> Enter Your Location</label>
                <input type="hidden" id="dealerId" />
                <input type="input" class="autocompleteAddress" name="to_location" id="toLocation" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-block btn-danger btn-md text-center" id="getDirection">Get Directions</button>
        </div>
    </div>
    <div class="clear"></div>
</div>

<div class="pages">
    <div class="cars-map">
        <a href="#" class="show-search-overlay" style="display: none" ><i class="fa fa-angle-right"></i></a>
        <div class="container-fluid no-padding">
            <div class="col-md-7 no-padding">
                <div class="search-overlay">
                    <a href="#" class="hide-search-overlay"><i class="fa fa-angle-left"></i></a>
                    <?php
                    $bodyTypes = Car::$BodyTypes;
                    echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarMapSearch', 'url' => array('controller' => 'cars', 'action' => 'map_search')));
                    echo $this->Form->hidden('type_id', array('value' => (!empty($params['type_id'])) ? $params['type_id'] : 'all'));
                    echo $this->Form->input('body', array('selected' => !empty($params['body']) ? $params['body'] : '', 'type' => 'select', 'name' => 'body', 'options' => Car::getBodyTypesDropDown(), 'empty' => 'select body', 'style' => 'display:none', 'div' => false, 'label' => false, 'multiple' => true));
                    ?>

                    <div class="search-options">
                        <div class="search-option-head">
                            <i class="fa fa-search"></i>
                            <h1>location search</h1>
                            <h3>Looking to buy a car? Find one at a dealership near you!</h3>
                        </div>    
                        <h3>fill in one or more of the following</h3>

                        <ul class="search-btns">
                            <li><a <?php echo (!empty($params['type_id']) && $params['type_id'] == 'all' || empty($params['type_id'])) ? 'class="active"' : '' ?> rel="all" href="#">SEARCH ALL</a></li>
                            <li><a <?php echo (!empty($params['type_id']) && $params['type_id'] == '1' ) ? 'class="active"' : '' ?> rel="1" href="#">SEARCH NEW CARS</a></li>
                            <li><a <?php echo (!empty($params['type_id']) && $params['type_id'] == '2' ) ? 'class="active"' : '' ?> rel="2" href="#">SEARCH USED CARS</a></li>
                            <li><a <?php echo (!empty($params['type_id']) && $params['type_id'] == '3' ) ? 'class="active"' : '' ?> rel="3" href="#">SEARCH DEMO CARS</a></li>
                        </ul>

                    </div>

                    <div class="search-area map-search">
                        <div class="collapse-section">
                            <div id="search_error" style="display:none"  class="search-area map-search">
                                <div class="flashMessage Notemessage">Your search has been unsuccessful. Please try again with different options</div>
                            </div>

                            <div class="collapse-bar head-bar m-b-md">
                                <h4>LOCATION SEARCH</h4>
                            </div>
                            <div class="collapse-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo $this->Form->input('dealer_location', array('selected' => !empty($params['dealer_location']) ? $params['dealer_location'] : '', 'options' => $dealers, 'label' => false, 'empty' => 'Select Dealer')) ?>
                                    </div>

                                    <div class="col-md-6">

                                        <?php echo $this->Form->input('location', array('value' => !empty($params['location']) ? $params['location'] : '', 'placeholder' => 'Location/Postcode', 'class' => 'autocompleteAddress', 'label' => false)) ?>

                                    </div>
                                    <div class="col-md-2 text-center">
                                        <div class="m-t-md"><strong>WITHIN</strong></div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('distance', array('selected' => !empty($params['distance']) ? $params['distance'] : '', 'options' => Car::$Distances, 'label' => false, 'empty' => 'Select Distance')) ?>
                                    </div>
                                </div>
                            </div>
                            <!-- / collapse-content -->

                            <div class="collapse-bar head-bar m-b-md">
                                <h4>CAR SEARCH</h4>
                            </div>
                            <div class="collapse-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('make', array('value' => !empty($params['make']) ? $params['make'] : '', 'options' => $makes, 'label' => false, 'class' => 'makes', 'empty' => 'Select Make')) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('model', array('empty' => 'Select Model', 'options' => array(), 'label' => false, 'type' => 'select', 'class' => 'models')) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('series', array('empty' => 'Select Series', 'options' => array(), 'label' => false, 'type' => 'select', 'class' => 'models')) ?>
                                    </div>

                                </div>

                                <div class="row m-b-md m-t-md rang-row">
                                    <div class="col-md-12">
                                        <label>Price</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="Price" data-from="<?php echo!empty($params['price_from']) ? $params['price_from'] : '' ?>" data-to="<?php echo!empty($params['price_to']) ? $params['price_to'] : '' ?>"  ></div> 
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('price_from', array('value' => !empty($params['price_from']) ? $params['price_from'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div class="m-t-md"><strong>TO</strong></div>
                                    </div>
                                    <div class="col-md-2">

                                        <?php echo $this->Form->input('price_to', array('value' => !empty($params['price_to']) ? $params['price_to'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>

                                    </div>
                                </div>

                                <div class="row m-b-md m-t-md rang-row">
                                    <div class="col-md-12">
                                        <label>Year</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="Years" data-from="<?php echo!empty($params['year_from']) ? $params['year_from'] : '' ?>" data-to="<?php echo!empty($params['year_to']) ? $params['year_to'] : '' ?>"></div> 
                                    </div>
                                    <div class="col-md-2">

                                        <?php echo $this->Form->input('year_from', array('value' => !empty($params['year_from']) ? $params['year_from'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>

                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div class="m-t-md"><strong>TO</strong></div>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('year_to', array('value' => !empty($params['year_to']) ? $params['year_to'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                                    </div>
                                </div>                    



                            </div>
                            <!-- / collapse-content --> 

                            <!-- / collapse-content -->

                            <div class="collapse-bar head-bar m-b-md">
                                <h3>Engine</h3>
                            </div>
                            <div class="collapse-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('fuel_type', array('selected' => !empty($params['fuel_type']) ? $params['fuel_type'] : '', 'options' => Car::$FuelTypes, 'label' => false, 'empty' => 'Fuel types')) ?>
                                    </div>

                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('transmission', array('selected' => !empty($params['transmission']) ? $params['transmission'] : '', 'options' => Car::$Transmissions, 'label' => false, 'empty' => 'Transmission')) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('cylinders', array('selected' => !empty($params['cylinders']) ? $params['cylinders'] : '', 'options' => range(1, 12), 'label' => false, 'empty' => 'Cylinders')) ?>
                                    </div>
                                </div>
                                <div class="row m-b-md m-t-md rang-row">
                                    <div class="col-md-12">
                                        <label>KMs</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="Kilometers" data-from="<?php echo!empty($params['kms_from']) ? $params['kms_from'] : '' ?>" data-to="<?php echo!empty($params['kms_to']) ? $params['kms_to'] : '' ?>"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('kms_from', array('value' => !empty($params['kms_from']) ? $params['kms_from'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div class="m-t-md"><strong>TO</strong></div>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('kms_to', array('value' => !empty($params['kms_to']) ? $params['kms_to'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                                    </div>
                                </div>
                                <div class="row m-b-md m-t-md rang-row">
                                    <div class="col-md-12">
                                        <label>Power (Kw)</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="Power" data-from="<?php echo!empty($params['power_from']) ? $params['power_from'] : '' ?>" data-to="<?php echo!empty($params['power_to']) ? $params['power_to'] : '' ?>" ></div> 
                                    </div>
                                    <div class="col-md-2">

                                        <?php echo $this->Form->input('power_from', array('value' => !empty($params['power_from']) ? $params['power_from'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>

                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div class="m-t-md"><strong>TO</strong></div>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('power_to', array('value' => !empty($params['power_to']) ? $params['power_to'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                                    </div>
                                </div>
                                <div class="row m-b-md m-t-md rang-row">
                                    <div class="col-md-12">
                                        <label>Engine Size (Ltr)</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="EngineSize" data-from="<?php echo!empty($params['engine_from']) ? $params['engine_from'] : '' ?>" data-to="<?php echo!empty($params['engine_to']) ? $params['engine_to'] : '' ?>"></div> 

                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('engine_from', array('value' => !empty($params['engine_from']) ? $params['engine_from'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <div class="m-t-md"><strong>TO</strong></div>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->input('engine_to', array('value' => !empty($params['engine_to']) ? $params['engine_to'] : '', 'label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                                    </div>
                                </div>


                            </div>
                            <div class="collapse-bar head-bar m-b-md">
                                <h3>Style</h3>
                            </div>
                            <div class="collapse-content">
                                <div class="body-types clearfix m-b-md">
                                    <ul class="table-view">
                                        <?php foreach ($bodyTypes as $key => $val) {
                                            ?>
                                            <li class="cell"> 
                                                <a class="type-ico text-center" rel="<?php echo $key ?>">
                                                    <img title="" alt="" src="<?php echo Router::url('/css/img/shape/' . $val['image'] . '.png') ?>"><?php echo $val['title'] ?></a>
                                            </li>
                                            <?php
                                        }
                                        ?>


                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('seats', array('selected' => !empty($params['seats']) ? $params['seats'] : '', 'options' => array_combine(range(2, 15), range(2, 15)), 'label' => false, 'empty' => 'Seats')) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('doors', array('selected' => !empty($params['doors']) ? $params['doors'] : '', 'options' => array_combine(range(1, 6), range(1, 6)), 'label' => false, 'empty' => 'Doors')) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $this->Form->input('colour', array('selected' => !empty($params['colour']) ? $params['colour'] : '', 'options' => $colours, 'label' => false, 'empty' => 'Colour')) ?>                
                                    </div>

                                </div>


                                <div class="row m-t-md">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-danger btn-md text-center">SEARCH</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-md text-left bold text-primary">Reset Search</button>
                                    </div>


                                </div>



                            </div>
                            <!-- / collapse-content -->


                        </div>
                        <!-- / collapse-section --> 

                        <a href="<?php echo Router::url('/cars/advanced_search') ?>" class="btn btn-block text-center text-primary search-toggle"> ADVANCED SEARCH <i class="fa fa-angle-up"></i></a>
                    </div> 
                    <div style="display:none" class="result-area map-search">		
                    </div>					
                    <div class="clear"></div>
                    <?php echo $this->Form->end(); ?>


                </div>
            </div>
        </div>
        <div class="tabs-popup" style="display:none " data-id=""></div>
        <div class="google-maps-container" id="map-canvas"></div>
    </div>

</div>
<?php echo $this->Html->css(array('reveal'), null, array('inline' => false)) ?>
<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel', 'jquery.nimble.loader', 'reveal'), array('inline' => false)) ?>
<?php echo $this->Html->script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places', array('inline' => false)) ?>
<?php echo $this->Html->script('http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js', array('inline' => false)) ?>



<?php $this->append('script') ?>
<script>
    var models = <?php echo json_encode($models) ?>;
    var serieses = <?php echo json_encode($serieses); ?>;
    var bodies = <?php echo json_encode($jsBodies); ?>;
    var suburbs = <?php echo json_encode($suburbs); ?>;
    var selMake = '<?php echo!empty($params['make']) ? $params['make'] : '' ?>';
    var selModel = '<?php echo!empty($params['model']) ? $params['model'] : '' ?>';
    var selseries = '<?php echo!empty($params['series']) ? $params['series'] : '' ?>';
    var markers = '';
    var map;
    var locationss =<?php echo json_encode($locationss) ?>;
    var overlays = [];
    var datas;
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();


    $(function () {
        $('.hide-search-overlay').on('click', function () {
            $('.search-overlay').fadeOut('fast');
            $('.show-search-overlay').show();
            return false;
        });

        $('.show-search-overlay').on('click', function () {
            $('.search-overlay').fadeIn('fast');
            $(this).hide();
            return false;
        });

<?php if (!empty($params)) { ?>
            // $('.hide-search-overlay').click();
            datas = '<?php echo http_build_query($params); ?>';
<?php }
?>
        getChildList($('select[name="model"]'), $('select[name="make"]').val(), 'models');
        getChildList($('select[name="series"]'), $('select[name="model"]').val(), 'serieses');
        if ($('#Years').length) {
            $("#Years").ionRangeSlider({
                type: "double",
                prettify_enabled: false,
                min: '<?php echo date('Y') - 30 ?>',
                max: '<?php echo date('Y') ?>',
                onChange: function (data) {
                    $('input[name="year_from"]').val(data.from);
                    $('input[name="year_to"]').val(data.to);
                    console.log(data);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="year_from"]').val('');
                        $('input[name="year_to"]').val('');
                    }


                }

            });
        }
        $('.search-btns li a').on('click', function () {
            $type = $(this).attr('rel');
            $('input[name="type_id"]').val($type);
            $('.search-area').show();
            $('.result-area').hide();
            $('#search_error').hide();
            $('.search-btns li a').removeClass('active');
            $(this).addClass('active');
            return false;
        });
        $('.search-toggle').on('click', function () {
            if (!$('.collapse-section').is(':hidden')) {
                $('.collapse-section').slideUp();
                $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
            } else {
                $('.collapse-section').slideDown();
                $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
            }
            return false;
        });

        $('select[name="make"]').on('change', function () {
            getChildList($('select[name="model"]'), $(this).val(), 'models')
        });

        $('select[name="state"]').on('change', function () {
            getChildList($('select[name="suburb"]'), $(this).val(), 'suburbs')
        });

        $('select[name="model"]').on('change', function () {
            getChildList($('select[name="series"]'), $(this).val(), 'serieses')
        });
        function getChildList(element, data_id, type) {
            if (data_id) {
                var data = window[type][data_id];

                element.html('<option value="">Select Model</option>');
                if (type == 'serieses') {
                    element.html('<option value="">Select Series</option>');
                }
                if (type == 'suburbs') {
                    element.html('<option value="">Select Suburb</option>');
                }
                if (data) {
                    $.each(data, function (key, value) {
                        selected = '';
                        if (selseries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                            selected = 'class="active"';
                        }
                        if (selModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                            selected = 'class="active"';
                        }

                        element.append('<option value="' + key + '" ' + selected + '>' + value + '</option>');
                        ;
                    });
                }
            }
        }
        $('#CarMapSearch').on('submit', function () {
            var action = $(this).attr('action');
            datas = $(this).serialize();
            $(this).find('input');
            $('body').nimbleLoader("show", {
                position: "fixed",
                loaderClass: "loading_bar_1",
                debug: true,
                speed: 'fast',
                hasBackground: true,
                zIndex: 999,
                backgroundColor: "#fff",
                backgroundOpacity: 0.9
            });
            search_car(datas);
            $.ajax({
                url: action,
                data: datas,
                method: 'GET',
                dataType: 'json'
            }).done(function (data) {

                //$('body').nimbleLoader("hide");
                locationss = data;
                if (locationss != '') {
                    //$('.hide-search-overlay').click();
                    $('#search_error').hide();
                } else {
                    $('#search_error').show();
                }
                setmarkers(data);
                // $('.hide-search-overlay').click();
//                alert();


            });
            return false;
        });


    });

    function CustomMarker(latlng, content, dealer, map) {
        this.latlng_ = latlng;
        this.content = content;
        this.dealer = dealer;

        // Once the LatLng and text are set, add the overlay to the map.  This will
        // trigger a call to panes_changed which should in turn call draw.
        this.setMap(map);
    }


    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function () {
        var me = this;

        // Check if the div has been created.
        var div = this.div_;
        if (!div) {
            // Create a overlay text DIV
            div = this.div_ = document.createElement('DIV');
            // Create the DIV representing our CustomMarker
            div.style.border = "none";
            div.style.position = "absolute";
            div.style.paddingLeft = "0px";
            div.style.cursor = 'pointer';

            var marker = document.createElement("span");
            marker.className = 'marker';
            marker.innerHTML = this.content;
            div.appendChild(marker);
            google.maps.event.addDomListener(div, "click", function (event) {
                google.maps.event.trigger(me, "click");
            });

            // Then add the overlay to the DOM
            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        // Position the overlay 
        var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
//        console.log(point);

        if (point) {
            div.style.left = (parseInt(point.x) - 30) + 'px';
            div.style.top = (parseInt(point.y) - 70) + 'px';
            if (this.dealer == $('.tabs-popup').data('id')) {
                $('.tabs-popup').css({left: parseInt(point.x - 178) + 'px', top: (parseInt(point.y) - 383) + 'px'});
            }
        }



    };


    CustomMarker.prototype.remove = function () {
        // Check if the overlay was on the map and needs to be removed.
        if (this.div_) {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        }
    };

    CustomMarker.prototype.getPosition = function () {
        return this.latlng_;
    };




    var point = 0;
    function setmarkers(locations) {
//        console.log(locations)
        for (var overlay in overlays) {
            over = overlays[overlay];
            over.setMap(null);
        }
        $('.tabs-popup').hide();
        for (var dealer in locations)
        {
            var over = new CustomMarker(new google.maps.LatLng(locations[dealer]['lat'], locations[dealer]['long']), locations[dealer]['count'], locations[dealer]['dealer_id'], map);
            google.maps.event.addListener(over, 'click', (function (over, dealer) {
                var ib = new InfoBox();
                var dealer_id = locations[dealer]['dealer_id'];
                return function () {
                    point = over.getProjection().fromLatLngToDivPixel(over.latlng_);
                    map.panTo(over.getPosition());

                    $('.hide-search-overlay').click();
                    ib.open(map, over);
                    ib.setContent('<div class="tab_loading" id="loading_' + dealer_id + '"></div>');
                    setTimeout(function () {
                        $('html, body').animate({
                            scrollTop: $('#loading_' + dealer_id).offset().top - 50
                        }, 500);
                    }, 300);
                    $.ajax({
                        url: '<?php echo Router::url(array('action' => 'get_map_snippet')) ?>/' + locations[dealer]['dealer_id'],
                        method: 'GET',
                        data: datas
                    }).done(function (data) {


                        ib.setContent(data);

                        setTimeout(function () {
                            $('#tabs_' + dealer_id + ' .tab-btn').click(function () {
                                $('#tabs_' + dealer_id + ' .tabs-box .tab').hide();
                                $('#tabs_' + dealer_id + ' li').removeClass('active');
                                $('#tabs_' + dealer_id + ' ' + $(this).data('id')).show();
                                $(this).closest('li').addClass('active');
                                return false;
                            });

                        }, 1000);




                        activate_tabs();



                    });






                }
            })(over, dealer));



            overlays['overlay-' + locations[dealer]['dealer_id']] = over;
        }



    }


    $(function () {
        $('#getDirection').on('click', function () {
            dealer_id = $('#dealerId').val();
            origin = $('#toLocation').val();
            var request = {
                origin: origin,
                destination: new google.maps.LatLng(locationss[dealer_id]['lat'], locationss[dealer_id]['long']),
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else {
                    alert('Directions request failed due to ' + status);
                }

            });
            $('.tabs-popup').hide();
            $('#skillform').trigger('reveal:close');
        });
        $('#skillform').bind('reveal:close', function () {
            $('#skillform').find('input').html('');
        });

<?php if (!empty($params)) { ?>
            $('#CarMake').trigger("change");
            $('#CarModel').val("<? echo $params['model'] ?>");
            $('#CarMapSearch').submit();
<? } ?>

    });


    function address_initialize() {
        $(".autocompleteAddress").each(function (index) {
            var txt_Obj = $(this)[0];
            autocomplete = new google.maps.places.Autocomplete((txt_Obj), {types: ['geocode'], componentRestrictions: {country: "Aus"}});
        });
    }

    function initialize() {
        address_initialize();
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
            center: new google.maps.LatLng(-24.60706913770968, 134.208984375),
            zoom: 5,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(mapCanvas, mapOptions);
        map.addListener('click', function () {
            $('.tabs-popup').hide();
        });
        map.addListener('drag', function () {

        });
        //alert(locationss);
        if (locationss != '') {
            // datas = $('#CarMapSearch').serialize();
            // search_car(datas);                    
            //$('.hide-search-overlay').click();
            $('#search_error').hide();
        } else {
            $('#search_error').show();
        }
        setmarkers(locationss);

        directionsDisplay.setMap(map);
    }

    function resize_map()
    {

        var win_h = $(window).height();
        var footer_end = $('.copyrights').offset().top + $('.copyrights').height();
        var overlay_end = $('#CarMapSearch').offset().top + $('#CarMapSearch').height();
        var page_end = overlay_end;
        if (footer_end > overlay_end)
            page_end = footer_end;
        var map_end = $('#map-canvas').offset().top + $('#map-canvas').height();

        console.log("Map End: " + map_end);
        console.log("Wind H: " + win_h);
        console.log("Footer End: " + footer_end);
        console.log("OverLay End: " + overlay_end);
        console.log("Page End: " + page_end);


        if (page_end > map_end && ($('#CarMapSearch').height() > $('#map-canvas').height() || win_h > map_end))
            $('#map-canvas').height($('#map-canvas').height() + (page_end - map_end) + 250);
        if ($('#CarMapSearch').height() - 350 > overlay_end && $('#CarMapSearch').height() - 350 > win_h)
            $('#map-canvas').height($('#map-canvas').height() - (map_end - overlay_end) + 250);

        google.maps.event.trigger(map, "resize");
    }

    google.maps.event.addDomListener(window, 'load', initialize);
    setTimeout(function () {
        resize_map();
    }, 1000);



    $(document).on('click', '.get-direction', function () {
        dealer_id = $(this).data('address');
        $('#dealerId').val(dealer_id);
        $('#skillform').reveal();

        return false;
    });

    function search_car(data) {
        console.log(data);
        if (data == '') {
            $('body').nimbleLoader("hide");
            return;
        }
        $.ajax({
            url: '<? echo Router::url(array('controller' => 'cars', 'action' => 'search', 'map_search')) ?>',
            data: data,
            method: 'GET'
        }).done(function (data) {
            $('body').nimbleLoader("hide");
            if (data == '') {
                return;
            }
            $('.search-area').hide();
            $('.result-area').show();
            $('.result-area').css('overflow-y', 'scroll');

            //$('.search-area').css('max-height',$( '.search-area' ).height());
            $('.result-area').html(data);
            $(".car-preview-thumb").owlCarousel({
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                transitionStyle: "backSlide"
            });
            setTimeout(function () {
                resize_map();
            }, 1000);

        });


    }
</script>
<?php $this->end(); ?>