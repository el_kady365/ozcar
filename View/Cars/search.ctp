<div class="pages">
    <div class="container">
        <div class="col-md-8">
            <h1>Search Results</h1>
            <div class="listing-section">

                <?php echo $this->element('cars/search-results'); ?>

            </div>
            <!-- /listing-section  --> 

        </div>
        <div class="col-md-4">
            <?php echo $this->element('cars/left-search'); ?>
        </div>
    </div>
</div>
<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel'), array('inline' => false)) ?>