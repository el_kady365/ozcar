<?php echo $this->element('cars/car-enquiry', array('offer' => false)); ?>
<?php echo $this->element('cars/car-enquiry', array('offer' => true)); ?>
<div class="pages">
    <div class="container">
        <div class="col-md-8">
            <div class="listing-section">
                <div class="listing-box-head">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="meta">
                                <?php $title = $car['Car']['year'] . ' ' . $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series']; ?>
                                <h3 class="inline-block"> <a href="#"><?php echo $title ?></a></h3>
                                <span class="badge"><?php echo $car[0]['CarsCount']; ?></span> <a href="#" class="inline-block">Available at <strong><?php echo $car['DealerLocation']['name']; ?></strong></a> <i class="fa fa-map-marker"></i> </div>
                        </div>
                        <div class="col-md-3 text-right"><a href="#"><img title="" alt="" src="<?php echo get_resized_image_url($car['Dealer']['logo'], 120) ?>"></a></div>
                    </div>
                </div>
                <!-- /listing-box-head  -->
                <div class="listing-box-content">
                    <div class="row row-sm">
                        <div class="col-md-9"> 
                            <?php
//                            debug($car['CarImage']);
                            if (!empty($car['CarImage'])) {
                                foreach ($car['CarImage'] as $key => $image) {
                                    if ($key == 0) {
                                        ?>
                                        <img class="main-photo" src="<?php echo get_resized_image_url($image['image'], 550, 370) ?>"> 
                                        <?php
                                    } else {
                                        ?>
                                        <img class="main-photo" src="<?php echo get_resized_image_url($image['image'], 550, 370) ?>" style="display: none;"> 
                                        <?php
                                    }
                                }
                            } else {
                                ?>

                                <img class="main-photo" src="<?php echo get_resized_image_url('default-car.png', 550, 370) ?>"> 
                            <?php }
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div class="listing-map">
                                <?php
                                echo $this->element('dealer_map', array('size' => array('width' => 169, 'height' => 125),
                                    'latitude' => $car['DealerLocation']['latitude'], 'longitude' => $car['DealerLocation']['longitude'], 'zoom' => $car['DealerLocation']['zoom'], 'label' => $car['DealerLocation']['name']))
                                ?>
                            </div>
                            <div class="map-caption m-b-sm">
                                <div class="row">
                                    <div class="col-md-8 text-center"> <span class="item-price">$<?php echo $this->Number->format((float) $car['Car']['selling_price']) ?></span> </div>
                                    <div class="col-md-4"> <span class="item-meta bold">Drive Away</span> </div>
                                </div>
                            </div>
                            <div class="car-thumbs clearfix">
                                <?php
                                if (!empty($car['CarImage'])) {
                                    foreach ($car['CarImage'] as $key => $image) {
                                        ?>
                                        <a href="<?php echo get_resized_image_url($image['image'], 550, 370) ?>"><img src="<?php echo get_resized_image_url($image['image'], 59, 59) ?>"></a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- listing box content -->

                <div class="row row-sm">
                    <div class="col-md-8">
                        <div class="tabs car-tabs">
                            <ul class="tab-buttons clearfix p-md">
                                <li data-id="#VehicleDetails" class="tab-btn active"><a href="#">Vehicle Details</a></li>
                                <li data-id="#KeyFeatures" class="tab-btn"><a href="#">Key Features</a></li>
                                <!--<li data-id="#Description" class="tab-btn"><a href="#">Description</a></li>-->
                                <!--<li data-id="#Video" class="tab-btn"><a href="#">Video</a></li>-->
                                <!--<li data-id="#CarStats" class="tab-btn"><a href="#">Car Stats</a></li>-->
                            </ul>
                            <div class="tabs-box">
                                <div class="tab current" id="VehicleDetails">
                                    <h3 class="title-bar">Vehicle details</h3>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="features-table">
                                        <tr>
                                            <th>Make</th>
                                            <td><?php echo $car['Car']['make'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Model</th>
                                            <td><?php echo $car['Car']['model'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Year</th>
                                            <td><?php echo $car['Car']['year'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Body</th>
                                            <td><?php echo $car['Car']['body'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Engine</th>
                                            <td><?php echo $car['Car']['engine_capacity'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Transmission</th>
                                            <td><?php echo $car['Car']['transmission'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Kilometers</th>
                                            <td><?php echo $car['Car']['odometer'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Fuel Economy</th>
                                            <td><?php echo $car['Car']['fuel_economy'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>ANCAP Rating</th>
                                            <td><div class="star-rating">
                                                    <?php
                                                    for ($i = 1; $i <= 5; $i++) {
                                                        if ($i <= $car['Car']['ancap_rating']) {
                                                            $class = "";
                                                        } else {
                                                            $class = "inactive";
                                                        }
                                                        ?>
                                                        <a href="#"><i class="fa fa-star-o <?php echo $class ?>"></i></a>
                                                    <?php } ?>

                                                </div></td>
                                        </tr>
                                        <tr>
                                            <th>Green Star Rating</th>
                                            <td><div class="star-rating"><div class="star-rating">
                                                        <?php
                                                        for ($i = 1; $i <= 5; $i++) {
                                                            if ($i <= $car['Car']['green_star_rating']) {
                                                                $class = "";
                                                            } else {
                                                                $class = "inactive";
                                                            }
                                                            ?>
                                                            <a href="#"><i class="fa fa-star-o <?php echo $class ?>"></i></a>
                                                        <?php } ?>

                                                    </div></td>
                                        </tr>
                                        <tr>
                                            <th>Registration Plate</th>
                                            <td><?php echo $car['Car']['registration']?></td>
                                        </tr>
                                        <tr>
                                            <th>Registration Expiry</th>
                                            <td><?php echo!empty($car['Car']['registration_expiry']) ? date('d/m/Y', strtotime($car['Car']['registration_expiry'])) : "" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tab" id="KeyFeatures">
                                    <h3 class="title-bar">Standard Features</h3>
                                    <div class="p-sm">

                                        <?php
                                        if (!empty($car['Car']['comments'])) {
                                            $features = explode(',', $car['Car']['comments']);
                                            foreach ($features as $key => $feature) {
                                                if ($key == 0) {
                                                    echo '<div class="row m-b-lg">
                                                            <div class="col-md-6">
                                                <ul class="standard-features">';
                                                }
                                                if (($key % 40) == 0 && $key != 0) {
                                                    echo '</div><div class="row m-b-lg">';
                                                }
                                                if (($key % 20) == 0 && $key != 0) {
                                                    echo '</ul></div><div class="col-md-6">
                                                <ul class="standard-features">';
                                                }
                                                ?>
                                                <li><span><?php echo $feature ?></span></li>
                                                <?php
                                            }
                                            echo '</ul></div></div>';
                                        }
                                        ?>


                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- tabs --> 

                    </div>
                    <div class="col-md-3  col-md-push-1">
                        <div class="side-car-actions m-b-xl">
                            <a href="#carEnquiry" class="btn btn-block bold btn-md text-center btn-default m-b-sm">ENQUIRE</a>
                            <a href="#offerEnquiry" class="btn btn-block bold btn-md text-center btn-dark m-b-sm">MAKE OFFER</a>
                            <a href="#" class="btn btn-block bold btn-md text-center btn-darkshape m-b-sm">CALL DEALER</a> 
                            <a href="<?php echo Router::url(array('controller' => 'user_cars', 'action' => 'add', $car['Car']['id'])) ?>" class="btn btn-block bold btn-md text-center btn-darkshape m-b-sm">SAVE CAR</a> 
                        </div>
                        <div class="side-social text-sm">
                            <h5 class="bold m-b-md">SHARE</h5>
                            <p>Found something you like?
                                Share it with your friends!</p>
                            <?php echo $this->element('share_links', array('share_title' => $title)); ?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /listing-section  --> 
        </div>
        <?php echo $this->element('cars/left-search'); ?>
        <?php //echo $this->element('Users.right_menu'); ?>
    </div>


</div>
</div>
<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel'), array('inline' => false)) ?>
<?php echo $this->Html->script(array('owl.carousel', 'flex', 'remodal'), array('inline' => false)) ?>
<?php echo $this->append('script') ?>
<script>
    $(function () {
        $('a[href="#carEnquiry"]').on('click', function () {
            var inst = $('[data-remodal-id=carEnquiry]').remodal();
            inst.open();
        });
        $('a[href="#offerEnquiry"]').on('click', function () {
            var inst = $('[data-remodal-id=offerEnquiry]').remodal();
            inst.open();
        });
        if (self.location.hash.match(/carEnquiry/)) {
            var inst = $('[data-remodal-id=carEnquiry]').remodal();
            inst.open();
        }
        if (self.location.hash.match(/offerEnquiry/)) {
            var inst = $('[data-remodal-id=offerEnquiry]').remodal();
            inst.open();
        }

        $('.car-thumbs a').on('click', function () {
            href = $(this).prop('href');
            if ($('img[src="' + href + '"]').is(':hidden')) {
                $('.main-photo:visible').hide();
                $('img[src="' + href + '"]').fadeIn('slow');
            }
            return false;
        });
    })
</script>
<?php
echo $this->end()?>