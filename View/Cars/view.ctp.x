<?php echo $this->element('cars/car-enquiry', array('offer' => false)); ?>
<?php echo $this->element('cars/car-enquiry', array('offer' => true)); ?>
<div class="pages">
    <div class="container">
        <div class="col-md-8">
            <div class="listing-section">
                <div class="listing-box-head">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="meta">
                                <?php $title = $car['Car']['year'] . ' ' . $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series']; ?>
                                <h3 class="inline-block"> <a href="#"><?php echo $title ?></a></h3>
                                <span class="badge"><?php echo $car[0]['CarsCount']; ?></span> <a href="#" class="inline-block">Available at <strong><?php echo $car['DealerLocation']['name']; ?></strong></a> <i class="fa fa-map-marker"></i> </div>
                        </div>
                        <div class="col-md-3 text-right"><a href="#"><img title="" alt="" src="<?php echo get_resized_image_url($car['Dealer']['logo'], 120) ?>"></a></div>
                    </div>
                </div>
                <!-- /listing-box-head  -->
                <div class="listing-box-content">
                    <div class="row row-sm">
                        <div class="col-md-9"> 
                            <?php
//                            debug($car['CarImage']);
                            if (!empty($car['CarImage'])) {
                                foreach ($car['CarImage'] as $key => $image) {
                                    if ($key == 0) {
                                        ?>
                                        <img class="main-photo" src="<?php echo get_resized_image_url($image['image'], 550, 370) ?>"> 
                                        <?php
                                    } else {
                                        ?>
                                        <img class="main-photo" src="<?php echo get_resized_image_url($image['image'], 550, 370) ?>" style="display: none;"> 
                                        <?php
                                    }
                                }
                            } else {
                                ?>

                                <img class="main-photo" src="<?php echo get_resized_image_url('default-car.png', 550, 370) ?>"> 
                            <?php }
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div class="listing-map">
                                <?php
                                echo $this->element('dealer_map', array('size' => array('width' => 169, 'height' => 125),
                                    'latitude' => $car['DealerLocation']['latitude'], 'longitude' => $car['DealerLocation']['longitude'], 'zoom' => $car['DealerLocation']['zoom'], 'label' => $car['DealerLocation']['name']))
                                ?>
                            </div>
                            <div class="map-caption m-b-sm">
                                <div class="row">
                                    <div class="col-md-8 text-center"> <span class="item-price">$<?php echo $this->Number->format((float) $car['Car']['selling_price']) ?></span> </div>
                                    <div class="col-md-4"> <span class="item-meta bold">Drive Away</span> </div>
                                </div>
                            </div>
                            <div class="car-thumbs clearfix">
                                <?php
                                if (!empty($car['CarImage'])) {
                                    foreach ($car['CarImage'] as $key => $image) {
                                        ?>
                                        <a href="<?php echo get_resized_image_url($image['image'], 550, 370) ?>"><img src="<?php echo get_resized_image_url($image['image'], 59, 59) ?>"></a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- listing box content -->

                <div class="row row-sm">
                    <div class="col-md-8">
                        <div class="tabs car-tabs">
                            <ul class="tab-buttons clearfix p-md">
                                <li data-id="#VehicleDetails" class="tab-btn active"><a href="#">Vehicle Details</a></li>
                                <li data-id="#KeyFeatures" class="tab-btn"><a href="#">Key Features</a></li>
                                <!--<li data-id="#Description" class="tab-btn"><a href="#">Description</a></li>-->
                                <!--<li data-id="#Video" class="tab-btn"><a href="#">Video</a></li>-->
                                <!--<li data-id="#CarStats" class="tab-btn"><a href="#">Car Stats</a></li>-->
                            </ul>
                            <div class="tabs-box">
                                <div class="tab current" id="VehicleDetails">
                                    <h3 class="title-bar">Vehicle details</h3>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="features-table">
                                        <tr>
                                            <th>Make</th>
                                            <td><?php echo $car['Car']['make'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Model</th>
                                            <td><?php echo $car['Car']['model'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Year</th>
                                            <td><?php echo $car['Car']['year'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Body</th>
                                            <td><?php echo $car['Car']['body'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Engine</th>
                                            <td><?php echo $car['Car']['engine_capacity'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Transmission</th>
                                            <td><?php echo $car['Car']['transmission'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Kilometers</th>
                                            <td><?php echo $car['Car']['odometer'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>Fuel Economy</th>
                                            <td>Cruz CDX</td>
                                        </tr>
                                        <tr>
                                            <th>ANCAP Rating</th>
                                            <td><div class="star-rating">
                                                    <?php
                                                    for ($i = 1; $i <= 5; $i++) {
                                                        if ($i <= $car['Car']['ancap_rating']) {
                                                            $class = "";
                                                        } else {
                                                            $class = "inactive";
                                                        }
                                                        ?>
                                                        <a href="#"><i class="fa fa-star-o <?php echo $class ?>"></i></a>
                                                    <?php } ?>

                                                </div></td>
                                        </tr>
                                        <tr>
                                            <th>Green Star Rating</th>
                                            <td><div class="star-rating"><div class="star-rating">
                                                        <?php
                                                        for ($i = 1; $i <= 5; $i++) {
                                                            if ($i <= $car['Car']['green_star_rating']) {
                                                                $class = "";
                                                            } else {
                                                                $class = "inactive";
                                                            }
                                                            ?>
                                                            <a href="#"><i class="fa fa-star-o <?php echo $class ?>"></i></a>
                                                        <?php } ?>

                                                    </div></td>
                                        </tr>
                                        <tr>
                                            <th>Registration Plate</th>
                                            <td>DUALIZ</td>
                                        </tr>
                                        <tr>
                                            <th>Registration Expiry</th>
                                            <td><?php echo!empty($car['Car']['registration_expiry']) ? date('d/m/Y', strtotime($car['Car']['registration_expiry'])) : "" ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tab" id="KeyFeatures">
                                    <h3 class="title-bar">Standard Features</h3>
                                    <div class="p-sm">

                                        <?php
                                        if (!empty($car['Car']['comments'])) {
                                            $features = explode(',', $car['Car']['comments']);
                                            foreach ($features as $key => $feature) {
                                                if ($key == 0) {
                                                    echo '<div class="row m-b-lg">
                                                            <div class="col-md-6">
                                                <ul class="standard-features">';
                                                }
                                                if (($key % 40) == 0 && $key != 0) {
                                                    echo '</div><div class="row m-b-lg">';
                                                }
                                                if (($key % 20) == 0 && $key != 0) {
                                                    echo '</ul></div><div class="col-md-6">
                                                <ul class="standard-features">';
                                                }
                                                ?>
                                                <li><span><?php echo $feature ?></span></li>
                                                <?php
                                            }
                                            echo '</ul></div></div>';
                                        }
                                        ?>
                                        <!--                                            <div class="col-md-6">
                                                                                        <ul class="standard-features">
                                                                                            <li><span>Dual Airbag Package</span></li>
                                                                                            <li><span>Dual Airbag Package</span></li>
                                                                                            <li><span>Anti-lock Braking</span></li>
                                                                                            <li><span>Air Conditioning</span></li>
                                                                                            <li><span>Adjustable Steering Wheel  - Tilt &amp; Telescopic</span></li>
                                                                                            <li><span>17 Inch Alloy Wheels</span></li>
                                                                                            <li><span>Brake Assist</span></li>
                                                                                            <li><span>Cruise Control</span></li>
                                                                                            <li><span>Central Locking Remote Control</span></li>
                                                                                            <li><span>Dusk Sensing Headlights</span></li>
                                                                                            <li><span>Electronic Brake Force  Distribution</span></li>
                                                                                            <li><span>Electronic Stability  Program</span></li>
                                                                                            <li><span>Fog Lights - Front</span></li>
                                                                                            <li><span>Head Airbags</span></li>
                                                                                            <li><span>Heated Front Seats</span></li>
                                                                                            <li><span>Engine Immobiliser</span></li>
                                                                                            <li><span>Leather Accented  Upholstery</span></li>
                                                                                            <li><span>Leather Steering Wheel</span></li>
                                                                                            <li><span>Multi Function Steering  Wheel</span></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <ul class="standard-features">
                                                                                            <li><span>Dual Airbag Package</span></li>
                                                                                            <li><span>Dual Airbag Package</span></li>
                                                                                            <li><span>Anti-lock Braking</span></li>
                                                                                            <li><span>Air Conditioning</span></li>
                                                                                            <li><span>Adjustable Steering Wheel  - Tilt &amp; Telescopic</span></li>
                                                                                            <li><span>17 Inch Alloy Wheels</span></li>
                                                                                            <li><span>Brake Assist</span></li>
                                                                                            <li><span>Cruise Control</span></li>
                                                                                            <li><span>Central Locking Remote Control</span></li>
                                                                                            <li><span>Dusk Sensing Headlights</span></li>
                                                                                            <li><span>Electronic Brake Force  Distribution</span></li>
                                                                                            <li><span>Electronic Stability  Program</span></li>
                                                                                            <li><span>Fog Lights - Front</span></li>
                                                                                            <li><span>Head Airbags</span></li>
                                                                                            <li><span>Heated Front Seats</span></li>
                                                                                            <li><span>Engine Immobiliser</span></li>
                                                                                            <li><span>Leather Accented  Upholstery</span></li>
                                                                                            <li><span>Leather Steering Wheel</span></li>
                                                                                            <li><span>Multi Function Steering  Wheel</span></li>
                                                                                        </ul>
                                                                                    </div>-->

                                    </div>
                                    <!--                                    <h3 class="title-bar">Tech Specs</h3>
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="features-table">
                                                                            <tr>
                                                                                <th>Make</th>
                                                                                <td>Holden</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Model</th>
                                                                                <td>Cruz CDX</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Year</th>
                                                                                <td>2001</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Body</th>
                                                                                <td>01/12/2014</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Engine</th>
                                                                                <td>Cruz CDX</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Transmission</th>
                                                                                <td>Cruz CDX</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Kilometers</th>
                                                                                <td>Cruz CDX</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Fuel Economy</th>
                                                                                <td>Cruz CDX</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>ANCAP Rating</th>
                                                                                <td><div class="star-rating"> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o inactive"></i></a> </div></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Green Star Rating</th>
                                                                                <td><div class="star-rating"> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o"></i></a> <a href="#"><i class="fa fa-star-o inactive"></i></a> </div></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Registration Plate</th>
                                                                                <td>DUALIZ</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Registration Expiry</th>
                                                                                <td>01/12/2014</td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <div class="tab" id="Description">
                                                                        <h3 class="title-bar">Standard Features</h3>
                                                                        <div class="p-sm">
                                                                            <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua.</strong> <br>
                                                                                <br>
                                                                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sintoccaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. <br>
                                                                                <br>
                                                                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.  Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed  </p>
                                                                            <table  border="0" cellspacing="0" cellpadding="0" class="m-t-md">
                                                                                <tr>
                                                                                    <td width="100"><img src="<?php echo Router::url('/css/img/raa.png') ?>" /></td>
                                                                                    <td><img src="<?php echo Router::url('/css/img/ancap.png') ?>" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>-->
                                    <!--                                <div class="tab" id="Video">
                                                                        <h3 class="title-bar">Video Title</h3>
                                                                        <div class="p-sm">
                                                                            <div id="Videoslides" class="video-slider">
                                                                                <div id="videoslider" class="flexslider">
                                                                                    <ul class="slides">
                                                                                        <li> <img src="http://dummyimage.com/460x300/000/000"> </li>
                                                                                        <li> <img src="http://dummyimage.com/460x300/c7110c/c7110c"> </li>
                                                                                        <li> <img src="http://dummyimage.com/460x300/3c5b9a/3c5b9a"> </li>
                                                                                        <li> <img src="http://dummyimage.com/460x300/f9dc48/f9dc48"> </li>
                                                                                        <li> <img src="http://dummyimage.com/460x300/3c5b9a/3c5b9a"> </li>
                                                                                        <li> <img src="http://dummyimage.com/460x300/f9dc48/f9dc48"> </li>
                                                                                         items mirrored twice, total of 12 
                                                                                    </ul>
                                                                                </div>
                                                                                <div id="videocarousel" class="video-carousel flexslider">
                                                                                    <ul class="slides">
                                                                                        <li> <img src="http://dummyimage.com/108x72/000/000"> </li>
                                                                                        <li> <img src="http://dummyimage.com/108x72/c7110c/c7110c"> </li>
                                                                                        <li> <img src="http://dummyimage.com/108x72/3c5b9a/3c5b9a"> </li>
                                                                                        <li> <img src="http://dummyimage.com/108x72/f9dc48/f9dc48"> </li>
                                                                                        <li> <img src="http://dummyimage.com/108x72/3c5b9a/3c5b9a"> </li>
                                                                                        <li> <img src="http://dummyimage.com/108x72/f9dc48/f9dc48"> </li>
                                                                                         items mirrored twice, total of 12 
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                </div>
                            </div>
                        </div>

                        <!-- tabs --> 

                    </div>
                    <div class="col-md-3  col-md-push-1">
                        <div class="side-car-actions m-b-xl">
                            <a href="#carEnquiry" class="btn btn-block bold btn-md text-center btn-default m-b-sm">ENQUIRE</a>
                            <a href="#offerEnquiry" class="btn btn-block bold btn-md text-center btn-dark m-b-sm">MAKE OFFER</a>
                            <a href="#" class="btn btn-block bold btn-md text-center btn-darkshape m-b-sm">CALL DEALER</a> 
							<a href="<?php echo Router::url(array('controller' => 'user_cars', 'action' => 'add', $car['Car']['id'])) ?>" class="btn btn-block bold btn-md text-center btn-darkshape m-b-sm">Save Car</a> 
							</div>
                                <div class="side-social text-sm">
                        <h5 class="bold m-b-md">SHARE</h5>
                        <p>Found something you like?
                            Share it with your friends!</p>
                        <?php echo $this->element('share_links', array('share_title' => $title)); ?>
							</div>
						   </div>
               
                </div>
            </div>
            <!-- /listing-section  --> 
        </div>
    </div>
    <?php echo $this->element('Users.right_menu'); ?>

</div>
</div>
<?php echo $this->Html->script(array('owl.carousel', 'flex', 'remodal'), array('inline' => false)) ?>
<?php echo $this->append('script') ?>
<script>
    $(function () {
        $('a[href="#carEnquiry"]').on('click', function () {
            var inst = $('[data-remodal-id=carEnquiry]').remodal();
            inst.open();
        });
        $('a[href="#offerEnquiry"]').on('click', function () {
            var inst = $('[data-remodal-id=offerEnquiry]').remodal();
            inst.open();
        });
        if (self.location.hash.match(/carEnquiry/)) {
            var inst = $('[data-remodal-id=carEnquiry]').remodal();
            inst.open();
        }
        if (self.location.hash.match(/offerEnquiry/)) {
            var inst = $('[data-remodal-id=offerEnquiry]').remodal();
            inst.open();
        }

        $('.car-thumbs a').on('click', function () {
            href = $(this).prop('href');
            if ($('img[src="' + href + '"]').is(':hidden')) {
                $('.main-photo:visible').hide();
                $('img[src="' + href + '"]').fadeIn('slow');
            }
            return false;
        });
    })
</script>
<?php
echo $this->end()?>