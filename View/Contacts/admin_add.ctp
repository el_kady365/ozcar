<div class="contacts form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'Contact' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('name' , array('class' => 'input required' ) );
		echo $this->Form->input('subject' , array('class' => 'input required' ) );
		echo $this->Form->input('email' , array('class' => 'input email required' ) );
		echo $this->Form->input('message' , array('class' => 'input required editor' ) );
		echo $this->Form->input('ip_address' , array('class' => 'input' ) );

// 		echo $this->Form->enableAjaxUploads();
		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
