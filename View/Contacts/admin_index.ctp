<div class="contacts index">
    <h2><?php echo __('Contacts'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'Contact.id' => array('edit_link' => array('action' => 'view', '%id%')),
        'Contact.name' => array('edit_link' => array('action' => 'view', '%id%')),
        'Contact.subject' => array('edit_link' => array('action' => 'view', '%id%')),
        'Contact.email' => array('edit_link' => array('action' => 'view', '%id%')),
//		'Contact.message' => array('edit_link' => array('action' => 'view' , '%id%')) , 
        'Contact.ip_address' => array('edit_link' => array('action' => 'view', '%id%')),
        'Contact.created' => array('edit_link' => array('action' => 'view', '%id%')),
//		'Contact.modified' => array('edit_link' => array('action' => 'view' , '%id%')) , 
    );
    
    $links = array(
        $this->Html->link(__('view', true), array('action' => 'view', '%id%'), array('class' => 'View')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), //, __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $contacts, $links, true, $multi_select_actions);
    ?>
</div>
