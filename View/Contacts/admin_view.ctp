<div class="contacts adminview">
    <h2><?php echo __('Company Profile'); ?></h2>
    <table class="listing-table" cellspacing="0" cellpaddin="0" width="100%">
        <tr>
            <td width="115"><strong><?php echo __('Id'); ?></strong></td>
            <td><?php echo h($contact['Contact']['id']); ?></td>
        </tr>
        <tr>
            <td><strong><?php echo __('Name'); ?></strong></td>
            <td><?php echo h($contact['Contact']['name']); ?></td>
        </tr>
        <tr>
            <td><strong><?php echo __('Subject'); ?></strong></td>
            <td><?php echo h($contact['Contact']['subject']); ?></td>
        </tr>
        <tr>
            <td><strong><?php echo __('Email'); ?></strong></td>
            <td><?php echo h($contact['Contact']['email']); ?></td>
        </tr>
       
        <tr>
            <td><strong><?php echo __('Message'); ?></strong></td>
            <td><?php echo h($contact['Contact']['message']); ?></td>
        </tr>
        <tr>
            <td><strong><?php echo __('Ip Adress'); ?></strong></td>
            <td><?php echo h($contact['Contact']['ip_address']); ?></td>
        </tr>
        <?php if (!empty($contact['Team']['id'])) { ?>
            <tr>
                <td><strong><?php echo __('Team member id'); ?></strong></td>
                <td><?php echo h($contact['Team']['id']); ?></td>
            </tr>
            <tr>
                <td><strong><?php echo __('Team member name'); ?></strong></td>
                <td><?php echo h($contact['Team']['name']); ?></td>
            </tr>
            <tr>
                <td><strong><?php echo __('Team member email'); ?></strong></td>
                <td><?php echo h($contact['Team']['email']); ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td><strong><?php echo __('Sent On'); ?></strong></td>
            <td><?php echo h($contact['Contact']['created']); ?></td>
        </tr>
    </table>
</div>
