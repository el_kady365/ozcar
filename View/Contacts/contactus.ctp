<?php
if (isset($customer_relations_thank_you)) {
    echo $this->element('ThankYouPopup', array('thank_you' => $customer_relations_thank_you));
}
?>
<div class="section finder-form">
    <div class="container">
        <h2 class="bold focobold text-xl m-b-md">Email us your enquiry</h2>

        <div class="enq-form">
            <?php
             echo $this->Form->create('Contact', array('url' => '/contact'));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="input text">
                        <?php echo $this->Form->input('fname', array("placeholder" => "Name", 'label' => false, 'type' => 'text', 'div' => false)); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input text">
                        <?php echo $this->Form->input('lname', array("placeholder" => "Last name", 'label' => false, 'type' => 'text', 'div' => false)); ?>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="input text">
                        <?php echo $this->Form->input('email', array("placeholder" => "Email", 'label' => false, 'type' => 'text', 'div' => false)); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input text">
                        <?php echo $this->Form->input('tel', array("placeholder" => "Mobile number", 'label' => false, 'type' => 'text', 'div' => false)); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input text">
                        <?php echo $this->Form->input('suburb', array("placeholder" => "Suburb", 'label' => false, 'type' => 'text', 'div' => false)); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input text">
                        <?php echo $this->Form->input('postcode', array("placeholder" => "Postcode", 'label' => false, 'type' => 'text', 'div' => false)); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="input textarea">
                        <?php echo $this->Form->input('message', array('type'=>"textarea","placeholder" => "Comments", 'label' => false, 'div' => false)); ?>
                    </div>
                </div>
                
                <div class="col-md-12">

                </div>
                <div class="col-md-3 col-md-push-9 m-t-sm">
                    <button class="btn btn-primary btn-md btn-block" type="submit">Submit</button>
                </div>

            </div>
            </form>
        </div>
    </div>
</div>


