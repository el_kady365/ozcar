
<h1>Contact us</h1>
<div class="contact">
    <div class="contact-form left">
        <div class="snippets">
            <?php echo $contact_header; ?>

        </div>
        <?php
        echo $this->Form->create('Contact', array('url' => '/contact'));
        echo $this->Form->input('name', array('class' => 'required'));
        echo $this->Form->input('subject', array('class' => 'required'));
        echo $this->Form->input('email', array('class' => 'email required'));
        echo $this->Form->input('message', array('class' => 'required '));
        ?>
        <div class="submit">
            <input type="submit" value="Submit" class="bg-a" />
        </div> 
        <?php echo $this->Form->end(); ?>



    </div>
    <div class="contact-map right">
        <div class="snippets">
            <?php echo $contact_address; ?>
        </div>

        <?php
        if (!empty($config['contact.map_coordinates'])):
            $coords = explode(',', $config['contact.map_coordinates']);
            ?>
            <div class="map">
                <?php echo $this->GoogleMap->create_map($coords[0], $coords[1], $coords[2], array('width' => '100%', 'height' => '300px')); ?>
            </div>
        <?php endif; ?>

    </div>
    <div class="clear"></div>
</div>