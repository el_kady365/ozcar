<div class="dealerLocations form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'DealerLocation';
        if ($this->action == "add") {
            echo $this->Form->create($modelClass, array('type' => 'file', "url" => array($this->data["DealerLocation"]["dealer_id"])));
        } else {
            echo $this->Form->create($modelClass, array('type' => 'file'));
        }

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('name', array('class' => 'input'));
        echo $this->Form->input('dealer_id', array('class' => 'input', "disabled", "disabled"));
        echo $this->Form->input('dealer_id', array('class' => 'input', "type" => "hidden"));
        echo $this->Form->input('code', array('class' => 'input'));
        echo $this->Form->input('email', array('class' => 'input'));
        $field = 'logo';
        echo $this->Form->input($field, array('class' => 'input', 'type' => 'file', 'between' => $this->element('image_input_between', array('info' => $file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data[$modelClass]['id']) ? $this->data[$modelClass]['id'] : null), 'base_name' => (!empty($this->request->data[$modelClass][$field]) ? $this->request->data[$modelClass][$field] : '')))));
        echo $this->Form->input('address1', array('class' => 'input'));
        echo $this->Form->input('address2', array('class' => 'input'));
        echo $this->Form->input('suburb', array('class' => 'input'));
        echo $this->Form->input('city', array('class' => 'input'));
        echo $this->Form->input('state', array('class' => 'input', "empty" => "Select State"));
        echo $this->Form->input('postal_code', array('class' => 'input'));
        echo $this->Form->input('phone1', array('class' => 'input'));
        echo $this->Form->input('phone2', array('label' => 'Fax', 'class' => 'input'));
        echo $this->Form->input('working_hours', array('class' => 'input'));
        ?>
        <div class="map">
            <?php
            echo $this->GoogleMap->embed("DealerLocation", "coordinates", "Location On Map", "latitude", "longitude", "zoom", array('width' => '100%', 'height' => '300px'));
            ?>
        </div>
        <br />
        <?
        echo $this->Form->input('display_order', array('class' => 'input'));
        echo $this->Form->input('active', array('class' => 'input'));

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
