<?php
$dealer_id = null;
if (!empty($_GET["dealer_id"])) {
    $dealer_id = $_GET["dealer_id"];
} elseif (!empty($_SESSION["dealer"])) {
    $dealer_id = $_SESSION["dealer"]["Dealer"]["id"];
}
?>
<div class="dealerLocations index">
    <?php
    if (!empty($dealer_id)) :
        ?>
        <a style="float:right" class="btn btn-success btn-sm" href="<?php echo $this->Html->url(array("action" => "add", $dealer_id)) ?>">
            <span aria-hidden="true" class="st-ico st-icon-plus-2"></span>+ Add New Location
        </a>
        <div class="clear"></div>
    <?php endif; ?>
    <h2><?php
        if (!empty($dealer_id)) {
            echo __("$dealers[$dealer_id] Locations");
        }else{
            echo __('Dealer Locations');
        }
        ?>
    </h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'DealerLocation.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'DealerLocation.name' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Dealer.name' => array("title" => "Dealer"),
        'logo' => array('title' => 'LOGO', 'php_expression' => '"."<img src=\"".$row["DealerLocation"]["logo_full_path"]."\" />"."'),
//		'DealerLocation.address1' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.address2' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.suburb' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.city' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.state' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.coutry' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.postal_code' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.phone1' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.phone2' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.working_hours' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.longitude' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.latitude' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
        'DealerLocation.active' => array('edit_link' => array('action' => 'edit', '%id%'), "format" => "bool"),
//		'DealerLocation.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'DealerLocation.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
    $links = array(
        $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), // , __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $dealerLocations, $links, true, $multi_select_actions);
    ?>
</div>
