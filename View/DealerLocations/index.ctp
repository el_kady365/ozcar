<div class="dealerLocations index">
	<h2><?php echo __('Dealer Locations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Name' ; ?></th>
	            <th><?php echo 'Dealer Id' ; ?></th>
	            <th><?php echo 'Address1' ; ?></th>
	            <th><?php echo 'Address2' ; ?></th>
	            <th><?php echo 'Suburb' ; ?></th>
	            <th><?php echo 'City' ; ?></th>
	            <th><?php echo 'State' ; ?></th>
	            <th><?php echo 'Coutry' ; ?></th>
	            <th><?php echo 'Postal Code' ; ?></th>
	            <th><?php echo 'Phone1' ; ?></th>
	            <th><?php echo 'Phone2' ; ?></th>
	            <th><?php echo 'Working Hours' ; ?></th>
	            <th><?php echo 'Longitude' ; ?></th>
	            <th><?php echo 'Latitude' ; ?></th>
	            <th><?php echo 'Active' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($dealerLocations as $dealerLocation): ?>
	<tr>
		<td><?php echo h($dealerLocation['DealerLocation']['id']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($dealerLocation['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $dealerLocation['Dealer']['id'])); ?>
		</td>
		<td><?php echo h($dealerLocation['DealerLocation']['address1']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['address2']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['suburb']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['city']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['state']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['coutry']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['postal_code']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['phone1']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['phone2']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['working_hours']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['longitude']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['latitude']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['active']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['created']); ?>&nbsp;</td>
		<td><?php echo h($dealerLocation['DealerLocation']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
