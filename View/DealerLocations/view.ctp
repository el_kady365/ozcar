<div class="dealerLocations view">
<h2><?php  echo __('Dealer Location'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dealer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($dealerLocation['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $dealerLocation['Dealer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address1'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['address1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address2'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['address2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Suburb'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['suburb']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Coutry'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['coutry']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Postal Code'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['postal_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone1'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['phone1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone2'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['phone2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Working Hours'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['working_hours']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Longitude'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['longitude']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Latitude'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['latitude']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($dealerLocation['DealerLocation']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Dealer Location'), array('action' => 'edit', $dealerLocation['DealerLocation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Dealer Location'), array('action' => 'delete', $dealerLocation['DealerLocation']['id']), null, __('Are you sure you want to delete # %s?', $dealerLocation['DealerLocation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealer Locations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer Location'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealers'), array('controller' => 'dealers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer'), array('controller' => 'dealers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cars'), array('controller' => 'cars', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Car'), array('controller' => 'cars', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cars'); ?></h3>
	<?php if (!empty($dealerLocation['Car'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Make Id'); ?></th>
		<th><?php echo __('Car Model Id'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Badge'); ?></th>
		<th><?php echo __('Series'); ?></th>
		<th><?php echo __('Body'); ?></th>
		<th><?php echo __('Engine Capacity'); ?></th>
		<th><?php echo __('Cylinders'); ?></th>
		<th><?php echo __('Fuel Type'); ?></th>
		<th><?php echo __('Doors'); ?></th>
		<th><?php echo __('Stock Number'); ?></th>
		<th><?php echo __('Rego'); ?></th>
		<th><?php echo __('Vin Number'); ?></th>
		<th><?php echo __('Dealer Id'); ?></th>
		<th><?php echo __('Odometer'); ?></th>
		<th><?php echo __('Colour'); ?></th>
		<th><?php echo __('Trim'); ?></th>
		<th><?php echo __('Feature Code'); ?></th>
		<th><?php echo __('Selling Price'); ?></th>
		<th><?php echo __('Comments'); ?></th>
		<th><?php echo __('Dealer Location Id'); ?></th>
		<th><?php echo __('Compliance Date'); ?></th>
		<th><?php echo __('Redbook Code'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($dealerLocation['Car'] as $car): ?>
		<tr>
			<td><?php echo $car['id']; ?></td>
			<td><?php echo $car['make_id']; ?></td>
			<td><?php echo $car['car_model_id']; ?></td>
			<td><?php echo $car['year']; ?></td>
			<td><?php echo $car['badge']; ?></td>
			<td><?php echo $car['series']; ?></td>
			<td><?php echo $car['body']; ?></td>
			<td><?php echo $car['engine_capacity']; ?></td>
			<td><?php echo $car['cylinders']; ?></td>
			<td><?php echo $car['fuel_type']; ?></td>
			<td><?php echo $car['doors']; ?></td>
			<td><?php echo $car['stock_number']; ?></td>
			<td><?php echo $car['rego']; ?></td>
			<td><?php echo $car['vin_number']; ?></td>
			<td><?php echo $car['dealer_id']; ?></td>
			<td><?php echo $car['odometer']; ?></td>
			<td><?php echo $car['colour']; ?></td>
			<td><?php echo $car['trim']; ?></td>
			<td><?php echo $car['feature_code']; ?></td>
			<td><?php echo $car['selling_price']; ?></td>
			<td><?php echo $car['comments']; ?></td>
			<td><?php echo $car['dealer_location_id']; ?></td>
			<td><?php echo $car['compliance_date']; ?></td>
			<td><?php echo $car['redbook_code']; ?></td>
			<td><?php echo $car['created']; ?></td>
			<td><?php echo $car['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cars', 'action' => 'view', $car['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cars', 'action' => 'edit', $car['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cars', 'action' => 'delete', $car['id']), null, __('Are you sure you want to delete # %s?', $car['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Car'), array('controller' => 'cars', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
