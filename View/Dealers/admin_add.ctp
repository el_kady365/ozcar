<div class="dealers form ExtendedForm">
    <h1>Setup Dealership</h1>
    <div class="form-wrap">


        <?php
        $modelClass = 'Dealer';
        echo $this->Form->create($modelClass, array('type' => 'file'));
        ?>

        <div class="form-section m-b-md">
            <div class="row">

                <div class="col-md-4">
                    <?php echo $this->Form->input('name', array('class' => 'input')); ?>
                    <?php
                    if ($this->action != "admin_add") {
                        echo $this->Form->input('main_location_id', array('class' => 'input'));
                    }
                    ?>
                </div>
                <div class="col-md-4"><?php echo $this->Form->input('email', array('class' => 'input email')); ?></div>
                <div class="col-md-4"><?php echo $this->Form->input('password', array('class' => 'input', "autocomplete" => "off")); ?></div>
                <div class="col-md-12"> 
                    <?php
                    echo $this->Form->input('active', array('class' => 'input'));
// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 
                    ?>

                </div>
            </div>
        </div>


        <div class="form-section">
            <h2 class="section-heading">1. Logo</h2>
            <?php echo $this->Form->input('id', array('class' => 'input required')); ?>
            <div class="row">
                <div class="col-md-4"> 
                    <?php echo $this->Form->label("logo", 'UPLOAD <i class="fa fa-folder-open-o"></i>', array('class' => 'm-b-sm block')) ?>
                    <div class="upload-box" <?php
                    if (!empty($this->request->data['Dealer']['logo'])) {
                        $label = "";
                        ?>
                             style="background-image:url('<?php echo $this->request->data['Dealer']['logo_full_path'] ?>');background-repeat: no-repeat;background-position: center;"
                             <?php
                         } else {
                             $label = "+";
                         }
                         ?>>
                             <?php
                             $field = 'logo';
                             echo $this->Form->input($field, array('class' => '', 'label' => $label, 'div' => false, 'type' => 'file', 'between' => $this->element('image_input_between', array('info' => $file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data[$modelClass]['id']) ? $this->data[$modelClass]['id'] : null), 'base_name' => (!empty($this->request->data[$modelClass][$field]) ? $this->request->data[$modelClass][$field] : '')))));
                             ?>
                    </div>
                </div>


            </div>
        </div>




        <div id="" class="form-section" >
            <h2 class="section-heading">2. Dealer Locations</h2>
            <?php
            echo $this->Html->script('many-add-delete', array('inline' => false));
            $DealerLocations = !empty($this->data['DealerLocation']) ? $this->data['DealerLocation'] : array(array());
            ?>
            <div class="page_tabs many-container">
                <ul id="page_tabs" class="many">
                    <?php foreach ($DealerLocations as $i => $dealer_location) :
                        ?>
                        <li class="record p-sm">
                            <h5 class="dealership-title">Dealership <?php echo ($i + 1) ?></h5>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.id");
                                    echo $this->Form->input("DealerLocation.$i.name", array('class' => 'input', 'label' => 'COMPANY NAME'));
                                    ?>
                                </div>



                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.address1", array('class' => 'input', 'label' => 'ADDRESS'));
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.suburb", array('class' => 'input', 'label' => 'SUBURB'));
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php echo $this->Form->input("DealerLocation.$i.city", array('class' => 'input', 'label' => 'CITY'));
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php echo $this->Form->input("DealerLocation.$i.state", array('class' => 'input', 'label' => 'STATE', "empty" => "Select State"));
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.postal_code", array('class' => 'input', 'label' => 'POSTCODE'));
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.phone1", array('class' => 'input', 'label' => 'PHONE LINE1',));
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.phone2", array('label' => 'PHONE LINE2', 'class' => 'input'));
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.email", array('class' => 'input', 'label' => 'EMAIL ADDRESS'));
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.display_order", array('class' => 'input', 'label' => 'DISPLAY ORDER'));
                                    ?>
                                </div>

                                <div class="col-md-4"> 
                                  <div class="input upload">
                                    <?php echo $this->Form->label("DealerLocation.$i.logo", 'LOCATION IMAGE <i class="fa fa-folder-open-o"></i>', array('class' => 'block')) ?>
                                    <div class="upload-box" <?php
                                    if (!empty($this->request->data['DealerLocation'][$i]['logo'])) {
                                        $label = "";
                                        ?>
                                             style="background-image:url('<?php echo $this->request->data['DealerLocation'][$i]['logo_full_path'] ?>');background-repeat: no-repeat;background-position: center;"
                                             <?php
                                         } else {
                                             $label = "+";
                                         }
                                         ?>>
                                             <?php
                                             echo $this->Form->input("DealerLocation.$i.logo", array('class' => '', 'label' => $label, 'div' => false, 'type' => 'file', 'between' => $this->element('image_input_between', array('info' => $dealerlocation_file_settings['logo'], 'field' => 'logo', 'id' => (!empty($this->request->data['DealerLocation'][$i]['id']) ? $this->data['DealerLocation'][$i]['id'] : null), 'base_name' => (!empty($this->request->data['DealerLocation'][$i]['logo']) ? $this->request->data['DealerLocation'][$i]['logo'] : '')))));
                                             ?>
                                    </div>
                                    </div>
                                </div>


                                <div class="col-md-4 working-hours ">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.working_hours", array('class' => 'input'));
                                    ?>
                                </div>
                                <div class="clear"></div>
                                <div class="col-md-4">
                                    <div class="map input">
                                        <?php
                                        echo $this->GoogleMap->oneTwoManyMap("DealerLocation", "$i.coordinates", "Location On Map", 'latitude', 'longitude', 'zoom');
                                        ?>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <?php
                                    echo $this->Form->input("DealerLocation.$i.active", array('class' => 'input', 'label' => 'ACTIVE'));
                                    ?>
                                </div>
                                <div class="clear"></div>
                                <a href="#" class="delete btn btn-danger" ><i class="fa fa-trash-o"></i></a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <a href="#" class="btn btn-block btn-md btn-primary text-center add">+ADD NEW YARD</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="input submit text-right">
            <button type="submit">SETUP DEALERSHIP</button>
        </div>

        <?php
        echo $this->Form->end();
        ?>
    </div>
</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).closest('.upload-box').css({
                    'background-image': 'url(' + e.target.result + ')',
                    'background-repeat': 'no-repeat',
                    'background-position': 'center'
                });
                $(input).closest('.upload-box').find('label').text('');
//                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', 'input[type="file"]', function () {
        readURL(this);
    });
</script>
