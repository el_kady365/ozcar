<div class="dealers index">
    <h2><?php echo __('Dealers'); ?></h2>

    <?php
    echo $this->List->filter_form($modelName, $filters);

    $fields = array(
        'Dealer.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Dealer.name' => array('edit_link' => array('action' => 'edit', '%id%')),
//		'Dealer.main_location_id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Dealer.email' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Dealer.password' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
        'logo' => array('title' => 'LOGO', 'php_expression' => '"."<img src=\"".$row["Dealer"]["logo_full_path"]."\" />"."'),
        'Dealer.active' => array('edit_link' => array('action' => 'edit', '%id%'), "format" => "bool"),
//		'Dealer.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Dealer.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
    $links = array(
        $this->Html->link(__('Manage Locations', true), array("controller" => "dealer_locations", 'action' => 'index', "?" => array("dealer_id" => '%id%')), array('class' => 'Edit')),
        $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), // , __('Are you sure?', true)),
        $this->Html->link(__('Login As', true), array("controller" => "admin", 'action' => 'loginAs', '%id%', "admin" => false), array('class' => 'LoginAs')),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $dealers, $links, true, $multi_select_actions);
    ?>
</div>
