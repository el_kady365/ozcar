<div class="dealer-account">
    <div class="row">
        <div class="col-md-6">
            <h3 class="m-b-md">My Account</h3>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table">

                <tr>
                    <td>Email:</td>
                    <td><?php echo $dealer['Dealer']['email'] ?></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>••••••••••</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td><a href="<?php echo Router::url(array('action' => 'edit', $dealer['Dealer']['id'])) ?>" class="m-t-sm btn btn-xs btn-block btn-default text-center">EDIT</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>