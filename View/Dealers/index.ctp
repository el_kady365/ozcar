<div class="dealers index">
	<h2><?php echo __('Dealers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Name' ; ?></th>
	            <th><?php echo 'Main Location Id' ; ?></th>
	            <th><?php echo 'Email' ; ?></th>
	            <th><?php echo 'Password' ; ?></th>
	            <th><?php echo 'Last Login' ; ?></th>
	            <th><?php echo 'Active' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($dealers as $dealer): ?>
	<tr>
		<td><?php echo h($dealer['Dealer']['id']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['name']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['main_location_id']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['email']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['password']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['last_login']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['active']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['created']); ?>&nbsp;</td>
		<td><?php echo h($dealer['Dealer']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
