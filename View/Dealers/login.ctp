<div class="login-section dealer-login">
    <div class="container">
        <div class="col-md-12">
            <div class="two-headers">
                <h3 class="text-center">Car DEALER CONFIDENCE</h3>
                <h2 class="text-center">WE CAN TURN YOUR VISITORS<br>
                    INTO CUSTOMERS</h2>
            </div>
        </div>
        <div class="col-md-push-1 col-md-10">
            <div class="row">
                <div class="col-md-6">
                    <div class="login-box">
                        <h5 class="text-primary text-center">DEALER CENTRE</h5>
                        <?php
                        echo $this->Session->flash('AdminLogin');
                        echo $this->Form->create('Admin', array('url' => '/admins/admin?dealer=1', 'autocomplete' => 'off')); ?>
                        <?php echo $this->Form->input('name', array('placeholder' => 'Login', 'div' => array('class' => 'input text'), 'autocomplete' => 'off', 'label' => false)); ?>
                        <?php echo $this->Form->input('password', array('placeholder' => 'Password', 'div' => array('class' => 'input text'), 'autocomplete' => 'off', 'label' => false)); ?>
                        <div class="row m-t-sm">
                            <div class="col-md-6">
                                <div class="m-t-sm"> <a class="btn btn-block text-left" href="#">Forgot your password?</a></div>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default btn-md bold pull-right" type="submit">LOGIN</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="login-box">
                        <div class="register-wrap">
                            <h5 class="text-primary text-center">Register</h5>
                            <p><?php echo Snippet::getSnippet('dealers-login-register-snippet');?> </p>

                            <div class="login-btns m-t-md">
                                <a href="<?php echo Router::url('/pages/static_page/why_register')?>" class="btn btn-default btn-md bold block">WHY SIGN UP?</a>
                                <a href="<?php echo Router::url('/pages/static_page/register')?>" class="btn btn-default btn-md bold">REGISTER NOW</a>
                            </div>

                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
    <div class="specials-promo text-center"> <a href="#"><img src="<?php echo Router::url('/') ?>css/img/banners/leader_banner_02.jpg" alt="" /></a> </div>
</div>