<div class="dealers view">
<h2><?php  echo __('Dealer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Main Location Id'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['main_location_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Login'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['last_login']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($dealer['Dealer']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Dealer'), array('action' => 'edit', $dealer['Dealer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Dealer'), array('action' => 'delete', $dealer['Dealer']['id']), null, __('Are you sure you want to delete # %s?', $dealer['Dealer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cars'), array('controller' => 'cars', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Car'), array('controller' => 'cars', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealer Locations'), array('controller' => 'dealer_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer Location'), array('controller' => 'dealer_locations', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cars'); ?></h3>
	<?php if (!empty($dealer['Car'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Make Id'); ?></th>
		<th><?php echo __('Car Model Id'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Badge'); ?></th>
		<th><?php echo __('Series'); ?></th>
		<th><?php echo __('Body'); ?></th>
		<th><?php echo __('Engine Capacity'); ?></th>
		<th><?php echo __('Cylinders'); ?></th>
		<th><?php echo __('Fuel Type'); ?></th>
		<th><?php echo __('Doors'); ?></th>
		<th><?php echo __('Stock Number'); ?></th>
		<th><?php echo __('Rego'); ?></th>
		<th><?php echo __('Vin Number'); ?></th>
		<th><?php echo __('Dealer Id'); ?></th>
		<th><?php echo __('Odometer'); ?></th>
		<th><?php echo __('Colour'); ?></th>
		<th><?php echo __('Trim'); ?></th>
		<th><?php echo __('Feature Code'); ?></th>
		<th><?php echo __('Selling Price'); ?></th>
		<th><?php echo __('Comments'); ?></th>
		<th><?php echo __('Dealer Location Id'); ?></th>
		<th><?php echo __('Compliance Date'); ?></th>
		<th><?php echo __('Redbook Code'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($dealer['Car'] as $car): ?>
		<tr>
			<td><?php echo $car['id']; ?></td>
			<td><?php echo $car['make_id']; ?></td>
			<td><?php echo $car['car_model_id']; ?></td>
			<td><?php echo $car['year']; ?></td>
			<td><?php echo $car['badge']; ?></td>
			<td><?php echo $car['series']; ?></td>
			<td><?php echo $car['body']; ?></td>
			<td><?php echo $car['engine_capacity']; ?></td>
			<td><?php echo $car['cylinders']; ?></td>
			<td><?php echo $car['fuel_type']; ?></td>
			<td><?php echo $car['doors']; ?></td>
			<td><?php echo $car['stock_number']; ?></td>
			<td><?php echo $car['rego']; ?></td>
			<td><?php echo $car['vin_number']; ?></td>
			<td><?php echo $car['dealer_id']; ?></td>
			<td><?php echo $car['odometer']; ?></td>
			<td><?php echo $car['colour']; ?></td>
			<td><?php echo $car['trim']; ?></td>
			<td><?php echo $car['feature_code']; ?></td>
			<td><?php echo $car['selling_price']; ?></td>
			<td><?php echo $car['comments']; ?></td>
			<td><?php echo $car['dealer_location_id']; ?></td>
			<td><?php echo $car['compliance_date']; ?></td>
			<td><?php echo $car['redbook_code']; ?></td>
			<td><?php echo $car['created']; ?></td>
			<td><?php echo $car['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cars', 'action' => 'view', $car['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cars', 'action' => 'edit', $car['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cars', 'action' => 'delete', $car['id']), null, __('Are you sure you want to delete # %s?', $car['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Car'), array('controller' => 'cars', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Dealer Locations'); ?></h3>
	<?php if (!empty($dealer['DealerLocation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Dealer Id'); ?></th>
		<th><?php echo __('Address1'); ?></th>
		<th><?php echo __('Address2'); ?></th>
		<th><?php echo __('Suburb'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Coutry'); ?></th>
		<th><?php echo __('Postal Code'); ?></th>
		<th><?php echo __('Phone1'); ?></th>
		<th><?php echo __('Phone2'); ?></th>
		<th><?php echo __('Working Hours'); ?></th>
		<th><?php echo __('Longitude'); ?></th>
		<th><?php echo __('Latitude'); ?></th>
		<th><?php echo __('Active'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($dealer['DealerLocation'] as $dealerLocation): ?>
		<tr>
			<td><?php echo $dealerLocation['id']; ?></td>
			<td><?php echo $dealerLocation['name']; ?></td>
			<td><?php echo $dealerLocation['dealer_id']; ?></td>
			<td><?php echo $dealerLocation['address1']; ?></td>
			<td><?php echo $dealerLocation['address2']; ?></td>
			<td><?php echo $dealerLocation['suburb']; ?></td>
			<td><?php echo $dealerLocation['city']; ?></td>
			<td><?php echo $dealerLocation['state']; ?></td>
			<td><?php echo $dealerLocation['coutry']; ?></td>
			<td><?php echo $dealerLocation['postal_code']; ?></td>
			<td><?php echo $dealerLocation['phone1']; ?></td>
			<td><?php echo $dealerLocation['phone2']; ?></td>
			<td><?php echo $dealerLocation['working_hours']; ?></td>
			<td><?php echo $dealerLocation['longitude']; ?></td>
			<td><?php echo $dealerLocation['latitude']; ?></td>
			<td><?php echo $dealerLocation['active']; ?></td>
			<td><?php echo $dealerLocation['created']; ?></td>
			<td><?php echo $dealerLocation['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'dealer_locations', 'action' => 'view', $dealerLocation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'dealer_locations', 'action' => 'edit', $dealerLocation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'dealer_locations', 'action' => 'delete', $dealerLocation['id']), null, __('Are you sure you want to delete # %s?', $dealerLocation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Dealer Location'), array('controller' => 'dealer_locations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
