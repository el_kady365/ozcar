<div class="directoryCars form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'DirectoryCar' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('make' , array('class' => 'input required' ) );
		echo $this->Form->input('model' , array('class' => 'input required' ) );
		echo $this->Form->input('body' , array('class' => 'input' ) );
		echo $this->Form->input('apl' , array('class' => 'input' ) );

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
