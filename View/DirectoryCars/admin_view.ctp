<div class="directoryCars view">
<h2><?php  echo __('Directory Car'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($directoryCar['DirectoryCar']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Make'); ?></dt>
		<dd>
			<?php echo h($directoryCar['DirectoryCar']['make']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($directoryCar['DirectoryCar']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($directoryCar['DirectoryCar']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Apl'); ?></dt>
		<dd>
			<?php echo h($directoryCar['DirectoryCar']['apl']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Directory Car'), array('action' => 'edit', $directoryCar['DirectoryCar']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Directory Car'), array('action' => 'delete', $directoryCar['DirectoryCar']['id']), null, __('Are you sure you want to delete # %s?', $directoryCar['DirectoryCar']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Directory Cars'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Directory Car'), array('action' => 'add')); ?> </li>
	</ul>
</div>
