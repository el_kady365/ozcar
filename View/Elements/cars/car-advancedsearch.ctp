<?php
$bodyTypes = Car::$BodyTypes;
echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarAdvancedSearch', 'url' => array('controller' => 'cars', 'action' => 'search')));
echo $this->Form->hidden('type_id');
echo $this->Form->input('body', array('type' => 'select', 'name' => 'body', 'options' => Car::getBodyTypesDropDown(), 'empty' => 'select body', 'style' => 'display:none', 'div' => false, 'label' => false, 'multiple' => true));
?>
<!--<div class="mega-search clearfix">
    <input type="text" value="" placeholder="The simple way to find your next car starts here" />
    <span class="search-ico"></span>
</div>-->
<div class="search-area">
    <p>You can narrow your search by filling in the fields below:</p>
    <div class="collapse-section">
        <div class="collapse-bar head-bar m-b-md">
            <h3 class="ico-14"><span class="ico"></span> General</h3>
        </div>
        <div class="collapse-content">
            <?php echo $this->Form->input('dealer_id', array('options' => $dealers, 'label' => false, 'empty' => 'Select Dealer')) ?>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->input('make', array('options' => $makes, 'label' => false, 'class' => 'makes', 'empty' => 'Select Make')) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->input('model', array('empty' => 'Select Model', 'options' => array(), 'label' => false, 'type' => 'select', 'class' => 'models')) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->input('series', array('empty' => 'Select Series', 'options' => array(), 'label' => false, 'type' => 'select', 'class' => 'models')) ?>
                </div>

            </div>
            <div class="row m-b-md m-t-md rang-row range-bordered">
                <div class="col-md-12">
                    <label>Price</label>
                </div>
                <div class="col-md-7">
                    <div id="Price"  ></div> 
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('price_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                </div>
                <div class="col-md-1 text-center">
                    <div class="m-t-md"><strong>TO</strong></div>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('price_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                </div>
            </div>
            <div class="row m-b-md m-t-md rang-row range-bordered">
                <div class="col-md-12">
                    <label>Year</label>
                </div>
                <div class="col-md-7">
                    <div id="Years" ></div> 
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('year_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                </div>
                <div class="col-md-1 text-center">
                    <div class="m-t-md"><strong>TO</strong></div>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('year_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                </div>
            </div>
        </div>
        <!-- / collapse-content -->

        <div class="collapse-bar head-bar m-b-md">
            <h3 class="ico-15"><span class="ico"></span> Location</h3>
        </div>
        <div class="collapse-content">
            <div class="row">

                <div class="col-md-6">
                    <?php echo $this->Form->input('location', array('placeholder' => 'Location/Postcode', 'class' => 'autocompleteAddress', 'label' => false)) ?>
                </div>
                <div class="col-md-2 text-center">
                    <div class="m-t-md"><strong>WITHIN</strong></div>
                </div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('distance', array('options' => Car::$Distances, 'label' => false, 'empty' => 'Select Distance')) ?>
                </div>
            </div>

        </div>
        <!-- / collapse-content --> 

        <!-- / collapse-content -->

        <div class="collapse-bar head-bar m-b-md">
            <h3 class="ico-16"><span class="ico"></span> Engine</h3>
        </div>
        <div class="collapse-content">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->input('fuel_type', array('options' => Car::$FuelTypes, 'label' => false, 'empty' => 'Fuel types')) ?>
                </div>
                
                <div class="col-md-6">
                    <?php echo $this->Form->input('transmission', array('options' => Car::$Transmissions, 'label' => false, 'empty' => 'Transmission')) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->input('cylinders', array('options' => range(1, 12), 'label' => false, 'empty' => 'Cylinders')) ?>
                </div>
            </div>
            <div class="row m-b-md m-t-md rang-row range-bordered">
                <div class="col-md-12">
                    <label>KMs</label>
                </div>
                <div class="col-md-7">
                    <div id="Kilometers" ></div> 
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('kms_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                </div>
                <div class="col-md-1 text-center">
                    <div class="m-t-md"><strong>TO</strong></div>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('kms_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                </div>
            </div>
            <div class="row m-b-md m-t-md rang-row range-bordered">
                <div class="col-md-12">
                    <label>Power (Kw)</label>
                </div>
                <div class="col-md-7">
                    <div id="Power" ></div> 
                </div>
                <div class="col-md-2">

                    <?php echo $this->Form->input('power_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>

                </div>
                <div class="col-md-1 text-center">
                    <div class="m-t-md"><strong>TO</strong></div>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('power_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                </div>
            </div>
            <div class="row m-b-md m-t-md rang-row range-bordered">
                <div class="col-md-12">
                    <label>Engine Size (Ltr)</label>
                </div>
                <div class="col-md-7">
                    <div id="EngineSize" ></div> 

                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('engine_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?>
                </div>
                <div class="col-md-1 text-center">
                    <div class="m-t-md"><strong>TO</strong></div>
                </div>
                <div class="col-md-2">
                    <?php echo $this->Form->input('engine_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?>
                </div>
            </div>
        </div>
        <div class="collapse-bar head-bar m-b-md">
            <h3 class="ico-17"><span class="ico"></span> Style</h3>
        </div>
        <div class="collapse-content">
            <div class="body-types clearfix m-b-md">
                <ul class="table-view">
                    <?php foreach ($bodyTypes as $key => $val) {
                        ?>
                        <li class="cell"> 
                            <a class="type-ico text-center" rel="<?php echo $key ?>">
                                <img title="" alt="" src="<?php echo Router::url('/css/img/shape/' . $val['image'] . '.png') ?>"><?php echo $val['title'] ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="row">
                <!--                <div class="col-md-6">
                
                                </div>-->
                <div class="col-md-6">
                    <?php echo $this->Form->input('seats', array('options' => range(2, 15), 'label' => false, 'empty' => 'Seats')) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->input('doors', array('options' => range(1, 6), 'label' => false, 'empty' => 'Doors')) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->input('colour', array('options' => $colours, 'label' => false, 'empty' => 'Colour')) ?>                
                </div>
            </div>
        </div>
        <!-- / collapse-content -->

        <div class="collapse-bar head-bar m-b-md">
            <h3 class="ico-18"><span class="ico"></span> Other</h3>
        </div>
        <div class="collapse-content">
            <!--            <div class="row row-sm search-tabs m-b-md">
                            <div class="col-md-3">
                                <div class="input radio">
                                    <label>P Plate Approved</label>
                                    <input type="radio" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input radio">
                                    <label>Car Deals</label>
                                    <input type="radio" />
                                </div>
                            </div>
                        </div>
            -->                        <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->input('green_star_rating', array('options' => Car::$carRatings, 'label' => false, 'empty' => 'Green Rating')) ?>                
                </div>
                <div class="col-md-6">
                   <?php echo $this->Form->input('ancap_rating', array('options' => Car::$carRatings, 'label' => false, 'empty' => 'ANCAP Rating')) ?>                
                </div>
                <!--                <div class="col-md-12">
                                    <div class="input textarea">
                                        <textarea placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit"></textarea>
                                    </div>
                                </div>-->
                <div class="col-md-12">
                    <div class="input submit text-center p-md">
                        <p class="bold">9,854 <strong class="text-primary">Cars Listed</strong></p>
                        <button type="submit" class="btn btn-md btn-info">SEARCH CARS</button>
                        <a href="#" class="btn reset-form btn-md btn-block text-primary">Reset Search</a> </div>
                </div>
            </div>
        </div>
        <!-- / collapse-content --> 

        <!--</div>-->
        <!-- / collapse-section --> 

    </div>
</div>
<?php echo $this->Form->end() ?>
<!-- / search-area --> 
<?php echo $this->Html->script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places', array('inline' => false)) ?>
<?php $this->append('script') ?>
<script>
    var models = <?php echo json_encode($models) ?>;
    var serieses = <?php echo json_encode($serieses); ?>;
    var bodies = <?php echo json_encode($jsBodies); ?>;
    var suburbs = <?php echo json_encode($suburbs); ?>;
    var selMake = '<?php echo!empty($params['make']) ? $params['make'] : '' ?>';
    var selModel = '<?php echo!empty($params['model']) ? $params['model'] : '' ?>';
    var selseries = '<?php echo!empty($params['series']) ? $params['series'] : '' ?>';

    $(function () {
        if ($('#Years').length) {
            $("#Years").ionRangeSlider({
                type: "double",
                prettify_enabled: false,
                min: '<?php echo date('Y') - 30 ?>',
                max: '<?php echo date('Y') ?>',
                onChange: function (data) {
                    $('input[name="year_from"]').val(data.from);
                    $('input[name="year_to"]').val(data.to);

                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="year_from"]').val('');
                        $('input[name="year_to"]').val('');
                    }


                }

            });
        }

        $('select[name="make"]').on('change', function () {
            getChildList($(this).closest('form').find('select[name="model"]'), $(this).val(), 'models')
        });



        $('select[name="model"]').on('change', function () {
            getChildList($(this).closest('form').find('select[name="series"]'), $(this).val(), 'serieses')
        });
        
        $('.reset-form').on('click', function () {
            $('#CarAdvancedSearch')[0].reset();
            return false;
        });
        function getChildList(element, data_id, type) {
            if (data_id) {
                var data = window[type][data_id];

                element.html('<option value="">Select Model</option>');
                if (type == 'serieses') {
                    element.html('<option value="">Select Series</option>');
                }

                if (data) {
                    $.each(data, function (key, value) {
                        selected = '';
                        if (selseries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                            selected = 'selected="selected"';
                        }
                        if (selModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                            selected = 'selected="selected"';
                        }

                        element.append('<option value="' + key + '" ' + selected + '>' + value + '</option>');
                        ;
                    });
                }
            }
        }
    });
    function address_initialize() {
        $(".autocompleteAddress").each(function (index) {
            var txt_Obj = $(this)[0];
            autocomplete = new google.maps.places.Autocomplete((txt_Obj), {types: ['geocode'],componentRestrictions: {country: "Aus"}});
        });
    }
    $(window).load(function () {
        address_initialize();
    })
</script>
<?php
$this->end()?>