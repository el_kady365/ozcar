<?php
App::uses('Enquiry', 'Model');
App::uses('Snippet', 'Model');
$type = 3;
$modal_id = 'carEnquiry';
$formID = 'carEnquiryForm';
$snippet_key = 'car-enquiry-top';
if ($offer) {
    $type = 4;
    $modal_id = 'offerEnquiry';
    $formID = 'offerEnquiryForm';
    $snippet_key = 'offer-enquiry-top';
}
$snippet = Snippet::getSnippet($snippet_key);
?>
<div class="msg-popup remodal" id="<?php echo $modal_id ?>"  data-remodal-id="<?php echo $modal_id ?>" data-remodal-options="hashTracking:false" >
    <a class="close-modal" data-remodal-action="close" aria-label="Close">&#215;</a>

    <h2><?php echo Enquiry::$types[$type]; ?></h2>
    <?php echo $snippet; ?>

    <div class="dealer-staff">
        <h3 class="text-primary">Contact the Dealership</h3>
        <div class="row">
            <?php
            if (!empty($dealer_location)) {
                ?>
                <div class="col-md-4">
                    <div class="staff-box">
                        <a href="#">
                            <img src="<?php echo get_resized_image_url($dealer_location['Dealer']['logo'], 136, 112) ?>">
                        </a>
                        <h3 class="bold"><?php echo $dealer_location['DealerLocation']['name'] ?></h3>
                        <?php if (!empty($dealer_location['DealerLocation']['phone1'])) { ?>
                            <p><strong>M</strong>&nbsp;&nbsp;<?php echo $dealer_location['DealerLocation']['phone1'] ?></p>
                            <?php
                        }

                        if (!empty($dealer_location['DealerLocation']['phone2'])) {
                            ?>
                            <p><strong>P</strong>&nbsp;&nbsp;<?php echo $dealer_location['DealerLocation']['phone2'] ?></p>
                        <?php } ?>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>
    </div>  

    <?php
//    debug($car['Car']);
    echo $this->Form->create('Enquiry', array('id' => $formID, 'url' => array('controller' => 'enquiries', 'action' => 'add', $type)));
    echo $this->Form->hidden('car_id', array('value' => $car['Car']['id']));
    echo $this->Form->hidden('type_id', array('value' => $type));
    echo $this->Form->hidden('dealer_location_id', array('value' => $car['Car']['dealer_location_id']));
    echo $this->Form->hidden('dealer_id', array('value' => $car['Car']['dealer_id']));
    ?>
    <h3 class="text-primary default-head">Or fill out the following form</h3>
    <?php
    if ($offer) {
        $step = 2;
        ?>
        <div class = "pop-form-group m-b-md m-t-md">
            <h3>1. Offer </h3>
            <div class = "row offer-form">
                <div class = "col-md-6">
                    <label>CURRENT PRICE</label>
                    <input type="text" value="$<?php echo!empty($car['Car']['selling_price']) ? $car['Car']['selling_price'] : '' ?>" readonly="readonly" />
                </div>
                <div class = "col-md-6">
                    <?php echo $this->Form->input('field1', Enquiry::$fields[$type]['input-options']) ?>
                </div>
            </div>
        </div>
        <?php
    } else {
        $step = 1;
    }
    ?>

    <div class="pop-form-group m-b-md m-t-md">
        <h3><?php echo $step ?>. Comments </h3>
        <div class="input textarea">
            <?php echo $this->Form->input('description', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => 'Please give a description')); ?>

        </div>
    </div>
    <div class="pop-form-group">
        <h3><?php echo ++$step ?>. Your details </h3>
        <div class="row">
            <div class="col-md-6">
                <div class="input text">
                    <?php echo $this->Form->input('first_name', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => 'FIRST NAME')); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input text">
                    <?php echo $this->Form->input('last_name', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => 'LAST NAME')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="input text">
                    <?php echo $this->Form->input('telephone', array('div' => false, 'label' => false, 'class' => 'number required', 'placeholder' => 'CONTACT NUMBER')); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input text">
                    <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'class' => 'email required', 'placeholder' => 'EMAIL')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="input text">
                    <?php echo $this->Form->input('post_code', array('div' => false, 'maxlength' => 4, 'label' => false, 'class' => 'required', 'placeholder' => 'POSTCODE')); ?>
                </div>
            </div>
            <div class="col-md-9">
                <script>
                    var a = Math.ceil(Math.random() * 10);
                    var b = Math.ceil(Math.random() * 10);
                    function DrawBotBoot(a, b) {
                        document.write("<div class = \"input text\">\n\
    <input type=\"text\" name=\"data[Enquiry][boot]\" math=\"" + a + "," + b + "\" placeholder=\"Compute " + a + " + " + b + "?\"  id=\"BotBootInput\" > \n\
                        </div>");
                    }

                    DrawBotBoot(a, b);
                </script>
                <!--                <div class="col-md-9">
                                    <div class="input text">
                                        <input type="text" value="" placeholder="PLEASE ENTER SECURTY CODE" />
                                    </div>
                                    <img src="<?php // echo Router::url('/image.jpg')                ?>">
                                </div>-->

            </div>

        </div>

        <div class="submit text-right">
            <button class="btn action-btn btn-md btn-danger" type="submit">SUBMIT</button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
    <div class="clear"></div>
</div>
<?php
echo $this->Html->script(array('remodal', 'jquery.validate', 'jquery.scrollTo'), array('inline' => false));
echo $this->append('script');
?>
<script>
    jQuery.validator.addMethod("math", function (value, element, params) {
        params = params.split(',');
        //            console.log(params);
        return this.optional(element) || value == parseInt(params[0]) + parseInt(params[1]);
    }, jQuery.validator.format = function (source, params) {
        source = source.split(',');
        return "Please enter the correct value for " + source[0] + " + " + source[1];
    });
    $(function () {

        $('#<?php echo $formID ?>').validate({
            rules: {
                'data[Enquiry][boot]': {
                    required: true
                }
            },
            messages: {
                'data[Enquiry][first_name]': {
                    required: "Required"
                },
                'data[Enquiry][last_name]': "Required",
                'data[Enquiry][email]': {
                    required: "Required"
                },
            },
            errorClass: "error-message",
            errorElement: "div",
            errorPlacement: function (error, element) {
//                    console.log(element.parent());
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: $(form).prop('action'),
                    data: $(form).serialize(),
                    dataType: 'json',
                }).done(function (data) {
                    if (data.status) {
                        notification('success', data.message);
                        $('.error-message').remove();
                        $('#<?php echo $formID ?>')[0].reset();
                    } else {
                        notification('error', data.message);
                        $('.error-message').remove();
                        if (data['validationErrors']) {
                            for (i in data['validationErrors']) {
                                $('#<?php echo $formID ?> *[name="data[Enquiry][' + i + ']"]').parent().append('<div class="error-message">' + data['validationErrors'][i] + '</div>');
                            }
                        }
                    }

                    $.scrollTo($('#<?php echo $modal_id ?>'), {duration: 1000});
                });


            }
        });

    });
</script>
<?php echo $this->end(); ?>