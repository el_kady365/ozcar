<?php
$bodyTypes = Car::$BodyTypes;
echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarHomeSearch', 'url' => array('controller' => 'cars', 'action' => 'search')));
echo $this->Form->hidden('type_id');
echo $this->Form->input('body', array('type' => 'select', 'name' => 'body', 'options' => Car::getBodyTypesDropDown(), 'empty' => 'select body', 'style' => 'display:none', 'div' => false, 'label' => false, 'multiple' => true));
?>

<div class="mega-search clearfix">
  <input id="mega_search" name="keywords" type="text" value="" placeholder="The simple way to find your next car starts here" />
  <span id="search_all" class="search-ico"></span> </div>
<div>
    <div class="search-bx">

        <div class="search-area clearfix"> <?php echo $this->Form->input('dealer_id', array('options' => $dealers, 'label' => false, 'empty' => 'Select Dealer')) ?>
            <div class="row">
                <div class="col-md-6"> <?php echo $this->Form->input('make', array('options' => $makes, 'label' => false, 'class' => 'makes', 'empty' => 'Select Make')) ?> </div>
                <div class="col-md-6"> <?php echo $this->Form->input('model', array('empty' => 'Select Model', 'label' => false, 'type' => 'select', 'class' => 'models')) ?> </div>
            </div>
            <div class="row m-b-md rang-row range-bordered">
                <div class="col-md-6">
                    <div id="Price" ></div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-5"> <?php echo $this->Form->input('price_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN')); ?> </div>
                        <div class="col-md-2 text-center">
                            <div class="m-t-md"><strong>TO</strong></div>
                        </div>
                        <div class="col-md-5"> <?php echo $this->Form->input('price_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX')); ?> </div>
                    </div>
                </div>
            </div>
            <!-- / price ranger -->

            <div class="search-submit"> <a class="m-t-xs pull-left" href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'advanced_search')) ?>"><span class="advanced-search-ico ico"></span> <strong>Advanced Search</strong></a> <a href="#" class="bold m-t-sm  pull-left m-l-md">9,854 <span class="text-primary">Cars Listed</span></a>
                <button type="submit" class="btn btn-info btn-md pull-right">SEARCH CARS</button>
            </div>
        </div>
    </div>
    <div class="type-bx">
        <div class="tabs location-search car-type-tab">
            <ul class="tab-buttons">
                <li data-id="#BodyType" class="tab-btn active"><a href="#" class="transition" >Body Type</a></li>

            </ul>
            
                <div id="BodyType" class=" current">
                    <div class="body-types clearfix">
                        <div class="row-sm">
                            <?php foreach ($bodyTypes as $key => $val) {
                                ?>
                                <div class="col-md-6"> <a class="type-ico text-center" rel="<?php echo $key ?>">
                                        <img title="" alt="" src="<?php echo Router::url('/css/img/shape/' . $val['image'] . '.png') ?>"><?php echo $val['title'] ?></a>
                                </div>
                                        <?php
                                    }
                                    ?>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?> <?php echo $this->append('script'); ?> 
<script type="text/javascript">
    var models = <?php echo json_encode($models) ?>;
    var selMake = '<?php echo!empty($params['make']) ? $params['make'] : '' ?>';
    var selModel = '<?php echo!empty($params['model']) ? $params['model'] : '' ?>';
    $(function () {
        $('select[name="make"]').on('change', function () {
            getChildList($(this).closest('form').find('select[name="model"]'), $(this).val(), 'models')
        });
		
        $('#search_all').on('click', function () {
            window.location.href='<? echo Router::url(array('controller' => 'cars', 'action' => 'search')) ?>?search_any='+$('#mega_search').val();
        });
		
    });
    function getChildList(element, data_id, type) {
        if (data_id) {
            var data = window[type][data_id];

            element.html('<option value="">Select Model</option>');
            if (type == 'serieses') {
                element.html('<option value="">Select Series</option>');
            }

            if (data) {
                $.each(data, function (key, value) {
                    selected = '';
                    if (selModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                        selected = 'selected="selected"';
                    }

                    element.append('<option value="' + key + '" ' + selected + '>' + value + '</option>');
                    ;
                });
            }
        }
    }
</script> 
<?php echo $this->end(); ?> 