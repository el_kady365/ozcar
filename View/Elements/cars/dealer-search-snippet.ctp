<?php // debug($dealers);             ?>
<?php if (!empty($dealers)) { ?>
    <div class="dealer-sort-bar">
        <div class="row">
            <div class="col-md-6">
                <h2>Nearby Dealerships</h2>
            </div>
            <?php
            $cars_count = 0;
            foreach ($dealers as $dealer) {
                $cars_count+=$dealer[0]['CarsCount'];
            }
            ?>
            <div class="col-md-6">
                <div class="sort-results pull-right">
                    <span class="pull-left"><?php echo $cars_count; ?> Cars</span>
                    <div class="show-results">
                        <?php
                        $this->Paginator->options = array('url' => array_merge($this->passedArgs, array('?' => $_SERVER['QUERY_STRING'])));
                        if ($this->Paginator->hasPrev()) {
                            echo $this->Paginator->prev(__('<<', true), array(), null, array('class' => 'disabled')) . ' ';
                        }
                        echo $this->Paginator->numbers(array('separator' => ' '));
                        if ($this->Paginator->hasNext()) {
                            echo ' ' . $this->Paginator->next(__('>>', true), array(), null, array('class' => 'disabled'));
                        }
                        ?>
                    </div>
                    <!--                    <div class="input select  pull-left">
                                            <select>
                                                <option>Sort by</option>
                                            </select>
                                        </div>-->
                </div>
            </div>
        </div>
    </div>
    <div class="scroll map-scroll">
        <div class="search-area map-search">
            <div class="collapse-section">
                <div class="collapse-content">

                    <?php foreach ($dealers as $dealer) { ?>
                        <div class="dealer-box">
                            <div class="row">

                                <div class="col-md-5">
                                    <?php if (!empty($dealer['DealerLocation']['logo_full_path'])) { ?>
                                        <div class="car-thumb "> 
                                            <a href="#">
                                                <img src="<?php echo get_resized_image_url($dealer['DealerLocation']['logo'], 285, 245, 1) ?>" alt="" title="">
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-7">
                                    <div class="dealer-details row">
                                        <div class="col-md-8">
                                            <h5 class="bold"><?php echo $dealer['DealerLocation']['name'] ?></h5>
                                            <p class="m-b-md m-t-md"><?php echo $dealer['DealerLocation']['address1'] ?><br>
                                                <?php echo $dealer['DealerLocation']['suburb'] ?> <?php echo $dealer['DealerLocation']['city'] ?> <?php echo $dealer['DealerLocation']['state'] ?></p>
                                            <p><?php echo $dealer['DealerLocation']['phone1'] ?><br>
                                                <?php echo $dealer['DealerLocation']['phone2'] ?><br>

                                                <?php echo $dealer['DealerLocation']['email'] ?></p>
                                        </div>
                                        <div class="col-md-4 text-center"> 
                                            <img title="" alt="" src="<?php echo Car::getDealerLogo($dealer['DealerLocation']['dealer_id']) ?>" />
                                            <div class="row m-t-md">
                                                <div class="col-md-12"><span class="features-ico ico-10"></span> <strong><?php echo $dealer[0]['CarsCount'] ?> Cars</strong></div>
                                                <?php if (isset($dealer[0]['dist']) && !empty($dealer[0]['dist'])) { ?>
                                                    <div class="col-md-12"><span class="features-ico ico-11"></span> <strong><?php echo round($dealer[0]['dist'], 1) ?> kms</strong></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row row-sm m-t-sm">
                                                <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-info" href="#">VIEW</a></div>
                                                <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-info" href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'inbox', '#' => 'newMsg-' . $dealer['DealerLocation']['id'])) ?>">ENQUIRE</a></div>
                                                <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-info get-direction" data-address="<?php echo $dealer['DealerLocation']['id']; ?>" href="#">Directions</a></div>
                                                <div class="col-md-3"><a class="save-dealership" href="<?php echo Router::url(array('controller' => 'user_dealer_locations', 'action' => 'add', $dealer['DealerLocation']['id'])) ?>">Save Dealership</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / dealerbox --> 
                    <?php } ?>


                </div>
            </div>
        </div>
        <!-- / collapse-section --> 
    </div>
<?php } else {
    ?>
    <div class="scroll map-scroll">
        <div class="search-area map-search">
            <div class="flashMessage Notemessage">
                There are not any dealerships that meet your criteria  
            </div>
        </div>
    </div>
<?php }
?>