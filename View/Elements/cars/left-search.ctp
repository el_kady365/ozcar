<div class="side-nav pull-right">

    <div class="filters">
        <div class="tabs search-tabs">
            <ul class="tab-buttons clearfix">
                <li class="tab-btn active" data-id="#Refine"><a href="#">Refine Your Search</a></li>
                <li class="tab-btn" data-id="#Tools"><a href="#">Tools</a></li>
            </ul>
            <div class="tabs-box">
                <div id="Refine" class="tab current">
                    <?php
                    $bodyTypes = Car::$BodyTypes;
                    echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarLeftSearch', 'url' => array('controller' => 'cars', 'action' => 'search')));
                    echo $this->Form->input('body', array('selected' => !empty($params['body']) ? $params['body'] : '', 'type' => 'select', 'name' => 'body', 'options' => Car::getBodyTypesDropDown(), 'empty' => 'select body', 'style' => 'display:none', 'div' => false, 'label' => false, 'multiple' => true));
                    echo $this->Form->hidden('make', array('value' => !empty($params['make']) ? $params['make'] : ''));
					echo $this->Form->hidden('keywords', array('value' => !empty($params['keywords']) ? $params['keywords'] : ''));
                    echo $this->Form->hidden('model', array('value' => !empty($params['model']) ? $params['model'] : ''));
                    echo $this->Form->hidden('series', array('value' => !empty($params['series']) ? $params['series'] : ''));
                    echo $this->Form->hidden('price_from', array('value' => !empty($params['price_from']) ? $params['price_from'] : ''));
                    echo $this->Form->hidden('price_to', array('value' => !empty($params['price_to']) ? $params['price_to'] : ''));
                    echo $this->Form->hidden('kms_from', array('value' => !empty($params['kms_from']) ? $params['kms_from'] : ''));
                    echo $this->Form->hidden('kms_to', array('value' => !empty($params['kms_to']) ? $params['kms_to'] : ''));
                    echo $this->Form->hidden('year_from', array('value' => !empty($params['year_from']) ? $params['year_from'] : ''));
                    echo $this->Form->hidden('year_to', array('value' => !empty($params['year_to']) ? $params['year_to'] : ''));
                    echo $this->Form->hidden('engine_from', array('value' => !empty($params['engine_from']) ? $params['engine_from'] : ''));
                    echo $this->Form->hidden('engine_to', array('value' => !empty($params['engine_to']) ? $params['engine_to'] : ''));
                    echo $this->Form->hidden('transmission', array('value' => !empty($params['transmission']) ? $params['transmission'] : ''));
                    echo $this->Form->hidden('fuel_type', array('value' => !empty($params['fuel_type']) ? $params['fuel_type'] : ''));
                    echo $this->Form->hidden('cylinders', array('value' => !empty($params['cylinders']) ? $params['cylinders'] : ''));
                    echo $this->Form->hidden('seats', array('value' => !empty($params['seats']) ? $params['seats'] : ''));
                    echo $this->Form->hidden('doors', array('value' => !empty($params['doors']) ? $params['doors'] : ''));
                    echo $this->Form->hidden('colour', array('value' => !empty($params['colour']) ? $params['colour'] : ''));
                    ?>
                    <div class="row row-sm">
                        <div class="col-md-3">
                            <div class="input radio">
                                <label>All</label>
                                <input type="radio" name="type_id" value="all" <?php echo (!empty($params['type_id']) && $params['type_id'] == 'all') || empty($params['type_id']) ? 'checked="checked"' : '' ?>>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input radio">
                                <label>New</label>
                                <input type="radio" name="type_id" value="1" <?php echo!empty($params['type_id']) && $params['type_id'] == '1' ? 'checked="checked"' : '' ?> />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input radio">
                                <label>Used</label>
                                <input type="radio" name="type_id" value="2" <?php echo!empty($params['type_id']) && $params['type_id'] == '2' ? 'checked="checked"' : '' ?> />

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input radio">
                                <label>Demo</label>
                                <input type="radio" name="type_id" value="3" <?php echo!empty($params['type_id']) && $params['type_id'] == '3' ? 'checked="checked"' : '' ?> />
                            </div>
                        </div>
                    </div>
                    <div class="accordion search-accordion">
                        <h5>General</h5>
                        <ul class="m-b-md">
                            <li class="accordion-panel <? echo @$params['make'] == "" ? '' : 'active-box'; ?>">
                                <div  class="toggle-btn"><a href="#">Make</a></div>
                                <div class="content-box">
                                    <ul class="side-list makes-list">

                                        <?php
                                        foreach ($makes as $make_id => $make) {
                                            if (empty($params['make'])) {
                                                ?>
                                                <li><a href="#" data-id="<?php echo $make_id ?>"><?php echo $make ?></a></li>
                                            <?php } elseif (strtolower($params['make']) == strtolower($make)) {
                                                ?>
                                                <li><a href="#" data-id="<?php echo $make_id ?>"><?php echo $make ?>  <span style="float: right">X</span></a></li>
                                                <?
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                            <li class="accordion-panel <? echo @$params['model'] == "" ? '' : 'active-box'; ?>">
                                <div  class="toggle-btn"><a href="#">Model</a></div>
                                <div class="content-box">
                                    <ul class="side-list models-list">
                                    </ul>
                                </div>
                            </li>
                            <li class="accordion-panel" <? echo @$params['series'] == "" ? '' : 'active-box'; ?>>
                                <div  class="toggle-btn"><a href="#">Series</a></div>
                                <div class="content-box">
                                    <ul class="side-list series-list">

                                    </ul>
                                </div>
                            </li>
                            <li  class="accordion-panel <?php echo!empty($params['price_from']) || !empty($params['price_to']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Price</a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">
                                            <div id="Price-left" data-from="<?php echo!empty($params['price_from']) ? $params['price_from'] : '' ?>" data-to="<?php echo!empty($params['price_to']) ? $params['price_to'] : '' ?>" ></div> 
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li  class="accordion-panel <?php echo!empty($params['year_from']) || !empty($params['year_to']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Year</a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">

                                            <div id="Years-left" data-from="<?php echo!empty($params['year_from']) ? $params['year_from'] : '' ?>" data-to="<?php echo!empty($params['year_to']) ? $params['year_to'] : '' ?>" ></div> 
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-panel <?php echo!empty($params['fuel_type']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Fuel Type </a></div>
                                <div class="content-box">
                                    <ul class="side-list fuel_type-list">
                                        <?php foreach (Car::$FuelTypes as $key => $value) {
                                              if (empty($params['fuel_type'])) {
                                            ?>
                                            <li><a href="#" data-id="<?php echo $key ?>"><?php echo $value ?></a></li>
                                       <?php } elseif (strtolower($params['fuel_type']) == strtolower($value)) {
                                                ?>
                                            <li><a class="active" href="#" data-id="<?php echo $key ?>"><?php echo $value ?> <span style="float: right">X</span></a></li>
                                                <?
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>

                            <li class="accordion-panel <?php echo!empty($params['transmission']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Transmission </a></div>
                                <div class="content-box">
                                    <ul class="side-list transmission-list">
                                        <?php foreach (Car::$Transmissions as $key => $value) {
                                             if (empty($params['transmission'])) {
                                            ?>
                                            <li><a href="#" data-id="<?php echo $key ?>"><?php echo $value ?></a></li>
                                         <?php } elseif (strtolower($params['transmission']) == strtolower($key)) {
                                             ?>
                                         <li><a class="active" href="#" data-id="<?php echo $key ?>"><?php echo $value ?> <span style="float: right">X</span></a></li>   
                                            <?
                                         }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                            <li  class="accordion-panel <?php echo!empty($params['engine_from']) || !empty($params['engine_to']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Engine Size (Ltr)</a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">
                                            <div id="EngineSize-left" data-from="<?php echo!empty($params['engine_from']) ? $params['engine_from'] : '' ?>" data-to="<?php echo!empty($params['engine_to']) ? $params['engine_to'] : '' ?>" ></div> 

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-panel <?php echo!empty($params['cylinders']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Cylinders </a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">
                                            <div id="Cylinders-left" data-from="<?php echo!empty($params['cylinders']) ? $params['cylinders'] : '' ?>"  ></div> 

                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li  class="accordion-panel <?php echo!empty($params['kms_from']) || !empty($params['kms_to']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Kilometers</a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">
                                            <div id="Kilometers-left" data-from="<?php echo!empty($params['kms_from']) ? $params['kms_from'] : '' ?>" data-to="<?php echo!empty($params['kms_to']) ? $params['kms_to'] : '' ?>" ></div> 
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-panel <?php echo!empty($params['body']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Body Type</a></div>
                                <div class="content-box">
                                    <div class="body-types clearfix">
                                        <div class="row-sm">
                                            <?php foreach ($bodyTypes as $key => $val) {
                                                ?>
                                                <div class="col-md-4">
                                                    <a class="type-ico text-center" rel="<?php echo $key ?>">
                                                        <img title="" alt="" src="<?php echo Router::url('/css/img/shape/' . $val['image'] . '.png') ?>"><?php echo $val['title'] ?></a>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-panel <?php echo!empty($params['seats']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Seats </a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">
                                            <div id="Seats-left" data-from="<?php echo!empty($params['seats']) ? $params['seats'] : '' ?>"  ></div> 

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-panel <?php echo!empty($params['doors']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Doors </a></div>
                                <div class="content-box">
                                    <div class="row m-b-md rang-row km-range">
                                        <div class="col-md-12">
                                            <div id="Doors-left" data-from="<?php echo!empty($params['doors']) ? $params['doors'] : '' ?>"  ></div> 

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-panel <?php echo!empty($params['colour']) ? 'active-box' : '' ?>">
                                <div  class="toggle-btn"><a href="#">Colours </a></div>
                                <div class="content-box">
                                    <ul class="side-list colours-list">
                                        <?php foreach ($colours as $key => $value) {
                                            if(empty($params['colour'])){
                                            ?>
                                            <li><a href="#" data-id="<?php echo $key ?>"><?php echo $value ?></a></li>
                                          <?php } elseif (strtolower($params['colour']) == strtolower($key)) {
                                          ?>
                                            <li><a class="active" href="#" data-id="<?php echo $key ?>"><?php echo $value ?> <span style="float: right">X</span></a> </li>       
                                          <?php
                                          }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>


                        </ul>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-6"><a class="m-t-sm btn-block" href="#" id="reset_link">Reset Search</a></div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-info btn-md btn-block pull-right">SEARCH</button>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div id="Tools" class="tab">
                    <?php // if (!empty($loggedUser)) { ?>
                        <div class="customer-panel m-b-lg">
                            <?php echo $this->element('Users.user_links') ?>
                        </div>
                    <?php // } ?>
                    <!-- /customer-panel  --> 

                </div>
            </div>
            <!-- / tabs-box --> 
        </div>
        <!-- / filters --> 

    </div>
    <!-- / tabs-box --> 

</div>
<?php
echo $this->Html->script(array('jquery.nimble.loader'), array('inline' => false));
echo $this->append('script');
?>
<script type="text/javascript">
    var models = <?php echo json_encode($models) ?>;
    var serieses = <?php echo json_encode($serieses); ?>;
    var bodies = <?php echo json_encode($jsBodies); ?>;
    var selMake = '<?php echo!empty($params['make']) ? $params['make'] : '' ?>';
    var selModel = '<?php echo!empty($params['model']) ? $params['model'] : '' ?>';
    var selseries = '<?php echo!empty($params['series']) ? $params['series'] : '' ?>';


    $(function() {
        $('#CarLeftSearch input').on("change", function() {
            $('#CarLeftSearch').submit();
        });
        //        $(".radio-btn").on('click', function () {
        //            var val = $(this).find('input:radio').val();
        //            $('#CarLeftSearch input[name="type_id"]').val(val);
        //            $('#CarLeftSearch').submit();
        //        });
        $("#CarLeftSearch select").on('change', function() {
            $('#CarLeftSearch').submit();
        });


        ///Makes Click
        $('.makes-list li a').on("click", function() {
            var $make_id = $(this).data('id');
            console.log($make_id);
            getChildList($('.models-list'), $make_id, 'models');
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="model"]').val('');
            $(this).closest('form').find('input[name="series"]').val('');
            $(this).closest('form').find('input[name="make"]').val($make_id).change();

            return false;
        });
        $('.makes-list li span').on("click", function() {
            var $make_id = $(this).data('id');
            console.log($make_id);
            getChildList($('.models-list'), $make_id, 'models');
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="model"]').val('');
            $(this).closest('form').find('input[name="series"]').val('');
            $(this).closest('form').find('input[name="make"]').val('').change();

            return false;
        });        
        ///Transmission Click
        $('.transmission-list li a').on("click", function() {
            var $transmission_id = $(this).data('id');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="transmission"]').val($transmission_id).change();
            return false;
        });
        ///Transmission Click
        $('.transmission-list li span').on("click", function() {
            var $transmission_id = $(this).data('id');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="transmission"]').val('').change();
            return false;
        });        
        ///Fuel Type Click
        $('.fuel_type-list li a').on("click", function() {
            var $fuel_type = $(this).data('id');
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="fuel_type"]').val($fuel_type).change();
            return false;
        });
        
        $('.fuel_type-list li span').on("click", function() {
            var $fuel_type = $(this).data('id');
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="fuel_type"]').val('').change();
            return false;
        });
        
        $('.colours-list li a').on("click", function() {
            var $colours_list = $(this).data('id');
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="colour"]').val($colours_list).change();
            return false;
        });
		
	$('.colours-list li span').on("click", function() {
            var $colours_list = $(this).data('id');
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $(this).closest('form').find('input[name="colour"]').val($colours_list).change();
            return false;
        });		
    });

    ///Models Click
    $(document).on('click', '.models-list li a', function() {


        var $model_id = $(this).data('id');
        getChildList($('.series-list'), $model_id, 'serieses');
        $(this).closest('form').find('input[name="series"]').val('');
        $(this).closest('form').find('input[name="model"]').val($model_id).change();

        return false;
    });
    $(document).on('click', '.models-list li span', function() {


        var $model_id = $(this).data('id');
        getChildList($('.series-list'), $model_id, 'serieses');
        $(this).closest('form').find('input[name="series"]').val('');
        $(this).closest('form').find('input[name="model"]').val('').change();

        return false;
    });    
    ///Series Click
    $(document).on('click', '.series-list li a', function() {
  //  console.log($(this).html());    
    
        var $series_id = $(this).data('id');
        $(this).closest('form').find('input[name="series"]').val($series_id).change();
        return false;
    });
    $(document).on('click', '.series-list li span', function() {
  //  console.log($(this).html());    
    
        var $series_id = $(this).data('id');
        $(this).closest('form').find('input[name="series"]').val('').change();
        return false;
    });    
    //Get the Models and Serieses List
    function getChildList(element, data_id, type) {
        
   console.log('getChildList');
        console.log(data_id);
        console.log(type);
        console.log(element);
        if (data_id) {
            var data = window[type][data_id];
            element.html('');
            if (data) {
                $.each(data, function(key, value) {
                    selected = '';
                    if (selseries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                        selected = 'class="active"';
                    }
                    
                    if (selModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                        selected = 'class="active"';
                    }
                    //console.log(value);
                    element.append('<li><a data-id="' + key + '" href="#" ' + selected + '>' + value + '</a> </li>');
                });
              //  element.find('a.active').parents('.accordion-panel').find('.toggle-btn').trigger('click');
            }
        }
    }



    $(function() {
      
        ///Years slider
        if ($('#Years-left').length) {
            $("#Years-left").ionRangeSlider({
                type: "double",
                prettify_enabled: false,
                min: '<?php echo date('Y') - 30 ?>',
                max: '<?php echo date('Y') ?>',
                onChange: function(data) {
                    $('input[name="year_from"]').val(data.from);
                    $('input[name="year_to"]').val(data.to);
                    //                    console.log(data);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="year_from"]').val('');
                        $('input[name="year_to"]').val('');
                    }


                }
                , onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }
        if ($('#Price-left').length) {
            $("#Price-left").ionRangeSlider({
                type: "double",
                min: 1000,
                max: 100000,
                step: 500,
                prefix: '$ ',
                onChange: function(data) {
                    $('input[name="price_from"]').val(data.from);
                    $('input[name="price_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="price_from"]').val('');
                        $('input[name="price_to"]').val('');
                    }
                }, onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }
        if ($('#EngineSize-left').length) {
            $("#EngineSize-left").ionRangeSlider({
                type: "double",
                min: 1,
                max: 7,
                step: 0.2,
                postfix: ' L',
                onChange: function(data) {
                    $('input[name="engine_from"]').val(data.from);
                    $('input[name="engine_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="engine_from"]').val('');
                        $('input[name="engine_to"]').val('');
                    }
                }, onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }
        if ($('#Cylinders-left').length) {
            $("#Cylinders-left").ionRangeSlider({
                min: 1,
                max: 12,
                step: 1,
                //                postfix: ' L',
                onChange: function(data) {
                    $('input[name="cylinders"]').val(data.from);

                    if (data.from == data.min) {
                        $('input[name="cylinders"]').val('');

                    }
                }, onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }
        if ($('#Seats-left').length) {
            $("#Seats-left").ionRangeSlider({
                min: 2,
                max: 15,
                step: 1,
                //                postfix: ' L',
                onChange: function(data) {
                    $('input[name="seats"]').val(data.from);

                    if (data.from == data.min) {
                        $('input[name="seats"]').val('');

                    }
                }, onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }
        if ($('#Doors-left').length) {
            $("#Doors-left").ionRangeSlider({
                min: 1,
                max: 6,
                step: 1,
                //                postfix: ' L',
                onChange: function(data) {
                    $('input[name="doors"]').val(data.from);

                    if (data.from == data.min) {
                        $('input[name="doors"]').val('');

                    }
                }, onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }


        if ($('#Kilometers-left').length) {
            $("#Kilometers-left").ionRangeSlider({
                type: "double",
                min: 10000,
                max: 100000,
                step: 1000,
                postfix: 'Km ',
                onChange: function(data) {
                    $('input[name="kms_from"]').val(data.from);
                    $('input[name="kms_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="kms_from"]').val('');
                        $('input[name="kms_to"]').val('');
                    }
                }, onFinish: function(data) {
                    $('#CarLeftSearch').submit();
                }
            });
        }

        //Add the 
        $('.side-list li a').each(function() {
            $hasMake = $(this).closest('ul').hasClass('makes-list');
            $hasModel = $(this).closest('ul').hasClass('model-list');

            if ($hasMake && $(this).data('id') == selMake) {
                //console.log(selMake);
                $(this).addClass('active');
                getChildList($('.models-list'), selMake, 'models');
                if (selModel) {
                    getChildList($('.series-list'), selModel, 'serieses');
                }
            //    $(this).parents('.accordion-panel').find('.toggle-btn').trigger('click');
            }

        });

//        $('#CarLeftSearch').on('submit', function() {
//            var action = $(this).attr('action');
//            var datas = $(this).serialize();
//            $(this).find('input');
//            //            console.log(datas);
//            //            return false;
//            //            return false;
//            $('body').nimbleLoader("show", {
//                position: "fixed",
//                loaderClass: "loading_bar_1",
//                debug: true,
//                speed: 'fast',
//                hasBackground: true,
//                zIndex: 999,
//                backgroundColor: "#fff",
//                backgroundOpacity: 0.9
//            });
//            $.ajax({
//                url: action,
//                data: datas,
//                method: 'GET'
//            }).done(function(data) {
//                $('.listing-section').html(data);
//                $('body').nimbleLoader("hide");
//                $(".car-preview-thumb").owlCarousel({
//                    slideSpeed: 300,
//                    paginationSpeed: 400,
//                    singleItem: true,
//                    transitionStyle: "backSlide"
//                });
//                //                alert();
//                history.replaceState({}, '', action + '?' + datas);
//
//            });
//            return false;
//        });

        $('#reset_link').on('click', function() {
            $('#CarLeftSearch input[type!="radio"]').val('');
            $('#CarLeftSearch select').val();
            $('#CarLeftSearch select option').attr('selected', false);
            $('#CarLeftSearch .type-ico').removeClass('active');
            $('#CarLeftSearch .accordion-panel').removeClass('active-box');
            $('#CarLeftSearch .accordion-panel content-box').hide();
            $('#CarLeftSearch .radio-btn:first').click();
            return false;

        })
  $('.models-list li a').each(function() {
      if($(this).html()!=selModel && selModel!=''){
      $(this).remove();
      }else if($(this).html()==selModel && selModel!=''){
      $(this).append('<span style="float: right">X</span>');    
      }
  });
  
  
  if(selModel!=''){
  $( ".models-list" ).closest('li').addClass('active-box').t;    
  }
  
  $('.series-list li a').each(function() {
      if($(this).html()!=selseries && selseries!=''){
            $(this).remove();
      }else if($(this).html()==selseries && selseries!=''){
      $(this).append('<span style="float: right">X</span>');      
      }
        });        
        
    });
 if(selseries!=''){
  $( ".series-list" ).closest('li').addClass('active-box');   
  $(".series-list").parents('.accordion-panel').find('.toggle-btn').trigger('click');
  }
    $(document).on('click', '.side-list li a', function() {
        $(this).closest('ul').find('a').removeClass('active');
        $(this).addClass('active');
    });



</script>
<?php echo $this->end(); ?>
