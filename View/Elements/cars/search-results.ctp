<div class="listing-bar">
    <div class="row">
        <div class="col-md-8">
            <ul class="results-statistics">
                <li><span><?php echo intval($total_cars) ?></span>Total Cars</li>
                <li><span><?php echo intval($new_cars) ?></span> New Cars
                <li><span><?php echo intval($used_cars) ?></span> Used Cars</li>
                <li><span><?php echo intval($demo_cars) ?></span> Demo Cars</li>
            </ul>
        </div>
        <div class="col-md-4">
            <div class="list-options pull-right"> 
                <a href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'map_search', '?' => $_SERVER['QUERY_STRING'])) ?>" class="display-map">Map</a>
                <!--<a href="#list-view" class="display-list tab-btn">List</a>-->
                <!--<a href="#grid-view" class="display-grid tab-btn">Grid</a>-->
            </div>
        </div>
    </div>
</div>
<!-- /listing-bar  -->

<div class="listing-wrap">
    <div class="listing-meta">
        <div class="row">
            <div class="col-md-3"> 
                <a class="m-t-sm btn" href="<?php echo $this->Html->url(array_merge(array('save' => 1), array('?' => $_SERVER['QUERY_STRING']))); ?>">
                    <i class="fa fa-folder-open"></i>
                    Save Search
                </a>
            </div>
            <div class="col-md-4">
                <!--                <div class="select">
                                    <select>
                                        <option>Filter By</option>
                                    </select>
                                </div>-->
            </div>
            <div class="col-md-5 text-right">
                <div class="show-results">
                    <p class="m-t-sm">Show Results:

                        <?php echo $this->Html->link('20', array_merge(array('limit' => 20), array('?' => $_SERVER['QUERY_STRING']))); ?>  
                        <?php echo $this->Html->link('50', array_merge(array('limit' => 50), array('?' => $_SERVER['QUERY_STRING']))); ?>  
                        <?php echo $this->Html->link('100', array_merge(array('limit' => 100), array('?' => $_SERVER['QUERY_STRING']))); ?>
                        <?php echo $this->Html->link('View All', array_merge(array('limit' => 'all'), array('?' => $_SERVER['QUERY_STRING']))); ?>

                    </p>
                </div>
                <div class="show-results">
                    <?php
                    $this->Paginator->options = array('url' => array_merge($this->passedArgs, array('?' => $_SERVER['QUERY_STRING'])));
                    if ($this->Paginator->hasPrev()) {
                        echo $this->Paginator->prev(__('<<', true), array(), null, array('class' => 'disabled')) . ' ';
                    }
                    echo $this->Paginator->numbers(array('separator' => ' '));
                    if ($this->Paginator->hasNext()) {
                        echo ' ' . $this->Paginator->next(__('>>', true), array(), null, array('class' => 'disabled'));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($cars)) { ?>
        <div class="list-view" id="list-view">
            <?php
            foreach ($cars as $car) {
                $title = $car['Car']['year'] . ' ' . $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series'];
                $view_url = Router::url(array('controller' => 'cars', 'action' => 'view', $car['Car']['id'], Inflector::slug($title, '-'), '?' => $_SERVER['QUERY_STRING']));
                ?>
                <div class="listing-box-primary">

                    <div class="listing-box-head">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="meta">
                                    <h3 class="inline-block"> <a href="<?php echo $view_url ?>"><?php echo $title ?></a></h3>
                                    <span class="badge"><?php echo $car[0]['CarsCount']; ?></span>
                                    <a class="inline-block" href="#">Available at 
                                        <strong><?php echo $car['DealerLocation']['name']; ?></strong></a>
                                    <i class="fa fa-map-marker"></i> </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <a href="#">
                                    <img src="<?php echo Car::getDealerLogo($car['Car']['dealer_id'], 120) ?>" alt="" title=""/></a>
                            </div>
                        </div>
                    </div>
                    <!-- /listing-box-head  -->
                    <div class="listing-box-content">
                        <div class="row row-sm">
                            <div class="col-md-5">
                                <?php ?>
                                <div class="car-preview-thumb owl-carousel owl-theme">
                                    <?php
                                    if (!empty($car['CarImage'])) {
                                        foreach ($car['CarImage'] as $car_image) {
                                            if (!empty($car_image['image_full_path'])) {
                                                ?>
                                                <a href="<?php echo $view_url ?>"><img src="<?php echo get_resized_image_url($car_image['image'], 290) ?>" alt="" title=""/></a>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                        <a href="#"><img src="<?php echo get_resized_image_url('default-car.png', 290) ?>" alt="" title=""/></a>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <p><?php echo $this->Text->truncate($car['Car']['comments']); ?></p>
                                <div class="feature-content">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td><span class="features-ico ico-01"></span><?php echo $car['Car']['body'] ?></td>
                                            <td><span class="features-ico ico-02"></span><?php echo $car['Car']['engine_capacity'] ?></td>
                                        </tr>
                                        <tr>
                                            <?php if (!empty($car['Car']['odometer'])) { ?>
                                                <td><span class="features-ico ico-05"></span><?php echo $this->Number->format($car['Car']['odometer']) ?> kms</td>
                                            <?php } ?>
                                            <td><span class="features-ico ico-06"></span><?php echo $car['Car']['transmission'] ?></td>
                                        </tr>
                                        <tr>
                                            <td><span class="features-ico ico-08"></span><a class="car-compare" href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'add_to_compare', $car['Car']['id'])) ?>">Compare</a></td>
                                            <td><span class="features-ico ico-09"></span><a href="<?php echo $view_url . "#carEnquiry" ?>">Enquire</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="listing-map">
                                    <?php
//                                    debug($car['DealerLocation']);
                                    echo $this->element('dealer_map', array('size' => array('width' => 169, 'height' => 125),
                                        'latitude' => $car['DealerLocation']['latitude'], 'longitude' => $car['DealerLocation']['longitude'], 'zoom' => $car['DealerLocation']['zoom'], 'label' => $car['DealerLocation']['name']))
                                    ?>
                                </div>
                                <?php if ($car['Car']['selling_price']) { ?>
                                    <div class="map-caption">
                                        <div class="row">
                                            <div class="col-md-8 text-center">

                                                <span class="item-price">$<?php echo $this->Number->format((float) $car['Car']['selling_price']) ?></span>

                                            </div>
                                            <div class="col-md-4"> <span class="item-meta bold">Drive Away</span> </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <a href="<?php echo $view_url . "#offerEnquiry" ?>" class="btn btn-dark btn-md btn-block bold">MAKE OFFER</a> </div>
                        </div>
                    </div>
                </div>
                <!-- /listing-box-primary  -->
            <?php } ?>
        </div>
        <!-- /Row's  --> 
    <?php } else {
        ?>
        <div class="scroll map-scroll">
            <div class="search-area map-search">
                <div class="flashMessage Notemessage">
                    Sorry - no cars found
                </div>
            </div>
        </div>
    <?php }
    ?>
</div>
