<div class="locations-box">
    <div class="location-box-head">
        <h3>LOCATION SEARCH</h3>
        <a href="#" class="dealers-marke">2350 DEALERS</a>
    </div>
    <div class="location-box-content">

        <div class="tabs location-search">
            <ul class="tab-buttons">
                <li class="tab-btn active"  data-id="#CarSearch"><a href="#CarSearch">Car search</a></li>
                <li class="tab-btn"  data-id="#DealerSearch"><a href="#DealerSearch">Dealer search</a></li>
            </ul>
            <div class="tabs-box">
                <div class="tab current" id="CarSearch">

                    <?php
                    $types = array();
                    $types['all'] = 'Select New, Used or Demo'; 
                    $types = $types+ Car::$types;
                    echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarHomeSearch', 'url' => array('controller' => 'cars', 'action' => 'map_search')));
                    echo $this->Form->input('type_id', array('options' => $types, 'label' => false));
                    echo $this->Form->input('make', array('options' => $makes, 'empty' => 'Select Car Make', 'label' => false));
                    echo $this->Form->input('model', array('options' => array(), 'empty' => 'Select a Car Model', 'label' => false));
                    ?>
                    <div class="input submit text-center m-t-sm">
                        <button type="submit" class="btn btn-danger btn-md">SEARCH</button>
                    </div>
                    <?php echo $this->Form->end(); ?>


                </div>
                <div class="tab" id="DealerSearch">

                    <?php echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarHomeSearch', 'url' => array('controller' => 'cars', 'action' => 'dealer_search'))); ?>
                    <?php echo $this->Form->input('location', array('placeholder' => 'Location/Postcode', 'required' => 'required', 'class' => 'autocompleteAddress', 'label' => false)) ?>
                    <?php echo $this->Form->input('distance', array('options' => Car::$Distances, 'label' => false, 'empty' => 'Kilometres radius')) ?>
                    <div class="input submit text-center m-t-sm">
                        <button type="submit" class="btn btn-danger btn-md">SEARCH</button>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>


    </div>
</div>
<?php echo $this->Html->script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places', array('inline' => false)) ?>
<?php $this->append('script') ?>
<script>
    $(function () {
        $(".autocompleteAddress").each(function (index) {
            var txt_Obj = $(this)[0];
            autocomplete = new google.maps.places.Autocomplete((txt_Obj), {types: ['geocode'], componentRestrictions: {country: "Aus"}});
        });
    });
</script>
<?php echo $this->end(); ?>
<!-- / locations-box -->