<div class="options-bar">
    <a href="#" class="left move">Move</a>
    <?php
    $this->request->data = $site_data;
    echo $this->Form->create('SiteConfig', array('url' => ('/admin/site_configs/edit'), 'class' => 'colors-form'));
    ?>
    <div class="left color-option">
        <label>Background</label>
        <div id="body_bg_color" class="color-selector" rel="body{ background-color: ${color}; color: ${invcolor};  } ">
            <div></div>
        </div>
        <?php $j = max($style_config_map); ?>
        <?php $i = !empty($style_config_map['style.background']) ? $style_config_map['style.background'] : ++$j; ?>
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'background')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'hidden', 'label' => false, 'class' => 'color', 'autocomplete' => 'off')); ?>
        <?php $i = !empty($style_config_map['style.text_color']) ? $style_config_map['style.text_color'] : ++$j; ?>
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'text_color')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'hidden', 'label' => false, 'class' => 'invcolor', 'autocomplete' => 'off')); ?>
    </div>
    <div class="left color-option">
        <label>Colour A</label>
        <div id="color_a_color" class="color-selector" rel=".bg-a { background-color: ${color}; color: ${invcolor}; } ">
            <div></div>
        </div>
        <?php $i = !empty($style_config_map['style.color_a']) ? $style_config_map['style.color_a'] : ++$j; ?>
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'color_a')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'hidden', 'label' => false, 'class' => 'color', 'autocomplete' => 'off')); ?>
        <?php $i = !empty($style_config_map['style.color_a_fore']) ? $style_config_map['style.color_a_fore'] : ++$j; ?>
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'color_a_fore')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'hidden', 'label' => false, 'class' => 'invcolor', 'autocomplete' => 'off')); ?>

    </div>    
    <div class="left color-option">
        <label>Colour B</label>
        <div id="color_b_color" class="color-selector" rel=" .bg-b, .nav a:hover, .nav li.active > a, .nav li > a.active { background-color: ${color}; } .color-a, h1, h2, h3, h4, h5, h6, a, .pages ul li  { color: ${color}} .bg-b, .nav a:hover, .nav li.active > a, .nav li > a.active   { color: ${invcolor}; } ">
            <div></div>
        </div>
        <?php $i = !empty($style_config_map['style.color_b']) ? $style_config_map['style.color_b'] : ++$j; ?>                
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'color_b')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'hidden', 'label' => false, 'class' => 'color', 'autocomplete' => 'off')); ?>
        <?php $i = !empty($style_config_map['style.color_b_fore']) ? $style_config_map['style.color_b_fore'] : ++$j; ?>                
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'color_b_fore')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'hidden', 'label' => false, 'class' => 'invcolor', 'autocomplete' => 'off')); ?>

    </div>
    <div class="left color-option">
        <label>Slider Effect</label>
        <?php $i = !empty($style_config_map['style.slider_effect']) ? $style_config_map['style.slider_effect'] : ++$j; ?>                
        <?php echo $this->Form->input("SiteConfig.$i.id", array('div' => false, 'type' => 'hidden', 'label' => false)); ?>
        <?php echo $this->Form->input("SiteConfig.$i.group", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'style')); ?>
        <?php echo $this->Form->input("SiteConfig.$i.setting", array('div' => false, 'type' => 'hidden', 'label' => false, 'autocomplete' => 'off', 'value' => 'slider_effect')); ?>                
        <?php echo $this->Form->input("SiteConfig.$i.value", array('div' => false, 'type' => 'select', 'options' => array('slide' => 'Slide Effect', 'fade' => 'Fade Effect'), 'label' => false, 'class' => 'color', 'autocomplete' => 'off')); ?>

    </div>

    <div class="submit right">
        <a href="#" class="right close">Hide</a>
        <input type="submit" class="right" value="Save Changes" />
    </div>
    <?php echo $this->Form->end(); ?>
    <div class="clear"></div>
</div>
<!-- color-bar -->

<script type="text/javascript">
    $('.colors-form').submit(function(evt){
        $.ajax({    
            url: $(this).attr('action'),
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'JSON',
            async:  false,
            cache: false,
            success: function(data){
                if(data.status == 'success'){
                    alert(data.message);
                } else{
                    alert(data.message); // too :D
                }
            }
        });
              
              
        evt.preventDefault();
              
    })
    $('.options-bar a.close').click(function(evt){
        evt.preventDefault();
        document.cookie = document.cookie.replace(/color_bar=1/, 'color_bar=0');  
        $(this).parents('.options-bar').animate({marginTop: - $(this).parents('.options-bar').height() }, 1500, 'swing', function(){$(this).remove();  $('body').removeClass('option-bar-active'); });
        $('body').animate({paddingTop: 0 }, 1500, 'swing');
    });
    
    
    $(function(){
        $('#uploadLogo').fineUploader({
            request: {
                endpoint: BASE_URL + 'admin/site_configs/upload/style.logo' ,
                inputName: 'data[SiteConfig][value]'
            }, 
            multiple: false,
            failedUploadTextDisplay:{
                mode: 'custom',
                responseProperty: 'error'
            },
            text: { uploadButton: 'Change Logo'
            }
                
        }).on('complete', function(e, id, filename, response){
            if(response.success){
                $('img', $(e.target).parent()).attr('src', response.url);
                $('ul.qq-upload-list', e.target).empty();                
            }
        });  
    
    });
  
</script>

