<?php if (!empty($add_site_select))  : ?>

    <script type="text/javascript">
            var select_site = '<?php echo $this->JqueryEngine->escape( $this->Form->input("{$modelName}.site_id", array( 'label' => 'Target Site',  'default' => SITE_ID)) ) ; ?>';            
            var inserted = $(select_site).insertBefore('.ExtendedForm form :first input[type=submit]:last');
            if(inserted.parents('.ExtendedForm').hasClass('change-site-disable')){
                inserted.find('select').attr('disabled', 'disabled');
            }
    </script>


<?php endif; ?>
