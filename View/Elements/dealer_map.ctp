<?php 
$url = "http://maps.googleapis.com/maps/api/staticmap?center="
        . "{$latitude},{$longitude}&zoom={$zoom}&size={$size['width']}x{$size['height']}" .
        "&maptype=roadmap&markers=color:red|label:{$label}|{$latitude},{$longitude}";
?>
<img src="<?php echo $url ?>" alt="<?php echo $label?>" title="<?php echo $label?>"/>