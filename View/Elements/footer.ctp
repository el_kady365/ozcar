<div class="dealer-footer">
    <div class="container">
        <div class="col-md-12">
            <hr>
        </div> 
        <div class="col-md-8 col-sm-8">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <ul class="footer-links">
                        <li><a href="#"><strong>Car Sales</strong></a></li>
                        <li><a href="#">voluptatum </a></li>
                        <li><a href="#">deleniti quas</a></li>
                        <li><a href="#">atque corrupti </a></li>
                        <li><a href="#">quos dolores et </a></li>
                        <li><a href="#">molestias</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3">
                    <ul class="footer-links">
                        <li><a href="#"><strong>Car Sales</strong></a></li>
                        <li><a href="#">voluptatum </a></li>
                        <li><a href="#">deleniti quas</a></li>
                        <li><a href="#">atque corrupti </a></li>
                        <li><a href="#">quos dolores et </a></li>
                        <li><a href="#">molestias</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3">
                    <ul class="footer-links">
                        <li><a href="#"><strong>Car Sales</strong></a></li>
                        <li><a href="#">voluptatum </a></li>
                        <li><a href="#">deleniti quas</a></li>
                        <li><a href="#">atque corrupti </a></li>
                        <li><a href="#">quos dolores et </a></li>
                        <li><a href="#">molestias</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3">
                    <ul class="footer-links">
                        <li><a href="#"><strong>Car Sales</strong></a></li>
                        <li><a href="#">voluptatum </a></li>
                        <li><a href="#">deleniti quas</a></li>
                        <li><a href="#">atque corrupti </a></li>
                        <li><a href="#">quos dolores et </a></li>
                        <li><a href="#">molestias</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-md-push-1  col-sm-push-1">
            <div class="dealer-center">
                <div class="m-b-md"> <a href="#" class="btn-block m-b-xs">Dealer Centre Login</a> <a href="#" class="btn-block m-b-xs ">Sign in to manage your account.</a> </div>
                <a href="#" class="btn btn-block text-center btn-md btn-danger m-b-sm bold">DEALER CENTRE</a> <a href="#" class="btn btn-block text-center btn-md btn-border m-b-sm bold">New to OzCar? Join</a> </div>
        </div>
    </div>
</div>