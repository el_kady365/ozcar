<?php

/* @var $javascript JavascriptHelper */
$javascript->link(array('jquery.validate'), false);
if (!isset($submit)) {
    $code = <<<SCRIPT
	jQuery(document).ready(function(){
		jQuery('#$FormId').validate(
                    {errorClass:"error-message",errorElement:"div",errorPlacement: function(error, element) {
     error.insertAfter( element.parent());
   }});
	});
SCRIPT;
} else {
    $code = <<<SCRIPT
	jQuery(document).ready(function(){
            jQuery('#$FormId').submit(function(){
                return $(this).validate({errorClass:"error-message",errorElement:"div",errorPlacement: function(error, element) {
     error.insertAfter( element.parent());
   }});
            });
	});
SCRIPT;
}
echo $javascript->codeBlock($code);
?>