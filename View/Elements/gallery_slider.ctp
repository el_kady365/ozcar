<?php
$gallery = ClassRegistry::init("Slider");
$slider = $gallery->find("first", array("conditions" => array("Slider.id" => $id, 'Slider.active' => 1)));

if (!empty($slider)):
    ?>
    <div class="page-banners slider">

        <div class="pattern"></div>
        <?php if (!empty($slider)) { ?>
            <ul class="slides">
                <?php
                if (!empty($slider['SliderImage'])) {
                    foreach ($slider['SliderImage'] as $image) {
                        ?>
                        <li>
                            <a href="#">
                                <img src="<?php echo $image['image_full_path'] ?>" alt="" title="" />
                            </a>
                        </li>

                        <?php
                    }
                }
                ?>
            </ul>
        <?php } ?>
    </div>
<?php endif; ?>