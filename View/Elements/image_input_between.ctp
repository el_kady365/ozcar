<div class="logo_image file-upload">
    <?php if ($info['width'] > 0 && $info['height'] > 0) { ?>
        <p class="note image_desc">Image size <?php echo $info['width']; ?>px X <?php echo $info['height']; ?>px   </p>
    <? } ?>
    <? if (isset($id) && isset($base_name) && !is_array($base_name) && !empty($base_name)) { ?>
        <span style="display:none" class="image_base_name file-name"><?php echo $base_name; ?></span>
        <div class="uploadbox-btns">
        <a target="_blank" class="btn btn-info btn-xs"  href="<?php echo Router::url('/' . $info['folder'] . $base_name); ?>"><i class="fa fa-search"></i></a>
         
         <a class="btn btn-default btn-xs" href="<?php echo (isset($delete_link) ? $delete_link : Router::url(array('action' => 'delete_field', 'image', $id, $field))) ?>" onclick="javascript: if(!confirm('<?php  __('Are you sure you want to delete this image?', true)?>')){ return false;}">
                <i class="fa fa-trash-o"></i>
            </a>
          </div> 
            
            
            
            <?
        
        
        
        
//        echo $this->Html->link(__('Delete', true), (isset($delete_link) ? $delete_link : array('action' => 'delete_field', 'image', $id, $field)), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete this image?', true));
    }
    ?>
</div>

