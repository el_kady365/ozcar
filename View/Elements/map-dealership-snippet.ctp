
<div class="tabs" id="tabs_<?php echo $dealer['DealerLocation']['id']  ?>">
    <ul class="tab-buttons clearfix">
        <li class="current active" ><a class="tab-btn current active" data-id="#mapdealers_<?php echo $dealer['DealerLocation']['id']  ?>" href="#">Dealership</a></li>
        <li ><a class="tab-btn" data-id="#mapcars_<?php echo $dealer['DealerLocation']['id']  ?>" href="#">Cars (<?php echo $dealer[0]['CarsCount'] ?>)</a></li>
    </ul>
    <div class="tabs-box">
        <div class="tab  current active" id="mapdealers_<?php echo $dealer['DealerLocation']['id']  ?>">
            <div class="sm-dealership">
                <div class="row row-sm">
                    <div class="col-md-12">
						<?php
                                    
                                    echo $this->element('dealer_map', array('size' => array('width' => 285, 'height' => 132),
                                        'latitude' => $dealer['DealerLocation']['latitude'], 'longitude' => $dealer['DealerLocation']['longitude'], 'zoom' => $dealer['DealerLocation']['zoom'], 'label' => $dealer['DealerLocation']['name']))
                        ?>
                            
                    </div>
                    <div class="col-md-7">
                        <div class="p-sm">
                            <h5 class="bold m-b-sm"><?php echo $dealer['DealerLocation']['name'] ?> </h5>
                            <p><?php echo $dealer['DealerLocation']['address1'] ?> <?php echo $dealer['DealerLocation']['address2'] ?> <?php echo $dealer['DealerLocation']['suburb'] ?> <?php echo $dealer['DealerLocation']['city'] ?> <?php echo $dealer['DealerLocation']['state'] ?></p>
                        </div>
                    </div>
                    <div class="col-md-5"> <span class="marker-lg"><?php echo $dealer[0]['CarsCount'] ?><em>CARS</em></span> </div>
                </div>

                <div class="row row-sm m-t-xs">
                    <div class="col-md-6"><a class="btn btn-block btn-md text-center btn-info" href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'inbox', '#' => 'newMsg-' . $dealer['DealerLocation']['id'])) ?>">CONTACT STORE</a></div>
                    <div class="col-md-6"><a class="btn btn-block btn-md text-center btn-info get-direction" data-address="<?php echo $dealer['DealerLocation']['id']; ?>" href="#">GET DIRECTIONS</a></div>
                </div>


            </div>
            <div class="arrow-down"></div>
        </div>
        <div class="tab" id="mapcars_<?php echo $dealer['DealerLocation']['id']  ?>">
            <?php
            if (!empty($cars)) {
				
                foreach ($cars as $car) {
                    $title = $car['Car']['year'] . ' ' . $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series'];
                    $view_url = Router::url(array('controller' => 'cars', 'action' => 'view', $car['Car']['id'], Inflector::slug($title, '-')));
                    ?>
                    <div class="sm-caritem">
                        <div class="row">
                            <div class="col-md-6">
                                <?php if (!empty($car['CarImage'])) { ?>

                                    <?php
                                    foreach ($car['CarImage'] as $car_image) {
                                        if (!empty($car_image['CarImage']['image_full_path'])) {
                                            ?>

                                            <a href="<?php echo $view_url ?>"><img src="<?php echo get_resized_image_url($car_image['CarImage']['image'], 120, 70) ?>" alt="" title=""/></a>
                                            <?php
                                            break;
                                        }
                                    }
                                    ?>

                                <?php } ?>


                            </div>
                            <div class="col-md-6">
                                <h6 class="bold"><?php echo $title ?></h6>
                                <?php if ($car['Car']['selling_price']) { ?>
                                    <h4 class="bold text-danger">$<?php echo $this->Number->format((float) $car['Car']['selling_price']) ?></h4>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row row-sm m-t-xs">
                            <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-default" href="<?php echo $view_url ?>">VIEW</a></div>
                            <div class="col-md-3"><a class="btn btn-block btn-sm-center btn-default" href="<?php echo $view_url ?>#carEnquiry">ENQUIRE</a></div>
                            <div class="col-md-6"><a class="btn btn-block btn-sm-center btn-default get-direction" data-address="<?php echo $dealer['DealerLocation']['id']; ?>" href="#">GET DIRECTIONS</a></div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <!-- / small car item -->


            <div class="p-md"> <a href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'search', '?' => $new_data)) ?>" class="btn btn-block btn-md text-center btn-info">SEE ALL CARS</a> </div>
            <div class="arrow-down"></div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>