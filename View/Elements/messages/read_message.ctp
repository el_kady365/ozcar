<div class="m-b-lg">
    <div class="row row-sm">
        <?php
        if ($sent) {
            $image = get_resized_image_url($dealer['Dealer']['logo'], 93, 93);
        } else {
            $image = get_resized_image_url($dealer['Dealer']['logo'], 93, 93);
        }
        ?>
        <div class = "col-md-3"><a href = "#">
                <img src = "<?php echo $image ?>" alt = "" title = "" class = "rounded-md"></a>
        </div>
        <div class = "col-md-6">
            <h4>
                <?php
                if ($sent) {
                    echo $dealer['Dealer']['name'] . ',<br>' . $message['Receiver']['name'];
                } else {
                    echo $dealer['Dealer']['name'] . ',<br>' . $message['Sender']['name'];
                }
                ?>

            </h4>
        </div>
        <div class = "col-md-3 text-right"><img class = "m-t-md" src = "<?php echo $dealer['Dealer']['logo_full_path'] ?>" alt = "" title = "" width = "100" ></div>
    </div>
    <div class = "form-row m-t-md">
        <div class = "row row-sm">
            <div class = "col-md-3">
                <h4>
                    <label class = "text-primary">Subject</label>
                </h4>
            </div>
            <div class = "col-md-7">
                <p class = "m-t-xs"><strong><?php echo $message['Message']['subject']
                ?></strong></p>
            </div>
            <div class="col-md-2 text-right">
                <p class="m-t-xs"><?php echo date('Y-m-d h:i a', strtotime($message['Message']['created'])) ?></p>
            </div>
        </div>
    </div>
    <?php if (!$sent) { ?>
        <div class="msg-actions">
            <ul>
                <li><a href="#newMsg"><span class="new-msg-ico"></span>New </a></li>
                <li><a href="#replyMsg" rel="<?php echo $message['Message']['id'] ?>"><span class="rep-msg-ico"></span>Reply</a></li>
                <li><a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'do_action', 'delete', $message['Message']['id'])) ?>"><span class="del-msg-ico"></span>Delete</a></li>
                <li><a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'do_action', 'mark_unread', $message['Message']['id'])) ?>"><span class="unread-msg-ico"></span>Mark unread</a></li>
                <li><a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'do_action', 'mark_read', $message['Message']['id'])) ?>"><span class="read-msg-ico"></span>Mark read</a></li>
            </ul>
        </div>
    <?php } ?>
    <!-- / msg-actions -->

    <div class="msg-body m-t-md">
        <p> <?php echo nl2br($message['Message']['body']) ?></p>
    </div>
</div>