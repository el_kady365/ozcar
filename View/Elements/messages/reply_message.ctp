<div class="original-msg m-b-lg">
    <div class="row row-sm">
        <div class="col-md-3"><a href="#"><img src="<?php echo get_resized_image_url($dealer['Dealer']['logo'], 93, 93);?>" alt="" title="" class="rounded-md"></a></div>
        <div class="col-md-6">
            <h4><?php echo $dealer['Dealer']['name'] ?>,<br>
                <?php echo $message['Sender']['name'] ?></h4>
        </div>
        <div class = "col-md-3 text-right"><img class = "m-t-md" src = "<?php echo $dealer['Dealer']['logo_full_path'] ?>" alt = "" title = "" width = "100" ></div>
    </div>
    <div class="form-row m-t-md">
        <div class="row row-sm">
            <div class="col-md-3">
                <h4>
                    <label class="text-primary">Subject</label>
                </h4>
            </div>
            <div class="col-md-7">
                <p class="m-t-xs"><strong><?php echo $message['Message']['subject'] ?></strong></p>
            </div>
            <div class="col-md-2 text-right">
                <p class="m-t-xs"><?php echo date('Y-m-d h:i a', strtotime($message['Message']['created'])) ?></p>
            </div>
        </div>
    </div>
    <div class="msg-body m-t-md">
        <p> <?php echo nl2br($message['Message']['body']) ?></p>
    </div>
</div>
<h2>Reply</h2>
<?php echo $this->Form->create('Message', array('url' => array('controller' => 'messages', 'action' => 'reply',$message['Message']['id']))); ?>
<div class="form-row">
    <div class="row">
        <div class="col-md-2">
            <h4>
                <label class="text-primary">Subject</label>
            </h4>
        </div>
        <div class="col-md-10">
            <?php echo $this->Form->input('subject', array('div' => false, 'label' => false,'value'=>strstr($message['Message']['subject'], __('Re:', true)) ? $message['Message']['subject'] : __('Re:', true) . ' ' . $message['Message']['subject'])); ?>
        </div>
    </div>
</div>
<div class="sm-editor">
    <div class="row">
         <div class="col-md-8">
                <div class="editor-bar"> 
                    <a href="#" class="ed-txt-bold"><strong>B</strong></a>
                    <a href="#" class="ed-txt-italic"><em>I</em></a>
                    <a href="#" class="ed-txt-underline"><u>U</u></a>
                    <a href="#" class="ed-txt-size">A <i class="fa fa-sort"></i>
                        <select class="fornt-size hide">
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="14">14</option>
                            <option value="18">18</option>
                            <option value="24">24</option>
                            <option value="36">36</option>
                        </select>

                    </a>
                </div>
            </div>
            <!--<div class="col-md-4 text-right">-->
                <!--<a class="add-attachment inline-block m-t-sm" href="#"><img title="" alt="" src="css/img/ico/attachment.png" class="inline-block"> Add attachment</a> </div>-->
            <!--                                <div class="col-md-12">
                                                <div class="attachment-list"> <a href="#">Attachment.jpg</a> </div>
                                            </div>-->
            <div class="col-md-12">
                <?php echo $this->Form->input('body', array('div' => false, 'label' => false, 'class' => 'summernote msg-text')); ?>

            </div>
    </div>
</div>
<div class="submit text-right">
    <button type="submit" class="btn action-btn btn-md btn-danger">SUBMIT</button>
</div>
<?php echo $this->Form->end(); ?>
<script>
    $(function(){
        $('.summernote').summernote({
            height: 300,
            toolbar: []
        });
        $('.ed-txt-bold').on('click', function () {
            $(".summernote").summernote("bold");
            return false;
        });
        $('.ed-txt-italic').on('click', function () {
            $(".summernote").summernote("italic");
            return false;
        });
        $('.ed-txt-underline').on('click', function () {
            $(".summernote").summernote("underline");
            return false;
        });
        $('.ed-txt-capitalize').on('click', function () {
//            $(".summernote").summernote("underline"); 
            return false;
        });
        $('.ed-txt-size').on('click', function () {
            $(".fornt-size").removeClass("hide").show();
            return false;
        });
        $(".fornt-size").on('change', function () {
            $(".summernote").summernote("fontSize", $(this).val());
            return false;
        })

    });
   
</script>