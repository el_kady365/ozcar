<div class="msg-popup remodal"  data-remodal-id="newMsg" data-remodal-options="hashTracking:false" >
    <a class="close-modal" data-remodal-action="close" aria-label="Close">&#215;</a>
    <h2>New Message</h2>
    <?php echo $this->Form->create('Message', array('id' => 'SendMessageForm', 'url' => array('controller' => 'messages', 'action' => 'send'))); ?>
    <div class="form-row">
        <div class="row">
            <div class="col-md-2">
                <h4>
                    <label>To</label>
                </h4>
            </div>
            <div class="col-md-10">
                <?php echo $this->Form->input('receiver_id', array('div' => false, 'label' => false, 'options' => $dealers, 'empty' => 'Please Select', 'class' => 'required')) ?>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="row">
            <div class="col-md-2">
                <h4>
                    <label>Subject</label>
                </h4>
            </div>
            <div class="col-md-10">
                <?php echo $this->Form->input('subject', array('div' => false, 'label' => false, 'class' => 'required')); ?>
            </div>
        </div>
    </div>
    <div class="sm-editor">
        <div class="row">
            <div class="col-md-8">
                <div class="editor-bar"> 
                    <a href="#" class="ed-txt-bold"><strong>B</strong></a>
                    <a href="#" class="ed-txt-italic"><em>I</em></a>
                    <a href="#" class="ed-txt-underline"><u>U</u></a>
                    <a href="#" class="ed-txt-capitalize">Aa</a>
                    <a href="#" class="ed-txt-size">A <i class="fa fa-sort"></i>
                        <select class="fornt-size hide">
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="14">14</option>
                            <option value="18">18</option>
                            <option value="24">24</option>
                            <option value="36">36</option>
                        </select>

                    </a>
                    <a href="#" class="ed-txt-color">A</a>
                </div>
            </div>
            <!--<div class="col-md-4 text-right">-->
                <!--<a class="add-attachment inline-block m-t-sm" href="#"><img title="" alt="" src="css/img/ico/attachment.png" class="inline-block"> Add attachment</a> </div>-->
            <!--                                <div class="col-md-12">
                                                <div class="attachment-list"> <a href="#">Attachment.jpg</a> </div>
                                            </div>-->
            <div class="col-md-12">
                <?php echo $this->Form->input('body', array('div' => false, 'label' => false, 'class' => 'summernote msg-text required')); ?>

            </div>
        </div>
    </div>
    <div class="submit text-right">
        <button type="submit" class="btn action-btn btn-md btn-danger">SUBMIT</button>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<?php
echo $this->Html->script(array('jquery.validate'), array('inline' => false));
echo $this->append('script');
?>
<script>
    $(function () {
        $.validator.setDefaults({ignore: ":hidden:not(.msg-text)"});
        $('#SendMessageForm').validate({
            messages: {
                'data[Message][receiver_id]': "Required",
                'data[Message][subject]': "Required",
                'data[Message][body]': "Required",
            },
            errorPlacement: function (label, element) {
                // position error label after generated textarea
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element);
                }
            }
        });
    });

</script>
<?php echo $this->end(); ?> 