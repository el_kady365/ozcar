<?php echo $this->Html->script('main'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('*:not(SelectList)').click(function () {
            $('#logos_list_<?= $prefix ?>').hide();
        });
        $('#logos_list a img').each(function () {
            if (jQuery.browser.msie && parseInt(jQuery.browser.version.substr(0, 1)) < 7 && $(this).attr('src').substring($(this).attr('src').length - 3) == 'png') {
                $(this).css({'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader (src=\'' + $(this).attr('src') + '\',sizingMethod=\'scale\')'});
                $(this).attr('src', '<?= Router::url('/css/img/') ?>blank.gif');
            }
        });
        $('#logos_list_<?= $prefix ?>  a').click(function () {
            img_w = $(this).find('img').width();
            img_h = $(this).find('img').height();
            $('#logos_list_<?= $prefix ?>').hide();
            $('.SelectedLogo_<?= $prefix ?>').html($(this).html());
//            $('#<?php //echo  $template_id  ?>').val($(this).attr('id'));
            //loadTemplate($(this).attr('id'), '<?php echo $prefix ?>');
            return false;
        });
        $('#click_list_<?= $prefix ?>,.SelectedLogo_<?= $prefix ?>').click(function () {
            //alert('Seect click');
            $('#logos_list_<?= $prefix ?>').toggle();
            return false;
        });
<?php if (!empty($current_template)) { ?>
            var image_home_id = '<?= $current_template ?>';
            var image_template_home = $('#logos_list_<?= $prefix ?> > li > a[id="' + image_home_id + '"]').html();
            $('.SelectedLogo_<?= $prefix ?>').html(image_template_home);
<?php } ?>

    });

</script>
<div class="UplaodLogo">
    <div class="ULogo">
        <div class="LogoSlect">
            <div class="SelectedLogo_<?= $prefix ?> SelectList SelectedLogo"><span class="no_logo"> No <?php echo $prefix; ?> </span></div>
            <div class="ChangeLogo SelectList"><a href="#"  class="SelectList" id="click_list_<?= $prefix ?>">Select Form List</a></div>
            <div class="clear"></div>
        </div>
        <ul style="display:none;" id="logos_list_<?= $prefix ?>" class="SelectList">
            <li class="no_logo">
                <a  href="#uboard" ><span class="no_logo"> No <?php echo $prefix; ?> </span></a>
            </li>
            <?php foreach ($templates as $i => $template) { ?>
                <li>
                    <a data-name="<?php echo $i; ?>" href="<?php echo resized_image_url($i . '.jpg', 800, 0, 'img/sections', 0); ?>" title="<?= $template== "Full Width Banner" ? "Specials: ".$template : $template ?>" id="<?php echo $i ?>" class="page-sectionp preview">
                        <img alt="<?= $i ?>" src="<?php echo resized_image_url($i . '.jpg', 400, 0, 'img/sections', 0); ?>" />
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
