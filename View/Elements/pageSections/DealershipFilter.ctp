<?php
$formHtml = ClassRegistry::init('Dealer');
$formHtml->id = 'DealershipFilter';
$dealerships['Lansvale'] = 'Lansvale';
$dealerships = $formHtml->find('list');

//debug($form_data);
// check if filtered

$dealerships = array_merge(array('all' => 'All Dealerships'), $dealerships);
if (isset($_GET['dealership']))
    $dealership = $_GET['dealership'];
else
    $dealership = 'Lansvale';
?> 
<div class="section specials-list">
    <div class="container">
        <div class="results-bar m-t-md m-b-lg">
            <div class="row">
                <div class="col-md-3">
                    <form class="inline-form sort-form" id="DealershipFilterForm" method="get">
                        <div class="input select sm-fields">
                            <label>Sort by:</label>
                            <?php echo $this->Form->input('dealerships', array('type' => 'select', 'id' => 'dealership', 'name' => 'dealership', 'options' => $dealerships, 'default' => $dealership, 'label' => false, 'onChange' => 'this.form.submit()')); ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- / results-bar -->