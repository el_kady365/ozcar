<?php
$ThreeImageBanner = ClassRegistry::init('ThreeImageBanner');
$conditions = array('ThreeImageBanner.id' => $value);
if (isset($_GET['dealership']) && $_GET['dealership'] != 'all' && $_GET['dealership'] != '') {
    //$conditions['ThreeImageBanner.dealerships like'] = "%" . $_GET['dealership'] . "%";
}
$ThreeImageBanners = $ThreeImageBanner->find('first', array('conditions' => $conditions));
if (!empty($ThreeImageBanners)) {
    $dealerships = (!empty($ThreeImageBanners['ThreeImageBanner']['dealerships']) ? $ThreeImageBanners['ThreeImageBanner']['dealerships'] : (isset($dealership) ? $dealership : ''));
    if (!empty($dealerships)) {
//    $conditions['ThreeImageBanner.yard'] = $dealerships;
    }
    ?>
    <div class="section specials-list">
        <div class="container">


            <div class="special-box m-b-lg">
                <a href="<?php echo $ThreeImageBanners['ThreeImageBanner']['url1'] ?>"><img src="<?php echo $ThreeImageBanners['ThreeImageBanner']['image1_full_path'] ?>" alt="" /></a>
            </div>

            <div class="special-box m-b-lg">
                <a href="<?php echo $ThreeImageBanners['ThreeImageBanner']['url2'] ?>"><img src="<?php echo $ThreeImageBanners['ThreeImageBanner']['image2_full_path'] ?>" alt="" /></a>
            </div>

            <div class="special-box m-b-lg">
                <a href="<?php echo $ThreeImageBanners['ThreeImageBanner']['url3'] ?>"><img src="<?php echo $ThreeImageBanners['ThreeImageBanner']['image3_full_path'] ?>" alt="" /></a>
            </div>

        </div>
    </div>
    <?php
}?>