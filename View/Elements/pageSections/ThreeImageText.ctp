<?php
$ThreeImageText = ClassRegistry::init('ThreeImageText');
$ThreeImageTexts = $ThreeImageText->find('first', array('conditions' => array('ThreeImageText.id' => $value, 'ThreeImageText.dealerships like' => "%" . $_GET['dealership'] . "%")));
if (!empty($ThreeImageTexts)) {
    $formHtml = ClassRegistry::init('FormHtml');
    $formHtml->id = 'ThreeImageText';
    $snippet = $formHtml->field('html');
    //extract($ThreeImageText->getAllFields());

    $dealerships = (!empty($ThreeImageTexts['ThreeImageText']['dealerships']) ? $ThreeImageTexts['ThreeImageText']['dealerships'] : (isset($dealership) ? $dealership : ''));
    if (!empty($dealerships)) {
//    $conditions['ThreeImageText.yard'] = $dealerships;
    }
    ?>
    <div class="section specials-list">
        <div class="container">


            <div class="other-specials m-t-lg">
                <h3 class="m-b-lg bold"><?php __('Check availability below:'); ?></h3>
                <div class="row">

                    <div class="col-md-4 col-sm-4 m-b-md">
                        <a href="<?php echo $ThreeImageTexts['ThreeImageText']['url1']; ?>" class="block m-b-md">
                            <img src="<?php echo $ThreeImageTexts['ThreeImageText']['image1_full_path']; ?>" alt="" title="" />
                        </a>
                        <h3 class="text-xl focobold m-b-md"><a class="text-primary" href="<?php echo $ThreeImageTexts['ThreeImageText']['url1']; ?>"><?php echo $ThreeImageTexts['ThreeImageText']['title1']; ?></a></h3>
                        <p><?php echo $ThreeImageTexts['ThreeImageText']['body1']; ?></p>
                    </div>
                    <div class="col-md-4 col-sm-4 m-b-md">
                        <a href="<?php echo $ThreeImageTexts['ThreeImageText']['url2']; ?>" class="block m-b-md">
                            <img src="<?php echo $ThreeImageTexts['ThreeImageText']['image2_full_path']; ?>" alt="" title="" />
                        </a>

                        <h3 class="text-xl focobold m-b-md"><a class="text-primary" href="<?php echo $ThreeImageTexts['ThreeImageText']['url2']; ?>"><?php echo $ThreeImageTexts['ThreeImageText']['title2']; ?></a></h3>
                        <p><?php echo $ThreeImageTexts['ThreeImageText']['body2']; ?></p>
                    </div>
                    <div class="col-md-4 col-sm-4 m-b-md">
                        <a href="<?php echo $ThreeImageTexts['ThreeImageText']['url3']; ?>" class="block m-b-md">
                            <img src="<?php echo $ThreeImageTexts['ThreeImageText']['image3_full_path']; ?>" alt="" title="" />
                        </a>
                        <h3 class="text-xl focobold m-b-md"><a class="text-primary" href="<?php echo $ThreeImageTexts['ThreeImageText']['url3']; ?>"><?php echo $ThreeImageTexts['ThreeImageText']['title3']; ?></a></h3>
                        <p><?php echo $ThreeImageTexts['ThreeImageText']['body3']; ?></p>
                    </div>

                    <div class="clear"></div>


                    <div class="col-md-2 col-md-push-5 m-t-xl"><a href="#"  class="btn text-center btn-block btn-primary btn-md">More news</a></div>

                </div>
            </div>
        </div>
    </div>
<?php
}?>