<?php $page_url = Router::url('', true); ?>
<ul>
    <li><a href="javascript:;" onclick="fbs_click();"><span class="social-fb"></span> Share on Facebook</a></li>
    <li><a href="javascript:;" onclick="twitt_click();"><span class="social-tw"></span> Share on Twitter</a></li>
    <li><a href="javascript:;" onclick="google_click();"><span class="social-g"></span> Share on Google+</a></li>
    <li><a href="mailto:?subject=<?php echo $share_title; ?>&body=Here is a link to a site I really like. <?php echo $page_url; ?>"><span class="social-e"></span> Email to a friend</a></li>
</ul>

<script type="text/javascript">
    u = location.href;
    t = '<?php echo $share_title; ?>';

    function fbs_click() {
        window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
        return false;
    }

    function twitt_click() {
        window.open('https://twitter.com/share?url=' + encodeURIComponent(u) + '&text=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
        return false;
    }
    function google_click() {
        window.open('https://plus.google.com/share?url=' + encodeURIComponent(u), 'sharer', 'toolbar=0,status=0,width=626,height=436');
        return false;
    }
</script>