<div class="logo_image file-upload input file_element" style="display: none;">
	<p class="note image_desc">Image size <span class="width"></span>px X <span class="height"></span>px   </p>
	<p class="note file_desc" >File formats (<span class="extensions"></span>), Max file size:<span class="file_size"></span>   </p>

	<div class="current-file">
        <span class="file-name"></span>
        <a target="_blank" class="button-secondary download_url" href=""><?php echo __('Preview'); ?></a><?
		echo $this->Html->link(__('Delete', true), '', array('class' => 'button-secondary delete_url'));
		?>
	</div>
</div>

