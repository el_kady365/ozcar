        <style id="body_bg_style" type="text/css">
            body{
                <?php if (!empty($config['style.background'])) : ?>
                    background-color: <?php echo $config['style.background']; ?>;
                <?php endif; ?>
                <?php if (!empty($config['style.text_color'])) : ?>
                    color: <?php echo $config['style.text_color']; ?>;
                <?php endif; ?>

            }

        </style>     
        <style id="color_a_style" type="text/css">
            .bg-a{
                <?php if (!empty($config['style.color_a'])) : ?>

                    background-color: <?php echo $config['style.color_a']; ?>;
                <?php endif; ?>                    
                <?php if (!empty($config['style.color_a_fore'])) : ?>

                    color: <?php echo $config['style.color_a_fore']; ?>;
                <?php endif; ?>                    
            }

        </style>     
        <style id="color_b_style" type="text/css">
            .bg-b, .nav a:hover, .nav li.active > a, .nav li > a.active {
                <?php if (!empty($config['style.color_b'])) : ?>
                    background-color: <?php echo $config['style.color_b']; ?>
                <?php endif; ?>

            }

            .color-a, h1, h2, h3, h4, h5, h6, a, .pages ul li  { 
                <?php if (!empty($config['style.color_b'])) : ?>
                    color: <?php echo $config['style.color_b']; ?>
                <?php endif; ?>

            }

            .bg-b, .nav a:hover, .nav li.active > a, .nav li > a.active {
                <?php if (!empty($config['style.color_b_fore'])) : ?>
                    color:  <?php echo $config['style.color_b_fore']; ?>
                <?php endif; ?>
            }                       
            
        </style>