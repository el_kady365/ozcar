<?php
// debug($topmenu);
if (!empty($topmenu)):
    ?>
    <ul  id="<?php echo (isset($menuid) ? $menuid : 'menu') ?>" class="pull-left">
        <?php
        $keys = array_keys($topmenu);
        $endkey = end($keys);
        if (!empty($isSupervisor)) {
            $topmenu[] = array('Menu' =>
                array(
                    'title' => 'Supervisor Logout',
                    'is_url' => 1,
                    'url' => Router::url(array('action' => 'logout', 'controller' => 'bookings')),
                    'routed' => 1,
                )
            );
        }
        $isCurrentMenuSet = false;
        foreach ($topmenu as $k => $menu):
            $title = $menu['Menu']['title'];
            if (($title == 'Login' && !empty($loginUser)) || $title == 'My Account' && empty($loginUser))
                continue;
            if ($title == 'My Account') {
                $menu['Menu']['Submenu'] = array(
                    array('Menu' => array(
                            'title' => 'Profile',
                            'is_url' => 1,
                            'url' => '/users/profile',
                        )),
                    array('Menu' => array(
                            'title' => 'Orders',
                            'is_url' => 1,
                            'url' => '/orders',
                        )),
                    array('Menu' => array(
                            'title' => 'Log Out',
                            'is_url' => 1,
                            'url' => '/users/logout',
                        )),
                );
            }


            $url = (!empty($menu['Menu']['is_url']) && !empty($menu['Menu']['url'])) ? ( isset($menu['Menu']['routed']) ? $menu['Menu']['url'] : Router::url($menu['Menu']['url']) ) : (Router::url("/content/{$menu['Menu']['permalink']}"));
            $class = (!empty($menu['Menu']['Submenu'])) ? "sub-item " : "";
            $class2 = (!empty($menu['Menu']['Submenu'])) ? "has-sub " : "";
            $class2 .= (($k === $endkey) ? 'last-child ' : '');
            $target = "";
            if ($url == $_SERVER['REQUEST_URI'] && empty($isCurrentMenuSet)) {
                $class.="active ";
                $isCurrentMenuSet = true;
            }
            ?>
            <li class="<?= $class2 ?>">
                <a target="<?= $target ?>" class="<?= $class ?>" href="<?= $url ?>"><span class="helvetica">
                        <?= $title ?>
                    </span></a>
                <?php if (!empty($menu['Menu']['Submenu'])) { ?>
                    <ul class="nav-box" style="display:none ;">
                        <?php
                        foreach ($menu['Menu']['Submenu'] as $i => $submenu):

                            $title = $submenu['Menu']['title'];
                            $url = (!empty($submenu['Menu']['is_url']) && !empty($submenu['Menu']['url'])) ? ( isset($submenu['Menu']['routed']) ? $submenu['Menu']['url'] : Router::url($submenu['Menu']['url']) ) : (Router::url("/content/{$submenu['Menu']['permalink']}"));
                            $class = !empty($menu['Menu']['Submenu'][($i + 1)]) ? "" : "last ";
                            $class2 = (!empty($submenu['Menu']['SubSubmenu'])) ? "has-sub " : "";

                            $target = "";
                            ?>
                            <li class="<?= $class ?> <?= $class2 ?>"><a href="<?= $url ?>"><span>
                                        <?= $title ?>
                                    </span></a>
                                <?php if (!empty($submenu['Menu']['SubSubmenu'])) { ?>
                                    <ul style="display:none ;">
                                        <?php
                                        foreach ($submenu['Menu']['SubSubmenu'] as $j => $submenu2):
                                            $title = $submenu2['Menu']['title'];
                                            $url = (!empty($submenu2['Menu']['is_url']) && !empty($submenu2['Menu']['url'])) ? ( isset($submenu2['Menu']['routed']) ? $submenu2['Menu']['url'] : Router::url($submenu2['Menu']['url']) ) : (Router::url("/content/{$submenu2['Menu']['permalink']}"));
                                            $class_last = !empty($submenu['Menu']['SubSubmenu'][($j + 1)]) ? "" : "last";
                                            $target = "";
                                            ?>
                                            <li class="<?= $class_last ?>"><a href="<?= $url ?>"><span>
                                                        <?= $title ?>
                                                    </span></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php } ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<?php echo $this->append('script') ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.has-sub').hover(function () {
            $(this).find('ul:first').show();
        }, function () {
            $(this).find('ul:first').hide();
        });

        $('.nav-box li').hover(function ()
        {
            $(this).parent('.nav-box:first').show();
        },
                function () {
                    $(this).parent('.nav-box:first').hide();
                });
    });
</script>
<?php echo $this->end(); ?>