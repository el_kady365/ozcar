<font face="Arial" style="font-size:18px;" > <?php echo __('Car that match your search', true) ?></font>
<br />
<hr />

<br />

<p>
    <?php echo __('Hi', true) ?> <?php echo $user['UserDetail']['first_name'] ?>,
</p>

<?php
foreach ($cars as $car) {
    $title = $car['Car']['year'] . ' ' . $car['Car']['make'] . ' ' . $car['Car']['model'] . ' ' . $car['Car']['series'];
    ?>
    <h3>
        <a href="<?php echo Router::url(array('controller' => 'cars', 'action' => 'view', $car['Car']['id']), true) ?>" > 
    <?php echo $title ?></a>
    </h3>
    <br />
    <hr />
    <?php
}
?>