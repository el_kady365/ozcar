<?php ?>
<font face="Arial" style="font-size:18px;" >Contact Us message </font><br /><br />
<ul style="font-family:Arial, Helvetica, sans-serif; font-size:12px; ">    
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
	<?php echo $this->Html->tag('li', $this->Html->tag('strong', 'Name') . ': ' . $contact_data['name']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
	<?php echo $this->Html->tag('li', $this->Html->tag('strong', 'Subject') . ': ' . $contact_data['subject']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
	<?php echo $this->Html->tag('li', $this->Html->tag('strong', 'Email Address') . ': ' . $contact_data['email']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
	<?php echo $this->Html->tag('li', $this->Html->tag('strong', 'Message') . ': ' . nl2br($contact_data['message'])); ?>
    </font>
	<font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
	<?php echo $this->Html->tag('li', $this->Html->tag('strong', 'Date on') . ': ' . date('d/m/Y')); ?>
    </font>
</ul>
