<?php ?>
<font face="Arial">
    Hi admin of <?php echo $config['basic.site_name'] ; ?>,<br /><br />
    You recently requested a new password.<br /> <br />
    Your email: <?php echo  $config['basic.admin_email'] ; ?><br /> <br />
    Your admin user name: <?php echo $config['auth.user_name'] ; ?><br /> <br />
    Here is your reset code, which you can enter on the password reset page:<br />
    <?php echo $confirm_code ; ?> <br /><br />You can also reset your password by following the link below:<br />
    <a href="<?php echo Router::url("/site_configs/confirm_password?email={$config['basic.admin_email']}&code=$confirm_code",true) ; ?>" >Go to reset page</a><br /><br />
    If you did not reset your password, please disregard this message.<br /><br />
    Thanks,<br />
    <?php echo $config['basic.site_name']  ; ?>
</font>