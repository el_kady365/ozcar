<?php
if ($message['Message']['from_type'] == 2) {
    $rname = $message['Receiver']['first_name'];
} else {
    $rname = $message['Receiver']['name'];
}
?>
<?php echo __('Hi', true) ?> <?php echo $rname ?>,
<br /><br />
<font face="Arial" style="" >
<?php
if ($message['Message']['from_type'] == 2) {
    $name = $message['Sender']['name'];
} else {
    $name = $message['Sender']['first_name'];
}
?>
<?php echo __('You received a new message from ', true) ?> <?php echo $name; ?>
</font>
<br /><br />
<hr />
<br /><br />
<p>
    <?php echo nl2br($message['Message']['body']); ?>
</p>
<br />
<?php echo __('To reply to this message please follow this link', true) ?>

<?php
if ($message['Message']['from_type'] == 1) {
    $view_link = Router::url(array('controller' => 'messages', 'action' => 'view', $message['Message']['id'], 'admin' => true), true);
} else {
    $view_link = Router::url(array('controller' => 'messages', 'action' => 'inbox', '#' => 'readMsg-' . $message['Message']['id'], 'admin' => false), true);
}
?>



<a href="<?php echo $view_link ?>">
    <?php echo $view_link ?>
</a>