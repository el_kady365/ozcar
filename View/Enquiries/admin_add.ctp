<div class="enquiries form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Enquiry';
        echo $this->Form->create($modelClass);

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('name', array('class' => 'input'));
        echo $this->Form->input('subject', array('class' => 'input'));
        echo $this->Form->input('description', array('class' => 'input editor'));
        echo $this->Form->input('email', array('class' => 'input email'));
        echo $this->Form->input('mobile', array('class' => 'input'));
        echo $this->Form->input('telephone', array('class' => 'input'));
        echo $this->Form->input('address', array('class' => 'input'));
        echo $this->Form->input('post_code', array('class' => 'input'));
        echo $this->Form->input('city', array('class' => 'input'));
        echo $this->Form->input('state', array('class' => 'input'));
        if (empty($_SESSION["dealer"])) {

            echo $this->Form->input('dealer_id', array('class' => 'input'));
            echo $this->Form->input('dealer_location_id', array('class' => 'input'));
        }
//		echo $this->Form->input('car_id' , array('class' => 'input' ) );
        echo $this->Form->input('type_id', array('class' => 'input'));
//		echo $this->Form->input('field1' , array('class' => 'input' ) );
//		echo $this->Form->input('field2' , array('class' => 'input' ) );
//		echo $this->Form->input('field3' , array('class' => 'input' ) );
//		echo $this->Form->input('field4' , array('class' => 'input' ) );
//		echo $this->Form->input('field5' , array('class' => 'input' ) );
//		echo $this->Form->input('field6' , array('class' => 'input' ) );
//		echo $this->Form->input('field7' , array('class' => 'input' ) );
//		echo $this->Form->input('field8' , array('class' => 'input' ) );
// 		echo $this->Form->enableAjaxUploads();
//		echo $this->Form->enableEditors('textarea.editor'); 

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
