<div class="content">
    <div class="container">
        <div class="col-md-12">
            <div class="actions-bar-border clearfix">
                <h1 class="pull-left">Reading Email</h1>
                <div class="pull-right">
                    <ul class="small-actions">

<!--                        <li><a href="#" class="sm-action"><span class="ico rep-msg"></span> Reply</a></li>
                        <li>|</li>-->

                        <li><a href="<?php echo Router::url('/admin/enquiries/delete/' . $enquiry['Enquiry']['id']) ?>" class="sm-action"><span class="ico del-msg"></span> Delete</a></li>
                        <!--<li>|</li>-->
<!--                        <li><a href="#" class="sm-action"><span class="ico unread-msg"></span> Mark as unread</a></li>
                        <li>|</li>
                        <li><a href="#" class="sm-action"><span class="ico read-msg"></span> Mark read</a></li>-->
                    </ul>
                </div>
            </div>
            <!-- / action bar -->

            <!--            <div class="m-t-md clearfix m-b-lg"> 
                            <a href="#" class="thumbs-up active type-ico pull-left"></a>
                            <img class="center pull-left m-l-md" src="css/img/ico/attachment_lg.png" alt="" title="" />
                        </div>-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="m-b-md">
                <tr>
                    <td width="70" height="30">From:</td>
                    <td><?php echo $enquiry['Enquiry']['first_name'] . ' ' . $enquiry['Enquiry']['last_name'] ?></td>
                </tr>
                <tr>
                    <td height="30">Date:</td>
                    <td><?php echo date('d-F-y H:i', strtotime($enquiry['Enquiry']['created'])) ?></td>
                </tr>
            </table>
            <h4 class="m-b-lg m-t-lg"><?php echo strtoupper(Enquiry::$types[$enquiry['Enquiry']['type_id']]) ?>: </h4>
            <!--<h6><span class="bold">Subject: </span> 'Hi do you have any further Holden Captivas in stock?' </h6>-->
            <div class="email-body m-t-md">
                <?php echo nl2br($enquiry['Enquiry']['description']) ?>
            </div>
            <?php
            if (!empty($enquiry['Car'])) {
                $title = $enquiry['Car']['year'] . ' ' . $enquiry['Car']['make'] . ' ' . $enquiry['Car']['model'] . ' ' . $enquiry['Car']['series'];
                ?>
                <div class="car-attachment">

                    <div class="listing-box-primary">
                        <div class="listing-box-head">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="meta">
                                        <h3 class="inline-block"> <a href="#"><?php echo $title ?></a></h3>
                                    </div>
                                </div>
                                <div class="col-md-3 text-right"><a href="#"><img src="<?php echo get_resized_image_url($enquiry['Dealer']['logo'], 120) ?>" alt="" title="" width="100"/></a></div>
                            </div>
                        </div>
                        <!-- /listing-box-head  -->
                        <div class="listing-box-content">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="car-preview-thumb owl-carousel owl-theme"> 
                                        <a href="#">
                                            <img width="285" src="<?php echo get_resized_image_url($carImage['CarImage']['image'], 290) ?>" alt="" title=""/>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <p><?php echo $this->Text->truncate($enquiry['Car']['comments']); ?></p>
                                    <div class="feature-content">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td height="55"><span class="features-ico ico-01"></span><?php echo $enquiry['Car']['body'] ?></td>
                                                <td height="55"><span class="features-ico ico-02"></span><?php echo $enquiry['Car']['engine_capacity'] ?></td>
                                            </tr>
                                            <tr>
                                                <td height="55"><span class="features-ico ico-05"></span><?php echo $this->Number->format((float) $enquiry['Car']['odometer']) ?> kms</td>
                                                <td height="55"><span class="features-ico ico-06"></span><?php echo $enquiry['Car']['transmission'] ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="listing-map"><?php
                                        echo $this->element('dealer_map', array('size' => array('width' => 169, 'height' => 125),
                                            'latitude' => $enquiry['DealerLocation']['latitude'], 'longitude' => $enquiry['DealerLocation']['longitude'], 'zoom' => $enquiry['DealerLocation']['zoom'], 'label' => $enquiry['DealerLocation']['name']))
                                        ?></div>
                                    <div class="map-caption">
                                        <div class="row">
                                            <div class="col-md-8 text-center"> <span class="item-price">$<?php echo $this->Number->format((float) $enquiry['Car']['selling_price']) ?></span> </div>
                                            <div class="col-md-4"> <span class="item-meta bold">Drive Away</span> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>        





                    </div>


                </div>
            <?php } ?>
            <!-- / car-attachment -->


            <!--            <div class="submit text-right">
                            <a href="#" class="btn btn-md btn-default action-btn">REPLY</a>
                        </div>-->




        </div>
    </div>
</div>