<div class="enquiries index">
	<h2><?php echo __('Enquiries'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Name' ; ?></th>
	            <th><?php echo 'Subject' ; ?></th>
	            <th><?php echo 'Description' ; ?></th>
	            <th><?php echo 'Email' ; ?></th>
	            <th><?php echo 'Mobile' ; ?></th>
	            <th><?php echo 'Telephone' ; ?></th>
	            <th><?php echo 'Address' ; ?></th>
	            <th><?php echo 'Post Code' ; ?></th>
	            <th><?php echo 'City' ; ?></th>
	            <th><?php echo 'State' ; ?></th>
	            <th><?php echo 'Dealer Id' ; ?></th>
	            <th><?php echo 'Dealer Location Id' ; ?></th>
	            <th><?php echo 'Car Id' ; ?></th>
	            <th><?php echo 'Type Id' ; ?></th>
	            <th><?php echo 'Field1' ; ?></th>
	            <th><?php echo 'Field2' ; ?></th>
	            <th><?php echo 'Field3' ; ?></th>
	            <th><?php echo 'Field4' ; ?></th>
	            <th><?php echo 'Field5' ; ?></th>
	            <th><?php echo 'Field6' ; ?></th>
	            <th><?php echo 'Field7' ; ?></th>
	            <th><?php echo 'Field8' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($enquiries as $enquiry): ?>
	<tr>
		<td><?php echo h($enquiry['Enquiry']['id']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['name']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['subject']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['description']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['email']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['mobile']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['telephone']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['address']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['post_code']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['city']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['state']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($enquiry['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $enquiry['Dealer']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($enquiry['DealerLocation']['name'], array('controller' => 'dealer_locations', 'action' => 'view', $enquiry['DealerLocation']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($enquiry['Car']['id'], array('controller' => 'cars', 'action' => 'view', $enquiry['Car']['id'])); ?>
		</td>
		<td><?php echo h($enquiry['Enquiry']['type_id']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field1']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field2']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field3']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field4']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field5']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field6']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field7']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['field8']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['created']); ?>&nbsp;</td>
		<td><?php echo h($enquiry['Enquiry']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
