<div class="enquiries view">
<h2><?php  echo __('Enquiry'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['subject']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['mobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telephone'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Post Code'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['post_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dealer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($enquiry['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $enquiry['Dealer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dealer Location'); ?></dt>
		<dd>
			<?php echo $this->Html->link($enquiry['DealerLocation']['name'], array('controller' => 'dealer_locations', 'action' => 'view', $enquiry['DealerLocation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Car'); ?></dt>
		<dd>
			<?php echo $this->Html->link($enquiry['Car']['id'], array('controller' => 'cars', 'action' => 'view', $enquiry['Car']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type Id'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['type_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field1'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field2'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field3'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field4'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field5'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field6'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field7'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field7']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field8'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['field8']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($enquiry['Enquiry']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Enquiry'), array('action' => 'edit', $enquiry['Enquiry']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Enquiry'), array('action' => 'delete', $enquiry['Enquiry']['id']), null, __('Are you sure you want to delete # %s?', $enquiry['Enquiry']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Enquiries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enquiry'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealers'), array('controller' => 'dealers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer'), array('controller' => 'dealers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealer Locations'), array('controller' => 'dealer_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer Location'), array('controller' => 'dealer_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cars'), array('controller' => 'cars', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Car'), array('controller' => 'cars', 'action' => 'add')); ?> </li>
	</ul>
</div>
