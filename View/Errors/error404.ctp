<div class="inner404">
    <h1>404 Error</h1>
    <h4>We Couldn't Find Your Page! </h4>
    <a href="<?php echo Router::url('/') ?>" class="btn btn-default bignoodle">Back to Home</a>
</div>