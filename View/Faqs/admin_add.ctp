<div class="faqs form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Faq';
        echo $this->Form->create($modelClass, array('type' => 'file'));

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('question', array('type' => 'text', 'class' => 'input'));
        echo $this->Form->input('answer', array('class' => 'input editor'));
        $field = 'file';
        echo $this->Form->input('file', array('class' => 'input', 'type' => 'file', 'between' => $this->element('file_input_between', array('info' => $file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data[$modelClass]['id']) ? $this->data[$modelClass]['id'] : null), 'base_name' => (!empty($this->request->data[$modelClass][$field]) ? $this->request->data[$modelClass][$field] : '')))));
        echo $this->Form->input('active', array('class' => 'input'));
        echo $this->Form->input('display_order', array('class' => 'input number'));

        echo $this->Form->enableAjaxUploads();
        echo $this->Form->enableEditors('textarea.editor');

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
