<div class="faqs index">
    <h2><?php echo __('Faqs'); ?></h2>
    <?php 
	 echo $this->List->filter_form($modelName, $filters); 
	$fields = array(
		'Faq.id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Faq.question' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Faq.answer' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Faq.file' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Faq.active' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Faq.display_order' => array() ,
//		'Faq.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Faq.updated' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
	); 
            $links = array(
                $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ), // , __('Are you sure?', true)),
            );

            $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

            echo $this->List->adminIndexList($fields, $faqs, $links,true, $multi_select_actions);


?>
</div>
