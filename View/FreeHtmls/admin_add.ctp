<div class="FormExtended">
    <?php echo $this->Form->create('FreeHtml'); ?>


    <?php
    echo $this->Form->input('id');
    echo $this->Form->input('title');
    echo $this->Form->input('html', array('class' => 'INPUT editor'));
    echo $this->Form->input('keywords', array('class' => 'keywords', 'type' => 'text'));
    ?>
    <p></p>
    <?php echo $this->Form->enableEditors(); ?>
    <?php echo $this->Form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (!empty($this->data['FreeHtml']['id'])) {
        echo $this->element('save_as_new', array('model' => 'FreeHtml'));
    }
    ?>
    <?php echo $this->Form->end(); ?>
</div>


<?php
echo $this->Html->css(array('select2', 'jquery.ui.datepicker', 'jquery-ui'), null, array(), false);
echo $this->Html->script(array('select2', 'http://code.jquery.com/ui/1.10.3/jquery-ui.min.js', 'jquery.ui.datepicker', 'jquery-ui-timepicker-addon', 'jquery-ui-sliderAccess.js'), array('inline' => false));
?>


