<div class="staticPages index">
    <h2><?php __('Free Html Page sections'); ?></h2>

    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'FreeHtml.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'FreeHtml.title' => array('edit_link' => array('action' => 'edit', '%id%')),
        
        
        //'Menu' => array('php_expression' => '".($row["Page"]["menu_id"])?$row["Menu"]["title"]:"No Parent"."'),
        // 'Permission' => array('title'=>'Permission','php_expression'=>'".$params[$row[$model]["access"]]."'),
        
    );
    $links = array(
        $this->Html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
    );

    $multi_select_actions = array(
        'delete' => array('action' => Router::url(array('action' => 'delete_multi', 'admin' => true)), 'confirm' => true)
    );
    //debug($pages);
    ?><?= $this->List->adminIndexList($fields, $freeHtmls, $links, true, $multi_select_actions, $accesses) ?></div>