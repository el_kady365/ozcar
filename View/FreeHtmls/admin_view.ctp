<div class="freeHtmls view">
<h2><?php  __('FreeHtml');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Html'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['html']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Permenant Display'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['permenant_display']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Activation Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['activation_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Deactivation Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['deactivation_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Keywords'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $freeHtml['FreeHtml']['keywords']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit FreeHtml', true), array('action'=>'edit', $freeHtml['FreeHtml']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete FreeHtml', true), array('action'=>'delete', $freeHtml['FreeHtml']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $freeHtml['FreeHtml']['id'])); ?> </li>
		<li><?php echo $html->link(__('List FreeHtmls', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New FreeHtml', true), array('action'=>'add')); ?> </li>
	</ul>
</div>
