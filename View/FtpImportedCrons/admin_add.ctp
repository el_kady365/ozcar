<div class="ftpImportedCrons form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'FtpImportedCron' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
//		echo $this->Form->input('dealer_id' , array('class' => 'input' ) );
		echo $this->Form->input('ftp_username' , array('class' => 'input' , "disabled" => ($this->action == "admin_edit") ? "disabled" : false) );
		echo $this->Form->input('ftp_password' , array('class' => 'input' ) );
		echo $this->Form->input('hour_to_run' , array('class' => 'input' ) );
//		echo $this->Form->input('last_run' , array('class' => 'input' ) );
//		echo $this->Form->input('is_ftp_created' , array('class' => 'input required' ) );
		echo $this->Form->input('template_id' , array("empty" => "Select Template",'class' => 'input required' ) );
		echo $this->Form->input('file_name' , array('class' => 'input' ) );
		echo $this->Form->input('active' , array('class' => 'input required' ) );
//		echo $this->Form->input('is_password_changed' , array('class' => 'input required' ) );

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
