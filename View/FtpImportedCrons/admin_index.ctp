<div class="ftpImportedCrons index">
    <h2><?php echo __('Ftp Imported Crons'); ?></h2>
    <?php 
	 echo $this->List->filter_form($modelName, $filters); 
	$fields = array(
		'FtpImportedCron.id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'FtpImportedCron.dealer_id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'FtpImportedCron.ftp_username' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'FtpImportedCron.ftp_password' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'FtpImportedCron.hour_to_run' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'FtpImportedCron.last_run' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'FtpImportedCron.is_ftp_created' => array('edit_link' => array('action' => 'edit' , '%id%') , "format" => "bool") , 
//		'FtpImportedCron.file_name' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'FtpImportedCron.is_password_changed' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Dealer.name' => array("title" => "Dealer") , 
		'Template.name' => array("title" => "Template") , 
		'FtpImportedCron.active' => array('edit_link' => array('action' => 'edit' , '%id%') , "format" => "bool") , 
//		'FtpImportedCron.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'FtpImportedCron.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
	); 
            $links = array(
                $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ), // , __('Are you sure?', true)),
            );

            $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

            echo $this->List->adminIndexList($fields, $ftpImportedCrons, $links,true, $multi_select_actions);


?>
</div>
