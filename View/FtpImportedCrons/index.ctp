<div class="ftpImportedCrons index">
	<h2><?php echo __('Ftp Imported Crons'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Dealer Id' ; ?></th>
	            <th><?php echo 'Ftp User Name' ; ?></th>
	            <th><?php echo 'Ftp Password' ; ?></th>
	            <th><?php echo 'Hour To Run' ; ?></th>
	            <th><?php echo 'Last Run' ; ?></th>
	            <th><?php echo 'Is Ftp Created' ; ?></th>
	            <th><?php echo 'Active' ; ?></th>
	            <th><?php echo 'File Name' ; ?></th>
	            <th><?php echo 'Is Password Changed' ; ?></th>
	            <th><?php echo 'Template Id' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($ftpImportedCrons as $ftpImportedCron): ?>
	<tr>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ftpImportedCron['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $ftpImportedCron['Dealer']['id'])); ?>
		</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['ftp_username']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['ftp_password']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['hour_to_run']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['last_run']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['is_ftp_created']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['active']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['file_name']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['is_password_changed']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ftpImportedCron['Template']['name'], array('controller' => 'templates', 'action' => 'view', $ftpImportedCron['Template']['id'])); ?>
		</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['created']); ?>&nbsp;</td>
		<td><?php echo h($ftpImportedCron['FtpImportedCron']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
