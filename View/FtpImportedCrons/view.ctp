<div class="ftpImportedCrons view">
<h2><?php  echo __('Ftp Imported Cron'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dealer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ftpImportedCron['Dealer']['name'], array('controller' => 'dealers', 'action' => 'view', $ftpImportedCron['Dealer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ftp User Name'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['ftp_username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ftp Password'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['ftp_password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hour To Run'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['hour_to_run']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Run'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['last_run']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Ftp Created'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['is_ftp_created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Name'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['file_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Password Changed'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['is_password_changed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Template'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ftpImportedCron['Template']['name'], array('controller' => 'templates', 'action' => 'view', $ftpImportedCron['Template']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($ftpImportedCron['FtpImportedCron']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ftp Imported Cron'), array('action' => 'edit', $ftpImportedCron['FtpImportedCron']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ftp Imported Cron'), array('action' => 'delete', $ftpImportedCron['FtpImportedCron']['id']), null, __('Are you sure you want to delete # %s?', $ftpImportedCron['FtpImportedCron']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ftp Imported Crons'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ftp Imported Cron'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dealers'), array('controller' => 'dealers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Dealer'), array('controller' => 'dealers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Templates'), array('controller' => 'templates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Template'), array('controller' => 'templates', 'action' => 'add')); ?> </li>
	</ul>
</div>
