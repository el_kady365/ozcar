<div class="pageBanners form">
    <?php echo $this->Form->create('FullWidthBanner', array('type' => 'file')); ?>

    <?php
    echo $this->Form->input('id');
    echo $this->Form->input('title');
    echo $this->Form->input('url');
    echo $this->Form->input('image', array('class' => 'INPUT', 'type' => 'file', 'div' => false, 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['FullWidthBanner']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['FullWidthBanner']['image'] : '')))
    ));
    echo $this->Form->input('dealerships', array('options' => array("all" => "All") + $dealerships, 'class' => 'dealerships', 'multiple' => true));
    echo $this->Form->input('keywords', array('class' => 'keywords', 'type' => 'text'));
    ?>
    <?php echo $this->Form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (!empty($this->data['FullWidthBanner']['id'])) {
        echo $this->element('save_as_new', array('model' => 'FullWidthBanner'));
    }
    ?>
    <?php echo $this->Form->end(); ?>
</div>

<?php
echo $this->Html->css(array('select2', 'jquery.ui.datepicker', 'jquery-ui'), null, array(), false);
echo $this->Html->script(array('select2', 'http://code.jquery.com/ui/1.10.3/jquery-ui.min.js', 'jquery.ui.datepicker', 'jquery-ui-timepicker-addon', 'jquery-ui-sliderAccess.js'), array('inline' => false));
?>
<script>
    $(function () {
        $(".dealerships").select2({
            width: '450px'
        });
    });
</script>