<div class="fullWidthBanners view">
<h2><?php  __('Full Width Banner');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fullWidthBanner['FullWidthBanner']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fullWidthBanner['FullWidthBanner']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fullWidthBanner['FullWidthBanner']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Image'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $f'FullWidthBanner']['image']; ?>
			&nbsp;
		</dd>
                <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Dealerships'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fullWidthBanner['FullWidthBanner']['dealerships']; ?>
			&nbsp;
		</dd>
		
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit FullWidthBanner', true), array('action'=>'edit', $fullWidthBanner['FullWidthBanner']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete FullWidthBanner', true), array('action'=>'delete', $fullWidthBanner['FullWidthBanner']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $fullWidthBanner['FullWidthBanner']['id'])); ?> </li>
		<li><?php echo $html->link(__('List FullWidthBanners', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New FullWidthBanner', true), array('action'=>'add')); ?> </li>
	</ul>
</div>
