<?php

/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

    function enableAjaxUploads() {
        if (get_class($this) == 'FormHelper')
            return $this->Html->css('fineuploader', null, array('inline' => false)) .
                    $this->Html->script('fuploaderjq.libs.min', array('inline' => false)) .
                    $this->Html->script('fuploaderjq.apply', array('inline' => false));
        return '';
    }

    function enableMapSelectors() {
        if (get_class($this) == 'FormHelper')
            return
                    $this->Html->css('reveal', null, array('inline' => false)) .
                    $this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places', false) .
                    $this->Html->script('jquery.reveal.js', array('inline' => false)) .
                    $this->Html->script('map-selector.js', array('inline' => false));
        return '';
    }

    function enableEditors($selector = 'textarea.editor', $css_styles = '') {
//        config.contentsCss = ['/css/mysitestyles.css', '/css/anotherfile.css'];
//        config.baseHref='http://www.example.com/path/'
        $css_str = '';
        if (empty($css_styles)) {
            $css_styles = "css/screen.css";
        }
        if (!empty($css_styles)) {
            $content_css = str_replace(",", "' , '", $css_styles);
            $css_str = ",contentsCss: ['" . $content_css . "']";
        }
        ////autoParagraph : false,
        if (get_class($this) == 'FormHelper')
            return
                    $this->Html->script('ckeditor/ckeditor', array('inline' => false)) .
                    $this->Html->script('ckeditor/adapters/adapter_jquery', array('inline' => false)) .
                    $this->Html->scriptBlock("$(function(){ 
                        CKEDITOR.dtd.\$removeEmpty.span=0;
                        $('$selector').ckeditor(
                        {
                         autoParagraph : false,
                        baseHref:'" . Router::url('/') . "' " . $css_str . "
                        }
                        ) ;});", array('inline' => false))
            ;

        return '';
    }

    function enableEditorsOLD($selector = 'textarea.editor') {
        if (get_class($this) == 'FormHelper')
            return
                    $this->Html->script('ckeditor/ckeditor', array('inline' => false)) .
                    $this->Html->script('ckeditor/adapters/adapter_jquery', array('inline' => false)) .
                    $this->Html->scriptBlock("$(function(){ $('$selector').ckeditor() ;});", array('inline' => false))
            ;

        return '';
    }

}
