<?php

//App::import('Vendor', 'fckeditor');
//class FckHelper extends AppHelper {
//
//    /**
//    * creates an fckeditor textarea
//    *
//    * @param array $namepair - used to build textarea name for views, array('Model', 'fieldname')
//    * @param string $content
//    */
//    function fckeditor($namepair, $content){
//        $editor_name = 'data';
//        foreach ($namepair as $name){
//            $editor_name .= "[" . $name . "]";
//        }
//
//        $oFCKeditor = new FCKeditor($editor_name) ;
//        $oFCKeditor->BasePath = Router::url('/js/fckeditor/') ;
//        $oFCKeditor->Value = $content ;
//        $oFCKeditor->Create() ;
//    }
//}

/**
 * @property FormHelper Form
 */
class FckHelper extends Helper {

    var $helpers = array('Javascript', 'Html', 'Form');

    /**
     * this function is deprectated, please use $fck->create() instead
     * @deprecated
     * @param String $id
     * @param String $toolbar
     * @return String
     */
    function load($id, $toolbar = 'Default') {
        $did = '';
        foreach (explode('[', str_replace(']', '', $id)) as $v) {
            $did .= ucfirst($v);
        }
        $js = $this->webroot . 'js/fckeditor/';
        //$css = $this->webroot . 'css/main.css';
        $baseHref = Router::url('/');
        $import = $this->Javascript->link('fckeditor/fckeditor');
        return <<<FCK_CODE
		$import
	<script type="text/javascript">
	fckLoader_$did = function () {
		var bFCKeditor_$did = new FCKeditor('$id');
		bFCKeditor_$did.BasePath = "$js";
		bFCKeditor_$did.ToolbarSet = '$toolbar';
		bFCKeditor_$did.Config['BaseHref'] = "$baseHref";
		bFCKeditor_$did.ReplaceTextarea();
	}
	fckLoader_$did();
	//</script>
FCK_CODE;
    }

    /**
     * Creates and return an FCKeditor field
     * @param String $modelName the model name to use for creating FCKeditor
     * @param String $fieldName the field name to use for creating FCKeditor
     * @param String $content   content to put in the editor area if field value is not set in $this->data
     * @param String $toolbar   the toolbar set to use, it must be one of those found in fckconfig.js
     * @param String $css       the content area CSS
     * @param String $options   an associative array with options for 'height' and 'width'
     * @return String HTML code for creating editor area
     */
    function create($modelName, $fieldName, $content = '', $toolbar = 'Basic', $css = false, $options = array()) {
        $default = array('width' => 700, 'height' => 250);
        $options = array_merge($default, $options);
        App::import('Vendor', 'fckeditor/fckeditor');
        if ($modelName === false) {
            $fckField = "data[$fieldName]";
        } else {
            $fckField = "data[$modelName][$fieldName]";
        }
        $oFCK = new FCKeditor($fckField);
        $oFCK->BasePath = Router::url('/js/fckeditor/');
        $oFCK->ToolbarSet = $toolbar;
        $oFCK->Config['BaseHref'] = Router::url('/', true);
        $oFCK->Value = empty($this->data[$modelName][$fieldName]) ? $content : $this->data[$modelName][$fieldName];
        //$oFCK->Value = $content;
        $oFCK->Height = $options['height'];
        $oFCK->Width = $options['width'];
        if ($css !== false) {
            $oFCK->Config['EditorAreaCSS'] = $css;
        }
//        $oFCK->Config['EditorAreaCSS']=Router::url('/css/style.css',true);

        if (isset($options['style'])) {
            $oFCK->Config['EditorAreaStyles'] = $options['style'];
        }
        $oFCK->Config['AutoDetectLanguage'] = false;
        $label = $this->Form->label($fieldName);
        if (isset($options['label']) && !empty($options['label'])) {
            $label = $this->Form->label($options['label']);
        }

        return $this->Html->div('input FCK', $label . $oFCK->CreateHtml());
    }

}

?>