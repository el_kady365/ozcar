<?php

/**
 * @property HtmlHelper Html
 * @property PaginatorHelper Paginator
 * @property JavascriptHelper Javascript
 * @property FormHelper Form
 */
class ListHelper extends AppHelper {

    public $helpers = array('Html', 'Paginator', 'Javascript', 'Form', 'Session', 'Time', 'Mixed','Text','Number');

    function adminIndexList($fields, $data = array(), $actions = array(), $multi_select = false, $multi_select_actions = array(), $params = array()) {
        $out = '';
        $urls = array();



        echo $this->Html->script('dynamic-delete', array('inline' => false));


        if (empty($params['no_paging']) || !$params['no_paging']) {
            $url_params = $this->params['url'];
            unset($url_params['url'], $url_params['page'], $url_params['sort'], $url_params['direction'], $url_params['ext']);
            $url = array('?' => $url_params);
            if (!empty($this->params['prefix'])) {
                $url[$this->params['prefix']] = true;
            }
            $this->Paginator->options(array('url' => $url));
        }

        if ($multi_select) {
            $out .='<form id="MultiSelectForm" method="post" name="MultiSelectForm" action="/" >';
        }

        $models = array_keys($this->params['models']);

        $basicModel = $models[0];

        if (stripos($this->here, "sort:{$basicModel}.display_order/direction:asc") !== false) {

            echo $this->Html->script('jquery-ui-1.9.2.core_picker.min', array('inline' => false));
            echo $this->Html->script('jquery-ui-1.9.2.mouse_widget_sortable.min', array('inline' => false));
            echo $this->Html->script('dynamic-order', array('inline' => false));
        }


        if (!empty($fields['basicModel'])) {
            $basicModel = $fields['basicModel'];
            unset($fields['basicModel']);
        }
        $title = !empty($params['title']) ? $params['title'] : Inflector::humanize(Inflector::underscore(Inflector::pluralize($basicModel)));

         $out .= '<table id="Table" class="table listing-table" cellpadding="0" cellspacing="0" width="100%">';
        //generate header
        $out.= '<thead><tr class="table-header">';
        $out.=$multi_select ? '<th class="multiSelect multi-select"><input type="checkbox" onclick=""/></th>' : '';
        $headers = array();
        $display_order = false;



        foreach ($fields as $name => $spec) {
            $model = $basicModel;
            $t = $name;
            if (strpos($name, '.')) {
                $a = explode('.', $name);
                $model = $a[0];
                $t = $a[1];
            }
            $title = empty($spec['title']) ? Inflector::humanize($t) : $spec['title'];
            $sort = empty($spec['sort']) ? $name : $spec['sort'];
            $class = '';
            $urlSort = empty($this->params['url']['sort']) ? '' : $this->params['url']['sort'];
            if ($urlSort == $sort) {
                $class = $this->Paginator->sortDir();
            }
            if (strpos($name, '.')) {

                if (empty($params['no_paging']) || !$params['no_paging']) {
                    $title = $this->Paginator->sort($sort, $title);
                }

                if ($t == 'display_order') {
                    $class .= ' ajax-reorder';
                }

                $out.="<th class=\"$class\">";
                if (!empty($spec['format']) && strtolower($spec['format']) == 'checkbox') {
                    $t = strtolower($t);
                    $out.="<input type='checkbox' class='CheckboxList' rel='{$t}'/>  ";
                }
                $out.= $title;
                if ($t == 'display_order') {
                    $out .= '<a id="display_order_save" href="javascript:void(0);" style="display: none;"  class="ajax-reorder save-order"><i class="fa fa-check-circle"></i></a><div class="clear"></div>';
                }
                $out.="</th>";
            }
            else
                $out.="<th class=\"$class\">" . $title . "</th>";

            if (strtolower($t) == 'display_order') {
                $do_action = array('action' => 'update_display_order');
                if (isset($params['update_display_order_link'])) {
                    $do_action = $params['update_display_order_link'];
                }
                /* if ($this->params['prefix']){
                  $do_action[$this->params['prefix']] = 1;
                  } */
                $multi_select_actions['Update display order'] = array('action' => Router::url($do_action));
                $urls["display_order"] = Router::url($do_action);
            }
            if (!empty($spec['format']) && strtolower($spec['format']) == 'checkbox') {
                $do_action = array('action' => 'update_active', strtolower($t));
                $multi_select_actions["Update $title"] = array('action' => Router::url($do_action));
                $urls[strtolower($t)] = Router::url($do_action);
            }
        }
        if (is_array($actions) && !empty($actions)) {
            $out .= '<th class="action">' . __('Actions', true) . '</th>';
        }
        $out .= "</tr></thead><tbody>";

        foreach ($data as $row) {
            $cells = array();
            if ($multi_select)
                $cells[] = '<input class="check_row" type="checkbox" value="' . $row[$basicModel]['id'] . '" name="ids[]" /> ';
            foreach ($fields as $name => $spec) {
                if (strpos($name, '.')) {
                    $a = explode('.', $name);

                    $model = $a[0];
                    $t = $a[1];

                    $cell = $row[$model][$t];
                    if (strtolower($t) == 'display_order') {
                        if (stripos($this->here, "sort:{$basicModel}.display_order/direction:asc") !== false) {
                            $handleClass = '';
                            $handleTitle = '';
                        } else {
                            $handleClass = 'inactive';
                            $handleTitle = 'You can only dynamic-sort when the table is sorted by Display Order, please click on \'Display Order\' header above';
                        }
                        $cell = "<div class='sort-box'><span class=\"sortHandle $handleClass \" title=\"$handleTitle\"><i class='fa fa-sort'></i></span><input type=\"text\" value=\"{$row[$model][$t]}\" class=\"AdditionOption\" rel='display_order' name=\"display_order_{$row[$basicModel]['id']}\"  size=\"5\" /></div>";
                    }

                    if (isset($spec['format'])) {
                        if ($spec['format'] == 'checkbox') {
                            $t = strtolower($t);
                            $check = 0;
                            if ($cell) {
                                $check = 1;
                            }
                            $cell = '<input  type="hidden" value="0" name="' . $t . '_' . $row[$basicModel]['id'] . '" /> ';
                            $cell .='<input ' . (($check) ? 'checked=checked' : '') . ' class="' . $t . '_checkbox AdditionOption" type="checkbox" rel="' . strtolower($t) . '" value="1" name="' . $t . '_' . $row[$basicModel]['id'] . '" /> ';
                            $cell .=($check) ? 'Yes' : 'No';
                        } elseif ($spec['format'] == 'bool') {
                            if ($cell)
                                $cell = __("Yes", true); else
                                $cell = __("No", true);
                        }
                        elseif ($spec['format'] == 'substr') {
                            $string = $cell;
                            $start = !empty($spec['options']['start']) ? $spec['options']['start'] : 0;
                            $length = !empty($spec['options']['length']) ? $spec['options']['length'] : null;
                            $cell = substr($string, $start, $length);
                        } elseif ($spec['format'] == 'image') {
                            $image_src = $cell;
                            $options = '';
                            if (!empty($spec['options']['width'])) {
                                $options.="width={$spec['options']['width']}";
                            }
                            if (!empty($spec['options']['height'])) {
                                $options.="height={$spec['options']['height']}";
                            }
                            $cell = "<img src='$image_src' $options />";
                        } elseif ($spec['format'] == 'get_from_array') {
                            $selected = $cell;
                            $items_list = !empty($spec['options']['items_list']) ? $spec['options']['items_list'] : array();
                            $selected = !empty($selected) ? $selected : 0;
                            $empty = !empty($spec['options']['empty']) ? $spec['options']['empty'] : '';

                            $cell = empty($items_list[$selected]) ? $empty : $items_list[$selected];
                        } else {
                            $cell = sprintf($spec['format'], $cell);
                        }
                    } elseif (isset($spec['date_format'])) {
                        $cell = date($spec['date_format'], strtotime($cell));
                    }
                } elseif (isset($spec['format'])) {
                    if ($spec['format'] == 'bool') {
                        if ($cell) {
                            $cell = __("Yes", true);
                        } else {
                            $cell = __("No", true);
                        }
                    } elseif ($spec['format'] == 'image' && !empty($spec['options'])) {
                        eval('$src = "' . $spec['options']['src'] . '";');
                        $other_options = '';
                        if (!empty($spec['options']['width'])) {
                            $other_options = "width = '{$spec['options']['width']}'";
                        }
                        if (!empty($spec['options']['height'])) {
                            $other_options .= "height = '{$spec['options']['height']}'";
                        }
                        if (!empty($spec['options']['alt'])) {
                            $alt = $spec['options']['alt'];
                            if (strpos($spec['options']['alt'], '$row') !== false) {
                                eval('$alt = "' . $spec['options']['alt'] . '";');
                            }
                            $other_options .= "alt = '{$alt}'";
                        }
                        $cell = "<img src='{$src}' $other_options />";
                    } elseif ($spec['format'] == 'substr' && !empty($spec['options'])) {
                        eval('$string = "' . $spec['options']['string'] . '";');

                        $start = !empty($spec['options']['start']) ? $spec['options']['start'] : 0;
                        $length = !empty($spec['options']['length']) ? $spec['options']['length'] : null;
                        $cell = substr($string, $start, $length);
                    } elseif ($spec['format'] == 'get_from_array' && !empty($spec['options'])) {
                        eval('$selected = "' . $spec['options']['selected'] . '";');
                        $items_list = !empty($spec['options']['items_list']) ? $spec['options']['items_list'] : array();
                        $selected = !empty($selected) ? $selected : 0;

                        $empty = !empty($spec['options']['empty']) ? $spec['options']['empty'] : 'None';

                        $cell = empty($items_list[$selected]) ? $empty : $items_list[$selected];
                    } elseif ($spec['format'] == 'check' && !empty($spec['options'])) {
                        eval('$conditions = "' . $spec['options']['conditions'] . '";');
                        eval('$true = "' . $spec['options']['true'] . '";');
                        eval('$false = "' . $spec['options']['false'] . '";');

                        $true = !empty($true) ? $true : '';
                        $false = !empty($false) ? $false : '';

                        $cell = ($conditions) ? $true : $false;
                    } else {

                        $cell = sprintf($spec['format'], $cell);
                    }
                } else {
                    eval('$cell="' . $spec['php_expression'] . '";');
                }
                if (!empty($spec['open_link'])) {
                    preg_match('/%(.+)%/', $spec['open_link'], $matches);
                    $spec['open_link'] = str_ireplace("%{$matches[1]}%", $row[$model][$matches[1]], $spec['open_link']);
                    $cell = $this->Html->link($cell, $spec['open_link'],array('target'=>'_blank'));
                }else if (!empty($spec['edit_link'])) {

                    preg_match('/%25(.+)%25/', Router::url($spec['edit_link']), $matches);

                    $spec['edit_link'] = str_ireplace("%{$matches[1]}%", $row[$model][$matches[1]], $spec['edit_link']);

                    $cell = $this->Html->link($cell, $spec['edit_link'],array('escape'=>false));
                }
                $cells[] = $cell;
            }

            $cell = '';
            if (is_array($actions) && !empty($actions)) {
                foreach ($actions as $action) {
                    if (is_array($action)) {
                        if(!isset($action['value'])) {
                            continue;
                        }
                        preg_match('/%25(.+)%25/', $action['value'], $matches);
                        $action['value'] = str_replace("%25{$matches[1]}%25", $row[$basicModel][$matches[1]], $action['value']);
                        
                        if(empty($action['condition'])){
                            $action['condition'] = '1';
                        }
                        //TODO: replace %model.field% in action condition
                        eval("if ({$action['condition']} ) {  \$cell .= \"<li>\" . \$action['value'] . \"</li>\"  ; }");
                    } else {
                        preg_match('/%25(.+)%25/', $action, $matches);
                        $cell .= '<li>' . str_replace("%25{$matches[1]}%25", $row[$basicModel][$matches[1]], $action) . '</li>';
                    }
                }

                $cells[] = '<ul class="action">' . $cell . '</ul>';
            }
            $out .= $this->Html->tableCells($cells, array('id' => $basicModel . '_' . $row[$basicModel]['id']), array('id' => $basicModel . '_' . $row[$basicModel]['id']));
        }
        $out .= '</tbody></table>';
 
        if ($multi_select) {
            if (is_array($multi_select_actions) && sizeof($multi_select_actions)) {
                $out .='<div class="listing-actions"><div class="multi_select_operations bulk-actions">
					<label for="select_action"   >' . __('With Selected', true) . '</label>
					 <select id="select_action" name="select_action2"  > ';
                foreach ($multi_select_actions as $title => $params) {
                    $out .='<option value="' . $params['action'] . '">' . __(Inflector::humanize($title), true) . '</option>';
                }
                $out.='</select> <input type="button" value="Go" class="GoSubmit btn btn-primary btn-sm" />  </div></div>';
            }
        }

        if ($multi_select)
            $out.="</form>";
        if (empty($params['no_paging']) || !$params['no_paging']) {
            $out.="<div class='Paging'>";
            $out.= $this->paging();
            $out.="</div>";
        }

        $urls = json_encode($urls);

        $out.= <<<CODEBLOCK
        <script type="text/javascript">
        var urls = $urls;
        $(document).ready(function(){
            $('.AdditionOption,.CheckboxList').bind('change click',function(){
                rel = $(this).attr('rel');
                url = urls[rel];
                $('#select_action').val(url);
                if(rel == 'display_order') {
                    $('#display_order_save').fadeIn(800) ;
                }
            });
            $('.GoSubmit').click(function(){
                $('#MultiSelectForm').get()[0].action= $('#select_action').val();
                $('#MultiSelectForm').submit();
                return false;
            });
            $('.multiSelect input').click(function(){
                if($(this).attr('checked'))
                    $('#MultiSelectForm input.check_row').attr('checked',$(this).attr('checked'));
                else
                    $('#MultiSelectForm input.check_row').removeAttr('checked');

                $('#MultiSelectForm input.check_row').trigger('change') ;

            });
            
            $('.CheckboxList').click(function(){
                rel = $(this).attr('rel');
                $("."+rel+"_checkbox").attr('checked',$(this).attr('checked'));
            });
            
            $('input.check_row').change(function(){            
                if($(this).attr('checked'))
                    $(this).parents('tr').addClass('selected');
                else
                    $(this).parents('tr').removeClass('selected') ;
            
            });
            
            $('#Table tr').click(function(e){
               if( e.target != this && e.target.nodeName.toLowerCase() != 'td' ) 
                   return;
               $(this).find('input.check_row').attr('checked', function(index, val){ return !val ; }).trigger('change'); 
                
            });
            


            $('#display_order_save').click(function(){
                var elem  = this;
                $.ajax({
                    url: urls['display_order'],
                    cache: false,
                    async: false,
                    type: 'POST',
                    data: $('#MultiSelectForm').serialize(),
                    dataType: 'json',
                    success: function(data){
                        if(data.status == 'success'){
                            $(elem).fadeOut(700);
                        } else{
                            alert('error saving display order');
                        }
                    }

                });
                return false;
            });

            $('input.check_row').trigger('change') ;


        });
        


        </script>
CODEBLOCK;

        return $out;
    }

    function adminRelatedList($fields, $data, $basicModel, $actions = array()) {
        $out = '';
        $controller = Inflector::underscore(Inflector::pluralize($basicModel));
        if (empty($actions)) {
            $actions = array(
                $this->Html->link(__('edit', true), array('controller' => $controller, 'action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('delete', true), array('controller' => $controller, 'action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure', true)),
            );
        }
        $out .= '<table width="100%" id="Table" class="listing-table" cellpadding="0" cellspacing="0">';
        $out.= '<tr class="table-header">';

        if (!empty($fields['basicModel'])) {
            $basicModel = $fields['basicModel'];
            unset($fields['basicModel']);
        }
        foreach ($fields as $name => $spec) {
            $model = $basicModel;
            $t = $name;
            if (strpos($name, '.')) {
                $a = explode('.', $name);
                $model = $a[0];
                $t = $a[1];
            }
            $title = empty($spec['title']) ? Inflector::humanize($t) : $spec['title'];
            $out.="<td>" . $title . "</td>";
        }
        if (is_array($actions) && !empty($actions)) {
            $out .= '<td class="actions">' . __('Actions', true) . '</td>';
        }
        $out .= "</tr>";

        foreach ($data as $row) {
            $cells = array();
            foreach ($fields as $name => $spec) {
                if (strpos($name, '.')) {
                    $a = explode('.', $name);

                    $model = $a[0];
                    $t = $a[1];

                    $cell = $row[$model][$t];
                    if (isset($spec['format']) && $spec['format'] == 'bool')
                        if ($cell)
                            $cell = __("Yes", true); else
                            $cell = __("No", true);
                }
                if (!empty($spec['edit_link'])) {

                    preg_match('/%(.+)%/', Router::url($spec['edit_link']), $matches);

                    $spec['edit_link'] = str_ireplace("%{$matches[1]}%", $row[$model][$matches[1]], $spec['edit_link']);

                    $cell = $this->Html->link($cell, $spec['edit_link']);
                }
                $cells[] = $cell;
            }
            $cell = '';
            if (is_array($actions) && !empty($actions)) {
                foreach ($actions as $action) {
                    preg_match('/%(.+)%/', $action, $matches);
                    $cell .= '<li>' . str_replace("%{$matches[1]}%", $row[$basicModel][$matches[1]], $action) . '</li>';
                }

                $cells[] = '<ul class="action">' . $cell . '</ul>';
            }
            $out .= $this->Html->tableCells($cells);
        }
        $out .= '</table>';

        return $out;
    }

    function paging($prev = true, $next = true) {
        $paging = '<ul>';
        if ($prev && $this->Paginator->hasPrev()) {
            $paging .= $this->Html->tag('li', $this->Paginator->prev('<<' . __('prev', true), array('class' => 'Prev'), null, array('class' => 'disabled')));
        }
        $paging .= $this->Paginator->numbers(array('tag' => 'li', 'separator' => ''));
        if ($next && $this->Paginator->hasNext()) {
            $paging .= $this->Html->tag('li', $this->Paginator->next(__('next', true) . '>>', array('class' => 'Next'), null, array('class' => 'disabled')));
        }
        $paging .= '</ul>';
        return $this->Html->div('paging', $paging) . $this->Html->div('clear', '');
    }

    function filter_form($model, $filters, $form_options = array(), $extra = array()) {
        if (empty($filters))
            return false;
        $url_params = $this->params['url'];
        unset($url_params['url'], $url_params['page'], $url_params['sort'], $url_params['direction']);

        $session_key = "{$model}_Filter";
        $lastFilter = $this->Session->read($session_key);
        if ($lastFilter && empty($url_params)) {
            $url_params = $lastFilter;
        }

        $display = 'none';
        if (!empty($url_params))
            foreach ($url_params as $variable) {
                if (!empty($variable) || (isset($variable) && strlen($variable))) {
                    $display = 'block';
                }
            }


        $hasDate = false;
        $defaults = array('action' => 'index', 'type' => 'get', 'id' => 'filterform', 'style' => "display:$display");
        $extra_defaults = array('input_class' => 'INPUT', 'submit_class' => 'Submit', 'div_class' => 'FormExtended', 'div_id' => 'filter', 'toggle_class' => 'Filter_Me filter-ico');
        $extra = array_merge($extra_defaults, $extra);
        $prefix = (empty($this->params['prefix'])) ? false : $this->params['prefix'];
        if ($prefix)
            $defaults[$prefix] = true;
        $form_options = array_merge($defaults, $form_options);
        $output = $this->Html->link(__('Show/Hide Filters', true), "javascript:$('#{$form_options['id']}').slideToggle('fast');void(0);", array('class' => $extra['toggle_class']));

        $output .= $this->Form->create($model, $form_options);
        foreach ($filters as $field => $filter) {
            if (is_numeric($field) || (!empty($filter['type']) && $filter['type'] == 'select')) {
                $div_id = 'Div' . $model . Inflector::slug($filter);
                $value = empty($url_params[$filter]) && !(isset($url_params[$filter]) && strlen($url_params[$filter])) ? '' : strval($url_params[$filter]);
                $output .= $this->Form->input($filter, array('class' => $extra['input_class'], 'value' => $value, 'selected' => $value, 'empty' => __('[Any ' . Inflector::humanize($filter) . ']', true), 'div' => array('id' => $div_id)));
            } elseif (is_string($filter)) {
                $div_id = 'Div' . $model . Inflector::slug($field);
                $value = empty($url_params[$field]) && !(isset($url_params[$field]) && strval($url_params[$field]) === '0') ? '' : strval($url_params[$field]);

                $output .= $this->Form->input($field, array('class' => $extra['input_class'], 'value' => $value, 'selected' => $value, 'empty' => __('[Any ' . Inflector::humanize($field) . ']', true), 'div' => array('id' => $div_id)));
            } else {
                if (!empty($filter['type']) && $filter['type'] == 'number_range') {
                    $from_div_id = "Div{$model}{$field}From";
                    $to_div_id = "Div$model{$field}To";
                    $from = empty($filter['from']) ? $field . ' from' : $filter['from'];
                    $to = empty($filter['to']) ? $field . ' to' : $filter['to'];
                    $from_value = empty($url_params[$field . '_from']) && !(isset($url_params[$from]) && strval($url_params[$field . '_from']) === '0') ? '' : strval($url_params[$field . '_from']);
                    $to_value = empty($url_params[$field . '_to']) && !(isset($url_params[$to]) && strval($url_params[$field . '_to']) === '0') ? '' : strval($url_params[$field . '_to']);
                    $output .= $this->Form->input($field . '_from', array('label' => $from, 'class' => $extra['input_class'], 'value' => $from_value, 'selected' => $from_value, 'div' => array('id' => $from_div_id)));
                    $output .=$this->Form->input($field . '_to', array('label' => $to, 'class' => $extra['input_class'], 'value' => $to_value, 'selected' => $to_value, 'div' => array('id' => $to_div_id)));
                } elseif (!empty($filter['type']) && strtolower($filter['type']) == 'date_range') {
                    $from_div_id = "{$model}{$field}From";
                    $to_div_id = "{$model}{$field}To";
                    $from = empty($filter['from']) ? $field . ' from' : $filter['from'];
                    $to = empty($filter['to']) ? $field . ' to' : $filter['to'];
                    $from_value = empty($url_params[$field . '_from']) ? '' : $url_params[$field . '_from'];
                    $to_value = empty($url_params[$field . '_to']) ? '' : $url_params[$field . '_to'];
                    $output .= $this->Form->input($field . '_from', array('label' => $from, 'class' => $extra['input_class'] . ' hasDate', 'id' => "{$field}From", 'value' => $from_value, 'selected' => $from_value, 'div' => array('id' => $from_div_id)));
                    $output .= $this->Form->input($field . '_to', array('label' => $to, 'class' => $extra['input_class'] . ' hasDate', 'id' => "{$field}To", 'value' => $to_value, 'selected' => $to_value, 'div' => array('id' => $to_div_id)));
                    $hasDate = true;
                } else {
                    $value = empty($url_params[$field]) && !(isset($url_params[$field]) && strval($url_params[$field]) === '0') ? '' : strval($url_params[$field]);
                    $label = empty($filter['title']) ? '' : $filter['title'];


                    $output .= $this->Form->input($field, array('label' => $label, 'class' => $extra['input_class'], 'value' => $value, 'selected' => $value, 'empty' => __('[Any ' . Inflector::humanize($field) . ']', true)));
                }
            }
        }
        $output .= $this->Html->div('FilterAction filter-action', $this->Form->submit(__('Filter', true), array('class' => 'btn btn-default btn-xs', 'div' => false)) . $this->Form->input(__('Clear', true), array('label' => false, 'div'=>false, 'value' => 'Clear', 'class' => 'btn btn-shape btn-xs', 'id' => 'FilterClear', 'type' => 'reset',)));
        $output .= $this->Form->end();

        $output .= $this->Html->div('clear', '');
        $clearUrl = Router::url(array('action' => 'reset_filter', 'admin' => false, 'prefix' => false));

        $script = <<<CODEBLOCK
$('#FilterClear').click(function(){ $.ajax({url : '$clearUrl', success: function(data, status){window.location = "$this->here"; }});});
CODEBLOCK;
        if ($hasDate) {
            $output .= $this->Javascript->link('jquery.datepick');
            $this->Html->css('jquery.datepick', false, false, false);
            $script .= '$(".hasDate").datepick({dateFormat: "dd-mm-yy"});';
        }
        $output .= $this->Html->scriptBlock('$(function(){' . $script . '});');
        return $this->Html->div($extra['div_class'], $output, array('id' => $extra['div_id']));
    }

    function export_csv($ModelName, $items, $schema, $filename = '') {
        Configure::write('debug', 0);
        $name = $filename;
        if (empty($name)) {
            $name = Inflector::pluralize($ModelName) . '_' . date('YmdHi') . '.csv';
        }
        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=' . $name);
        
        $separator = !empty($_POST['separator']) ? $_POST['separator'] : CSV_SEPARATOR;
        $output = implode(array_map('quote', $schema), $separator ) . "\n";
        
        foreach ($items as $item) {
            $line = array();
            if (isset($item[$ModelName]['active'])) {
                $item[$ModelName]['active'] = empty($item[$ModelName]['active']) ? 'NO' : 'Yes';
            }
            foreach ($item[$ModelName] as $field) {
                $line[] = quote_csv($field);
            }
            $output .= implode($separator, $line) . "\n";
        }
        $this->layout = '';
        return $output;
    }

}
