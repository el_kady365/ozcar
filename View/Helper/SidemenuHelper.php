<?php

class SidemenuHelper extends AppHelper {

    public $helpers = array('Html');

    function outputAdminMenu($selected = null) {

        $adminMenu = array(
            __('Configurations', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x2', 'title' => __('Edit Configurations', true), 'url' => array('controller' => 'site_configs', 'action' => 'edit', 'plugin' => false))
                )
                ,'icon' => 'ico-10',
            )
            ,
            __('Cars Configurations', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x1'),
                    array('container_class' => 'item-x2', 'title' => __('Cars Directory', true), 'url' => array('controller' => 'directory_cars', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Add to directory', true), 'url' => array('controller' => 'directory_cars', 'action' => 'add', 'plugin' => false)),
                )
                ,'icon' => 'ico-10',
            ),
            __('Cars', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x1'),
                    array('container_class' => 'item-x1', 'title' => __('List Cars', true), 'url' => array('controller' => 'cars', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Add Car', true), 'url' => array('controller' => 'cars', 'action' => 'add', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Import Cars', true), 'url' => array('controller' => 'cars', 'action' => 'importing_data', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __(' Importing File Templates', true), 'url' => array('controller' => 'templates', 'action' => 'index', 'plugin' => false)),
                )
                ,'icon' => 'ico-11',
            ),
            __('Specials', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    
                    array('container_class' => 'item-x1', 'title' => __('Manage Page', true), 'url' => array('controller' => 'pages', 'action' => 'edit',16, 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Three Image Texts', true), 'url' => array('controller' => 'three_image_texts', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Add', true), 'url' => array('controller' => 'three_image_texts', 'action' => 'add', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Full Width Banners', true), 'url' => array('controller' => 'full_width_banners', 'action' => 'index', 'plugin' => false)),                    
                    array('container_class' => 'item-x1', 'title' => __('Add', true), 'url' => array('controller' => 'full_width_banners', 'action' => 'add', 'plugin' => false)),                    
                    
                )
                ,'icon' => 'ico-03',
            ),
            __('Dealers', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x1', 'title' => __('List Dealers', true), 'url' => array('controller' => 'dealers', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Add Dealer', true), 'url' => array('controller' => 'dealers', 'action' => 'add', 'plugin' => false))
                )
                ,'icon' => 'ico-04',
            ),
            __('Ftp Importing Accounts', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x1'),
                    array('container_class' => 'item-x2', 'title' => __('List Ftp Importing Accounts', true), 'url' => array('controller' => 'ftp_imported_crons', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Add New Account', true), 'url' => array('controller' => 'ftp_imported_crons', 'action' => 'add', 'plugin' => false))
                )
                ,'icon' => 'ico-08',
            ),
            __('Enquiries', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x2'),
                    array('container_class' => 'item-x1', 'title' => __('List Enquiries', true), 'url' => array('controller' => 'enquiries', 'action' => 'index', 'plugin' => false)),
                )
                ,'icon' => 'ico-06',
            ),
            __('Pages', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x1', 'title' => __('List Pages', true), 'url' => array('controller' => 'pages', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Add Page', true), 'url' => array('controller' => 'pages', 'action' => 'add', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('List Snippets', true), 'url' => array('controller' => 'snippets', 'action' => 'index', 'plugin' => false)),
                )
                ,'icon' => 'ico-03',
            ),
            __('Pages', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x1', 'title' => __('List Pages', true), 'url' => array('controller' => 'pages', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Add Page', true), 'url' => array('controller' => 'pages', 'action' => 'add', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('List Snippets', true), 'url' => array('controller' => 'snippets', 'action' => 'index', 'plugin' => false)),
                )
                ,'icon' => 'ico-03',
            ),
            __('SEO', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x1', 'title' => __('List SEO rules', true), 'url' => array('controller' => 'seos', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Add SEO rule', true), 'url' => array('controller' => 'seos', 'action' => 'add', 'plugin' => false))
                )
                ,'icon' => 'ico-07',
            ),
            __('Customers', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x2'),
                    array('container_class' => 'item-x2', 'title' => __('List Customers', true), 'url' => array('controller' => 'users', 'action' => 'index')),
                )
                ,'icon' => 'ico-02',
            ),
        );

        return $this->__outputMenu($adminMenu, $selected, 'dropdown-menu');
    }

    function outputDealerMenu($selected = null) {
        $dealerMenu = array(
            __('Dashboard', true) => array(
                'subs' => array(),
                'icon' => 'ico-01',
                'url' => array('action' => 'notfound'),
                'params' => array('escape' => false)
            ),
            __('Profile', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x1'),
                    array('container_class' => 'item-x2', 'title' => __('Setup Dealership', true), 'url' => array('controller' => 'dealers', 'action' => 'edit', $_SESSION["dealer"]["Dealer"]['id'], 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('My Account', true), 'url' => array('controller' => 'dealers', 'action' => 'view', $_SESSION["dealer"]["Dealer"]['id'], 'plugin' => false)),
                ),
                'icon' => 'ico-02',
                'url' => '#',
                'params' => array('escape' => false)
            ),
            __('Dealer site', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x2'),
                    array('container_class' => 'item-x1', 'title' => __('View Pages', true)),
                    array('container_class' => 'item-x1', 'title' => __('Add Page', true)),
                    array('container_class' => 'item-x1', 'title' => __('Configurations', true)),
                    array('container_class' => 'item-x1', 'title' => __('Live View', true)),
                    array('container_class' => 'item-x1', 'title' => __('Edit History', true)),
                    array('container_class' => 'item-x1', 'title' => __('Manual', true)),
                ),
                'icon' => 'ico-03',
                'url' => '#',
                'params' => array('escape' => false)
            ),
            __('Cars', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x1'),
                    array('container_class' => 'item-x2', 'title' => __('List Cars', true), 'url' => array('controller' => 'cars', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Add Car', true), 'url' => array('controller' => 'cars', 'action' => 'add', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Import Cars', true), 'url' => array('controller' => 'cars', 'action' => 'importing_data', 'plugin' => false)),
                    array('container_class' => 'item-x2', 'title' => __('Importing File Templates', true), 'url' => array('controller' => 'templates', 'action' => 'index', 'plugin' => false)),
                ),
                'icon' => 'ico-04',
                'url' => '#',
                'params' => array('escape' => false)
            ),
            __('News &<br />Specials', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x1'),
                    array('container_class' => 'item-x2', 'title' => __('News upload & Configurations', true)),
                    array('container_class' => 'item-x2', 'title' => __('Specials & Configurations', true)),
                ),
                'icon' => 'ico-05',
                'url' => array('action' => 'notfound'),
                'params' => array('escape' => false)
            ),
            __('Enquiries', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x1', 'title' => __('Enquiries', true), 'url' => array('controller' => 'enquiries', 'action' => 'index', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Inbox', true), 'url' => array('controller' => 'messages', 'action' => 'inbox', 'plugin' => false)),
                    array('container_class' => 'item-x1', 'title' => __('Sent', true), 'url' => array('controller' => 'messages', 'action' => 'sent', 'plugin' => false)),
                ),
                'icon' => 'ico-06',
                'url' => '#',
                'params' => array('escape' => false)
            ),
            __('EDM\'s', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x2'),
                    array('container_class' => 'item-x2'),
                    array('container_class' => 'item-x2', 'title' => __('EDM Advertising', true)),
                    array('container_class' => 'item-x2', 'title' => __('EDM Advertising Stats', true)),
                ),
                'icon' => 'ico-07',
                'url' => array('action' => 'notfound'),
                'params' => array('escape' => false)
            ),
            __('Help', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x2', 'title' => __('Search', true)),
                    array('container_class' => 'item-x2', 'title' => __('Videos', true)),
                ),
                'icon' => 'ico-08',
                'url' => array('action' => 'notfound'),
                'params' => array('escape' => false)
            ),
            __('Subscriptions', true) => array(
                'subs' => array(
                    array('container_class' => 'item-x3'),
                    array('container_class' => 'item-x2'),
                    array('container_class' => 'item-x2', 'title' => __('Upgrade Service', true)),
                    array('container_class' => 'item-x1', 'title' => __('Payment', true)),
                    array('container_class' => 'item-x1', 'title' => __('Invoices', true)),
                ),
                'icon' => 'ico-09',
                'url' => array('action' => 'notfound'),
                'params' => array('escape' => false)
            ),
        );

        return $this->__outputMenu($dealerMenu, $selected, 'dropdown-menu');
    }

    function outputSuperAdminMenu($selected = null) {

        $superAdminMenu = array(
            __('Sites', true) => array(
                'list' => array('title' => __('List Sites', true), 'url' => array('controller' => 'sites', 'action' => 'index')),
                'add' => array('title' => __('Add Site', true), 'url' => array('controller' => 'sites', 'action' => 'add')),
                'edit' => array('title' => __('Edit Current Site', true), 'url' => array('controller' => 'sites', 'action' => 'edit', SITE_ID)),
            ),
            __('Domain Names', true) => array(
                'list' => array('title' => __('List Linked Domains', true), 'url' => array('controller' => 'domains', 'action' => 'index')),
                'add' => array('title' => __('Add Domain', true), 'url' => array('controller' => 'domains', 'action' => 'add')),
            ),
        );


        return $this->__outputTopMenu($superAdminMenu, $selected);
    }

    function outputSubAdminMenu($selected) {
        $subAdminMenu = array(
        );
        return $this->__outputMenu($subAdminMenu);
    }

    function __outputMenu($menu, $selected) {
        $output = '<ul class="main-nav">';
        $selected = empty($selected) ? strtolower($this->params['controller']) : $selected;
        foreach ($menu as $title => $content) {
            $class = (strtolower($title) == strtolower($selected)) ? '%CLASS% active' : '';


            $params = !empty($content['params']) ? $content['params'] : array();
            $params['escape'] = false;
            $params['class'] = $class;
            $output .= '<li>';
            if (isset($content['icon'])) {
                $title = '<i class="dealer-ico ' . $content['icon'] . '"></i>' . $title;
            }
            if (!isset($content['url'])) {
                $url = '#';
            } else {
                $url = $content['url'];
            }
            $output .= $this->Html->link($title, $url, $params);

            if (!empty($content['subs'])) {
                $output .='<ul class="sub-nav">';

                foreach ($content['subs'] as $key => $item) {
                    $params = !empty($item['params']) ? $item['params'] : array();
                    $params['escape'] = false;
                    if (!empty($item['url']["controller"]) && $item['url']["controller"] == $this->params['controller'])
                        $class = 'active';

                    if (isset($item['url']) && isset($item['title'])) {
                        $link = $this->Html->link($item['title'], $item['url'], $params);
                    } elseif (isset($item['title'])) {
                        $link = $this->Html->link($item['title'], array('action' => 'notfound'), $params);
                    } else {
                        $link = '&nbsp;';
                    }
                    $output .= '<li class="' . $item['container_class'] . '">' . $link . "</li>";
                }

                $output = str_replace('%CLASS%', $class, $output);
                $output .= '</ul>';
            }

            $output .= '</li>';
        }
        if (!empty($_SESSION["dealer"])) {
            $output .= $this->_View->element('dealer_progress');
        }
        $output .= '</ul>';
        return $output;
    }

    function __outputTopMenu($menu, $selected) {
        $output = '';
        $selected = empty($selected) ? strtolower($this->params['controller']) : $selected;
        foreach ($menu as $title => $content) {
            $class = (strtolower($title) == strtolower($selected)) ? 'current' : 'menu_options';

            $output .= '<li>';
            $output .= $this->Html->link($title, 'javascript: void(0)', array('escape' => false, 'class' => '%CLASS% sub-nav'));
            $output .= "<ul class=\"%CLASS% dropdown-menu\">";

            foreach ($content as $item) {
                if ($item['url']["controller"] == $this->params['controller'])
                    $class = 'current';

                $output .= "<li>" . $this->Html->link($item['title'], $item['url'], array('escape' => false)) . "</li>";
            }

            $output = str_replace('%CLASS%', $class, $output);
            $output .= '</ul>';
            $output .= '</li>';
        }
        return $output;
    }

}
