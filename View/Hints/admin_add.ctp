<div class="hints form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'Hint' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		
                echo $this->Form->input('hints_category_id' , array('class' => 'input','empty'=>"Please select" ) );
                echo $this->Form->input('title' , array('class' => 'input' ) );
		echo $this->Form->input('description' , array('class' => 'input editor' ) );
		$field = 'image' ; 
		echo $this->Form->input('image' , array('class' => 'input' , 'type' => 'file' , 'between' => $this->element('image_input_between', array('info' => $file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data[$modelClass]['id']) ? $this->data[$modelClass]['id'] : null), 'base_name' => (!empty($this->request->data[$modelClass][$field]) ? $this->request->data[$modelClass][$field] : ''))) ) );
                
                echo $this->Form->input('date' , array('type'=>'text','class' => 'input hasDate' ) );
                echo $this->Form->input('no' , array('class' => 'input' , 'label'=>"# (number)" ) );
                
                echo $this->Form->input('url' , array('class' => 'input'  ) );
                echo $this->Form->input('new_window' , array('class' => 'input' , 'label'=>"Open in new window" ) );
                
                echo $this->Form->input('active' , array('class' => 'input' ) );

 		echo $this->Form->enableAjaxUploads();
		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
