<div class="hints index">
    <h2><?php echo __('Hints'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'Hint.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'HintsCategory.title' => array(),
        'Hint.title' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Hint.no' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Hint.date' => array('date_format' => 'd/m/Y'),
//		'Hint.description' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
        'Hint.active' => array('format' => 'bool', 'edit_link' => array('action' => 'edit', '%id%')),
//		'Hint.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Hint.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
    $links = array(
        $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), // , __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $hints, $links, true, $multi_select_actions);
    ?>
</div>
