<div class="hints view">
<h2><?php  echo __('Hint'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category Id'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['category_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($hint['Hint']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Hint'), array('action' => 'edit', $hint['Hint']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Hint'), array('action' => 'delete', $hint['Hint']['id']), null, __('Are you sure you want to delete # %s?', $hint['Hint']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Hints'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hint'), array('action' => 'add')); ?> </li>
	</ul>
</div>
