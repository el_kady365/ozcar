<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $title_for_layout; ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <?php
        echo $this->fetch('meta');
        ?>
        <?php
        echo $this->fetch('css');
        echo $this->Html->css(array('grid', 'core', 'icons', 'dealer', 'chosen', 'jquery.confirm', 'jquery.datepick','animate'));
        echo $this->Html->script(array('jquery.min','http://code.jquery.com/jquery-migrate-1.0.0.js', 'jquery.circliful.min', 'listswap', 'script', 'chosen.jquery.min', 'jquery.confirm', 'jquery.datepick','jquery.noty.packaged'));
        ?>
        <script type="text/javascript">
            var BASE_URL = '<?php echo Router::url('/'); ?>';
            var resizeIndex = '<?php echo empty($resizeIndex) ? "" : $resizeIndex; ?>';
        </script>
    </head>
    <body>



        <div class="dealer-header">
            <div class="container">
                <div class="col-md-7">
                    <?php if ($this->Session->check('dealer')) { ?>
                        <div class="logo pull-left"><a href="<?php echo Router::url('/admin') ?>"><img src="<?php echo $this->Session->read('dealer.Dealer.logo_full_path') ?>" alt="" title="" /></a></div>
                        <h3 class="slogan pull-left">Welcome <?php echo $this->Session->read('dealer.Dealer.name') ?></h3>
                    <?php } ?>
                </div>
                <div class="col-md-5 text-right">
                    <div class="oz-logo pull-right"><a href="<?php echo Router::url('/') ?>"><img src="<?php echo Router::url('/') ?>css/img/oz_car.png" width="90" alt="" title="" /></a></div>
                    <div class="login-as pull-right">Signed in as <a href="#" class="bold text-danger"><?php echo!empty($_SESSION['dealer']) ? $_SESSION['dealer']["Dealer"]["name"] : "Admin" ?></a>
                        <a class="underline text-danger" href="<?php echo Router::url(array('controller' => 'admins', 'action' => 'logout', 'admin' => true)); ?>">Log out</a></div>
                </div>
            </div>
        </div>
        <!-- / dealer header -->
        <div class="fluid-banner">
            <img src="<?php echo Router::url('/') ?>css/img/banners/dealer_01.jpg" alt="" title="" />
        </div>
        <!-- / dealer banner -->
        <div class="dealer-nav">
            <div class="container">
                <div class="col-md-12">
                    <?php
                    if (!empty($_SESSION["dealer"])) {
                        echo $this->Sidemenu->outputDealerMenu($selectedMenu);
                    } else {
                        echo $this->Sidemenu->outputAdminMenu($selectedMenu);
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- / dealer banner -->
        <div class="content">
            <div class="container">
                <div class="col-md-12">
                    <?php echo $this->Session->flash(); ?> <?php echo $this->fetch('content'); ?>
                    <?php if (strtolower($this->params['controller']) != 'domains') echo $this->element('content_after'); ?>
                </div>
            </div>
        </div>

        <?php // echo $this->element('dealer_footer'); ?>
        <div class="copyrights">
            <div class="container">
                <div class="col-md-6 col-sm-6">
                    <p>&copy; Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <p>
                        <a href="<?php echo Router::url('/cars/dealer_search') ?>">Find a Dealer</a>
                        <span>|</span>
                        <a href="<?php echo Router::url('/pages/static_page/privacy') ?>">Privacy</a>
                        <span>|</span>
                        <a href="<?php echo Router::url('/pages/static_page/terms') ?>">Terms</a>
                        <span>|</span>
                        <a href="<?php echo Router::url('/pages/static_page/site_map') ?>">Site Map</a> 
                    </p>
                </div>
                <div class="col-md-2 col-sm-2">
                    <div class="oz-logo pull-right">
                        <a href="<?php echo Router::url('/') ?>">
                            <img src="<?php echo Router::url('/') ?>css/img/oz_car.png" width="110" alt="" title="" /></a>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->element('sql_dump'); ?> 
        <?php echo $this->fetch('script'); ?>

    </body>
</html>
