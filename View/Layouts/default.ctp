<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Router::url('/css/img/favicon.ico') ?>">
            <?php
            if (!empty($metaRobots)) {
                echo $this->Html->meta('robots', $metaRobots);
            }
            if (!empty($metaKeywords)) {
                echo $this->Html->meta('keywords', $metaKeywords);
            }
            if (!empty($metaDescription)) {
                echo $this->Html->meta('description', $metaDescription);
            }
            ?>
            <script type="text/javascript">
                var BASE_URL = '<?php echo Router::url('/'); ?>';
            </script>
            <?php
            if (!empty($config['basic.google_analytics'])) {
                echo $config['basic.google_analytics'];
            }

            echo $this->Html->meta('icon');
            echo $this->Html->css(array('grid', 'core', 'icons', 'customer', 'animate'));
            echo $this->Html->script(array('http://code.jquery.com/jquery-migrate-1.0.0.js', 'jquery'));
            echo $this->fetch('meta');
            echo $this->fetch('css');
            ?>

    </head>
    <body>
        <?php echo $this->element('UserPopup'); ?>
        <div class="customer-header">
            <div class="customer-nav clearfix">
                <div class="container">
                    <div class="col-md-12">
                        <?php echo $this->element('top-menu'); ?>
                        <?php if (!empty($loggedUser['UserDetail'])) { ?>
                            <ul class="pull-right">
                                <li>
                                    <a href="<?php echo Router::url('/users/dashboard'); ?>">
                                        <span class="user-ico"></span>
                                        <i class="fa fa-angle-down"></i>
                                        G'day <strong><?php echo $loggedUser['UserDetail']['first_name'] . ' ' . $loggedUser['UserDetail']['last_name'] ?></strong></a>
                                </li>
                            </ul>
                        <?php } else { ?>
                            <ul class="pull-right">
                                <li>
                                    <a href="<?php echo Router::url('/users/login'); ?>">
                                        <span class="user-ico"></span>

                                        <strong> User Login</strong></a>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- /customer-header  -->
            <div class="header-wrap">
                <div class="container">
                    <div class="col-md-4"><a class="logo" href="/"><img src="<?php echo Router::url('/css/img/oz_car.png') ?>" alt="" title=""/></a></div>
                    <div class="col-md-8"><a class="top-banner" href="#"><img src="<?php echo Router::url('/css/img/banners/leader_banner.jpg') ?>" alt="" title=""/></a></div>
                </div>
            </div>
        </div>

        <?php
        echo $this->Session->flash();
        echo $this->fetch('content');
        ?>
        <!-- / home -->


        <?php // echo $this->element('footer');?>
        <div class="copyrights">
            <div class="container">
                <div class="col-md-6 col-sm-6">
                    <div class="bottom-social"> 
                        <a href="<?php echo $config['links.facebook'] ?>"><i class="fa fa-facebook-official"></i></a>
                        <a href="<?php echo $config['links.twitter'] ?>"><i class="fa fa-twitter"></i></a>
                        <a href="<?php echo $config['links.youtube'] ?>"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 text-right"> 
                    <p> 
                        <a href="<?php echo Router::url('/cars/dealer_search') ?>">Find a Dealer</a>
                        <span>|</span>
                        <a href="<?php echo Router::url('/content/privacy') ?>">Privacy</a>
                        <span>|</span>
                        <a href="<?php echo Router::url('/content/terms') ?>">Terms</a>
                        <span>|</span>
                        <a href="<?php echo Router::url('/content/site-map') ?>">Site Map</a>
                    </p>
                </div>
                <div class="col-md-2 col-sm-2">
                    <div class="oz-logo pull-right">
                        <a href="#"><img src="<?php echo Router::url('/css/img/oz_car.png') ?>" width="110" alt="" title="" /></a></div>
                </div>
            </div>
        </div>

        <?php
        echo $this->element('sql_dump');
        echo $this->Html->script(array('jquery.noty.packaged', 'jquery.circliful.min', 'remodal', 'script'));
        echo $this->fetch('script');
        ?>
        <script>
            $('.customer-panel ul li a').each(function () {
                if ($(this).attr('href') == '<?php echo Router::url() ?>') {

                    $(this).addClass('active');
                }
            });
        </script>
    </body>
</html>