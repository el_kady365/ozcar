<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        if (!empty($metaRobots)) {
            echo $this->Html->meta('robots', $metaRobots);
        }
        if (!empty($metaKeywords)) {
            echo $this->Html->meta('keywords', $metaKeywords);
        }
        if (!empty($metaDescription)) {
            echo $this->Html->meta('description', $metaDescription);
        }
        ?>

        <script type="text/javascript">
            var BASE_URL = '<?php echo Router::url('/'); ?>'; 
            
        </script>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css(array('http://fonts.googleapis.com/css?family=Oswald', 'screen', 'slider', 'notifications'));
        echo $this->Html->script(array('jquery-1.8.3.min', 'flexslider-min', 'jquery-easing', 'nav', 'scroll-top', 'eye', 'utils'));

        echo $this->fetch('meta');
        echo $this->fetch('css');


        if (!empty($color_bar_active)) {
            echo $this->Html->css('fineuploader_logo', null, array('inline' => true));
            echo $this->Html->css('colorpicker', null, array('inline' => true));
        }

        echo $this->element('theme_style');
        ?>

    </head>
    <body <?php echo!empty($color_bar_active) ? ' class="option-bar-active" ' : ''; ?> >
        <?php
        if (!empty($color_bar_active)) {
            echo $this->element('color_bar');
        }
        ?>
        <div class="wrap">
            <div class="header">
                <?php if (!empty($logo)): ?>
                    <div class="logo left">
                        <div class="current-logo">
                            <?php if (!empty($color_bar_active)) : ?>
                                <div id="uploadLogo" class="edit-ico"></div>
                            <?php endif; ?>

                            <a href="<?php echo Router::url('/'); ?>"><img src="<?php echo $logo; ?>" alt="<?php $config['basic.site_name']; ?>" /></a>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="social right">                    
                    <?php if (!empty($config['links.facebook'])) : ?><a href="<?php echo $config['links.facebook']; ?>"><img src="<?php echo Router::url('/css/img/share/facebook-icon.png'); ?>" alt="Facebook" /></a><?php endif; ?>
                    <?php if (!empty($config['links.twitter'])) : ?><a href="<?php echo $config['links.twitter']; ?>"><img src="<?php echo Router::url('/css/img/share/twitter-icon.png'); ?>" alt="twitter" /></a><?php endif; ?>
                    <?php if (!empty($config['links.linked_in'])) : ?><a href="<?php echo $config['links.linked_in']; ?>"><img src="<?php echo Router::url('/css/img/share/linkedin-icon.png'); ?>" alt="linked-in" /></a><?php endif; ?>
                    <?php if (!empty($config['links.mailto'])) : ?><a href="mailto:<?php echo $config['links.mailto']; ?>"><img src="<?php echo Router::url('/css/img/share/email-icon.png'); ?>" alt="email" /></a><?php endif; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- / header -->
        <div class="nav bg-a">
            <div class="wrap">
                <?php echo $this->element('top-menu'); ?>
            </div>
        </div>
        <!-- / nav -->
        <?php
        if ($this->params['controller'] == 'pages' && $this->params['action'] == 'home') :
            if (!empty($home_banners)) :
                ?>
                <div class="banners flexslider">
                    <ul class="slides">
                        <?php foreach ($home_banners as $banner) : $banner = $banner['TopBanner'] ?>
                            <li><a href="<?php echo $banner['url']; ?>"  <?php echo!empty($banner['new_window']) ? ' target="_blank" ' : ''; ?> ><img src="<?php echo $banner['image_full_path']; ?>" alt="<?php echo $banner['title']; ?>"  title="<?php echo $banner['title']; ?>" /></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!-- / banners -->
            <?php endif; ?>
        <?php endif; ?>

        <div class="content wrap">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
            <?php if (strtolower($this->params['controller']) != 'domains') echo $this->element('content_after'); ?>
        </div>

        <div class="footer bg-a">
            <div class="wrap"><p> &copy; <?php echo date('Y') . ' ' . (!empty($config['basic.site_name']) ? $config['basic.site_name'] : '' ); ?> </p></div>
        </div>
        <!-- / footer -->
        <div id="back-top" class="back-top"><a href="#top"><span></span>Top</a></div>

        <?php echo $this->element('sql_dump'); ?>
        <?php echo $this->fetch('script'); ?> 
        <?php
        if (!empty($color_bar_active)) {
            echo $this->Html->script('fuploaderjq.libs.min', array('inline' => true));
            echo $this->Html->script('colorpicker', array('inline' => true));
            echo $this->Html->script('customize_colors', array('inline' => true));
        }
        ?>
        <?php if ($this->params['controller'] == 'pages' && $this->params['action'] == 'home') : ?>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('.flexslider').flexslider({animation: "<?php echo!empty($config['style.slider_effect']) ? $config['style.slider_effect'] : 'slide'; ?>", easing: "swing", directionNav: false});
                });
            </script>
        <?php endif; ?>
    </body>
</html>
