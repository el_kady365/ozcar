<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

		<?php echo $this->Html->charset(); ?>
        <title>
			<?php echo $title_for_layout; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php
		if (!empty($metaRobots)) {
			echo $this->Html->meta('robots', $metaRobots);
		}
		if (!empty($metaKeywords)) {
			echo $this->Html->meta('keywords', $metaKeywords);
		}
		if (!empty($metaDescription)) {
			echo $this->Html->meta('description', $metaDescription);
		}
		?>
        <script type="text/javascript">
			var BASE_URL = '<?php echo Router::url('/'); ?>';
        </script>
		<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('screen'));
 		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>
     </head>
    <body>
         <!-- / header-links -->
	<div class="wrap error-page">
<div class="logo"><a href="<?php echo Router::url('/'); ?>" title="<?php echo $config['basic.site_name'] ?>">
<!--        <img src="<?php echo $this->webroot ?>css/img/transparent_seo.png" alt="<?php echo $config['basic.site_name'] ?>" title="<?php echo $config['basic.site_name'] ?>" />-->
    </a></div>    
		<?php
		echo $this->fetch('content');
		?>
        
     </div>   
        <!-- / home -->


    </body>
</html>