<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        if (!empty($metaRobots)) {
            echo $this->Html->meta('robots', $metaRobots);
        }
        if (!empty($metaKeywords)) {
            echo $this->Html->meta('keywords', $metaKeywords);
        }
        if (!empty($metaDescription)) {
            echo $this->Html->meta('description', $metaDescription);
        }
        ?>
        <script type="text/javascript">
            var BASE_URL = '<?php echo Router::url('/'); ?>';

        </script>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('font', 'screen'));
        echo $this->Html->script(array('jquery-1.8.3.min', 'dropdown'));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        ?>
        <script type="text/javascript">
            $(function() {
                $('.nav-box').hide();
                $(".mainNavItem").each(function() {
                    var $left = Math.floor(($(this).width()) / 2);
                    $(this).parent().prevAll('li').each(function() {
                        $left += $(this).find('.mainNavItem').width();
                        $left += 14;
                    });
                    $(this).siblings('.nav-box').find('.nav-arrow').css({left: $left});

                });
                $(document).on('click', ".mainNavItem", function() {
                    $('.nav-box').removeClass('current');
                    var $currentOBJ = $(this).siblings('.nav-box');
                    $currentOBJ.addClass('current')
                    $('.nav-box').not(".current").hide();
                    $currentOBJ.slideToggle(350);
                    return false;
                });
                //                $(document).on('click', ".mainLI", function() {
                //                    $(this).find('.nav-box').slideToggle(350);
                //
                //                });
                //                $(document).on('mouseenter', ".mainLI", function() {
                //                    $(this).find('.nav-box').slideToggle(1000);
                //
                //                });
                //                $(document).on('mouseleave', ".mainLI", function() {
                //                    $(this).find('.nav-box').slideToggle(1000);
                //                });
            });
        </script>

    </head>
    <body>
        <div class="header">
            <div class="wrap">
                <div class="slogan"><a href="<?php echo Router::url('/'); ?>"><img src="<?php echo Router::url('/css/img/bright-duggan.png'); ?>" alt="" title="" /></a></div>
                <div class="links-nav">
                    <?php echo $headerTopnav; ?>
                </div>
                <div class="nav">

                    <ul>
                        <?php foreach ($MenuItems as $tmenu) { ?>
                            <li class="mainLI"><a class="mainNavItem" title="<?php echo $tmenu["TopMenu"]['title']; ?>" href="<?php echo Router::url('/sitecontent/' . $tmenu["TopMenu"]['permalink']); ?>"><span class="museo"><?php echo $tmenu["TopMenu"]['title']; ?></span></a>
                                <?php if (!empty($tmenu['MenuPages'])) { ?>
                                    <div class="nav-box" style="display: none;">
                                        <div class="nav-arrow"><span></span></div>
                                        <?php foreach ($tmenu['MenuPages'] as $menupage) { ?>
                                            <ul>        <li class="active"><a title="<?php echo $menupage['Page']['title']; ?>" href="<?php echo pageURL($menupage['Page']); ?>"><?php echo $menupage['Page']['title']; ?></a></li>
                                                <?php
                                                $submenus = empty($menupage['SubPage']) ? array() : $menupage['SubPage'];
                                                foreach ($submenus as $submenupage) {
                                                    ?>
                                                    <li>
                                                        <a title="<?php echo $submenupage['title']; ?>" href="<?php echo pageURL($submenupage); ?>"><?php echo $submenupage['title']; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="logo"><a href="<?php echo Router::url('/'); ?>" title="<?php echo $config['basic.site_name'] ?>"><img src="<?php echo Router::url('/css/img/bright-duggan-logo.png'); ?>" alt=""  /></a></div>
            </div>
        </div>
        <div class="main">
            <?php
            $pageHeading = empty($pageHeading) ? "TEST" : $pageHeading;
            if (!empty($pageHeading) && $this->action != 'home') {
                ?>
                <div class="heading">
                    <div class="wrap">

                        <h1 class="museo left" ><?php echo $pageHeading; ?></h1>
                        <?php echo $this->element('portal/searchbox', array('moreClass' => 'right')); ?>

                        <div class="clear"></div>
                    </div>
                </div>
            <?php } ?>
            <?php
            if ($this->action == 'home') {
                echo $this->element('portal/home');
            }
            ?>
            <div class="pages wrap">
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
                <div class="clear"></div>
            </div>
        </div>
        <?php echo $this->element('portal/footer'); ?>

        <?php echo $this->element('sql_dump'); ?>
        <?php echo $this->fetch('script'); ?> 
    </body>
</html>