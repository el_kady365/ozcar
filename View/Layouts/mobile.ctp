<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title><?php echo $title_for_layout; ?></title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        
        <?php
        echo $this->Html->script(array('jquery-1.8.3.min'));
        echo $this->Html->css(array('mobile/mobile', 'mobile/font', 'notifications'));
        echo $this->Html->script(array('mobile/flexslider'));
        echo $this->fetch('css');
        ?>
        <script type="text/javascript">
            $(window).load(function() {
                $(".nav-ico").click(function() {
                    $("#homenav").toggle("slow");
                    $(".nav-ico ").toggleClass("in-active");
                });
            });
        </script>
    </head>
    <body>

        <div class="mb-header">
            <div class="mb-logo"><a href="<?php echo Router::url('/'); ?>"><img src="<?php echo Router::url('/css/mobile/img/bright-duggan-logo.png'); ?>" height="55" alt="" title="" /></a></div>
            <div class="mb-nav">
                <?php if ($this->action == 'home') { ?>
                    <div class="mb-slogan" ><a href="<?php echo Router::url('/'); ?>"><img src="<?php echo Router::url('/css/mobile/img/bright-duggan.png'); ?>" height="60" alt="" title="" /></a></div>
                <?php } else { ?>
                    <a href="<?php echo $mobileBackLink; ?>" class="back-link"><span class="back-arrow"></span></a> <a href="#" class="nav-ico"><span>&#9776;</span></a>
                <?php } ?>
                <div class="clear"></div>
                <?php
                if ($this->action != 'home') {
                    echo $this->element('mobile/nav', array('menuPages' => $mobilePages, 'addMainLink'=>true,'navid'=>'homenav' ,'navstyle' => 'display:none;'));
                }
                ?>
            </div>
            <?php echo $this->fetch('content'); ?>
        </div>
        
        <?php echo $this->fetch('script'); ?> 
        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
