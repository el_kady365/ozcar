<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        if (!empty($metaRobots)) {
            echo $this->Html->meta('robots', $metaRobots);
        }
        if (!empty($metaKeywords)) {
            echo $this->Html->meta('keywords', $metaKeywords);
        }
        if (!empty($metaDescription)) {
            echo $this->Html->meta('description', $metaDescription);
        }
        ?>
        <style type="text/css">
            html {width:700px;}
        </style>
        <script type="text/javascript">
            var BASE_URL = '<?php echo Router::url('/'); ?>';
        </script>
        <script type="text/javascript" src="<?php echo WWW_ROOT . "/js/jquery-1.8.3.min.js"; ?>"></script>
        <script type="text/javascript" src="<?php echo WWW_ROOT . "/js/dropdown.js"; ?>"></script>

        <link rel="stylesheet" type="text/css" href="<?php echo WWW_ROOT . "/css/font.css"; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo WWW_ROOT . "/css/screen.css"; ?>" />

        <?php
        if (!empty($_GET['html'])) {
            echo $this->Html->css(array('font', 'screen', "flex"));
            echo $this->Html->script(array('jquery-1.8.3.min', 'dropdown', "jquery.flexslider"));
        }

        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        ?>
    </head>
    <body class="pdf-print">
        <div class="main">
            <div class="pages wrap">
                <div class="content">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <?php
        echo $this->fetch('script');
        ?>
    </body>
</html>