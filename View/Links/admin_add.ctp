<div class="links form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Link';
        echo $this->Form->create($modelClass);
        ?>
        <div class="input text">
            <label>Button Preview</label>
            <div class="side-box side-links"> 
                <a class="btn-succ" href="#">
                    <span class="link-icon icon-microphone"></span>
                    <span class="link-text"></span>
                </a> 
            </div>
        </div>
        <?php
        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('title', array('class' => 'input'));
        echo $this->Form->input('url', array('class' => 'input', 'value' => empty($this->request->data['Link']['url']) ? 'http://' : $this->request->data['Link']['url']));
        ?>
        <div class="input text">
            <label>Button Icon</label>
            <div class="ico-select-wrap">
                <div class="ico-select">
                    <a class="dd-selected">Select your favorite icon</a> 
                    <span class="dd-pointer dd-pointer-down"></span> 
                </div>
                <ul class="dd-options dd-click-off-close">
                    <li><a href="#"><span class="icon-microphone"></span></a></li>
                    <li><a href="#"><span class="icon-checkmark"></span></a></li>
                    <li><a href="#"><span class="icon-pictures"></span></a></li>
                    <li><a href="#"><span class="icon-suitcase"></span></a></li>
                    <li><a href="#"><span class="icon-number"></span></a></li>
                    <li><a href="#"><span class="icon-number2"></span></a></li>
                    <li><a href="#"><span class="icon-number3"></span></a></li>
                    <li><a href="#"><span class="icon-number4"></span></a></li>
                    <li><a href="#"><span class="icon-number5"></span></a></li>
                    <li><a href="#"><span class="icon-number6"></span></a></li>
                    <li><a href="#"><span class="icon-calendar"></span></a></li>
                    <li><a href="#"><span class="icon-calendar2"></span></a></li>
                    <li><a href="#"><span class="icon-file"></span></a></li>
                    <li><a href="#"><span class="icon-file2"></span></a></li>
                    <li><a href="#"><span class="icon-file3"></span></a></li>
                    <li><a href="#"><span class="icon-camera"></span></a></li>
                    <li><a href="#"><span class="icon-film"></span></a></li>
                    <li><a href="#"><span class="icon-film2"></span></a></li>
                    <li><a href="#"><span class="icon-film3"></span></a></li>
                    <li><a href="#"><span class="icon-microphone2"></span></a></li>
                    <li><a href="#"><span class="icon-ice-cream"></span></a></li>
                    <li><a href="#"><span class="icon-cake"></span></a></li>
                    <li><a href="#"><span class="icon-inbox"></span></a></li>
                    <li><a href="#"><span class="icon-download"></span></a></li>
                    <li><a href="#"><span class="icon-upload"></span></a></li>
                    <li><a href="#"><span class="icon-inbox2"></span></a></li>
                    <li><a href="#"><span class="icon-notice"></span></a></li>
                    <li><a href="#"><span class="icon-notice2"></span></a></li>
                    <li><a href="#"><span class="icon-cogs"></span></a></li>
                    <li><a href="#"><span class="icon-cog"></span></a></li>
                    <li><a href="#"><span class="icon-warning"></span></a></li>
                    <li><a href="#"><span class="icon-marvin"></span></a></li>
                    <li><a href="#"><span class="icon-pacman"></span></a></li>
                    <li><a href="#"><span class="icon-cassette"></span></a></li>
                    <li><a href="#"><span class="icon-watch"></span></a></li>
                    <li><a href="#"><span class="icon-chronometer"></span></a></li>
                    <li><a href="#"><span class="icon-watch2"></span></a></li>
                    <li><a href="#"><span class="icon-position"></span></a></li>
                    <li><a href="#"><span class="icon-site-map"></span></a></li>
                    <li><a href="#"><span class="icon-site-map2"></span></a></li>
                    <li><a href="#"><span class="icon-cloud"></span></a></li>
                    <li><a href="#"><span class="icon-upload2"></span></a></li>
                    <li><a href="#"><span class="icon-chart"></span></a></li>
                    <li><a href="#"><span class="icon-gamepad"></span></a></li>
                    <li><a href="#"><span class="icon-alarm-cancel"></span></a></li>
                    <li><a href="#"><span class="icon-phone"></span></a></li>
                    <li><a href="#"><span class="icon-phone2"></span></a></li>
                    <li><a href="#"><span class="icon-lab"></span></a></li>
                    <li><a href="#"><span class="icon-tie"></span></a></li>
                    <li><a href="#"><span class="icon-football"></span></a></li>
                    <li><a href="#"><span class="icon-eight-ball"></span></a></li>
                    <li><a href="#"><span class="icon-bowling"></span></a></li>
                    <li><a href="#"><span class="icon-bowling-pin"></span></a></li>
                    <li><a href="#"><span class="icon-smiley"></span></a></li>
                    <li><a href="#"><span class="icon-sad"></span></a></li>
                    <li><a href="#"><span class="icon-mute"></span></a></li>
                    <li><a href="#"><span class="icon-hand"></span></a></li>
                    <li><a href="#"><span class="icon-radio"></span></a></li>
                    <li><a href="#"><span class="icon-satellite"></span></a></li>
                    <li><a href="#"><span class="icon-unlocked"></span></a></li>
                    <li><a href="#"><span class="icon-magnifier"></span></a></li>
                    <li><a href="#"><span class="icon-zoom-in"></span></a></li>
                    <li><a href="#"><span class="icon-zoom-out"></span></a></li>
                    <li><a href="#"><span class="icon-stack"></span></a></li>
                    <li><a href="#"><span class="icon-stack2"></span></a></li>
                    <li><a href="#"><span class="icon-lamp"></span></a></li>
                    <li><a href="#"><span class="icon-lamp2"></span></a></li>
                    <li><a href="#"><span class="icon-umbrella"></span></a></li>
                    <li><a href="#"><span class="icon-street-light"></span></a></li>
                    <li><a href="#"><span class="icon-bomb"></span></a></li>
                    <li><a href="#"><span class="icon-archive"></span></a></li>
                    <li><a href="#"><span class="icon-pil"></span></a></li>
                    <li><a href="#"><span class="icon-injection"></span></a></li>
                    <li><a href="#"><span class="icon-thermometer"></span></a></li>
                    <li><a href="#"><span class="icon-lamp3"></span></a></li>
                    <li><a href="#"><span class="icon-lamp4"></span></a></li>
                    <li><a href="#"><span class="icon-lamp5"></span></a></li>
                    <li><a href="#"><span class="icon-list"></span></a></li>
                    <li><a href="#"><span class="icon-list2"></span></a></li>
                    <li><a href="#"><span class="icon-ruler"></span></a></li>
                    <li><a href="#"><span class="icon-tools"></span></a></li>
                    <li><a href="#"><span class="icon-screwdriver"></span></a></li>
                    <li><a href="#"><span class="icon-volume"></span></a></li>
                    <li><a href="#"><span class="icon-volume2"></span></a></li>
                    <li><a href="#"><span class="icon-volume3"></span></a></li>
                    <li><a href="#"><span class="icon-equalizer"></span></a></li>
                    <li><a href="#"><span class="icon-resize"></span></a></li>
                    <li><a href="#"><span class="icon-resize2"></span></a></li>
                    <li><a href="#"><span class="icon-stats"></span></a></li>
                    <li><a href="#"><span class="icon-stats2"></span></a></li>
                    <li><a href="#"><span class="icon-hourglass"></span></a></li>
                    <li><a href="#"><span class="icon-abacus"></span></a></li>
                    <li><a href="#"><span class="icon-pencil"></span></a></li>
                    <li><a href="#"><span class="icon-profile"></span></a></li>
                    <li><a href="#"><span class="icon-rotate"></span></a></li>
                    <li><a href="#"><span class="icon-rotate2"></span></a></li>
                    <li><a href="#"><span class="icon-reply"></span></a></li>
                    <li><a href="#"><span class="icon-forward"></span></a></li>
                    <li><a href="#"><span class="icon-retweet"></span></a></li>
                    <li><a href="#"><span class="icon-coins"></span></a></li>
                    <li><a href="#"><span class="icon-pig"></span></a></li>
                    <li><a href="#"><span class="icon-bookmark"></span></a></li>
                    <li><a href="#"><span class="icon-bookmark2"></span></a></li>
                    <li><a href="#"><span class="icon-address-book"></span></a></li>
                    <li><a href="#"><span class="icon-aids"></span></a></li>
                    <li><a href="#"><span class="icon-info"></span></a></li>
                    <li><a href="#"><span class="icon-info2"></span></a></li>
                    <li><a href="#"><span class="icon-piano"></span></a></li>
                    <li><a href="#"><span class="icon-rain"></span></a></li>
                    <li><a href="#"><span class="icon-truck"></span></a></li>
                    <li><a href="#"><span class="icon-bus"></span></a></li>
                    <li><a href="#"><span class="icon-bike"></span></a></li>
                    <li><a href="#"><span class="icon-plane"></span></a></li>
                    <li><a href="#"><span class="icon-paper-plane"></span></a></li>
                    <li><a href="#"><span class="icon-rocket"></span></a></li>
                    <li><a href="#"><span class="icon-target"></span></a></li>
                    <li><a href="#"><span class="icon-badge"></span></a></li>
                    <li><a href="#"><span class="icon-badge2"></span></a></li>
                    <li><a href="#"><span class="icon-ticket"></span></a></li>
                    <li><a href="#"><span class="icon-ticket2"></span></a></li>
                    <li><a href="#"><span class="icon-ticket3"></span></a></li>
                    <li><a href="#"><span class="icon-checked"></span></a></li>
                    <li><a href="#"><span class="icon-error"></span></a></li>
                    <li><a href="#"><span class="icon-add"></span></a></li>
                    <li><a href="#"><span class="icon-minus"></span></a></li>
                    <li><a href="#"><span class="icon-alert"></span></a></li>
                    <li><a href="#"><span class="icon-zip"></span></a></li>
                    <li><a href="#"><span class="icon-anchor"></span></a></li>
                    <li><a href="#"><span class="icon-locked-heart"></span></a></li>
                    <li><a href="#"><span class="icon-magnet"></span></a></li>
                    <li><a href="#"><span class="icon-navigation"></span></a></li>
                    <li><a href="#"><span class="icon-number7"></span></a></li>
                    <li><a href="#"><span class="icon-number8"></span></a></li>
                    <li><a href="#"><span class="icon-number9"></span></a></li>
                    <li><a href="#"><span class="icon-number10"></span></a></li>
                    <li><a href="#"><span class="icon-number11"></span></a></li>
                    <li><a href="#"><span class="icon-number12"></span></a></li>
                    <li><a href="#"><span class="icon-number13"></span></a></li>
                    <li><a href="#"><span class="icon-number14"></span></a></li>
                    <li><a href="#"><span class="icon-number15"></span></a></li>
                    <li><a href="#"><span class="icon-number16"></span></a></li>
                    <li><a href="#"><span class="icon-number17"></span></a></li>
                    <li><a href="#"><span class="icon-number18"></span></a></li>
                    <li><a href="#"><span class="icon-number19"></span></a></li>
                    <li><a href="#"><span class="icon-number20"></span></a></li>
                    <li><a href="#"><span class="icon-quote"></span></a></li>
                    <li><a href="#"><span class="icon-link"></span></a></li>
                    <li><a href="#"><span class="icon-link2"></span></a></li>
                    <li><a href="#"><span class="icon-cabinet"></span></a></li>
                    <li><a href="#"><span class="icon-quote2"></span></a></li>
                    <li><a href="#"><span class="icon-tag"></span></a></li>
                    <li><a href="#"><span class="icon-tag2"></span></a></li>
                    <li><a href="#"><span class="icon-cabinet2"></span></a></li>
                    <li><a href="#"><span class="icon-phone3"></span></a></li>
                    <li><a href="#"><span class="icon-tablet"></span></a></li>
                    <li><a href="#"><span class="icon-window"></span></a></li>
                    <li><a href="#"><span class="icon-monitor"></span></a></li>
                    <li><a href="#"><span class="icon-ipod"></span></a></li>
                    <li><a href="#"><span class="icon-tv"></span></a></li>
                    <li><a href="#"><span class="icon-microphone3"></span></a></li>
                    <li><a href="#"><span class="icon-files"></span></a></li>
                    <li><a href="#"><span class="icon-drink"></span></a></li>
                    <li><a href="#"><span class="icon-drink2"></span></a></li>
                    <li><a href="#"><span class="icon-drink3"></span></a></li>
                    <li><a href="#"><span class="icon-drink4"></span></a></li>
                    <li><a href="#"><span class="icon-coffee"></span></a></li>
                    <li><a href="#"><span class="icon-mug"></span></a></li>
                    <li><a href="#"><span class="icon-cancel"></span></a></li>
                    <li><a href="#"><span class="icon-plus"></span></a></li>
                    <li><a href="#"><span class="icon-plus2"></span></a></li>
                    <li><a href="#"><span class="icon-minus2"></span></a></li>
                    <li><a href="#"><span class="icon-cancel2"></span></a></li>
                    <li><a href="#"><span class="icon-checkmark2"></span></a></li>
                    <li><a href="#"><span class="icon-calendar3"></span></a></li>
                    <li><a href="#"><span class="icon-camera2"></span></a></li>
                    <li><a href="#"><span class="icon-health"></span></a></li>
                    <li><a href="#"><span class="icon-suitcase2"></span></a></li>
                    <li><a href="#"><span class="icon-suitcase3"></span></a></li>
                    <li><a href="#"><span class="icon-picture"></span></a></li>
                    <li><a href="#"><span class="icon-pictures2"></span></a></li>
                    <li><a href="#"><span class="icon-android"></span></a></li>
                    <li><a href="#"><span class="icon-alarm-clock"></span></a></li>
                    <li><a href="#"><span class="icon-time"></span></a></li>
                    <li><a href="#"><span class="icon-time2"></span></a></li>
                    <li><a href="#"><span class="icon-headphones"></span></a></li>
                    <li><a href="#"><span class="icon-wallet"></span></a></li>
                    <li><a href="#"><span class="icon-checkmark3"></span></a></li>
                    <li><a href="#"><span class="icon-cancel3"></span></a></li>
                    <li><a href="#"><span class="icon-eye"></span></a></li>
                    <li><a href="#"><span class="icon-chart2"></span></a></li>
                    <li><a href="#"><span class="icon-chart3"></span></a></li>
                    <li><a href="#"><span class="icon-chart4"></span></a></li>
                    <li><a href="#"><span class="icon-chart5"></span></a></li>
                    <li><a href="#"><span class="icon-chart6"></span></a></li>
                    <li><a href="#"><span class="icon-location"></span></a></li>
                    <li><a href="#"><span class="icon-download2"></span></a></li>
                    <li><a href="#"><span class="icon-basket"></span></a></li>
                    <li><a href="#"><span class="icon-image"></span></a></li>
                    <li><a href="#"><span class="icon-open"></span></a></li>
                    <li><a href="#"><span class="icon-sale"></span></a></li>
                    <li><a href="#"><span class="icon-direction"></span></a></li>
                    <li><a href="#"><span class="icon-map"></span></a></li>
                    <li><a href="#"><span class="icon-trashcan"></span></a></li>
                    <li><a href="#"><span class="icon-vote"></span></a></li>
                    <li><a href="#"><span class="icon-graduate"></span></a></li>
                    <li><a href="#"><span class="icon-baseball"></span></a></li>
                    <li><a href="#"><span class="icon-soccer"></span></a></li>
                    <li><a href="#"><span class="icon-3d-glasses"></span></a></li>
                    <li><a href="#"><span class="icon-microwave"></span></a></li>
                    <li><a href="#"><span class="icon-refrigerator"></span></a></li>
                    <li><a href="#"><span class="icon-oven"></span></a></li>
                    <li><a href="#"><span class="icon-washing-machine"></span></a></li>
                    <li><a href="#"><span class="icon-mouse"></span></a></li>
                    <li><a href="#"><span class="icon-medal"></span></a></li>
                    <li><a href="#"><span class="icon-medal2"></span></a></li>
                    <li><a href="#"><span class="icon-switch"></span></a></li>
                    <li><a href="#"><span class="icon-key"></span></a></li>
                    <li><a href="#"><span class="icon-cord"></span></a></li>
                    <li><a href="#"><span class="icon-locked"></span></a></li>
                    <li><a href="#"><span class="icon-unlocked2"></span></a></li>
                    <li><a href="#"><span class="icon-locked2"></span></a></li>
                    <li><a href="#"><span class="icon-stack3"></span></a></li>
                    <li><a href="#"><span class="icon-skeletor"></span></a></li>
                    <li><a href="#"><span class="icon-battery"></span></a></li>
                    <li><a href="#"><span class="icon-battery2"></span></a></li>
                    <li><a href="#"><span class="icon-battery3"></span></a></li>
                    <li><a href="#"><span class="icon-battery4"></span></a></li>
                    <li><a href="#"><span class="icon-battery5"></span></a></li>
                    <li><a href="#"><span class="icon-megaphone"></span></a></li>
                    <li><a href="#"><span class="icon-megaphone2"></span></a></li>
                    <li><a href="#"><span class="icon-patch"></span></a></li>
                    <li><a href="#"><span class="icon-cube"></span></a></li>
                    <li><a href="#"><span class="icon-box"></span></a></li>
                    <li><a href="#"><span class="icon-box2"></span></a></li>
                    <li><a href="#"><span class="icon-diamond"></span></a></li>
                    <li><a href="#"><span class="icon-bag"></span></a></li>
                    <li><a href="#"><span class="icon-money-bag"></span></a></li>
                    <li><a href="#"><span class="icon-grid"></span></a></li>
                    <li><a href="#"><span class="icon-grid2"></span></a></li>
                    <li><a href="#"><span class="icon-paint"></span></a></li>
                    <li><a href="#"><span class="icon-hammer"></span></a></li>
                    <li><a href="#"><span class="icon-brush"></span></a></li>
                    <li><a href="#"><span class="icon-pen"></span></a></li>
                    <li><a href="#"><span class="icon-chat"></span></a></li>
                    <li><a href="#"><span class="icon-comments"></span></a></li>
                    <li><a href="#"><span class="icon-chat2"></span></a></li>
                    <li><a href="#"><span class="icon-chat3"></span></a></li>
                    <li><a href="#"><span class="icon-stretch"></span></a></li>
                    <li><a href="#"><span class="icon-narrow"></span></a></li>
                    <li><a href="#"><span class="icon-resize3"></span></a></li>
                    <li><a href="#"><span class="icon-download3"></span></a></li>
                    <li><a href="#"><span class="icon-calculator"></span></a></li>
                    <li><a href="#"><span class="icon-library"></span></a></li>
                    <li><a href="#"><span class="icon-auction"></span></a></li>
                    <li><a href="#"><span class="icon-justice"></span></a></li>
                    <li><a href="#"><span class="icon-pen2"></span></a></li>
                    <li><a href="#"><span class="icon-pin"></span></a></li>
                    <li><a href="#"><span class="icon-pin2"></span></a></li>
                    <li><a href="#"><span class="icon-discout"></span></a></li>
                    <li><a href="#"><span class="icon-edit"></span></a></li>
                    <li><a href="#"><span class="icon-scissors"></span></a></li>
                    <li><a href="#"><span class="icon-profile2"></span></a></li>
                    <li><a href="#"><span class="icon-shuffle"></span></a></li>
                    <li><a href="#"><span class="icon-loop"></span></a></li>
                    <li><a href="#"><span class="icon-crop"></span></a></li>
                    <li><a href="#"><span class="icon-square"></span></a></li>
                    <li><a href="#"><span class="icon-square2"></span></a></li>
                    <li><a href="#"><span class="icon-circle"></span></a></li>
                    <li><a href="#"><span class="icon-dollar"></span></a></li>
                    <li><a href="#"><span class="icon-dollar2"></span></a></li>
                    <li><a href="#"><span class="icon-safe"></span></a></li>
                    <li><a href="#"><span class="icon-envelope"></span></a></li>
                    <li><a href="#"><span class="icon-envelope2"></span></a></li>
                    <li><a href="#"><span class="icon-radio-active"></span></a></li>
                    <li><a href="#"><span class="icon-music"></span></a></li>
                    <li><a href="#"><span class="icon-presentation"></span></a></li>
                    <li><a href="#"><span class="icon-male"></span></a></li>
                    <li><a href="#"><span class="icon-female"></span></a></li>
                    <li><a href="#"><span class="icon-snow"></span></a></li>
                    <li><a href="#"><span class="icon-lightning"></span></a></li>
                    <li><a href="#"><span class="icon-sun"></span></a></li>
                    <li><a href="#"><span class="icon-moon"></span></a></li>
                    <li><a href="#"><span class="icon-cloudy"></span></a></li>
                    <li><a href="#"><span class="icon-cloudy2"></span></a></li>
                    <li><a href="#"><span class="icon-car"></span></a></li>
                    <li><a href="#"><span class="icon-bike2"></span></a></li>
                    <li><a href="#"><span class="icon-book"></span></a></li>
                    <li><a href="#"><span class="icon-book2"></span></a></li>
                    <li><a href="#"><span class="icon-barcode"></span></a></li>
                    <li><a href="#"><span class="icon-barcode2"></span></a></li>
                    <li><a href="#"><span class="icon-expand"></span></a></li>
                    <li><a href="#"><span class="icon-collapse"></span></a></li>
                    <li><a href="#"><span class="icon-pop-out"></span></a></li>
                    <li><a href="#"><span class="icon-pop-in"></span></a></li>
                    <li><a href="#"><span class="icon-microphone4"></span></a></li>
                    <li><a href="#"><span class="icon-cone"></span></a></li>
                    <li><a href="#"><span class="icon-blocked"></span></a></li>
                    <li><a href="#"><span class="icon-stop"></span></a></li>
                    <li><a href="#"><span class="icon-keyboard"></span></a></li>
                    <li><a href="#"><span class="icon-keyboard2"></span></a></li>
                    <li><a href="#"><span class="icon-radio2"></span></a></li>
                    <li><a href="#"><span class="icon-atom"></span></a></li>
                    <li><a href="#"><span class="icon-eyedropper"></span></a></li>
                    <li><a href="#"><span class="icon-globe"></span></a></li>
                    <li><a href="#"><span class="icon-globe2"></span></a></li>
                    <li><a href="#"><span class="icon-shipping"></span></a></li>
                    <li><a href="#"><span class="icon-ying-yang"></span></a></li>
                    <li><a href="#"><span class="icon-compass"></span></a></li>
                    <li><a href="#"><span class="icon-zip2"></span></a></li>
                    <li><a href="#"><span class="icon-usb"></span></a></li>
                    <li><a href="#"><span class="icon-clipboard"></span></a></li>
                    <li><a href="#"><span class="icon-clipboard2"></span></a></li>
                    <li><a href="#"><span class="icon-clipboard3"></span></a></li>
                    <li><a href="#"><span class="icon-switch2"></span></a></li>
                    <li><a href="#"><span class="icon-ruler2"></span></a></li>
                    <li><a href="#"><span class="icon-heart"></span></a></li>
                    <li><a href="#"><span class="icon-heart2"></span></a></li>
                    <li><a href="#"><span class="icon-tags"></span></a></li>
                    <li><a href="#"><span class="icon-pictures3"></span></a></li>
                    <li><a href="#"><span class="icon-printer"></span></a></li>
                    <li><a href="#"><span class="icon-profile3"></span></a></li>
                    <li><a href="#"><span class="icon-attachment"></span></a></li>
                    <li><a href="#"><span class="icon-alarm"></span></a></li>
                    <li><a href="#"><span class="icon-folder"></span></a></li>
                    <li><a href="#"><span class="icon-cog2"></span></a></li>
                    <li><a href="#"><span class="icon-minus3"></span></a></li>
                    <li><a href="#"><span class="icon-camera3"></span></a></li>
                    <li><a href="#"><span class="icon-address-book2"></span></a></li>
                    <li><a href="#"><span class="icon-heart3"></span></a></li>
                </ul>
            </div>
            <!-- / button icon --> 
        </div>
        <?php
       
        echo $this->Form->input('new_window', array('class' => 'input'));
        echo $this->Form->hidden('icon', array('class' => 'input'));
        echo $this->Form->input('display_order', array('class' => 'input'));
        echo $this->Form->input('active', array('class' => 'input'));
// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        set_text($('#LinkTitle').val());
        color = $('#LinkColor');
        $('.side-links a').css({
            background: color.val()
        });
        icon = $('#LinkIcon').val();
        if (icon !== '') {
            $('.link-icon').removeClass().addClass('link-icon ' + icon);
        }

        $('.dd-options').hide();
        $('#LinkTitle').keyup(function() {
            set_text($(this).val());
        });
        $('.ico-select').click(function() {
            if ($('.dd-options').is(':visible')) {
                $('.dd-options').fadeOut('slow');
            } else {
                $('.dd-options').fadeIn();
            }
            return false;
        });
		
        $('.dd-options li a').click(function() {
            $('.link-icon').removeClass().addClass('link-icon ' + $(this).find('span').attr('class'));
            $('#LinkIcon').val($(this).find('span').attr('class'));
            return false;
        });

    });

    $(document).mouseup(function(e)
    {
        var container = $(".dd-options");

        if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.fadeOut('slow');
        }
    });

    function set_text(text) {
        if (text != '') {
            $('.link-text').text(text);
        } else {
            $('.link-text').text('Lorem Ipsum Link');
        }
    }
</script>
<?php echo $this->Html->script('colorpicker', array('inline' => false)); ?>


