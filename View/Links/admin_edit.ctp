<div class="links form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'Link' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('title' , array('class' => 'input' ) );
		echo $this->Form->input('url' , array('class' => 'input' ) );
		echo $this->Form->input('color' , array('class' => 'input' ) );
		echo $this->Form->input('icon' , array('class' => 'input' ) );

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
