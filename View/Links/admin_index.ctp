<div class="links index">
    <h2><?php echo __('Header Links'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'Link.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Link.title' => array('edit_link' => array('action' => 'edit', '%id%')),
//		'Link.url' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Link.color' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Link.icon' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
    $Links = array(
        $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), // , __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $links, $Links, true, $multi_select_actions);
    ?>
</div>
