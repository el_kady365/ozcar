<div class="links index">
    <h2><?php echo __('Links'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo 'Id'; ?></th>
            <th><?php echo 'Title'; ?></th>
            <th><?php echo 'Url'; ?></th>
            <th><?php echo 'Pages Id'; ?></th>
        </tr>
        <?php foreach ($links as $link): ?>
            <tr>
                <td><?php echo h($link['Link']['id']); ?>&nbsp;</td>
                <td><?php echo h($link['Link']['title']); ?>&nbsp;</td>
                <td><?php echo h($link['Link']['url']); ?>&nbsp;</td>
                <td><?php echo h($link['Link']['pages_id']); ?>&nbsp;</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}')
        ));
        ?>	</p>

    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
