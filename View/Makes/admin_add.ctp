<div class="makes form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'Make' ; 
	echo $this->Form->create($modelClass ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('name' , array('class' => 'input' ) );
		echo $this->Form->input('active' , array('class' => 'input required' ) );

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
