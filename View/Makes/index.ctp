<div class="makes index">
	<h2><?php echo __('Makes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Name' ; ?></th>
	            <th><?php echo 'Active' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($makes as $make): ?>
	<tr>
		<td><?php echo h($make['Make']['id']); ?>&nbsp;</td>
		<td><?php echo h($make['Make']['name']); ?>&nbsp;</td>
		<td><?php echo h($make['Make']['active']); ?>&nbsp;</td>
		<td><?php echo h($make['Make']['created']); ?>&nbsp;</td>
		<td><?php echo h($make['Make']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
