<div class="mapFields form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'MapField';
        echo $this->Form->create($modelClass);

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('template_id', array('class' => 'input'  , "disabled" => "disabled"));
        echo $this->Form->input('csv_field_order', array('class' => 'input' ,"label" => "Csv Field Order (starting from 0)"));
        echo $this->Form->input('field', array('class' => 'input'));

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
