<div class="mapFields index">
    <a style="float:right" class="btn btn-success btn-sm" href="<?php echo $this->Html->url(array("action" => "add", $this->params->query["template_id"])) ?>">
        <span aria-hidden="true" class="st-ico st-icon-plus-2"></span>+ Add New Field
    </a>
    <h2><?php echo __('Map Fields'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'MapField.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'field' => array('php_expression' => '".Car::$fields[$row["MapField"]["field"]]."'),
        'MapField.csv_field_order' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Template.name' => array("title" => "Template") , 
//		'MapField.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'MapField.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
    $links = array(
        $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), // , __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $mapFields, $links, true, $multi_select_actions);
    ?>
</div>
