<div class="mapFields index">
	<h2><?php echo __('Map Fields'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
	            <th><?php echo 'Id' ; ?></th>
	            <th><?php echo 'Template Id' ; ?></th>
	            <th><?php echo 'Csv Field Order' ; ?></th>
	            <th><?php echo 'Field' ; ?></th>
	            <th><?php echo 'Created' ; ?></th>
	            <th><?php echo 'Modified' ; ?></th>
		</tr>
	<?php
	foreach ($mapFields as $mapField): ?>
	<tr>
		<td><?php echo h($mapField['MapField']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mapField['Template']['name'], array('controller' => 'templates', 'action' => 'view', $mapField['Template']['id'])); ?>
		</td>
		<td><?php echo h($mapField['MapField']['csv_field_order']); ?>&nbsp;</td>
		<td><?php echo h($mapField['MapField']['field']); ?>&nbsp;</td>
		<td><?php echo h($mapField['MapField']['created']); ?>&nbsp;</td>
		<td><?php echo h($mapField['MapField']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
