<div class="mapFields view">
<h2><?php  echo __('Map Field'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mapField['MapField']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Template'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mapField['Template']['name'], array('controller' => 'templates', 'action' => 'view', $mapField['Template']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Csv Field Order'); ?></dt>
		<dd>
			<?php echo h($mapField['MapField']['csv_field_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field'); ?></dt>
		<dd>
			<?php echo h($mapField['MapField']['field']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mapField['MapField']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($mapField['MapField']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Map Field'), array('action' => 'edit', $mapField['MapField']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Map Field'), array('action' => 'delete', $mapField['MapField']['id']), null, __('Are you sure you want to delete # %s?', $mapField['MapField']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Map Fields'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Map Field'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Templates'), array('controller' => 'templates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Template'), array('controller' => 'templates', 'action' => 'add')); ?> </li>
	</ul>
</div>
