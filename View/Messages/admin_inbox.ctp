<div class="enquiries index">
    <h2><?php echo __('Inbox'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'Message.id' => array('edit_link' => array('action' => 'view', '%id%')),
        'name' => array('title' => 'From', 'php_expression' => '".$row["Sender"]["first_name"]." ".$row["Sender"]["last_name"]."', 'edit_link' => array('action' => 'view', '%id%')),
        'subject' => array('title' => 'Subject', 'php_expression' => '".$this->Text->truncate($row["Message"]["subject"],20) ."', 'edit_link' => array('action' => 'view', '%id%')),
//        'Enquiry.email' => array('edit_link' => array('action' => 'edit', '%id%')),
//        'Type' => array("title" => "Type", 'php_expression' => '".Enquiry::$types[$row["Enquiry"]["type_id"]]."'),
//        'Dealer.name' => array("title" => "Dealer"),
 
    );
    $links = array(
        $this->Html->link(__('View', true), array('action' => 'view', '%id%'), array('class' => 'View')),
        $this->Html->link(__('Reply', true), array('action' => 'reply', '%id%'), array('class' => 'Reply')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), // , __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $inbox, $links, true, $multi_select_actions);
    ?>
</div>
<?php if (empty($_SESSION["dealer"])): ?>
    <script type="text/javascript" >

        function getLocations(obj) {


            var $this = obj;
            //        if ($this.val()) {
            $.ajax({
                type: "GET",
                dataType: "JSON",
                async: false,
                url: '<?php echo $this->Html->url(array("controller" => "dealers", "action" => "getLocations")) ?>/' + $this.val(),
                success: function (data) {
                    console.log(data);
                    $('#EnquiryDealerLocationId').html("<option>Select Dealer Location</option>");
                    $.each(data, function (key, value) {
                        $('#EnquiryDealerLocationId')
                                .append($("<option></option>")
                                        .attr("value", key)
                                        .text(value));
                    });
                }});
            //        }
        }

        $(document).ready(function () {
            var $selected_location = null;
            if ($('#EnquiryDealerLocationId').val() != null) {
                $selected_location = $('#EnquiryDealerLocationId').val();
            }
            getLocations($('#EnquiryDealerId'));
            $('#EnquiryDealerLocationId').val($selected_location);
            $('#EnquiryDealerId').on("change", function () {
                $('#EnquiryDealerLocationId').val(null);
                getLocations($('#EnquiryDealerId'));

            });
        });
    </script>
<?php endif; ?>