<div class="enquiries form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Message';
        echo $this->Form->create($modelClass);

        echo $this->Form->input('id', array('class' => 'input required'));

        echo $this->Form->input('subject', array('class' => 'input', 'value' => strstr($message['Message']['subject'], __('Re:', true)) ? $message['Message']['subject'] : __('Re:', true) . ' ' . $message['Message']['subject']));
        echo $this->Form->input('body', array('class' => 'input editor'));
        //		echo $this->Form->input('field1' , array('class' => 'input' ) );
//		echo $this->Form->input('field2' , array('class' => 'input' ) );
//		echo $this->Form->input('field3' , array('class' => 'input' ) );
//		echo $this->Form->input('field4' , array('class' => 'input' ) );
//		echo $this->Form->input('field5' , array('class' => 'input' ) );
//		echo $this->Form->input('field6' , array('class' => 'input' ) );
//		echo $this->Form->input('field7' , array('class' => 'input' ) );
//		echo $this->Form->input('field8' , array('class' => 'input' ) );
// 		echo $this->Form->enableAjaxUploads();
        echo $this->Form->enableEditors('textarea.editor');

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>