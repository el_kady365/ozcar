<div class="content">
    <div class="container">
        <div class="col-md-12">
            <div class="actions-bar-border clearfix">
                <h1 class="pull-left">Reading Email</h1>
                <div class="pull-right">
                    <ul class="small-actions">
                        <?php if ($message['Message']['from_type'] != 2) { ?>
                            <li><a href="<?php echo Router::url(array('action' => 'reply', $message['Message']['id'])) ?>" class="sm-action"><span class="ico rep-msg"></span> Reply</a></li>

                            <li>|</li>
                        <?php } ?>
                        <li><a href="<?php echo Router::url(array('action' => 'delete', $message['Message']['id'])) ?>" class="sm-action"><span class="ico del-msg"></span> Delete</a></li>
                        <!--<li>|</li>-->
<!--                        <li><a href="#" class="sm-action"><span class="ico unread-msg"></span> Mark as unread</a></li>
                        <li>|</li>
                        <li><a href="#" class="sm-action"><span class="ico read-msg"></span> Mark read</a></li>-->
                    </ul>
                </div>
            </div>
            <!-- / action bar -->


            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="m-b-md">
                <tr>
                    <td width="70" height="30">From:</td>
                    <?php if ($message['Message']['from_type'] == 2) {
                        ?>
                        <td><?php echo $message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'] ?></td>
                        <?php
                    } else {
                        ?>
                        <td><?php echo $message['Sender']['first_name'] . ' ' . $message['Sender']['last_name'] ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td height="30">Date:</td>
                    <td><?php echo date('d-F-y H:i', strtotime($message['Message']['created'])) ?></td>
                </tr>
            </table>
            <h4 class="m-b-lg m-t-lg"><?php echo strtoupper($message['Message']['subject']) ?>: </h4>
            <!--<h6><span class="bold">Subject: </span> 'Hi do you have any further Holden Captivas in stock?' </h6>-->
            <div class="email-body m-t-md">
                <?php echo nl2br($message['Message']['body']) ?>
            </div>


            <?php if ($message['Message']['from_type'] != 2) { ?>
                <div class="submit text-right">
                    <a href="<?php echo Router::url(array('action' => 'reply', $message['Message']['id'])) ?>" class="btn btn-md btn-default action-btn">REPLY</a>
                </div>
            <?php } ?>



        </div>
    </div>
</div>