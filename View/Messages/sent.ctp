<div class="contents">
    <div class="container">
        <div class="col-md-8">
            <h1>Dashboard</h1>
            <div class="collapse-section">
                <div class="collapse-bar head-bar m-b-md">
                    <h3 class="pull-left"><a href="#" class="ico-02"><i class="fa fa-minus-square-o"></i>
                            <span class="ico"></span> Messages (Sent)</a></h3>
                    <ul class="pull-right actions-links">
                        <li><a rel="inbox" href="<?php echo Router::url(array('action' => 'inbox')) ?>">Inbox</a></li>
                        <span class="hide display-link">
                            <li>|</li>
                            <li><a href="#" rel="delete">Delete</a></li>
                        </span>


                    </ul>
                </div>
                <div class="collapse-content">
                    <div class="msg-popup remodal" data-remodal-id="readMsg"  data-remodal-options="hashTracking:false">
                        <a class="close-modal" data-remodal-action="close" aria-label="Close">&#215;</a>
                        <div id="readContent">
                        </div>
                    </div>
                    <form action="<?php echo Router::url(array("action" => "do_operation")) ?>" id="forn" method="post">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table msg-list">
                            <?php
                            if (!empty($sent)) {
                                foreach ($sent as $message) {
                                    
                                    ?>
                                    <tr >
                                        <td><input type="checkbox" name="chk[]" value="<?php echo $message['Message']['id'] ?>" /></td>
            <!--                                    <td width="10"><a href="#">
                                                <img src="<?php echo Router::url('/css/img/ico/attachment.png') ?>" alt="" title=""></a></td>-->
                                        <!--<td width="50"><a href="#"><img src="http://dummyimage.com/33x33/eee/eee" alt="" title=""></a></td>-->
                                        <td><a  href="<?php echo Router::url('/messages/read/' . $message['Message']['id'].'?sent=1') ?>" class="text-sm read-message">
                                                <strong><?php echo $message['Receiver']['name'] ?></strong>
                                            </a>
                                            <p class="text-sm"><?php echo $message['Message']['subject'] ?></p></td>

                                        <td align="right"><a href="#" class="text-sm"><strong><?php echo date('d-m-Y', strtotime($message['Message']['created'])) ?></strong></a>
                                            <p class="text-sm"><?php echo date('h:ia', strtotime($message['Message']['created'])) ?></p>
                                        </td>
                                        <td><a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'do_action', 'delete', $message['Message']['id'])); ?>"><img src="<?php echo Router::url('/css/img/ico/row_del.png') ?>" alt="" title=""/></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>

                        </table>
                    </form>
                </div>
            </div>
            <!-- /collapse-section  --> 

        </div>
        <?php echo $this->element('Users.right_menu') ?>
    </div>
</div>

<?php
echo $this->Html->css(array('summernote'), null, array('inline' => false));
echo $this->Html->script(array('remodal', 'summernote', '//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js', 'jquery.nimble.loader'), array('inline' => false))
?>
<script>
    
    
    $(function () {
        if ($('input[name="chk[]"]:checked').length) {
            $('span.display-link').removeClass('hide').show();
            if ($('input[name="chk[]"]:checked').length == 1) {
                $('span.edit-link').removeClass('hide').show();
            } else {
                $('span.edit-link').addClass('hide').hide();
            }
        } else {
            $('span.display-link').addClass('hide').hide();
            $('span.edit-link').addClass('hide').hide();
        }

        $('.read-message').on('click', function () {
            href = $(this).prop('href');
            get_content(href, $('#readContent'), $('[data-remodal-id=readMsg]'));
            return false;
        });
        $('input[name="chk[]"]').on('change', function () {
            if ($('input[name="chk[]"]:checked').length) {
                $('span.display-link').removeClass('hide').show();
                if ($('input[name="chk[]"]:checked').length == 1) {
                    $('span.edit-link').removeClass('hide').show();
                } else {
                    $('span.edit-link').addClass('hide').hide();
                }
            } else {
                $('span.display-link').addClass('hide').hide();
                $('span.edit-link').addClass('hide').hide();
            }
//            alert('hello');
        });
        $(".actions-links li a").on('click', function () {
            action = $(this).prop('rel');
            form = $(this).closest('.collapse-section').find('form');
//            console.log(form);
            if (action != "")
            {
                if ($.inArray(action, ['reply','inbox']) != -1) {
                    if (action == 'reply') {
                        href = '<?php echo Router::url('/messages/reply/') ?>' + $('input[name="chk[]"]:checked').val();
                        get_content(href, $('#replyContent'), $('[data-remodal-id=replyMsg]'));
                    } else {
                        self.location = $(this).prop('href');
                    }
                } else {
                    if ($('input[name="chk[]"]:checked').length == 0)
                    {
                        alert("You must choose one element at least");
                    } else {
                        del = confirm("Are you sure you want to perform this operation?");
                        if (del)
                        {
                            form_action = form.prop('action');
                            form.prop('action', form_action + '?action=' + action);
                            console.log(form.prop);
                            form.submit();
                        }
                    }

                }
            }


            return false;
        });
        $('.summernote').summernote({
            height: 300,
            toolbar: []
        });
        $('.ed-txt-bold').on('click', function () {
            $(".summernote").summernote("bold");
            return false;
        });
        $('.ed-txt-italic').on('click', function () {
            $(".summernote").summernote("italic");
            return false;
        });
        $('.ed-txt-underline').on('click', function () {
            $(".summernote").summernote("underline");
            return false;
        });
        $('.ed-txt-capitalize').on('click', function () {
//            $(".summernote").summernote("underline"); 
            return false;
        });
        $('.ed-txt-size').on('click', function () {
            $(".fornt-size").removeClass("hide").show();
            return false;
        });
        $(".fornt-size").on('change', function () {
            $(".summernote").summernote("fontSize", $(this).val());
            return false;
        });

    });

    function get_content(href, element, modal_element) {
        console.log(href);
        $.ajax({
            beforeSend: function () {
                $('body').nimbleLoader("show", {
                    position: "fixed",
                    loaderClass: "loading_bar_1",
                    debug: true,
                    speed: 'fast',
                    hasBackground: true,
                    zIndex: 999,
                    backgroundColor: "#fff",
                    backgroundOpacity: 0.9
                });
                element.html('');
            },
            url: href
        }).done(function (data) {
            var options = {hashTracking: false}; //Do you need hash tracking?
            var inst = modal_element.remodal(options);
            inst.open();
            element.html(data);
            $('body').nimbleLoader("hide");
        });
    }



</script>