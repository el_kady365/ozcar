<?
echo $this->Html->css(array('jquery.ui.datepicker', 'jquery-ui'), null, array(), false);
echo $this->Html->script(array('select2', 'http://code.jquery.com/ui/1.10.3/jquery-ui.min.js', 'jquery.ui.datepicker', 'jquery-ui-timepicker-addon', 'jquery-ui-sliderAccess.js'), array('inline' => false));
?>
<?php echo $this->Html->script('menu'); ?>

<div class="ExtendedForm">
    <?php echo $this->Form->create('Page'); ?>
    <?php
    echo $this->Form->input('type', array('class' => 'INPUT', 'type' => 'hidden'));
    if (isset($this->data['Page']['category_id'])) {
        echo $this->Form->input('category_id', array('class' => 'INPUT', 'type' => 'hidden'));
    }
    echo $this->Form->input('id', array('class' => 'INPUT'));
    echo $this->Form->input('title', array('class' => 'INPUT'));
    //  if (empty($this->data['Page']['default'])) {
    echo $this->Form->input('permalink', array('class' => 'INPUT'));
    //}
    ?>
    <div class="PageSelector">
        <h4>Page will act as: </h4>
        <div>
            <div class="tab-bar">
                <ul>
                    <li><a href="#" id="OpenPage" class="active" >Content Page</a> </li>
                    <li><a href="#" id="OpenLink" >Just a Link</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="LinkContainer" class="tab-content">
        <?php echo $this->Form->input('url', array('class' => 'INPUT', 'label' => 'URL')); ?>
        <?php echo $this->Form->hidden('is_url', array('class' => 'INPUT', 'id' => 'is_url', 'label' => __('Is URL', true))); ?>
    </div>
    <div id="PageContainer" class="tab-content">

        <table cellpadding = "0" cellspacing = "0" border = "0" width = "100%">
            <tr>
                <td valign = "top" width = "300">
                    <p class="hint">
                        <strong>
                            How to insert a Page Section on the page:<br />
                        </strong>
                    </p>
                    <ul>
                        <li>Select from available Page Sections in the drop-down box below</li>
                        <li>In the editor window: Place cursor and click the position you want the section to show</li>
                        <li>Click "Insert section code" to insert section "short-code"; which displays the page section
                            on the live page.</li>

                    </ul>
                    <?php
                    echo $this->Form->label('Use saved Section');
                    echo $this->element('pageSection_list', array('templates' => $sections, 'prefix' => 'PageSection'));
                    echo $this->Form->input('model_id', array('class' => 'INPUT insertgallery', 'type' => 'select', 'id' => 'PageSectionId', 'options' => null, 'empty' => __('Select an Element', true), 'label' => false));
                    echo $this->Form->input('permanenet_display', array('class' => 'INPUT', 'type' => 'checkbox', 'label' => 'Permanent Display'));
                    echo '<div id="Dates">';
                    echo $this->Form->input('activation_date', array('class' => 'INPUT hasDate', 'type' => 'text', 'label' => 'Activation Date'));
                    echo $this->Form->input('deactivation_date', array('class' => 'INPUT hasDate', 'type' => 'text', 'label' => 'Deactivation Date'));
                    echo '</div>';
                    ?> 
                    <div class="submit">
                        <br/><input  type="button" id="InsertPageSection" class="Submit btn btn-primary" value="Insert Page Section">
                    </div>
                </td>



            </tr>

        </table>


        <?php
        echo $this->Form->input('content', array('class' => 'INPUT editor'));
        ?>

        <?php
        echo $this->Form->input('keywords', array('class' => 'INPUT', 'label' => __("Meta Keywords", true)));
        echo $this->Form->input('description', array('class' => 'INPUT', 'label' => __("Meta description", true)));
        ?>	
    </div>



    <?php
    //if (empty($this->data['Page']['default'])) {
    //echo $this->Form->input('access', array('class' => 'INPUT'));
    //}
    echo $this->Form->input('add_to_main_menu', array('id' => 'add-to-main', 'class' => 'INPUT', 'label' => 'Add To Menu?'));
    echo "<div id='MenuList' style='display:none;'>";

    echo $this->Form->input('menu_id', array('id' => 'SelectMenu', 'class' => 'INPUT', 'empty' => 'No Parent'));

    echo $this->Form->input('submenu_id', array('options' => array(), 'class' => 'INPUT', 'empty' => 'No Submenu'));
    echo "</div>";


    if (!empty($this->params['named']['dce']) && $this->params['named']['dce'] == 'yes') { //to enable/disable dynamic children
        echo $this->Form->input('has_dynamic_children', array('id' => 'has_dc', 'class' => 'INPUT', 'label' => 'Dynamic Children: Does this menu have dynamic children? ( e.g. parent of a list from the database, such as categories, products etc.. )', 'style' => 'display:'));

        echo '<div id="children-options" style="display: ; ">';

        echo $this->Form->input('dc_model', array('id' => 'dc_model', 'class' => 'INPUT', 'label' => 'Children Model Name e.g. Category'));
        echo $this->Form->input('dc_condition', array('id' => 'dc_condition', 'class' => 'INPUT', 'label' => 'Condition e.g. active = \'1\''));
        echo $this->Form->input('dc_limit', array('id' => 'dc_limit', 'class' => 'INPUT', 'label' => 'Limit (max items)'));
        echo $this->Form->input('dc_order', array('id' => 'dc_order', 'class' => 'INPUT', 'label' => 'order eg. display_order asc, published desc'));
        echo $this->Form->input('dc_controller', array('id' => 'dc_controller', 'class' => 'INPUT', 'label' => 'Controller e.g. categories'));
        echo $this->Form->input('dc_action', array('id' => 'dc_action', 'class' => 'INPUT', 'label' => 'action e.g. view'));
        echo $this->Form->input('dc_title_field', array('id' => 'dc_title_field', 'class' => 'INPUT', 'label' => 'Title field (the field that will be used as title for menu items'));
        echo $this->Form->input('dc_uri_field', array('id' => 'dc_uri_field', 'class' => 'INPUT', 'label' => 'Url field (the field that will be used as url for menu items e.g. permalink or id'));

        echo "</div>";
    }

    echo $this->Form->input('display_order', array('class' => 'INPUT'));
    //echo $this->Form->input('active', array('class' => 'INPUT'));
    // if (empty($this->data['Page']['default'])) {
    echo $this->Form->input('active', array('class' => 'INPUT'));



    // }
    ?>
    <?php echo $this->Form->enableEditors(); ?>
    <?php echo $this->Form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (!empty($this->data['Page']['id'])) {
        //  echo $this->element('save_as_new', array('model' => 'Page'));
    }
    ?>
    <?php echo $this->Form->end(); ?> </div>
<?php
//if (empty($this->data['Page']['default'])) {
echo $this->element('permalink');
//}

echo $this->Html->script('chosen.jquery.min');
//echo $this->Html->css('chosen');
?>
<script type="text/javascript"> $(".chzn-select").chosen({width: '350px'});</script> 

<script type="text/javascript">
    function check_menu_list() {
        if ($('#add-to-main').is(':checked')) {
            $('#MenuList').show();
        } else {
            $('#MenuList').hide();
        }
    }
    $(function () {

        check_menu_list();
        $('#add-to-main').click(function () {
            check_menu_list();
        });

        $('#LinkConatiner,#PageContainer').hide();
        var is_url = $('#is_url').val();
        if (is_url == '1') {
            $('#LinkContainer').show();
            $('#OpenLink').addClass('active');
            $('#OpenPage').removeClass('active');
            $('#PageContainer').hide();
        } else {
            $('#LinkContainer').hide();
            $('#PageContainer').show();
            $('#OpenLink').removeClass('active');
            $('#OpenPage').addClass('active');
        }
        $('#OpenLink').bind('click', function () {
            $('#LinkContainer').fadeIn();
            $('#is_url').val(1);
            $('#PageContainer').hide();
            $(this).addClass('active');
            $('#OpenPage').removeClass('active');
            return false;
        });

        $('#OpenPage').bind('click', function () {
            $('#PageContainer').fadeIn();
            $('#LinkContainer').hide();
            $(this).addClass('active');
            $('#OpenLink').removeClass('active');
            $('#is_url').val(0);
            return false;
        });


    });
</script> 
<script type="text/javascript">//<![CDATA[
    $(function () {
        //		$('a', '.breadcrumb').click(function(){
        //			var href = $(this).attr('href');
        //
        //			$('.attachment-tab').hide();
        //			$(href).show();
        //			console.log(href);
        //			$('.active', '.breadcrumb').removeClass('active');
        //			$(this).addClass('active');
        //
        //			window.location.hash = 'tab:' + href.substr(1);
        //			return false;
        //		});
        //		
        //		
        //		var activeSelector = ':first';
        //		var rx = new RegExp('#tab:');
        //		if (window.location.hash != '' && rx.test(window.location.hash)){
        //			var hash = window.location.hash;
        //			activeSelector = hash.replace('tab:', '');
        //		}
        //		
        //		$('.attachment-tab').not(activeSelector).hide();
    });
    //]]></script> 
<script type="text/javascript">
    $('document').ready(function () {
        $("#PageSubmenuId").attr("disabled", true);
        change_submenu($('#SelectMenu').val());

        if (!$('#PageSubmenuId').val())
        {
            document.getElementById('PageSubmenuId').disabled = true;
            $('#PageSubmenuId').html('<option></option>');
        }
        $('#SelectMenu').change(function () {
            change_submenu($(this).val());
        });
    });
    // Function to handle ajax.




    function change_submenu(menu_id)
    {
        if (!menu_id) {
            return false;
        }
        $.ajax({
            async: true,
            type: "GET",
            url: "<?php echo Router::url('/admin/pages/get_submenus/'); ?>" + menu_id + "/" +<?php echo empty($this->data['Page']['id']) ? 0 : $this->data['Page']['id']; ?>,
            dataType: "json",
            success: function (data) {
                $('#PageSubmenuId').html('<option value=0>No Submenu</option>');

                for (var i = 0; i < data.submenus.length; i++)
                {
                    var submenu = data.submenus[i];
                    var is_Selected = "";
                    var selected_submenu = "<?php echo!empty($this->data['Page']['submenu_id']) ? $this->data['Page']['submenu_id'] : 0; ?>";
                    //alert(submenu.Submenu.id);
                    if (submenu.Page.id == selected_submenu)
                    {
                        is_Selected = "selected=selected";
                    }
                    $('#PageSubmenuId').append('<option value="' + submenu.Page.id + '"' + is_Selected + '>' + submenu.Page.title + '</option>');

                }
                document.getElementById('PageSubmenuId').disabled = false;
                return false;
            }


        });

    }
</script> 



<script type="text/javascript">
<?php /* //   var  templates=<?= $this->Js->object($templates); ?>; */ ?>

    var sections = <?php echo $this->Js->object($sections); ?>;


    function loadPageSection(id) {

        var oEditor = CKEDITOR.instances.PageContent;
        NewText = sections[id].html;
        oEditor.insertHtml(NewText);
        return false;
    }
    $(document).on('click', '#addPageSection', function () {
        var section_id = $('#UseSection').val();

        if (!section_id) {
            return false;
        }
        loadPageSection(section_id);
        return false;
    });
</script> 


<script>
    var pagesection = '';
    var NewText;
    var arr = ['Contact', "DealershipList", "DealershipView", "DealershipNewsSocialMedia", 'Testimonial', 'BecomeMember', 'MediumSearch'];
    $(document).ready(function () {

        $('.hasDate').datepicker({
            dateFormat: 'dd/mm/yy'
        });


        $('#PagePermanenetDisplay').on('change', function () {
            if ($(this).prop('checked')) {
                $('#Dates').hide();
                $('#Dates input').prop('disabled', 'disabled');
            } else {
                $('#Dates').show();
                $('#Dates input').removeAttr('disabled');
            }
        });

        $('.page-sectionp').click(function () {
            var PageSectionName = $(this).data('name');
            pagesection = PageSectionName;

            if (arr.indexOf(PageSectionName) == -1 && PageSectionName != '') {
                $("#PageSectionId").removeAttr('disabled').show();
                $.ajax({
                    async: true,
                    type: "GET",
                    url: "<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'pageSection')); ?>/" + PageSectionName,
                    dataType: "json",
                    success: function (data) {
                        $("#PageSectionId").empty();
                        $("#PageSectionId").append('<option value="">Select an Element</option>');
                        $.each(data, function (i, item) {
                            $("#PageSectionId").append('<option value="' + item.id + '">' + item.name + '</option>');
                        });
                        return false;
                    }


                });
            } else {
                $("#PageSectionId").attr('disabled', 'disabled').hide();
            }
        });
        $('#PageSectionName').on('change', function () {

            //var PageSectionId = $("#PageSectionId :selected").val();
            alert($(this).find("option:selected").val());

        });

        $('#InsertPageSection').on('click', function () {
            var PageSectionName = pagesection;
            var PageSectionId = $("#PageSectionId :selected").val();
            var PageSectionTitle = $("#PageSectionId :selected").text();
            var PermanentDisplay = ($("#PagePermanenetDisplay").prop('checked') == true) ? 1 : 0;

            var ActivationDate = $("#PageActivationDate").val();
            var DeactivationDate = $("#PageDeactivationDate").val();
            if ((!PermanentDisplay && !ActivationDate) || (!PermanentDisplay && !DeactivationDate)) {
                alert("Please select Activation Date & Deactivation Date");
                return false;
            }

            if (arr.indexOf(PageSectionName) !== -1) {
                PageSectionId = 0;
                PageSectionTitle = '';
                AddPlaceHolder(PageSectionName, PageSectionId, PageSectionTitle, PermanentDisplay, ActivationDate, DeactivationDate);
            }
            else if (PageSectionName && PageSectionId) {
                AddPlaceHolder(PageSectionName, PageSectionId, PageSectionTitle, PermanentDisplay, ActivationDate, DeactivationDate);
            } else {
                if (!PageSectionName) {
                    alert("Please select Page Section");
                    $("#PageSectionName").focus();
                }
                else if (!PageSectionId) {
                    alert("Please select an element of Page Section you selected");
                    $("#PageSectionId").focus();
                }
            }
        });



    });
    function AddPlaceHolder(modelname, elementid, PageSectionTitle, PermanentDisplay, ActivationDate, DeactivationDate) {

        NewText = "{PageSection_" + modelname + "_%" + elementid;
        if (PageSectionTitle != '') {
            //  NewText += "-" + PageSectionTitle;
        }

        NewText += "%_" + PermanentDisplay;

        if (ActivationDate !== "" && DeactivationDate !== "") {

            NewText += "_" + ActivationDate + "_" + DeactivationDate + "}";
        } else {
            NewText += "}";
        }
        var oEditor = CKEDITOR.instances.PageContent;
        oEditor.insertHtml(NewText);
        return false;
    }

</script> 