<h2>Sections List</h2>
<div class="row">
    <?php
    $formHtml = ClassRegistry::init('FormHtml');
    $form_sections = $formHtml->find("list");
    $form_sections = array_keys($form_sections);
    $hiddenSections = array('DealershipNewsSocialMedia', 'DealershipList', 'DealershipFilter', 'DealershipView','ThreeImageBanner','ThreeImageText');
    foreach ($sections as $key => $section):
        if (!in_array($key, $hiddenSections)) {
            ?>
            <div class="col-md-4">
                <div class="section-box">
                    <h4><?php echo $section == "Full Width Banner" ? "Specials: ".$section : $section ?></h4>
                    <?php
                    $controller = '';
                    $type = false;
//                    switch ($key) {
//                        case 'FlippedGallery1':
//                            $controller = 'FlippedGalleries';
//                            $type = 1;
//                            break;
//                        case 'FlippedGallery2':
//                            $controller = 'FlippedGalleries';
//                            $type = 2;
//                            break;
//                        default:
                    $controller = Inflector::underscore(Inflector::pluralize($key));
//                            break;
//                    }
                    ?>
                    <div class="thumb-wrap">    
                        <img src="<?php echo resized_image_url($key . '.jpg', 350, 0, 'img/sections/', 0); ?>" alt="<?php echo $section; ?>" title="<?php echo $section; ?>" />
                    </div>
                    <div class="section-actions">
                        <?php if (in_array($key, $form_sections)) { ?>
                            <a href="<?php echo $This->html->url(array('controller' => 'form_htmls', 'action' => 'edit', $key)); ?>" class="btn btn-warning btn-sm"><span class="st-ico st-icon-list-2" aria-hidden="true"></span>Edit Html </a>
                        <?php } else {
                            ?>
                            <a href="<?php echo $this->Html->url(array('controller' => $controller, 'action' => 'index', $type)); ?>" class="btn btn-warning btn-sm"><span class="st-ico st-icon-list-2" aria-hidden="true"></span>List</a>
                            <a href="<?php echo $this->Html->url(array('controller' => $controller, 'action' => 'add', $type)); ?>" class="btn btn-success btn-sm"><span class="st-ico st-icon-plus-2" aria-hidden="true"></span> Add New</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- / col end -->
        <?php } ?>
    <?php endforeach; ?>
</div>
