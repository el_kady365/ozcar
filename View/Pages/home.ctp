<div class="pages">
    <div class="section-search">
        <div class="container">
            <div class="col-md-8-f">
                <div class="tabs default-tabs">
                    <ul class="tab-buttons all-search-tabs clearfix">
                        <li class="tab-btn active" data-id="#AllCars"><a rel="all" href="#">ALL CARS</a></li>
                        <li class="tab-btn" data-id="#NewCars"><a rel="1" href="#" rel="1">NEW CARS</a></li>
                        <li class="tab-btn" data-id="#UsedCars"><a rel="2" href="#">USED CARS</a></li>
                        <li class="tab-btn" data-id="#DemoCars"><a rel="3" href="#">DEMO CARS</a></li>
                    </ul>
                    <div class="tabs-box">
                        <?php echo $this->element('cars/car-search'); ?>
                    </div>

                    <!-- / tabs-box --> 
                </div>
                <!-- / tabs --> 
            </div>
            <div class="col-md-4-f">

                <?php echo $this->element('cars/small-search')?>

            </div>

        </div>
        <div class="container">
            <div class="col-md-12">
                <div class="tabs  default-tabs">
                    <ul class="tab-buttons clearfix">
                        <li class="tab-btn active" data-id="#DealershipsTab"><a href="#">DEALERSHIPS</a></li>
                        <li class="tab-btn" data-id="#DealershipSpecialsTab"><a href="#">DEALER SPECIALS</a></li>
                        <li class="tab-btn" data-id="#FeaturedCarsTab"><a href="#">FEATURED CARS</a></li>
                        <li class="tab-btn" data-id="#RecentlyViewedTab"><a href="#">RECENTLY VIEWED</a></li>
                        <li class="tab-btn" data-id="#RegiserNowTab"><a href="#">REGISTER NOW</a></li>
                    </ul>
                    <div class="tabs-box p-xmd">
                        <div class="tab current" id="DealershipsTab">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row-sm m-b-md">
                                        <div class="col-md-6">
                                            <h5 class="text-primary bold">POPULAR DEALERSHIPS</h5>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <p class="text-sm">View all <a href="#" class="bold underline">Popular Dealerships</a></p>
                                        </div>
                                    </div>
                                    <div class="popular-dealerships"> <a class="next"><i class="fa fa-angle-right"></i></a> <a class="prev"><i class="fa fa-angle-left"></i></a>
                                        <div class="popular-dealerships-slider owl-carousel owl-theme">
                                            <div class="item">
                                                <div class="dealers-list">
                                                    <ul>
                                                        <li class="table-view"><a class="cell" href="#"><img src="css/img/dealers/1.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/2.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/3.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/4.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell" href="#"><img src="css/img/dealers/5.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell" href="#"><img src="css/img/dealers/6.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/7.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/8.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/1.png" alt="" title="" /></a></li>
                                                    </ul>
                                                </div>
                                                <!-- / row --> 
                                            </div>
                                            <!-- / item -->
                                            <div class="item">
                                                <div class="dealers-list">
                                                    <ul>
                                                        <li class="table-view"><a class="cell" href="#"><img src="css/img/dealers/1.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/2.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/3.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/4.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell" href="#"><img src="css/img/dealers/5.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell" href="#"><img src="css/img/dealers/6.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/7.png" alt="" title="" /></a></li>
                                                        <li class="table-view"><a class="cell"  href="#"><img src="css/img/dealers/8.png" alt="" title="" /></a></li>
                                                    </ul>
                                                </div>
                                                <!-- / row --> 

                                            </div>
                                            <!-- / item --> 

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="promo-widget"> <a href="#"><img title="" alt="" src="css/img/banners/ads.png"></a> </div>
                                </div>
                            </div>
                        </div>
                        <!-- / DealershipsTab -->
                        <div class="tab" id="DealershipSpecialsTab">
                            <div class="row">
                                <div class="col-md-9">
                                    <h5 class="text-primary bold">DEALER SPECIALS</h5>
                                    <div class="row m-t-md">
                                        <div class="col-md-4"> <a href="#" class=" btn-block text-center m-b-md"><img src="css/img/banners/b_01.jpg" alt="" title=""/></a> <a href="#" class=" btn-block text-center"><img src="css/img/heartland_dealer.png" alt="" title=""/></a> </div>
                                        <div class="col-md-4"> <a href="#" class=" btn-block text-center m-b-md"><img src="css/img/banners/b_02.jpg" alt="" title=""/></a> <a href="#" class=" btn-block text-center"><img src="css/img/heartland_dealer.png" alt="" title=""/></a> </div>
                                        <div class="col-md-4"> <a href="#" class=" btn-block text-center m-b-md"><img src="css/img/banners/b_03.jpg" alt="" title=""/></a> <a href="#" class=" btn-block text-center"><img src="css/img/heartland_dealer.png" alt="" title=""/></a> </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="promo-widget"> <a href="#"><img title="" alt="" src="css/img/banners/ads.png"></a> </div>
                                </div>
                            </div>
                        </div>
                        <!-- / DealershipSpecialsTab -->
                        <div class="tab" id="FeaturedCarsTab">
                            <div class="row">
                                <div class="col-md-9">
                                    <div>
                                        <h5 class="text-primary bold">FEATURED CARS FROM THE MOTOR DEALER</h5>
                                        <div class="featured-cars m-t-md">
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                            <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                                <div class="p-sm">
                                                    <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                    <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- / featured cars --> 

                                </div>
                                <div class="col-md-3">
                                    <div class="promo-widget"> <a href="#"><img title="" alt="" src="css/img/banners/ads.png"></a> </div>
                                </div>
                            </div>
                        </div>
                        <!-- / DealershipSpecialsTab -->

                        <div class="tab" id="RecentlyViewedTab">
                            <div class="row">
                                <div class="col-md-9">
                                    <h5 class="text-primary bold">RECENTLY VIEWED</h5>
                                    <div class="featured-cars m-t-md">
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                        <div class="featured-cars-item pull-left item"> <a href="#"><img src="http://dummyimage.com/165x125/ccc/ccc" alt="" title="" /></a>
                                            <div class="p-sm">
                                                <p class="bold">Toyota 381 <span class="btn-block text-primary">$32,990</span></p>
                                                <a class="btn-block p-sm text-center" href="#"><img src="css/img/heartland_dealer.png" alt="" title="" /></a> </div>
                                        </div>
                                    </div>
                                    <!-- / featured cars --> 

                                </div>
                                <div class="col-md-3">
                                    <div class="promo-widget"> <a href="#"><img title="" alt="" src="css/img/banners/ads.png"></a> </div>
                                </div>
                            </div>
                        </div>
                        <!-- / DealershipSpecialsTab --> 

                        <div class="tab" id="RegiserNowTab">
                            <div class="row">
                                <div class="col-md-9">
                                    <h5 class="text-primary bold">RECENTLY VIEWED</h5>
                                    <div class="snippets m-b-lg  m-t-md">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et sed dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <ul class="customer-panel register-welcome">
                                                <li><a href="#" class="ico-04"><span class="ico"></span> Save your searches</a></li>
                                                <li><a href="#" class="ico-05"><span class="ico"></span> Compare
                                                        cars</a></li>
                                                <li><a href="#" class="ico-10"><span class="ico"></span> Be first to
                                                        see specials</a></li>
                                                <li><a href="#" class="ico-03"><span class="ico"></span> Receive
                                                        alerts</a></li>
                                                <li><a href="#" class="ico-13"><span class="ico"></span> Get a
                                                        quote</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h5 class="text-primary bold text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit! </h5>
                                            <a href="<? echo Router::url(array('controller'=>'users','action'=>'register')) ?>" class="btn btn-info btn-md btn-block m-t-md">REGISTER NOW</a>


                                        </div>
                                    </div>

                                    <!-- / featured cars --> 

                                </div>
                                <div class="col-md-3">
                                    <div class="promo-widget"> <a href="#"><img title="" alt="" src="css/img/banners/ads.png"></a> </div>
                                </div>
                            </div>
                        </div>
                        <!-- / DealershipSpecialsTab -->             


                    </div>
                    <!-- / tabs-box --> 
                </div>
            </div>
        </div>
    </div>
    <div class="testimonials m-b-lg">
        <div class="container">
            <div class="col-md-12">
                <div class="testimonials-slider">
                    <div class="item">
                        <div class="text-center">
                            <h3>BUYER CONFIDENCE</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="text-center">
                            <h3>BUYER CONFIDENCE</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /testimonials  -->

    <div class="content-section">
        <div class="container">
            <div class="col-md-12">
                <h5 class="bold text-primary text-center">TITLE GOES HERE</h5>
                <div class="m-t-md m-b-md text-center"> <a href="#" class="blue-circle transition"><img src="css/img/ico/icon_smp_01.png" alt="" title=""/></a> <a href="#" class="blue-circle transition"><img src="css/img/ico/icon_smp_02.png" alt="" title=""/></a> <a href="#" class="blue-circle transition"><img src="css/img/ico/icon_smp_03.png" alt="" title=""/></a> </div>
                <div class="row text-sm">
                    <div class="col-md-6">
                        <p>Morbi accumsan ipsum velit. Nam nec tellus a odio sit tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Nam nec tellus a odio sit tincidunt auctor a ornare odio. Conubia nostra,  mauris inter erat justo. Nullam ac urna eu felis dapibus condiment entum sit amet a auguec urna eufelis duo.Class aptent taciti sociosqu ad litora torquent permis conubia nostra, per inceptos himenaeos mauris inter erat justo. Nullam ac urna eu felis dapibus condiment entum sit amet a augue.</p>
                    </div>
                    <div class="col-md-6">
                        <p>Morbi accumsan ipsum velit. Nam nec tellus a odio sit tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Nam nec tellus a odio sit tincidunt auctor a ornare odio. Conubia nostra,  mauris inter erat justo. Nullam ac urna eu felis dapibus condiment entum sit amet a auguec urna eufelis duo.Class aptent taciti sociosqu ad litora torquent permis conubia nostra, per inceptos himenaeos mauris inter erat justo. Nullam ac urna eu felis dapibus condiment entum sit amet a augue.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="random-cars m-t-xl">
        <div class="container">
            <div class="col-md-12">
                <div class="post-adv text-center m-b-lg"> <a href="#"><img src="http://dummyimage.com/728x90/ccc/ccc" alt="http://dummyimage.com/728x90/eee/eee"></a> </div>
                <h5 class="bold highlight m-b-md">guaranteed quality cars <span class="text-primary">all around the country in one place</span></h5>
            </div>
        </div>
        <div class="random-cars-slider"> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/1.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> <a href="#" class="zoom-effect"><img src="css/img/thumbs/3.png"><span class="car-location"><i class="fa fa-map-marker"></i> Aldinga Beach SA</span></a> </div>
    </div>
    <!-- /section-search  --> 

</div>
<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel'), array('inline' => false)) ?>
<!-- /contents  -->