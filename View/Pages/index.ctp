<div id="static_page_container" class="pages cms">
    <?php echo $contents ?>
</div>


<?php
if (isset($with_gallery)) {
    /*@var $javascript JavascriptHelper*/
    echo $html->css("jquery.lightbox-0.5");
    echo $javascript->link(array('jquery.lightbox-0.5'));
    ?>
    <script type="text/javascript">
        function active_light_box(){
            $('a.light-box-image').lightBox({
                imageLoading: '<?= Router::url("/css/img/lightbox-ico-loading.gif") ?>',
                imageBtnClose:'<?= Router::url("/css/img/lightbox-btn-close.gif") ?>',
                imageBtnNext: '<?= Router::url("/css/img/lightbox-btn-next.gif") ?>',
                imageBtnPrev: '<?= Router::url("/css/img/lightbox-btn-prev.gif") ?>',
                imageBlank  : '<?= Router::url("/css/img//lightbox-blank.gif") ?>'
            });
        }
            
        $(function(){
            active_light_box();
            $('.gallery-paging').find('a').live('click' , function(){
                if($(this).parent().hasClass('current')){
                    return false;
                }
                loadpagination($(this).attr('href') , $(this).attr('rel'));
                //active_light_box();
                return false;
            });
        });
        function loadpagination(href , div_id){
            if(href=="javascript: void(0);"){
                return false;
            }
            var div_height = $('#'+div_id+' .gallery-thumbs').height();
            $('#'+div_id).html('<div style="height:'+div_height+'px" class="gallery-load"></div>');
            $.ajax({
                url: href,
                dataType: 'html',
                cache: false,
                success: function(data){
                    var object_length = $(".gallery-thumbs li",data).length;
                    var object_height = (object_length > 4)?361:220;
                    $(".gallery-load").animate({height:object_height} , 800 , function(){
                            $('#'+div_id).html(data);
                    });
                    /*$('#'+div_id).animate({height:object_height} , 400 , function(){
                        $('#'+div_id).html(data);
                    });*/
                }
            });
        }
    </script>

<?php } ?>