<div class="heading">
    <div class="wrap">
        <h1 class="museo">Search results</h1>
    </div>
</div>
<?php
if (!empty($results)) {
    foreach ($results as $result) {
        ?>
        <div class="section">
            <div class="wrap">
                <div class="wrap-inner">
                    <p> <?php echo $this->Text->truncate(strip_tags($result['Page']['content']), 100); ?> </p>
                </div>
            </div>
        </div>
        <?php
    }
}?>