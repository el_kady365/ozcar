<?php
//echo $html->tag('h1', h($page['title']), array('class' => 'default-title'));
$content = $page['content'];
$_SESSION["videos_gallery_counter"] = 0;

if (!empty($slideshows)) {
    echo $javascript->link(array('jquery-easing', 'coda-slider'));
    $html->css('coda-slider', false, false, false);
    foreach ($slideshows as $slideshow) {
        $content = preg_replace('/\\[\\{SLIDESHOW:\s?' . $slideshow['Slideshow']['id'] . '[^\\}]*\\}\\]/i', $this->element('slideshow', array('slideshow' => $slideshow)), $content);
    }
    ?>

    <script type="text/javascript">
        $(function () {
            $(".codaslider").codaSlider({
                autoSlide: true
            });
            $('.stripNav').each(function () {
                $(this).css('width', $('li', this).length * 20 + 12);
            });
        });



    </script>

    <?php
}

if (!empty($videos)) {
    $this->Html->script(array('video/swfobject'));
    foreach ($videos as $video) {
        $content = preg_replace('/\\[\\{VIDEO:\s?' . $video['Video']['id'] . '[^\\}]*\\}\\]/i', $this->element('video-player', array('video' => $video)), $content);
    }
}
?>
<div class="Pagescontainer">

    <?php
    $class = "landing-content";
    if ($page['enable_scroller']) {
        $class = "pages-scroll";
    }
    ?>
    <div class="<?php echo $class ?>">
        <?php
        if ($page['enable_scroller']) {
            $class = "pages-scroll";
            ?>

            <div class="scroll-pane">
                <div class="scroll-pane-inner">
                    <h1 class="blanch"><?php echo $page['title'] ?></h1>
                <? } ?>

                <?php
                preg_match_all("/{PageSection_(\w+)_%(\d+)%_?(\d*)_?([^_]*)_?([^_]*)}/", $content, $matches);
                if ($matches[3] || (!empty($matches[4]) && strtotime($matches[4]) < time() && !empty($matches[5]) && strtotime($matches[5]) > time() )) {
                    if (!empty($matches[2])) {
                        foreach ($matches[2] as $key => $value) {
                            preg_match('/[A-Za-z]+/', $matches[1][$key], $matchss);

                            echo $this->element('pageSections/' . $matches[1][$key], compact('value'));
                        }
                    }
                } else {
                    echo $content;
                }



//                echo $content;
                ?>
                <?php if ($page['enable_scroller']) { ?>
                </div>
            </div>

        <? } ?>
    </div>

</div>
<?php echo $this->Html->script(array('jScrollPane', 'jquery.mousewheel')); ?>
<script>
    $(window).load(function () {
<?php if ($page['enable_scroller']) { ?>
            $('.scroll-pane').jScrollPane();
<? } ?>
    });

</script>

<?php
$this->set('selectedPageId', $page['id']);
$this->set('selectedLink', $topLink);

if (file_exists(WWW_ROOT . 'css' . DS . $page['permalink'] . '.css')) {
    $html->css($page['permalink'], false, false, false);
}

if (file_exists(WWW_ROOT . 'js' . DS . $page['permalink'] . '.js')) {
    echo $this->Html->script($page['permalink']);
}
//debug($this->params)
?>