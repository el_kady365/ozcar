<div class="sectionsLayouts view">
<h2><?php  echo __('Sections Layout'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sectionsLayout['SectionsLayout']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($sectionsLayout['SectionsLayout']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo h($sectionsLayout['SectionsLayout']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($sectionsLayout['SectionsLayout']['image']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sections Layout'), array('action' => 'edit', $sectionsLayout['SectionsLayout']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sections Layout'), array('action' => 'delete', $sectionsLayout['SectionsLayout']['id']), null, __('Are you sure you want to delete # %s?', $sectionsLayout['SectionsLayout']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sections Layouts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sections Layout'), array('action' => 'add')); ?> </li>
	</ul>
</div>
