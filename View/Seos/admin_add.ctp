<div class="seos form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Seo';
        echo $this->Form->create($modelClass);

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('criteria', array('class' => 'input',
            'between' => '
<div class="note">
    <p>Criteria = URL to apply custom page title and meta tags to. </p>
    <p> <strong>\'%\'</strong> may be used to substitute characters in a URL </p>
    <p>For every requested URL, the most specific \'SEO Rule\' will apply.</p>
</div>  

',
            'after' => '
<div class="note">

    <p><strong>Criteria Examples:</strong></p>

    <table cellpadding="2" cellspacing="2">
        <tr>
            <td><strong>/</strong></td><td>This matches the Home page only</td>
        </tr>
        <tr>
            <td><strong>/%</strong></td><td>This matches every page on the site <em>(except any page with a more specific rule applied)</em> </td>
        </tr>
        <tr>
            <td><strong>content/%</strong></td><td>This matches every Content page <em>(except any page with a more specific rule applied)</em> </td>
        <tr>
            <td><strong>/content/about-us</strong></td><td>This matches the About Us page only</td>
        </tr>
    </table>
</div>
'));
        echo $this->Form->input('title', array('class' => 'input', 'label' => 'Custom Page Title'));
        echo $this->Form->input('keywords', array('class' => 'input' , 'between' => $this->Html->para('note', 'Custom Meta Keywords')));
        echo $this->Form->input('description', array('class' => 'input editor', 'between' => $this->Html->para('note', 'Custom Meta Description')));

// 		echo $this->Form->enableAjaxUploads();
//		echo $this->Form->enableEditors('textarea.editor'); 

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
