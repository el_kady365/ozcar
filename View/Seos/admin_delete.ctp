
 <?php echo $this->Form->create('Seo', array('action' => $this->action)); ?><div class="rounded-item config-msg">
    <b class="bl-corners"></b><b class="br-corners"></b>
    <div class="FormExtended confirm-message">
        <h3><?php echo __('Confirmation Message !', true); ?></h3>
        <?php echo __('Are you sure you want to delete?'); ?>
        <div class='delete-items'>
            <?php  foreach ($seos as $i => $seo): ?>
                <input type="hidden" value="<?php echo $seo['Seo']['id'] ?>" name="ids[]" />
                <span class="one-item"><?php  echo $this->Html->link($seo['Seo']['id'], array('action' => 'edit', $seo['Seo']['id']), array('target' => '_blank')) ?></span>
                <?php if (!empty($seo[$i + 1])) { ?>
                    <span class="seprated-item">,</span>
                <?php } ?>
            <?php endforeach; ?>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="confirm-action">
    <button class="green-button" type="submit" name="submit_btn" value="yes" ><?php echo __('Yes', true); ?></button>
    <button class="red-button" type="submit" name="submit_btn" value="no" ><?php echo __('No', true); ?></button>
</div>

<?php echo $this->Form->end(); ?>