<div class="seos index">
    <h2><?php echo __('SEO'); ?></h2>
    <?php 
	echo $this->List->filter_form($modelName, $filters); 
	$fields = array(
		'Seo.id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Seo.criteria' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Seo.title' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Seo.keywords' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Seo.description' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Seo.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Seo.updated' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
	); 
            $links = array(
                $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ),
            );

            $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

            echo $this->List->adminIndexList($fields, $seos, $links,true, $multi_select_actions , array('title'=>"SEO"));


?>
</div>
