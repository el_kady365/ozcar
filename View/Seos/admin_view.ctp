<div class="seos view">
<h2><?php  echo __('Seo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Criteria'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['criteria']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Keywords'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['keywords']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($seo['Seo']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Seo'), array('action' => 'edit', $seo['Seo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Seo'), array('action' => 'delete', $seo['Seo']['id']), null, __('Are you sure you want to delete # %s?', $seo['Seo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Seos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Seo'), array('action' => 'add')); ?> </li>
	</ul>
</div>
