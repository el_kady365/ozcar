<h2>Configuration</h2>

<div class="siteconfigs form ExtendedForm">
    <div class="form-wrap">
        <?php
        $modelClass = 'SiteConfig';
        echo $this->Form->create($modelClass, array('type' => 'file'));
        ?>
        <div>
            <div class="tab-bar">
                <ul>

                    <?php foreach ($config_schema as $gid => $group) : ?>
                        <li><a id="<?php echo $gid; ?>_link"  class="tabLink" ><?php echo (!empty($group['title']) ? $group['title'] : Inflector::humanize($gid) ); ?></a> </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>

        <?php
        $j = !empty($map) ? (max($map) + 1 ) : 0;
        foreach ($config_schema as $gid => $group) {
            ?>     <div id="<?php echo $gid; ?>_tab" class="tab-content"> <?php if ($gid == 'style') { ?>


                    <?php
                }
                foreach ($group['vars'] as $varid => $var) {
                    if (!empty($var['input']['type']) && $var['input']['type'] == 'file') {
                        // continue;
                    }
                    $i = isset($map[$gid . '.' . $varid]) ? $map[$gid . '.' . $varid] : ++$j;
                    if (!isset($map[$gid . '.' . $varid])) {
                        $var['input']['class'] = (isset($var['input']['class']) ? $var['input']['class'] : '' ) . ' isNull';
                    }

                    if (!empty($var['input']['eval_between'])) {
                        $var['input']['between'] = eval('return ' . $var['input']['eval_between'] . ' ;');
                        unset($var['input']['eval_between']);
                    }

                    echo $this->Form->input("SiteConfig.$i.id");
                    echo $this->Form->input("SiteConfig.$i.group", array('class' => 'input', 'type' => 'hidden', 'value' => $gid));
                    echo $this->Form->input("SiteConfig.$i.setting", array('class' => 'input', 'value' => $varid, 'type' => 'hidden'));
                    echo $this->Form->input("SiteConfig.$i.value", array_merge(array('class' => 'input', 'label' => !empty($var['title']) ? $var['title'] : Inflector::humanize($varid)), !empty($var['input']) ? $var['input'] : array() ));
                }
                ?></div><?php
        }



// 		echo $this->Form->enableAjaxUploads(); 
//        echo $this->Form->enableEditors('textarea.editor');
        echo $this->Form->enableMapSelectors();
        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
<?php echo $this->Html->script('colorpicker', array('inline' => false)); ?>
<?php echo $this->Html->css('colorpicker', null, array('inline' => false)); ?>
<?php
echo $this->Html->css('fineuploader', null, array('inline' => false));
echo $this->Html->script('fuploaderjq.libs.min', array('inline' => false));
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('a.tabLink').bind('click', function() {
            if ($(this).hasClass('active'))
                return false;
            $('.tab-content').hide();
            // $(this).attr('name', $(this).attr('id').split('_')[0]);
            $(this).attr('href', '#' + $(this).attr('id').split('_')[0]);
            $($(this).attr('href') + '_tab').fadeIn();
            $('a.tabLink').removeClass('active');
            $(this).addClass('active');
            //return false;
        });
        if ($(window.location.hash + '_link').attr('id'))
            $(window.location.hash + '_link').trigger('click');

        else
            $('.tabLink').first().trigger('click');

        $('input.form-error').parents('.tab-content').each(function() {
            $('#' + $(this).attr('id').replace(/_tab$/, '_link')).addClass('error');
        });
        $('.tabLink.error').first().trigger('click');



        $('input.hasColorSelector').ColorPicker({
            //color: $('input:last', $(this).parent()).val(),
            color: '0000ff',
            onShow: function(colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function(hsb, hex, rgb, originator) {
                $(originator).val('#' + hex);
            }
        }).bind('keyup', function() {
            $(this).ColorPickerSetColor(this.value);
        });

        $('input.hasColorSelector').each(function() {
            $(this).ColorPickerSetColor($(this).val());
        });
    });


    var file_element = '<?php echo $this->JqueryEngine->escape($this->element('site_config_file_between')); ?>';
    $(document).ready(function() {
        $(':file[name^=data]').each(function() {
            var group = $('#' + $(this).attr('id').replace(/Value$/, 'Group')).val();
            var setting = $('#' + $(this).attr('id').replace(/Value$/, 'Setting')).val();

            var $parent = $(this).parent();
            $(this)
                    .before($(file_element))
                    .after($('<div class="uploadBox"></div>')
                    .append($('<div class="uploader"  data-input-name="' + $(this).attr('name') + '"  data-input-id="' + $(this).attr('id') + '"></div>')))
                    .remove();

            $fileEl = $parent.find('.file_element');

            $.ajax({
                url: BASE_URL + 'admin/site_configs/getFileSettings/' + group + '.' + setting,
                async: true,
                cache: true,
                dataType: 'json',
                type: 'GET',
                success: function(json) {
                    if (json.width && json.height) {
                        $fileEl.find('.image_desc').find('span.width').text(json.width);
                        $fileEl.find('.image_desc').find('span.height').text(json.height);
                    } else {
                        $fileEl.find('.image_desc').hide();
                    }

                    $fileEl.find('.extensions').text(typeof json.extensions === 'string' ? json.extensions : json.extensions.join(', '));
                    $fileEl.find('.file_size').text(json.max_file_size);

                    if (json.current_value) {
                        $fileEl.find('a.download_url').attr('href', json.download_url);
                        $fileEl.find('a.delete_url').attr('href', json.delete_url);
                        $fileEl.find('.file-name').text(json.current_value);
                    } else {
                        $fileEl.find('.current-file').hide();
                    }

                    $fileEl.show();

                }

            });
        });

        $(document).on('click', '.file_element .delete_url', function() {
            if (confirm('Really delete this file?')) {
                $el = $(this);
                $.ajax({
                    url: $(this).attr('href'),
                    cache: false,
                    async: true,
                    dataType: 'json',
                    success: function(json) {
                        if (json.success) {
                            $el.parents('.file_element').find('.current-file').hide();
                        }
                    }
                });
            }
            return false;
        });

        $('form div.uploadBox div.uploader').each(function() {
            var buttonText = 'Select File';//$('input.hiddenHash', $(this).parent()).attr('title'); 
            var group = $('#' + $(this).data('input-id').replace(/Value$/, 'Group')).val();
            var setting = $('#' + $(this).data('input-id').replace(/Value$/, 'Setting')).val();
            $('#' + $(this).data('input-id').replace(/Value$/, 'Group')).remove();
            $('#' + $(this).data('input-id').replace(/Value$/, 'Setting')).remove();
            $('#' + $(this).data('input-id').replace(/Value$/, 'Id')).remove();
            $(this).fineUploader({
                request: {
                    endpoint: BASE_URL + 'admin/site_configs/upload/' + group + '.' + setting,
                    inputName: 'data[SiteConfig][value]',
                },
                multiple: false,
                autoUpload: false,
                failedUploadTextDisplay: {
                    mode: 'custom',
                    responseProperty: 'error'
                },
                text: {uploadButton: buttonText ? buttonText : 'Select File'
                }


            }).on('complete', function(e, id, filename, response) {
                var $form = $('#SiteConfigAdminEditForm');
                $form.data('uploads').pop();
                if (!$form.data('uploads').length) {
                    $form.submit();
                }

            });
        });


        $('#SiteConfigAdminEditForm').on('submit', function(e) {
            var $form = $(this);
            if (!$form.find('.qq-upload-cancel:visible').length)
                return true;

            $form.data('uploads', []);
            $('form div.uploadBox div.uploader').each(function(i) {
                $(this).fineUploader('uploadStoredFiles');
                $form.data('uploads').push(i);
            });

            return false;

        });
    });
</script>
