<h1>Reset Password!</h1>
<?php echo $this->Form->create('SiteConfig', array('url'=>'/site_configs/confirm_password', 'class' => 'large-forms')); ?>
<?php
echo $this->Form->input('password', array('label' => array('class' => 'bebas', 'text' => 'New Password <span class="required">*</span>')));									
echo $this->Form->input('paswrd', array('type'=>'password','label' => array('class' => 'bebas', 'text' => 'Confirm password <span class="required">*</span>')));									
?>
<div class="submit">
<input type="submit" value="Reset" class="bg-a" />
</div> 
<?php echo $this->Form->end(); ?>
				
