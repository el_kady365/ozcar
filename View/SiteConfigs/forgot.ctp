<div class="content-box">
			<h1>Reset Password</h1>
			<h6>Trouble Accessing Your Account?</h6>
				<?php echo $this->Form->create('SiteConfig', array('url'=>'/site_configs/forgot', 'class' => 'form-box')); ?>
				
					<?php
					echo $this->Form->input('email', array('label' => array( 'text' => 'Email <span class="required">*</span>')));
					echo $this->Form->input('security_code', array('label' => array( 'text' => 'Security Code <span class="required">*</span>'), 'value' => '', 'autocomplete' => 'off', 'between' => $this->Html->div('captcha', $this->Html->image('/image.jpg'))));
					?>
        <div class="submit">
            <input type="submit" value="Reset" class="bg-a" />
        </div> 
        <?php echo $this->Form->end(); ?>


</div>