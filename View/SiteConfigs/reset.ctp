<h1>Reset Password!</h1>
<?php echo $this->Form->create('SiteConfig', array('url'=>'/site_configs/reset', 'class' => 'large-forms')); ?>
<?php echo $this->Form->input('code', array('label' => array('class' => 'bebas', 'text' => 'Code <span class="required">*</span>')));?>
<div class="submit">
  <input type="submit" value="Submit" class="bg-a" />
</div>
<?php echo $this->Form->end(); ?> 