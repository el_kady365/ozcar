<div class="sliders form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'Slider' ; 
	echo $this->Form->create($modelClass , array('type' => 'file') ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('title' , array('class' => 'input required' ) );
		echo $this->Form->input('description' , array('class' => 'input required editor' ) );
		echo $this->Form->input('active' , array('class' => 'input required' ) );
		$field = 'image' ; 
		echo $this->Form->input('image' , array('class' => 'input required' , 'type' => 'file' , 'between' => $this->element('image_input_between', array('info' => $file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data[$modelClass]['id']) ? $this->data[$modelClass]['id'] : null), 'base_name' => (!empty($this->request->data[$modelClass][$field]) ? $this->request->data[$modelClass][$field] : ''))) ) );

		echo $this->Form->enableAjaxUploads();
		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
