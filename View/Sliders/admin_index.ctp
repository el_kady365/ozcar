<div class="sliders index">
    <h2><?php echo __('Sliders'); ?></h2>
    <?php 
	 echo $this->List->filter_form($modelName, $filters); 
	$fields = array(
		'Slider.id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Slider.title' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Slider.description' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Slider.active' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Slider.image' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
	); 
            $links = array(
                $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ), // , __('Are you sure?', true)),
            );

            $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

            echo $this->List->adminIndexList($fields, $sliders, $links,true, $multi_select_actions);


?>
</div>
