<div class="ExtendedForm change-site-disable">
    <?php
    echo $this->Form->create();
    echo $this->Form->input('id');
    echo $this->Form->input('content', array('class' => 'INPUT editor'));
    echo $this->Form->end(array('label' => __('Submit'), 'class' => "Submit"));
    /* @var $this View */
    echo $this->Form->enableEditors();
    ?>
</div>