<div class="index">
	<div class="widget-header"><?php echo __("Snippets") ?></div>
	<table class="table listing-table" width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr class="table-header">
				<th><a href="#"><?php echo __("Snippet") ?></a></td>
				<th class="action"><?php echo __('Actions') ?></td>
			</tr>
		</thead>
		<?php foreach ($snippets as $placeholder => $title): ?>
			<tr>
				<td><?php echo $this->Html->link($title, array('action' => 'edit', $placeholder)); ?></td>
				<td>
					<ul class="action">
						<li><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $placeholder), array('class' => 'Edit')) ?></li>
					</ul>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<?php /* @var $this View */ ?>
