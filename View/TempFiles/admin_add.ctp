<div class="tempFiles FormExtended">
    <div class="form-wrap">
        <?php echo $this->Form->create('TempFile'); ?>

            <?php
            echo $this->Form->input('name');
            echo $this->Form->input('hash');
            echo $this->Form->input('uploaded_for');
            echo $this->Form->input('behavior');
            ?>
        <?php echo $this->Form->end(__('Submit')); ?>
    </div>
</div>