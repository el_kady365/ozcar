<div class="index">
    <h2><?php echo __('Temp Files', true); ?></h2>

    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'TempFile.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'TempFile.name' => array('edit_link' => array('action' => 'edit', '%id%')),
        'TempFile.hash' => array('edit_link' => array('action' => 'edit', '%id%')),
        'TempFile.uploaded_for' => array('edit_link' => array('action' => 'edit', '%id%')),
        'TempFile.behavior' => array('edit_link' => array('action' => 'edit', '%id%')),
		
    );
    $links = array(
        $this->Html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')),//, __('Are you sure?', true)),
    );

    $multi_select_actions = array(
        'delete' => array('action' => Router::url(array('action' => 'delete_multi', 'admin' => true)), 'confirm' => true)
    );
    //debug($TempFiles);
    ?><?= $this->List->adminIndexList($fields, $tempFiles, $links, true, $multi_select_actions) ?></div>
