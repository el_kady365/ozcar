<div class="tempFiles view">
<h2><?php  echo __('Temp File'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tempFile['TempFile']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($tempFile['TempFile']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tmp Name'); ?></dt>
		<dd>
			<?php echo h($tempFile['TempFile']['tmp_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hash'); ?></dt>
		<dd>
			<?php echo h($tempFile['TempFile']['hash']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($tempFile['TempFile']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Temp File'), array('action' => 'edit', $tempFile['TempFile']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Temp File'), array('action' => 'delete', $tempFile['TempFile']['id']), null, __('Are you sure you want to delete # %s?', $tempFile['TempFile']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Temp Files'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Temp File'), array('action' => 'add')); ?> </li>
	</ul>
</div>
