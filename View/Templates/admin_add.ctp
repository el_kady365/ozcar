<div class="templates form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Template';
        echo $this->Form->create($modelClass);

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('name', array('class' => 'input'));
        echo $this->Form->input('type_id', array("label" => "Cars Types","options" => Car::$types ,'class' => 'input'));
        if (!empty($_SESSION["dealer"])) {
            echo $this->Form->input('dealer_id', array('value' => $_SESSION["dealer"]["Dealer"]["id"], "type" => "hidden", 'class' => 'input'));
        } else {
            echo $this->Form->input('dealer_id', array('class' => 'input' , "empty" => "Select Dealer"));
        }
        echo $this->Form->input('delimiter', array('class' => 'input', "options" => array("," => "Comma Separated ( , ) ", ";" => "Semi Column Separated ( ; )", "\t" => "Tab Delimited")));
        echo $this->Form->input('import_first_row', array('class' => 'input'));

// 		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
