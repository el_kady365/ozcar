<div class="templates index">
    <h2><?php echo __('Templates'); ?></h2>
    <?php 
	 echo $this->List->filter_form($modelName, $filters); 
	$fields = array(
		'Template.id' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Template.name' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
		'Dealer.name' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Template.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'Template.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
	); 
            $links = array(
                $this->Html->link(__('Manage Fields', true), array("controller" => "map_fields",'action' => 'index', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ), // , __('Are you sure?', true)),
            );

            $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

            echo $this->List->adminIndexList($fields, $templates, $links,true, $multi_select_actions);


?>
</div>
