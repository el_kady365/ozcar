<div class="templates view">
<h2><?php  echo __('Template'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($template['Template']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($template['Template']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Delimiter'); ?></dt>
		<dd>
			<?php echo h($template['Template']['delimiter']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($template['Template']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($template['Template']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Template'), array('action' => 'edit', $template['Template']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Template'), array('action' => 'delete', $template['Template']['id']), null, __('Are you sure you want to delete # %s?', $template['Template']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Templates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Template'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Map Fields'), array('controller' => 'map_fields', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Map Field'), array('controller' => 'map_fields', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Map Fields'); ?></h3>
	<?php if (!empty($template['MapField'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Template Id'); ?></th>
		<th><?php echo __('Csv Field Order'); ?></th>
		<th><?php echo __('Field'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($template['MapField'] as $mapField): ?>
		<tr>
			<td><?php echo $mapField['id']; ?></td>
			<td><?php echo $mapField['template_id']; ?></td>
			<td><?php echo $mapField['csv_field_order']; ?></td>
			<td><?php echo $mapField['field']; ?></td>
			<td><?php echo $mapField['created']; ?></td>
			<td><?php echo $mapField['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'map_fields', 'action' => 'view', $mapField['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'map_fields', 'action' => 'edit', $mapField['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'map_fields', 'action' => 'delete', $mapField['id']), null, __('Are you sure you want to delete # %s?', $mapField['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Map Field'), array('controller' => 'map_fields', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
