<div class="threeImageTexts form">
    <?php  echo $this->Form->create('ThreeImageText', array('type' => 'file')); ?>

    <?php
    echo  $this->Form->input('id');
    echo  $this->Form->input('title');
    echo  $this->Form->input('dealerships', array('options' => $dealerships, 'class' => 'dealerships', 'multiple' => true));
    
    echo  $this->Form->input('url1');
    echo  $this->Form->input('title1', array('class' => 'title', 'type' => 'text'));
    echo  $this->Form->input('body1', array('class' => 'body', 'type' => 'textarea'));
    echo  $this->Form->input('image1', array('class' => 'INPUT', 'type' => 'file', 'div' => false, 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image1'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['ThreeImageText']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['ThreeImageText']['image1'] : '')))
    ));
    
    echo  $this->Form->input('title2', array('class' => 'title', 'type' => 'text'));
    echo  $this->Form->input('body2', array('class' => 'body', 'type' => 'textarea'));
    echo  $this->Form->input('url2');
    echo  $this->Form->input('image2', array('class' => 'INPUT', 'type' => 'file', 'div' => false, 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image2'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['ThreeImageText']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['ThreeImageText']['image2'] : '')))
    ));
    
    echo  $this->Form->input('title3', array('class' => 'title', 'type' => 'text'));
    echo  $this->Form->input('body3', array('class' => 'body', 'type' => 'textarea'));
    echo  $this->Form->input('url3');
    echo  $this->Form->input('image3', array('class' => 'INPUT', 'type' => 'file', 'div' => false, 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image3'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['ThreeImageText']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['ThreeImageText']['image3'] : '')))
    ));?>
    
    <p></p>
    
    <?php
        echo  $this->Form->input('keywords', array('class' => 'keywords', 'type' => 'text'));
        //echo $fck->create('Body', 'body', null, 'Default', Router::url('/css/screen.css'), array('height' => 550, 'style' => "body {background:#FFF }"));
    ?>

     <?php echo  $this->Form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (!empty($this->data['ThreeImageText']['id'])) {
        echo $this->element('save_as_new', array('model' => 'ThreeImageText'));
    }
    ?>
    <?php echo  $this->Form->end(); ?>
</div>

<?php
echo $this->Html->css(array('select2', 'jquery.ui.datepicker', 'jquery-ui'), null, array(), false);
echo $this->Html->script(array('select2', 'http://code.jquery.com/ui/1.10.3/jquery-ui.min.js', 'jquery.ui.datepicker', 'jquery-ui-timepicker-addon', 'jquery-ui-sliderAccess.js'), array('inline' => false));
?>
<script>
    $(function () {
        $(".keywords").select2({
            width: '300px',
            tags: [],
            tokenSeparators: [",", " "]
        });
    });
</script>

<script>
    $(function () {
        $(".dealerships").select2({
            width: '450px'
        });
    });
</script>