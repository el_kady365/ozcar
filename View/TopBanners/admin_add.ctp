<div class="topBanners form ExtendedForm">
    <div class="form-wrap">

	<?php 
	$modelClass = 'TopBanner' ; 
	echo $this->Form->create($modelClass , array('type' => 'file') ); 

		echo $this->Form->input('id' , array('class' => 'input required' ) );
		echo $this->Form->input('title' , array('class' => 'input required' ) );
		$field = 'image' ; 
		echo $this->Form->input('image' , array('class' => 'input' , 'type' => 'file' , 'between' => $this->element('image_input_between', array('info' => $file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data[$modelClass]['id']) ? $this->data[$modelClass]['id'] : null), 'base_name' => (!empty($this->request->data[$modelClass][$field]) ? $this->request->data[$modelClass][$field] : ''))) ) );
		echo $this->Form->input('url' , array('class' => 'input' ) );
		echo $this->Form->input('new_window' , array('label' => 'Open in new window' , 'class' => 'input required' ) );
		echo $this->Form->input('active' , array('class' => 'input required' ) );
		echo $this->Form->input('display_order' , array('class' => 'input number' ) );

		echo $this->Form->enableAjaxUploads();
// 		echo $this->Form->enableEditors('textarea.editor'); 

	echo $this->Form->end(__('Submit')); 
	?>
    </div>
</div>
