<div class="topBanners index">
    <h2><?php echo __('Top Banners'); ?></h2>
    <?php
    echo $this->List->filter_form($modelName, $filters);
    $fields = array(
        'TopBanner.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'TopBanner.title' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Image' => array('php_expression' => '" . "<img src=\"" . $row["TopBanner"]["image_thumb_full_path"]  . "\"/>" . "',),
//		'TopBanner.image' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'TopBanner.url' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'TopBanner.new_window' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
        'TopBanner.active' => array('edit_link' => array('action' => 'edit', '%id%') , 'format' => 'bool'),
        'TopBanner.display_order' => array(),
//		'TopBanner.created' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
//		'TopBanner.modified' => array('edit_link' => array('action' => 'edit' , '%id%')) , 
    );
    $links = array(
        $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ), //, __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $this->List->adminIndexList($fields, $topBanners, $links, true, $multi_select_actions);
    ?>
</div>
