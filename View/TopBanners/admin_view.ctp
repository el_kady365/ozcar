<div class="topBanners view">
<h2><?php  echo __('Top Banner'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('New Window'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['new_window']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Display Order'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['display_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($topBanner['TopBanner']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Top Banner'), array('action' => 'edit', $topBanner['TopBanner']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Top Banner'), array('action' => 'delete', $topBanner['TopBanner']['id']), null, __('Are you sure you want to delete # %s?', $topBanner['TopBanner']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Top Banners'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Top Banner'), array('action' => 'add')); ?> </li>
	</ul>
</div>
