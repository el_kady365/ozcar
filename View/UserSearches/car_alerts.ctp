<div class="contents">
    <div class="container">
        <div class="col-md-8">
            <h1>Dashboard</h1>
            <?php
            echo $this->Form->create('Car', array('type' => 'get', 'id' => 'CarAlert', 'url' => array('controller' => 'user_searches', 'action' => 'car_alerts', $id)));
            ?>
            <div class="collapse-section">
                <div class="collapse-bar head-bar m-b-md">
                    <h3 class="pull-left">Car Alerts</h3>
                </div>
                <div class="collapse-content">

                    <div class="row m-b-md">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <h6 class="m-b-md">Search Type</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="input radio pull-left">
                                        <input type="radio" name="type_id" value="all" <?php echo(!empty($params['type_id']) && $params['type_id'] == 'all' || empty($params['type_id'])) ? 'checked="checked"' : '' ?>>
                                        <label>All cars</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input radio pull-left">
                                        <input type="radio" name="type_id" value="2" <?php echo!empty($params['type_id']) && $params['type_id'] == '2' ? 'checked="checked"' : '' ?> />
                                        <label>Used cars</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input radio pull-left">
                                        <input type="radio" name="type_id" value="1" <?php echo!empty($params['type_id']) && $params['type_id'] == '1' ? 'checked="checked"' : '' ?> />
                                        <label>New cars</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input radio pull-left">
                                        <input type="radio" name="type_id" value="2" <?php echo!empty($params['type_id']) && $params['type_id'] == '3' ? 'checked="checked"' : '' ?> />
                                        <label>Demo cars</label>
                                    </div>
                                </div>
                            </div>
                            <!-- / form-group  --> 

                        </div>
                        <div class="col-md-3">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <h6 class="m-b-md">Frequency</h6>
                                </div>
                                <div class="col-md-12">
                                    <div class="input radio pull-left">
                                        <input type="radio" name="frequency" value="1"  <?php echo(!empty($params['frequency']) && $params['frequency'] == '1' || empty($params['frequency'])) ? 'checked="checked"' : '' ?> />
                                        <label>Daily</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input radio pull-left">
                                        <input type="radio" name="frequency" value="7"  <?php echo(!empty($params['frequency']) && $params['frequency'] == '7') ? 'checked="checked"' : '' ?> />
                                        <label>Weekly</label>
                                    </div>
                                </div>
                            </div>
                            <!-- / form-group  --> 

                        </div>
                        <div class="col-md-3">
                            <div class="note">

                            </div>
                        </div>
                    </div>
                    <div class="row m-b-md">
                        <div class="col-md-6">
                            <?php echo $this->Form->input('make', array('options' => $makes, 'label' => false, 'class' => 'makes', 'empty' => 'Select Make', 'selected' => !empty($params['make']) ? $params['make'] : '')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('model', array('empty' => 'Select Model', 'options' => array(), 'label' => false, 'type' => 'select')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('series', array('empty' => 'Select Series', 'options' => array(), 'label' => false, 'type' => 'select')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('fuel_type', array('label' => false, 'options' => Car::$FuelTypes, 'label' => false, 'empty' => 'Fuel Type', 'selected' => !empty($params['fuel_type']) ? $params['fuel_type'] : '')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('transmission', array('label' => false, 'options' => Car::$Transmissions, 'label' => false, 'empty' => 'Transmission', 'selected' => !empty($params['transmission']) ? $params['transmission'] : '')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('cylinders', array('options' => array_combine(range(1, 12), range(1, 12)), 'label' => false, 'empty' => 'Cylinders', 'selected' => !empty($params['cylinders']) ? $params['cylinders'] : '')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('body', array('label' => false, 'options' => Car::getBodyTypesDropDown(), 'empty' => 'Body', 'selected' => !empty($params['body']) ? $params['body'] : '')); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('seats', array('selected' => !empty($params['seats']) ? $params['seats'] : '', 'options' => array_combine(range(2, 15), range(2, 15)), 'label' => false, 'empty' => 'Seats')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('doors', array('selected' => !empty($params['doors']) ? $params['doors'] : '', 'options' => array_combine(range(1, 6), range(1, 6)), 'label' => false, 'empty' => 'Doors')) ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo $this->Form->input('colour', array('selected' => !empty($params['colour']) ? $params['colour'] : '', 'options' => $colours, 'label' => false, 'empty' => 'Colour')) ?>                
                        </div>
                    </div>
                    <div class="row m-b-md m-t-md rang-row range-bordered">
                        <div class="col-md-12">
                            <label>Engine Size</label>
                        </div>
                        <div class="col-md-7">
                            <div id="EngineSize" data-from="<?php echo!empty($params['engine_from']) ? $params['engine_from'] : '' ?>" data-to="<?php echo!empty($params['engine_to']) ? $params['engine_to'] : '' ?>" ></div> 
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('engine_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN', 'value' => !empty($params['engine_from']) ? $params['engine_from'] : '')); ?>
                        </div>
                        <div class="col-md-1 text-center">
                            <div class="m-t-md"><strong>TO</strong></div>
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('engine_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX', 'value' => !empty($params['engine_to']) ? $params['engine_to'] : '')); ?>
                        </div>
                    </div>
                    <div class="row m-b-md m-t-md rang-row range-bordered">
                        <div class="col-md-12">
                            <label>Price</label>
                        </div>
                        <div class="col-md-7">
                            <div id="Price" data-from="<?php echo!empty($params['price_from']) ? $params['price_from'] : '' ?>" data-to="<?php echo!empty($params['price_to']) ? $params['price_to'] : '' ?>" ></div> 
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('price_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN', 'value' => !empty($params['price_from']) ? $params['price_from'] : '')); ?>
                        </div>
                        <div class="col-md-1 text-center">
                            <div class="m-t-md"><strong>TO</strong></div>
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('price_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX', 'value' => !empty($params['price_to']) ? $params['price_to'] : '')); ?>
                        </div>
                    </div>
                    <div class="row m-b-md m-t-md rang-row range-bordered">
                        <div class="col-md-12">
                            <label>Year</label>
                        </div>
                        <div class="col-md-7">
                            <div id="Years" data-from="<?php echo!empty($params['year_from']) ? $params['year_from'] : '' ?>" data-to="<?php echo!empty($params['year_to']) ? $params['year_to'] : '' ?>" ></div> 
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('year_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN', 'value' => !empty($params['year_from']) ? $params['year_from'] : '')); ?>
                        </div>
                        <div class="col-md-1 text-center">
                            <div class="m-t-md"><strong>TO</strong></div>
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('year_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX', 'value' => !empty($params['year_to']) ? $params['year_to'] : '')); ?>
                        </div>
                    </div>

                    <div class="row m-b-md m-t-md rang-row range-bordered">
                        <div class="col-md-12">
                            <label>KMs</label>
                        </div>
                        <div class="col-md-7">
                            <div id="Kilometers" data-from="<?php echo!empty($params['kms_from']) ? $params['kms_from'] : '' ?>" data-to="<?php echo!empty($params['kms_to']) ? $params['kms_to'] : '' ?>"></div> 
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('kms_from', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MIN', 'value' => !empty($params['kms_from']) ? $params['kms_from'] : '')); ?>
                        </div>
                        <div class="col-md-1 text-center">
                            <div class="m-t-md"><strong>TO</strong></div>
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('kms_to', array('label' => false, 'class' => 'text-center', 'placeholder' => 'MAX', 'value' => !empty($params['kms_to']) ? $params['kms_to'] : '')); ?>
                        </div>
                    </div>

                    <!-- / date ranger -->

                    <div class="row">

                        <div class="col-md-6">
                            <?php echo $this->Form->input('location', array('placeholder' => 'Location/Postcode', 'class' => 'autocompleteAddress', 'label' => false, 'value' => !empty($params['location']) ? $params['location'] : '')) ?>
                        </div>
                        <div class="col-md-2 text-center">
                            <div class="m-t-md"><strong>WITHIN</strong></div>
                        </div>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('distance', array('options' => Car::$Distances, 'label' => false, 'empty' => 'Select Distance', 'selected' => !empty($params['distance']) ? $params['distance'] : '')) ?>
                        </div>



                    </div>

                </div>



                <div class="collapse-content">
                    <div class="row m-t-sm">
                        <div class="col-md-8">


                            <div class="input submit">
                                <button type="submit" class="btn btn-lg  btn-info">SAVE ALERTS</button>
                            </div>


                        </div>
                    </div>


                </div>



            </div>
            <?php echo $this->Form->end(); ?>
            <!-- /collapse-section  --> 





        </div>
        <?php echo $this->element('Users.right_menu') ?>
    </div>
</div>
<?php echo $this->Html->script(array('ion.rangeSlider', 'owl.carousel'), array('inline' => false)) ?>
<?php echo $this->Html->script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places', array('inline' => false)) ?>
<script>
    var models = <?php echo json_encode($models) ?>;
    var serieses = <?php echo json_encode($serieses); ?>;

//    var selMake = '<?php echo!empty($params['make']) ? $params['make'] : '' ?>';
    var selModel = '<?php echo!empty($params['model']) ? $params['model'] : '' ?>';
    var selseries = '<?php echo!empty($params['series']) ? $params['series'] : '' ?>';

    $(function () {
        if ($('#Years').length) {
            $("#Years").ionRangeSlider({
                type: "double",
                prettify_enabled: false,
                min: '<?php echo date('Y') - 30 ?>',
                max: '<?php echo date('Y') ?>',
                onChange: function (data) {
                    $('input[name="year_from"]').val(data.from);
                    $('input[name="year_to"]').val(data.to);

                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="year_from"]').val('');
                        $('input[name="year_to"]').val('');
                    }


                }

            });
        }

        $('select[name="make"]').on('change', function () {
            getChildList($('select[name="model"]'), $(this).val(), 'models')
        }).change();

        $('select[name="model"]').on('change', function () {
            getChildList($('select[name="series"]'), $(this).val(), 'serieses')
        }).change();
        function getChildList(element, data_id, type) {
            if (data_id) {
                var data = window[type][data_id];

                element.html('<option value="">Select Model</option>');
                if (type == 'serieses') {
                    element.html('<option value="">Select Series</option>');
                }
                if (type == 'suburbs') {
                    element.html('<option value="">Select Suburb</option>');
                }
                if (data) {
                    $.each(data, function (key, value) {
                        selected = '';
                        if (selseries.toLowerCase() == value.toLowerCase() && type == 'serieses') {
                            selected = 'selected="selected"';
                        }
                        if (selModel.toLowerCase() == value.toLowerCase() && type == 'models') {
                            selected = 'selected="selected"';
                        }

                        element.append('<option value="' + key + '" ' + selected + '>' + value + '</option>');
                        ;
                    });
                }
            }
        }
    });
    function address_initialize() {
        $(".autocompleteAddress").each(function (index) {
            var txt_Obj = $(this)[0];
            autocomplete = new google.maps.places.Autocomplete((txt_Obj), {types: ['geocode'], componentRestrictions: {country: "Aus"}});
        });
    }
    $(window).load(function () {
        address_initialize();
    })
</script>