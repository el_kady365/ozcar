<div class="cars form ExtendedForm">
    <div class="form-wrap">

        <?php
        $modelClass = 'Car';
        echo $this->Form->create($modelClass, array('type' => 'file'));

        echo $this->Form->input('id', array('class' => 'input required'));
        echo $this->Form->input('dealer_id', array('class' => 'input', "empty" => "Select Dealer", "id" => "dealer_id"));
        echo $this->Form->input('dealer_location_id', array('class' => 'input', "options" => null, "id" => "dealer_location_id", "empty" => "Select Dealer Location"));
        echo $this->Form->input('type_id', array('class' => 'input', "options" => Car::$types, "empty" => "Select Type"));
        echo $this->Form->input('make_id', array('class' => 'input', "id" => "make_id", "empty" => "Select Make"));
        echo $this->Form->input('car_model_id', array('class' => 'input', "options" => null, "id" => "model_id", "empty" => "Select Model"));
        echo $this->Form->input('stock_number', array('class' => 'input'));
        echo $this->Form->input('year', array('class' => 'input'));
        echo $this->Form->input('badge', array('class' => 'input'));
        echo $this->Form->input('series', array('class' => 'input'));
        echo $this->Form->input('body', array('class' => 'input'));
        echo $this->Form->input('engine_capacity', array('class' => 'input'));
        echo $this->Form->input('cylinders', array('class' => 'input'));
        echo $this->Form->input('fuel_type', array('class' => 'input'));
        echo $this->Form->input('doors', array('class' => 'input'));
        echo $this->Form->input('rego', array('class' => 'input'));
        echo $this->Form->input('vin_number', array('class' => 'input'));
        echo $this->Form->input('odometer', array('class' => 'input'));
        echo $this->Form->input('colour', array('class' => 'input'));
        echo $this->Form->input('trim', array('class' => 'input'));
        echo $this->Form->input('feature_code', array('class' => 'input'));
        echo $this->Form->input('selling_price', array('class' => 'input'));
        echo $this->Form->input('comments', array('class' => 'input editor'));
        echo $this->Form->input('compliance_date', array('class' => 'input calendar', "type" => "text"));
        echo $this->Form->input('redbook_code', array('class' => 'input'));
        ?>
        <h4>Car Images</h4>
        <div id="" class="" style="margin: 20px 0 40px 20px;">
            <?php
            echo $this->Html->script('many-add-delete', array('inline' => false));
            $car_images = !empty($this->data['CarImage']) ? $this->data['CarImage'] : array(array());
            ?>
            <div class="page_tabs many-container">
                <ul id="page_tabs" class="many">
                    <?php foreach ($car_images as $i => $car_image) : ?>
                        <li class="record">
                            <?php
                            echo $this->Form->input("CarImage.$i.id");
                            $field = 'image';
                            echo $this->Form->input("CarImage.$i.image", array('class' => 'input', 'div' => array('class' => 'input file'), 'type' => 'file', 'between' => $this->element('image_input_between', array('info' => $car_file_settings[$field], 'field' => $field, 'id' => (!empty($this->request->data['CarImage'][$i]['id']) ? $this->data['CarImage'][$i]['id'] : null), 'base_name' => (!empty($this->request->data['CarImage'][$i][$field]) ? $this->request->data['CarImage'][$i][$field] : '')))));
                            echo $this->Form->input("CarImage.$i.display_order", array("class" => "input"));
                            ?>
                            <div class="clear"></div>
                            <a href="#" class="delete" >Delete</a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <a href="#" class="add">Add New Car Image</a>
            </div>
            <hr/>
        </div>
        <?
// 		echo $this->Form->enableAjaxUploads();
        echo $this->Form->enableEditors('textarea.editor');

        echo $this->Form->end(__('Submit'));
        ?>
    </div>
</div>
<?php
$this->Html->css(array('jquery-ui.datepicker'), null, array('inline' => false));
echo $this->Html->script(array('jquery-ui-1.9.2.core_picker.min'));
?>
<script type="text/javascript" >

    function getLocations(obj) {


        var $this = obj;
        if ($this.val()) {
            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: '<?php echo $this->Html->url(array("controller" => "dealers", "action" => "getLocations")) ?>/' + $this.val(),
                success: function(data) {
                    console.log(data);
                    $('#dealer_location_id').html("<option>Select Dealer Location</option>");
                    $.each(data, function(key, value) {
                        $('#dealer_location_id')
                                .append($("<option></option>")
                                .attr("value", key)
                                .text(value));
                    });
                    $("#dealer_location_id").prop('disabled', false);

                }});
        } else {
            $('#dealer_location_id').val(null);
        }
    }

    function getModels(obj) {


        var $this = obj;
        if ($this.val()) {
            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: '<?php echo $this->Html->url(array("controller" => "makes", "action" => "getModels")) ?>/' + $this.val(),
                success: function(data) {
                    console.log(data);
                    $('#model_id').html("<option>Select Model</option>");
                    $.each(data, function(key, value) {
                        $('#model_id')
                                .append($("<option></option>")
                                .attr("value", key)
                                .text(value));
                    });
                    $("#model_id").prop('disabled', false);

                }});
        } else {
            $('#model_id').val(null);
        }
    }
    $(document).ready(function() {
<?php if (!empty($_SESSION["dealer"])): ?>
            $('#dealer_id').prop('disabled', true);
    <?php if ($this->action == "add"): ?>
                getLocations($('#dealer_id'));
    <?php endif; ?>
<?php endif; ?>
        if ($('#dealer_location_id').val() == null) {
            $('#dealer_location_id').prop('disabled', true);
        }
        if ($('#model_id').val() == null) {
            $('#model_id').prop('disabled', true);
        }

        $('.calendar').datepicker({
            dateFormat: "yy-mm-dd"
        });
        $('#dealer_id').on("change", function() {
            $('#dealer_location_id').val(null);
            $('#dealer_location_id').prop('disabled', true);
            getLocations($('#dealer_id'));

        });

        $('#make_id').on("change", function() {
            $('#model_id').val(null);
            $('#model_id').prop('disabled', true);
            getModels($('#make_id'));

        });
    });
</script>