<?php
/**
* Bake Template for Controller action generation.
*
* PHP 5
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       Cake.Console.Templates.default.actions
* @since         CakePHP(tm) v 1.3
* @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
*/
?>


    /**
    * <?php echo $admin ?>index method
    *
    * @return void
    */
    public function <?php echo $admin ?>index() {
    <?php if(!empty($admin)) : ?>
        $conditions = $this->_filter_params() ;
    <?php else:
        $modelObj = new $currentModelName();
        $schema = $modelObj->schema() ;
        if(!empty($shcema['active']) && $schema['active']['type'] == 'boolean')
            $conditionsStr = "array('active' => 1) ;" ;
        else 
            $conditionsStr = "array() ;";
        $order = ''; 
        if(!empty($shcema['display_order']))
            $order =  "\$this->paginate['$currentModelName']['order'] = array('display_order' => 'asc') ;"; 

            ?>
        $conditions = <?php echo $conditionsStr ; ?> 
    <?php endif; ?>
        $this-><?php echo $currentModelName ?>->recursive = 0;
        <?php if(!empty($order)) echo $order; ?>
        $this->set('<?php echo $pluralName ?>', $this->paginate($conditions));
    }

    /**
    * <?php echo $admin ?>view method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function <?php echo $admin ?>view($id = null) {
            $this-><?php echo $currentModelName; ?>->id = $id;
            if (!$this-><?php echo $currentModelName; ?>->exists()) {
                    throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
            }
            $this->set('<?php echo $singularName; ?>', $this-><?php echo $currentModelName; ?>->read(null, $id));
    }
    
<?php if($admin) : ?>
    <?php $compact = array(); ?>

    /**
    * <?php echo $admin ?>add method
    *
    * @return void
    */
    public function <?php echo $admin ?>add() {
            if ($this->request->is('post')) {
                    $this->__submit_common();
                    $this-><?php echo $currentModelName; ?>->create();
                    if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
                            $this->flashMessage(__('The <?php echo strtolower($singularHumanName); ?> has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
<?php else: ?>
                            $this->flash(__('<?php echo ucfirst(strtolower($currentModelName)); ?> saved.'), array('action' => 'index'));
<?php endif; ?>
                    } else {
<?php if ($wannaUseSession): ?>
                            $this->flashMessage(__('The <?php echo strtolower($singularHumanName); ?> could not be saved. Please, try again.'));
<?php endif; ?>
                    }
            }
<?php
    foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
            foreach ($modelObj->{$assoc} as $associationName => $relation):
                    if (!empty($associationName)):
                            $otherModelName = $this->_modelName($associationName);
                            $otherPluralName = $this->_pluralName($associationName);
                            echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
                            $compact[] = "'{$otherPluralName}'";
                    endif;
            endforeach;
    endforeach;
    if (!empty($compact)):
            echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
    endif;
    
    echo "\t\t\$this->__form_common();\n";
?>
    }
    


<?php $compact = array(); ?>
    /**
    * <?php echo $admin ?>edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function <?php echo $admin; ?>edit($id = null) {
            $this-><?php echo $currentModelName; ?>->id = $id;
            if (!$this-><?php echo $currentModelName; ?>->exists()) {
                    throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                    $this->__submit_common();
                    if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
                            $this->flashMessage(__('The <?php echo strtolower($singularHumanName); ?> has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
<?php else: ?>
                            $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), array('action' => 'index'));
<?php endif; ?>
                    } else {
<?php if ($wannaUseSession): ?>
                    $this->flashMessage(__('The <?php echo strtolower($singularHumanName); ?> could not be saved. Please, try again.'));
<?php endif; ?>			}
            } else {
                    $this->request->data = $this-><?php echo $currentModelName; ?>->read(null, $id);
            }
<?php
            foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
                    foreach ($modelObj->{$assoc} as $associationName => $relation):
                            if (!empty($associationName)):
                                    $otherModelName = $this->_modelName($associationName);
                                    $otherPluralName = $this->_pluralName($associationName);
                                    echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
                                    $compact[] = "'{$otherPluralName}'";
                            endif;
                    endforeach;
            endforeach;
            if (!empty($compact)):
                    echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
            endif;
            echo "\t\t\$this->__form_common();\n";
            echo "\t\t\$this->render('{$admin}add') ;\n" ;

    ?>
    }
    
    <?php if($admin){ ?>
    /**
    * common method for add and edit
    *
    * @return boolean 
    */

    
    function __form_common(){ 

<?php 
        /** BY M.Azzam **/
		$out = "" ;
        $fields = array_keys($modelObj->schema());
         if(in_array('file' , $fields)|| in_array('image' , $fields)) {
            $out .= "\t\t\$this->set('file_settings',\$this->{$currentModelName}->getFileSettings());\n";
        echo $out ;			
        }
        /** End M.Azzam **/

?>
        return true;
    }
    
    <?php } ?>


    <?php if($admin){ ?>
    /**
    * common method for add and edit submissions
    *
    * @return boolean 
    */

    
    function __submit_common(){ 

        return true;
    }
    
    <?php } ?>
    
    
    /**
    * <?php echo $admin ?>delete method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void 
    */
    public function <?php echo $admin; ?>delete($id = null) {
            if( $this->request->data('submit_btn') === 'no'){
              $this->flashMessage(__('Deletion cancelled by user'), 'Notemessage');
              $this->redirect( array('action' => 'index')); 

            } elseif ((!$this->request->is('post') && !empty($id)) || (!empty($this->request->data['ids']) &&  $this->request->data('submit_btn') !== 'yes' && !$this->request->is('ajax')  )) {
                $id = !empty($id) ? array($id) : $this->request->data('ids') ;
                $<?php echo $pluralName; ?> = $this-><?php echo $currentModelName; ?>->find('all', array('conditions' => array('<?php echo $currentModelName; ?>.id' => $id))) ;
                $this->set(compact('<?php echo $pluralName; ?>'));
                return; 
            }

            if (!empty($id)) {

                $this-><?php echo $currentModelName; ?>->id = $id;
                if (!$this-><?php echo $currentModelName; ?>->exists()) {
                    throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
                }
                if ($this-><?php echo $currentModelName; ?>->delete()) {
                    if ($this->request->is('ajax')) {
                            die(json_encode(array('status' => 'success')));
                    } else {
<?php if ($wannaUseSession): ?>
                        $this->flashMessage(__('<?php echo ucfirst(strtolower($singularHumanName)); ?> deleted'), 'Sucmessage' );
                        $this->redirect(array('action' => 'index'));
<?php else: ?>
                        $this->flash(__('<?php echo ucfirst(strtolower($singularHumanName)); ?> deleted'), array('action' => 'index'));
<?php endif; ?>
                    }
               }
               if ($this->request->is('ajax')) {
                    die(json_encode(array('status' => 'error')));
               } else {
<?php if ($wannaUseSession): ?>
                    $this->flashMessage(__('<?php echo ucfirst(strtolower($singularHumanName)); ?> was not deleted'));
<?php else: ?>
                    $this->flash(__('<?php echo ucfirst(strtolower($singularHumanName)); ?> was not deleted'), array('action' => 'index'));
<?php endif; ?>
                    $this->redirect(array('action' => 'index'));
               } 
            } elseif(is_array($this->request->data('ids'))){
                if($this-><?php echo $currentModelName; ?>->deleteAll(array( '<?php echo $currentModelName; ?>.id' => $this->request->data('ids')), true , true)){
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'success')));
                    } else {
<?php if ($wannaUseSession): ?>
                        $this->flashMessage(__('<?php echo ucfirst(strtolower($pluralName)); ?> deleted successfully'), 'Sucmessage');
<?php else: ?>
                        $this->flash(__('<?php echo ucfirst(strtolower($singularHumanName)); ?> was not deleted'), array('action' => 'index'));
<?php endif; ?>
                        $this->redirect(array('action' => 'index'));
                    }      
                } else {
                    if($this->request->is('ajax')){
                        die(json_encode(array('status' => 'error')));
                    } else {
<?php if ($wannaUseSession): ?>
                        $this->flashMessage(__('Error deleting selected <?php echo strtolower($pluralName); ?>'));
<?php else: ?>
                        $this->flash(__('<?php echo ucfirst(strtolower($singularHumanName)); ?> was not deleted'), array('action' => 'index'));
<?php endif; ?>
                        $this->redirect(array('action' => 'index'));
                    }
                }            
            }
    }   
<?php endif; ?>    