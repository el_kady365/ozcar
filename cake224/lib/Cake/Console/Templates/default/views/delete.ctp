<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

 <?php echo '<?php ' ; ?>echo $this->Form->create('<?php echo $modelClass; ?>', array('action' => $this->action)); ?><div class="rounded-item config-msg">
    <b class="bl-corners"></b><b class="br-corners"></b>
    <div class="FormExtended confirm-message">
        <h3><?php echo '<?php' ; ?> echo __('Confirmation Message !', true); ?></h3>
        <?php echo '<?php' ;?> echo __('Are you sure you want to delete?'); ?>
        <div class='delete-items'>
            <?php echo '<?php '; ?> foreach ($<?php echo $pluralVar; ?> as $i => $<?php echo $singularVar; ?>): ?>
                <input type="hidden" value="<?php echo '<?php' ;?> echo $<?php echo $singularVar; ?>['<?php echo $modelClass; ?>']['id'] ?>" name="ids[]" />
                <span class="one-item"><?php echo '<?php '; ?> echo $this->Html->link($<?php echo $singularVar; ?>['<?php echo $modelClass; ?>']['id'], array('action' => 'edit', $<?php echo $singularVar ;?>['<?php echo $modelClass; ?>']['id']), array('target' => '_blank')) ?></span>
                <?php echo '<?php' ; ?> if (!empty($<?php echo $singularVar;?>[$i + 1])) { ?>
                    <span class="seprated-item">,</span>
                <?php echo '<?php' ;?> } ?>
            <?php echo '<?php' ;?> endforeach; ?>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="confirm-action">
    <button class="green-button" type="submit" name="submit_btn" value="yes" ><?php echo '<?php' ;?> echo __('Yes', true); ?></button>
    <button class="red-button" type="submit" name="submit_btn" value="no" ><?php echo '<?php' ; ?> echo __('No', true); ?></button>
</div>

<?php echo '<?php' ;?> echo $this->Form->end(); ?>