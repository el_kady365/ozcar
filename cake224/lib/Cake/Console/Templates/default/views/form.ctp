<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="<?php echo $pluralVar; ?> form ExtendedForm">
    <div class="form-wrap">
<?php
        $formType = ( in_array('image', $fields) || in_array('file', $fields) || in_array('pdf_file', $fields) ) ? ", array('type' => 'file') " : '';
        $hasEditors = false; 
        echo "\n\t<?php \n" ;
        echo "\t\$modelClass = '$modelClass' ; \n";
        
        echo "\techo \$this->Form->create(\$modelClass " . ( !empty($formType) ? $formType : '' ) . "); \n";
        ?>

<?php
        $modelObj = new $modelClass();
        $schema = $modelObj->schema();
        foreach ($fields as $field) {
            if (!in_array($field, array('created', 'modified', 'updated'))) {
                $type = '';
                $between = '';
                if (in_array($field, array('file', 'image', 'pdf_file'))) {
                    $type = 'file';
                    $btype = ($field == 'image') ? 'image' : 'file';
                    echo "\t\t\$field = '$field' ; \n";
                    $between = "\$this->element('{$btype}_input_between', array('info' => \$file_settings[\$field], 'field' => \$field, 'id' => (!empty(\$this->request->data[\$modelClass]['id']) ? \$this->data[\$modelClass]['id'] : null), 'base_name' => (!empty(\$this->request->data[\$modelClass][\$field]) ? \$this->request->data[\$modelClass][\$field] : '')))";
                }


                $class = 'input';
                if ($field == 'email') {
                    $class .= ' email';
                }
                if (!empty($schema[$field]) && !$schema[$field]['null']) {
                    $class .= ' required';
                }

                if (!empty($schema[$field]) && $schema[$field]['type'] == 'text') {
                    $class .= ' editor';
                    $hasEditors = true ;
                }

                if ($field == 'display_order') {
                    $class .= ' number';
                }


                echo "\t\techo \$this->Form->input('{$field}' , array('class' => '$class' " . (!empty($type) ? ", 'type' => '$type' " : "") . ( !empty($between) ? ", 'between' => $between " : "" ) . ") );\n";
            }
        }
        if (!empty($associations['hasAndBelongsToMany'])) {
            foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                echo "\t\techo \$this->Form->input('{$assocName}');\n";
            }
        }
        ?>
<?php
        echo "\n" ;
        if (empty($formType)) {
            echo "// ";
        }

        echo "\t\techo \$this->Form->enableAjaxUploads();\n";

        
        if (empty($hasEditors)) {
            echo "// ";
        }

        echo "\t\techo \$this->Form->enableEditors('textarea.editor'); \n";
        echo "\n" ;
        
        echo "\techo \$this->Form->end(__('Submit')); \n";
        echo "\t?>\n";

        ?>
    </div>
</div>
