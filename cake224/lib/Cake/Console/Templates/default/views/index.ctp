<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="<?php echo $pluralVar; ?> index">
    <h2><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h2>
    <?php echo "<?php \n"; 
    echo "\t echo \$this->List->filter_form(\$modelName, \$filters); \n" ;
    echo "\t\$fields = array(\n" ;
    
    foreach ($fields as $field) {
        if (!in_array($field, array('id', 'name', 'title', 'username', 'user', 'status', 'active', 'display_order', 'type', 'department', 'category', 'section', 'domain', 'category_id', 'department_id', 'default'))) {
            echo "//" ;
        }
        if ($field === 'display_order') {
            echo "\t\t'$modelClass.$field' => array() ,\n";
        } else{
            echo "\t\t'$modelClass.$field' => array('edit_link' => array('action' => 'edit' , '%id%')) , \n";
        }
        
    }
    
    echo "\t); \n";
?>
            $links = array(
                $this->Html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
                $this->Html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete') ), // , __('Are you sure?', true)),
            );

            $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

            echo $this->List->adminIndexList($fields, $<?php echo $pluralVar;
            ?>, $links,true, $multi_select_actions);

<?php echo "\n?>"; ?>

</div>
<?php /*
<div class="actions">
    <h3><?php echo "<?php echo __('Actions'); ?>"; ?></h3>
    <ul>
        <li><?php echo "<?php echo \$this->Html->link(__('New " . $singularHumanName . "'), array('action' => 'add')); ?>"; ?></li>
        <?php
        $done = array();
        foreach ($associations as $type => $data) {
            foreach ($data as $alias => $details) {
                if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                    echo "\t\t<li><?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index')); ?> </li>\n";
                    echo "\t\t<li><?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add')); ?> </li>\n";
                    $done[] = $details['controller'];
                }
            }
        }
        ?>
    </ul>
</div>
*/ ?>