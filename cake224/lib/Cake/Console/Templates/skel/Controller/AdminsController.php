<?php

class AdminsController extends AppController {

    var $name = 'Admins';
    var $uses = array();
    var $helpers = array('Html', 'Form');

    function admin_logout() {
        $this->Session->delete('admin');
        $this->redirect('/');
    }

    function __redirect(){
            $admin_url = array('controller' => 'pages', 'action' => 'index', 'prefix' => 'admin', 'admin' => true);
            $admin_redirecte = ($this->Session->read('admin_redirect')) ? $this->Session->read('admin_redirect') : '';
            $this->Session->delete('admin_redirect');
            if ($admin_redirecte) {
                header("location:http://" . $_SERVER["HTTP_HOST"] . $admin_redirecte);
            } else {
                $this->redirect($admin_url);
            }
            die('');
    }
    
    function admin() {
        $admin_url = array('controller' => 'pages', 'action' => 'index', 'prefix' => 'admin', 'admin' => true);
        if ($this->Session->read('admin')) {
            $this->__redirect();
            exit('');
        }
        if (!empty($this->data)) {
            $password = $this->data['Admin']['password'];
            $name = $this->data['Admin']['name'];
            $admin = ( ($name == $this->config['txt.admin_user_name']) && ($this->config['pwd.admin_password'] == md5($password)) );
            if ($admin) {
                $this->Session->write('admin', $admin);
                $this->__redirect();
            } else {
                $this->flashMessage(__('Invalid User name or password', true), 'Errormessage', 'AdminLogin');
            }
        }
        $this->layout = "";
        $this->AdminTitle = "Admin Login";
        $this->render('adminlogin');
    }

}

?>