<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = array('List');
    public $components = array('Session');

    function beforeFilter() {
        parent::beforeFilter();
        //$l10n = new L10n ( );
        //$l10n->get('eng');
        Configure::write('Config.language', 'en');
        //Loading configurations
        $this->__load_config();
        $this->set('config', $this->config);

        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];

        if (strtolower($prefix) == 'admin') {
            $this->__authenticate_admin();
            $this->layout = 'admin';
        }

        if (empty($this->titleAlias)) {
            $this->titleAlias = Inflector::humanize(Inflector::underscore($this->name));
        }
        $this->set('selectedMenu', $this->titleAlias);
        $this->set('titleAlias', $this->titleAlias);
    }

    function beforeRender() {
        //	$this->__change_title();
        $this->_set_seo();
        //	$this->load_topmenus();
        if ($this->request->is('ajax')) {
            $this->layout = '';
        }
    }

    function __load_config() {
        if (empty($this->config) && !is_array($this->config)) {
            $this->config = parse_ini_file(WWW_ROOT . '../app_config.ini');
        }
    }

    function __authenticate_admin() {
        if ($this->Session->read('admin')) {
            return true;
        } else {
            $this->Session->write('admin_redirect', $_SERVER["REQUEST_URI"]);
            $this->redirect('/admin/');
            die('');
        }
    }

    function flashMessage($message, $class = 'Errormessage', $key = 'flash') {
        $this->Session->setFlash($message, 'default', array('class' => $class), $key);
    }

    function admin_update_display_order() {
        //debug($this->params);
        $params = $this->params['data'];
        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName))
            $model = $this->modelName;
        foreach ($params as $field => $value) {
            if (strpos($field, 'display_order') === false) {
                continue;
            }
            $id = substr($field, strlen('display_order_'));
            $this->{$model}->create();
            $this->{$model}->id = $id;
            $this->{$model}->saveField('display_order', intval($value));
        }
        if ($this->request->is('ajax'))
            die(json_encode(array('status' => 'success', 'message' => 'Display Order Updated Successfully')));
        else
            $this->redirect($this->referer(array('action' => 'index'), true));
    }

    function admin_update_active($field_name = 'active') {
        $params = $this->params['data'];

        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName))
            $model = $this->modelName;
        foreach ($params as $field => $value) {
            if (strpos($field, $field_name) === false) {
                continue;
            }
            $id = substr($field, strlen($field_name . '_'));
            $this->{$model}->create();
            $this->{$model}->id = $id;
            $this->{$model}->saveField($field_name, intval($value));
        }
        $this->redirect($this->referer(array('action' => 'index'), true));
    }

    function _set_seo() {
        if (empty($this->metaDescription)) {
            $this->metaDescription = $this->config['txt.description'];
        }
        if (empty($this->metaKeywords)) {
            $this->metaKeywords = $this->config['txt.keywords'];
        }
        if (empty($this->metaRobots)) {
            $this->metaRobots = $this->config['txt.robots'];
        }

        $this->set('metaDescription', h(substr(preg_replace('/\\s+/', ' ', $this->metaDescription), 0, 500)));
        $this->set('metaKeywords', $this->metaKeywords);
        $this->set('metaRobots', $this->metaRobots);
    }

    function __change_title($title = false) {
        $this->loadModel('Seo');
        $record = $this->Seo->find(array('"' . $this->here . '" LIKE `criteria`'));
        if (!empty($record)) {
            $this->pageTitle = $record['Seo']['title'];
            $this->metaDescription = $record['Seo']['description'];
            $this->metaKeywords = $record['Seo']['keywords'];
            return true;
        }


        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
        if (strtolower($prefix) == 'admin') {

            $prefix = empty($this->params['prefix']) ? false : $this->params['prefix'];
            $action = $this->params['action'];
            if ($prefix) {
                $titleArr[] = Inflector::humanize($this->params['prefix']);
                $action = substr($action, strlen("{$prefix}_"));
            }
            $titleArr[] = $this->titleAlias;
            if (strtolower($action) != 'index') {
                $titleArr[] = Inflector::humanize($action);
            }
        } else {
            $titleArr = array();
            $titleArr[] = $this->pageTitle;
        }

        $titleArr[] = $this->config['txt.site_name'];

        $this->pageTitle = implode(' - ', $titleArr);
    }

    function reset_filter() {
        $this->autoRender = false;
        $modelName = Inflector::singularize($this->name);

        if (!empty($this->modelName)) {
            $modelName = $this->modelName;
        }

        $this->Session->delete("{$modelName}_Filter");
        echo "Success";
    }

    function _filter_params($params = false) {

        $modelName = Inflector::singularize($this->name);
        $conditions = array();
        if (!empty($this->modelName)) {
            $modelName = $this->modelName;
        }

        $filters = false;

        if (!empty($this->{$modelName}->filters)) {
            $filters = $this->{$modelName}->filters;
        } else {
            $this->set(compact('filters', 'modelName'));
            return $conditions;
        }
        $this->set(compact('filters', 'modelName'));
        $url_params = $this->params['url'];
        unset($url_params['url'], $url_params['page'], $url_params['sort'], $url_params['direction']);
        $params = empty($params) ? $url_params : $params;
        $sessionParams = $this->Session->read("{$modelName}_Filter");
        if ($sessionParams && empty($params))
            $params = $sessionParams;
        elseif (!empty($params)) {
            $this->Session->write("{$modelName}_Filter", $params);
        }

        foreach ($filters as $field => $filters) {
            if (is_numeric($field)) {
                $field = $filters;
                $type = '=';
                $param = $field;
            } elseif (is_string($filters)) {
                $type = $filters;
                $param = $field;
            } elseif (is_array($filters)) {
                $type = empty($filters['type']) ? '=' : $filters['type'];
                $param = $field;
            }
            switch (strtolower($type)) {
                case '=':
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field"] = $params[$param];
                    }
                    break;
                case 'like':
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field LIKE"] = "%{$params[$param]}%";
                    }
                    break;
                case 'date_range':
                    $from_param = empty($filters['from']) ? "from" : $filters['from'];
                    $to_param = empty($filters['from']) ? 'to' : $filters['to'];
                    if (!empty($params[$from_param])) {

                        $conditions["$modelName.$field >="] = date('Y-m-d 00:00:00', strtotime($params[$from_param]));
                    }
                    if (!empty($params[$to_param])) {

                        $conditions["$modelName.$field <="] = date('Y-m-d 23:59:59', strtotime($params[$to_param]));
                    }
                    break;
                case 'number_range':
                    $from_param = empty($filters['from']) ? 'from' : $filters['from'];
                    $to_param = empty($filters['from']) ? 'to' : $filters['to'];
                    if (!empty($params[$from_param]) || (isset($params[$param]) && strval($params[$from_param]) === '0')) {
                        $conditions["$modelName.$field >="] = $params[$from_param];
                    }
                    if (!empty($params[$to_param]) || (isset($params[$to_param]) && strval($params[$to_param]) === '0')) {
                        $conditions["$modelName.$field <="] = $params[$to_param];
                    }
                    break;
                default:
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field $type"] = $params[$param];
                    }
                    break;
            }
        }
        return $conditions;
    }

//    function get_snippet($name) {
//        $this->loadModel('Snippet');
//        $conditions['Snippet.name'] = $name;
//        $snippet = $this->Snippet->find('first', array('conditions' => $conditions));
//        return $snippet['Snippet']['content'];
//    }

    function load_topmenus() {
        $this->loadModel('Page');

        $conditions = array();
        $conditions['Page.active'] = 1;
        $conditions['Page.add_to_main_menu'] = 1;
        $conditions[] = "(Page.menu_id = 0 OR Page.menu_id IS NULL)";
        $topmenus = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order'));

        $this->set('topmenus', $topmenus);
    }

    //function loadModel($modelName = null, $id = null){
    //  parent::loadModel($modelName, $id);
    // $this->$modelName->alias = 'Site' . $modelName ;
    //  }
}

