<?php

class ConfigurationsController extends AppController {

    public $name = 'Configurations';
    public $uses = array();
    public $helpers = array('Html', 'Form', 'googleMap');
    public $extensions = array(
        'txt' => 'TEXT',
        'pwd' => 'PASSWORD'
    );

    function admin_edit() {
        $formData = $this->data;
        $this->set('extensions', $this->extensions);
        if (!empty($formData)) {
            $formData['map_map_coordiate'] = "0,0,0";
            if (!empty($this->data['Configurations']['map_map_coordiate'])) {
                unset($formData['Configurations']);
                $formData['map_map_coordiate'] = "{$this->data['Configurations']['map_map_coordiate']}";
            }
            $output = '';
            $confFile = WWW_ROOT . '../app_config.ini';
            

            foreach ($formData as $key => $value) {
                $key = preg_replace("/_/", '.', $key, 1);
                $ext = substr($key, 0, strpos($key, '.'));
                if ($ext == 'pwd') {
                    if (!empty($value)) {
                        $value = md5($value);
                    } else {
                        $value = $this->config[$key];
                    }
                }
                $value = str_replace('"', '""', $value);
                $output .= "$key = \"$value\"\n";
            }

            if (is_writable($confFile)) {
                if (file_put_contents($confFile, $output) !== false) {                    
                    $this->flashMessage('Configurations saved successfully.', 'Sucmessage');
                }
            } else {
                $this->flashMessage('The configuration file is not writable.');
            }
            $this->redirect(array('action' => 'admin_edit'));
        }


    }

}

?>
