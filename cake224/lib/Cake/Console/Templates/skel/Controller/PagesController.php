<?

/**
 * @property Page Page
 *
 */
class PagesController extends AppController {

    public $name = 'Pages';
    public $helpers = array('Html', 'Form');
    public $paginate = array() ;
    
    
    function admin_menu() {
        
    }

    function admin_index() {
        $conditions = $this->_filter_params();
        $this->Page->recursive = 0;
        $this->paginate['Page']['order'] = 'Page.id desc';
        $this->set('menus', $this->Page->find('list', array('conditions' => array('(Page.menu_id = 0 OR Page.menu_id IS NULL)', 'Page.add_to_main_menu' => 1))));

        $this->set('submenus', $this->Page->find('list', array('conditions' => array('Page.submenu_id' => 0))));
        $this->set('pages', $this->paginate('Page', $conditions));
    }

    function admin_add($category_id=false) {
        if (!empty($this->data)) {
            if (!empty($this->data['Page']['attachments'])) {
                $attachments = array_map('array_filter', $this->data['Page']['attachments']);
                $this->data['Page']['attachments'] = json_encode($attachments);
            }

            if (isset($this->data['Page']['category_id'])) {
                $this->data['Page']['type'] = 'categories';
                $this->data['Page']['id_on_table'] = $this->data['Page']['category_id'];
            }

            $this->Page->create();
            if ($this->Page->save($this->data)) {
                $this->flashMessage(__('The Page has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Page could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data) && $category_id) {
            $this->loadModel('Category');
            $category = $this->Category->findById($category_id);
            $this->data['Page']['category_id'] = $category['Category']['id'];
            $this->data['Page']['title'] = $category['Category']['title'];
            $this->data['Page']['permalink'] = $category['Category']['permalink'];
            $this->data['Page']['url'] = '/products/index/' . $category['Category']['permalink'];
            $this->data['Page']['add_to_main_menu'] = true;
            $this->data['Page']['active'] = true;
        }

        $this->set('menus', $this->Page->find('list', array('conditions' => array('(Page.menu_id = 0 OR Page.menu_id IS NULL)', 'Page.add_to_main_menu' => 1))));

        //$this->set('templates', $this->Page->Template->get_templates_list());
        /*  $this->loadModel('Video');
          $this->set('videos', $this->Video->find('list'));
          $this->loadModel('Document');
          $this->set('documents', $this->Document->find('list'));
          $this->loadModel('Link');
          $this->set('links', $this->Link->find('list')); */
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Page', true));
            $this->redirect(array('action' => 'index'));
        }

        if (!empty($this->data)) {

            if (!empty($this->data['Page']['attachments'])) {
                $attachments = array_map('array_filter', $this->data['Page']['attachments']);
                $this->data['Page']['attachments'] = json_encode($attachments);
            }

            if ($this->Page->save($this->data)) {
                $this->flashMessage(__('The Page has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Page could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Page->read(null, $id);
        }

        $this->set('menus', $this->Page->find('list', array('conditions' => array('(Page.menu_id = 0 OR Page.menu_id IS NULL)', 'Page.add_to_main_menu' => 1, 'Page.id <> ' => $id))));
        //$this->set('templates', $this->Page->Template->get_templates_list());
        /*
          $this->loadModel('Video');
          $this->set('videos', $this->Video->find('list'));
          $this->loadModel('Document');
          $this->set('documents', $this->Document->find('list'));
          $this->loadModel('Link');
          $this->set('links', $this->Link->find('list')); */

        $this->render('admin_add');
    }


    function home() {
        $this->pageTitle = "Homepage";

    }

    //-----------------------

    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid id for Page', true));
            $this->redirect(array('action' => 'index'));
        }

        $page = $this->Page->findById($id);
        if (!empty($page['Page']['default'])) {
            $this->flashMessage(__('Default Page could not be deleted', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Page->delete($id)) {
            $this->flashMessage(__('Page deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(__('Invalid id for Page', true));
            $this->redirect(array('action' => 'index'));
        }
    }

    function admin_delete_multi() {
        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for Page', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Page->deleteAll(array('Page.id' => $_POST['ids'], 'Page.default' => 0))) {
            $this->flashMessage(__('Page items deleted', true), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action' => 'index'));
        }
    }

    function index($permalink) {

        $data = $this->Page->find('first', array('conditions' => array('Page.permalink' => $permalink, 'Page.active' => 1)));
        if (empty($data)) {
            $this->cakeError('error404');
        }
//        $this->__is_item_authenticated($data['Page']);

        $content = $data[$this->Page->name]['content'];
        $contents = $content;

//        if (!empty($data['Page']['attachments']['links'])) {
//            $this->get_links($data['Page']['attachments']['links']);
//        }
//        if (!empty($data['Page']['attachments']['documents'])) {
//            $this->get_resources($data['Page']['attachments']['documents']);
//        }
//        if (!empty($data['Page']['attachments']['videos'])) {
//            $this->get_videos($data['Page']['attachments']['videos']);
//        }

        $this->pageTitle = $data[$this->Page->name]['title'];
        $this->set('contents', $contents);
        $this->set('page_id', $data['Page']['id']);
        $this->set('page_title', $data[$this->Page->name]['title']);
    }

    function admin_get_submenus($menu_id=null, $submenu=null) {
        Configure::write('debug', 0);
        $this->autoRender = false;
        if (empty($menu_id)) {
            json_encode(array("submenus" => array()));
            exit();
        }
        if (!empty($submenu)) {
            $conditions[] = "Page.id<>$submenu";
        }
        $conditions['Page.menu_id'] = $menu_id;
        $conditions['Page.submenu_id'] = 0;
        $this->Page->recursive = -1;
        $submenus = array();
        $submenus = $this->Page->find('all', array('conditions' => $conditions, 'fields' => array('id', 'title')));
        echo json_encode(array("submenus" => $submenus));
        exit();
    }

    function menus() {
        $this->loadModel('Page');
        $conditions['Page.active'] = 1;
        $conditions[] = "(Page.menu_id = 0 OR Page.menu_id IS NULL)";
        $conditions['Page.add_to_main_menu'] = 1;
        $pages = array();
        $menus = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order', 'recursive' => -1));

        foreach ($menus as $i => $menu) {

            $pages[$i]['Menu'][$i] = $menu['Page'];
            $conditions = array();
            $conditions['Page.active'] = 1;
            $conditions['Page.menu_id'] = $menu['Page']['id'];
            $menus2 = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order', 'recursive' => -1));

            foreach ($menus2 as $j => $menu2) {

                $pages[$i]['Page'][$j] = $menu2['Page'];
                $conditions = array();
                $conditions['Page.active'] = 1;
                $conditions['Page.submenu_id'] = $menu2['Page']['id'];
                $menus3 = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order', 'recursive' => -1));

                foreach ($menus3 as $k => $menu3) {
                    $pages[$i]['submenu'][$k] = $menu3['Page'];
                }
            }
        }
        exit();
    }

    function test($name) {
        $this->render($name);
    }

    function static_page($page) {
        $this->render($page);
    }

}

?>