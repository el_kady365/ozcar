<?php
App::uses('AppController', 'Controller');
/**
 * SiteConfigs Controller
 *
 * @property SiteConfig $SiteConfig
 */
class SiteConfigsController extends AppController {



    /**
    * admin_edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function admin_edit() {
            if ($this->request->is('post') || $this->request->is('put')) {
                    $valid = $this->__submit_common();
                    if ($valid && $this->SiteConfig->saveAll($this->request->data)) {
                            $this->flashMessage(__('The site config has been saved'), 'Sucmessage');
                            $this->redirect(array('action' => 'index'));
                    } else {
                    $this->flashMessage(__('The site config could not be saved. Please, try again.'));
			}
            } else {
                    $this->request->data = $this->SiteConfig->find('all');
            }
		$this->__form_common();
		$this->render('admin_add') ;
    }
    

    /**
     * common method for add and edit
     *
     * @return boolean 
     */
    function __form_common() {
	debug($this->data);
        $map = array();
        if (!empty($this->request->data)) {
            $map = Set::combine($this->request->data, array('{0}.{1}', '{n}.SiteConfig.group', '{n}.SiteConfig.setting'), '{n}.SiteConfig.id');
            $map = array_combine(array_keys($map), array_keys($this->request->data));

            $dbSettings = array_diff(array_keys($map), array_keys($this->SiteConfig->getStructure()));

            foreach ($dbSettings as $setting) {
                $ex = explode('.', $setting);
                if (count($ex) == 2 && !empty($ex[0])) {
                    $gid = $ex[0];
                    $varid = $ex[1];
                } else {
                    $gid = 'misc';
                    $varid = $ex[0];
                }
                $this->SiteConfig->vars[$gid]['vars'][$varid] = array('title' => Inflector::humanize($varid), 'input' => array('class' => 'from-db'));
            }
            
        }
        $this->set('config_schema', $this->SiteConfig->vars);

        $this->set('map', $map);

        return true;
    }

    /**
     * common method for add and edit submissions
     *
     * @return boolean 
     */
    function __submit_common() {        
        if (!empty($this->request->data)) {
            $map = Set::combine($this->request->data['SiteConfig'], array('{0}.{1}', '{n}.group', '{n}.setting'), '{n}.id');
            $map = array_combine(array_keys($map), array_keys($this->request->data['SiteConfig']));
            
            foreach ($map as $key => $id) {
                $structure = $this->SiteConfig->getStructure($key);
                
                if (!empty($structure['compare'])){
                    if($this->request->data['SiteConfig'][$id]['value'] != $this->request->data['SiteConfig'][$map[$structure['compare']]]['value']){
                        $this->SiteConfig->validationErrors = array($id => array('value' => array("Values do not match")), $map[$structure['compare']] => array('value' => array("Values do not match")));
                        return false;
                    }
                }
                if (!empty($structure['nosave']))
                    unset($this->request->data['SiteConfig'][$id]);
            }
        }


        return true;
    }
	
   
}
