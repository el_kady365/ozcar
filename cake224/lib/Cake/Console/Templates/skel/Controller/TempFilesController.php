<?php

App::uses('AppController', 'Controller');

/**
 * TempFiles Controller
 *
 */
class TempFilesController extends AppController {

    /**
     * Scaffold
     *
     * @var mixed
     */
    //public $scaffold;

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->TempFile->save($this->data)) {
                $this->flashMessage('Saved!', 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage('Error!');
            }
        }
        // $this->set('file_settings', $this->TempFile->getFileSettings());
    }

    function admin_edit($id = null) {
        if (!$id) {
            $this->flashMessage('Invalid TempFile');
            $this->redirect(array('action' => 'index'));
        }

        if (!empty($this->data)) {
            if ($this->TempFile->save($this->data)) {
                $this->flashMessage('Saved!', 'Sucmessage');
                //  $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage('Error!');
            }
        } elseif (empty($this->data)) {
            $this->data = $this->TempFile->read(null, $id);
            if (empty($this->data)) {
                $this->flashMessage('Invalid TempFile');
            }
        }
        //  $this->set('file_settings', $this->TempFile->getFileSettings());
        $this->render('admin_add');
    }

    function admin_index() {
        $conditions = $this->_filter_params();
        $tempFiles = $this->paginate('TempFile', $conditions);
        $this->set(compact('tempFiles'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage('Invalid TempFile');
            $this->redirect(array('action' => 'index'));
        }

        $this->TempFile->id = $id;
        $tempfile = $this->TempFile->read();
        $this->TempFile->Behaviors->attach(!empty($tempfile['TempFile']['behavior']) ? $tempfile['TempFile']['behavior'] : 'File', array('name' => array('folder' => $this->TempFile->folder)));

        if ($this->TempFile->delete())
            $this->flashMessage('Deleted','Sucmessage');
        else {
            $this->flashMessage('Invalid TempFile');
        }
        //  die();
        $this->redirect(array('action' => 'index'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage('Invalid TempFile');
            $this->redirect(array('action' => 'index'));
        }

        $this->TempFile->id = $id;
        $TempFile = $this->TempFile->read(null, $id);
        debug($tempFile);
        if (!$TempFile) {
            $this->flashMessage('Invalid TempFile');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->set(compact('tempFile'));
        }
    }

    function admin_delete_multi() {
        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for TempFiles', true));
            $this->redirect(array('action' => 'index'));
        }

        $error = array();

        foreach ($_POST['ids'] as $id) {

            $this->TempFile->id = $id;
            $tempfile = $this->TempFile->read();
            $this->TempFile->Behaviors->attach(!empty($tempfile['TempFile']['behavior']) ? $tempfile['TempFile']['behavior'] : 'File', array('name' => array('folder' => $this->TempFile->folder)));

            if (!$this->TempFile->delete())
                $error[] = 'Error deleteing TempFile #' . $id;

            $this->TempFile->Behaviors->unload(!empty($tempfile['TempFile']['behavior']) ? $tempfile['TempFile']['behavior'] : 'File');
        }

        if (empty($error)) {
            $this->flashMessage('Temp Files successfully deleted.. ', 'Sucmessage');
        } else {
            $this->flashMessage(implode('<bt/>', $error));
        }

        $this->redirect(array('action' => 'index'));
    }

    function upload() {
        //Configure::write('debug', 0);
        if (!empty($this->data) || $_SERVER['REQUEST_METHOD'] === 'POST') {

            $modelname = !empty($this->data) ? array_keys($this->data) : array_keys($this->params->query['data']);  //preg_replace("/data\[(.*?)\]\[.*?\].*/", '\1', $inputName  );;

            $modelname = $modelname[0];
            $this->loadModel($modelname);


            $allowedmodels = $this->TempFile->allowedModels;
            $targetbehaviors = $this->TempFile->targetBehaviors;


            $targetbehaviors = array_combine($targetbehaviors, $targetbehaviors);

            $foundbehaviors = array_intersect($targetbehaviors, array_keys($this->{$modelname}->actsAs));

            $validmodel = in_array($modelname, $allowedmodels);
            if (!$validmodel || empty($foundbehaviors)) {
                die(json_encode(array('success' => false, 'error' => 'Permission denied')));
            }

            $filtered_behaviors = array_intersect_key($this->{$modelname}->actsAs, $targetbehaviors);

            foreach ($filtered_behaviors as $behavior => &$options) {
                if (!empty($options)) {
                    foreach ($options as &$option) {
                        $option['folder'] = $this->TempFile->folder;
                    }
                }
                else
                    $options = array(stripos($behavior, 'image') !== false ? 'image' : 'file' => array('folder' => $this->TempFile->folder));
            }

            foreach ($filtered_behaviors as $bhv => $opts)
                if (!empty($bhv))
                    $this->TempFile->Behaviors->attach($bhv, $opts);

            $error = array();
            $hash = md5(rand() . uniqid() . rand());


            if (!isset($_SERVER['CONTENT_TYPE'])) { // 
; // do nothing
            } else if (strpos(strtolower($_SERVER['CONTENT_TYPE']), 'multipart/') === 0) { // hidden iframe form submission
                foreach ($this->data[$modelname] as $i => $file) {
                    $behavior = $this->_getFieldBehavior($filtered_behaviors, $i);
                    if (!$behavior) {
                        $error[] = 'Permission Denied';
                    } elseif (!$this->TempFile->save(array('TempFile' => array($i => $file, 'hash' => $hash, 'behavior' => $behavior, 'uploaded_for' => "$modelname.$i")))) {
                        $error[] = reset($this->TempFile->validationErrors);
                    }
                }
            } else { // ajax upload
                $input = fopen("php://input", "r");
                $temp = tmpfile();
                $realSize = stream_copy_to_stream($input, $temp);
                fclose($input);

                $meta_data = stream_get_meta_data($temp);

                $tmp_name = $meta_data["uri"];

                $field = array_keys($this->params->query['data'][$modelname]);
                $field = reset($field);

                $behavior = $this->_getFieldBehavior($filtered_behaviors, $field);

                $this->data = array('TempFile' => array($field => array(
                            'name' => $this->params->query['data'][$modelname][$field],
                            'tmp_name' => $tmp_name,
                            'size' => filesize($tmp_name),
                            'type' => 'application/octet-stream',
                            'error' => 0
                        ),
                        'hash' => $hash,
                        'behavior' => $behavior,
                        'uploaded_for' => "$modelname.$field",
                        ));

                if (!$behavior) {
                    $error[] = 'Permission Denied';
                } elseif (!$this->TempFile->save($this->data)) {
                    $error[] = reset($this->TempFile->validationErrors);
                }
            }
            header('Content-Type: text/plain'); // respond as text/plain so ie can parse json correctly

            if (empty($error)) {
                //  $this->flashMessage('Success');
                die(json_encode(array('success' => true, 'hash' => $hash)));
            } else {
                //   $this->flashMessage(count($error) . 'Error(s) occured <br/>' . implode('<br/>', ($error)));
                die(json_encode(array('success' => false, 'error' => implode('<br/>', $error))));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function form() {
        
    }

    function _getFieldBehavior($behaviors, $field) {
        foreach ($behaviors as $behavior => $options) {
            if (array_key_exists($field, $options))
                return $behavior;
        }
        if ($field == 'file')
            return 'File';
        return false;
    }

}
