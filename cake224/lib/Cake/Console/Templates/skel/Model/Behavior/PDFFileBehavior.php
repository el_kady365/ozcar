<?php

App::uses('FileBehavior', 'Model/Behavior');

class PDFFileBehavior extends FileBehavior {

    protected static $_defaults = array(
        'pdf' => array(
            'max_file_size' => '2 MB',
            'extensions' => array('pdf'),
        )
    );

    /**
     * Behaviour initialization
     * @param AppModel $model
     * @param array $config
     */
    function setup(Model $model, $config = array()) {
        $thisdefaults = array();
        $defaults = array();

        $thisdefaults['pdf'] = array_merge(parent::$_defaults['file'], self::$_defaults['pdf']);

        if (!empty($config)) {
            foreach ($config as $confField => $conf) {
                $defaults[$confField] = array_merge($thisdefaults['pdf'], $config[$confField]);
            }
        } else {
            $defaults = $thisdefaults;
        }
        
        parent::setup($model, $defaults);
    }

}

?>