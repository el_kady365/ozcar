<?php

App::uses('AppModel', 'Model');

/**
 * SiteConfig Model
 *
 * @property Site $Site
 * @property Site $Site
 */
class SiteConfig extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'value';
    public $virtualFields = array(
        'var' => 'CONCAT(SiteConfig.group, ".", SiteConfig.setting)'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'setting' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'type' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'active' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $vars = array(
        'group1' => array(
            'title' => 'Group1',
            'vars' => array(
                'site_name' => array(
                    'title' => 'Site Name',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'notEmpty',
                            'required' => true
                        ),
                    ),
                ),
                'user_name' => array(
                    'title' => 'User Name',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'notEmpty',
                            'required' => true
                        ),
                    ),
                ),
                'password' => array(
                    'title' => 'Password',
                    'input' => array(
                        'type' => 'password',
                        'value' => '',
                        'title' => 'Leave blank to use current password',
                        'placeholder' => 'Type in to set or change password...',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'notEmpty',
                            'required' => 'create',
                            'allowEmpty' => true,
                            'on' => 'update'
                        ),
                        'size' => array(
                            'rule' => array('between', 8, 32),
                            'message' => 'Password should be at least 8 chars long'
                        ),
                    ),
                ),
                'repassword' => array(
                    'title' => 'Retype Password',
                    'input' => array(
                        'type' => 'password',
                        'value' => '',
                    ),
                    'nosave' => true,
                    'compare' => 'group1.password',
                ),
                'admin_email' => array(
                    'title' => 'Admin Email',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'email',
                            'required' => true
                        ),
                    ),
                ),                
                'send_email_from' => array(
                    'title' => 'Send Email From',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                        'required' => array(
                            'rule' => 'email',
                            'required' => true
                        ),
                    ),
                ),                
                'keywords' => array(
                    'title' => 'Keywords',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                    ),
                ),
                'description' => array(
                    'title' => 'Description',
                    'input' => array(
                        'type' => 'textarea',
                    ),
                    'validation' => array(
                    ),
                ),                
                'robots' => array(
                    'title' => 'Robots',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                    ),
                ),
                'currency_symbol' => array(
                    'title' => 'Currency Symbol',
                    'input' => array(
                        'type' => 'text',
                        'default' => '$',
                    ),
                    'validation' => array(
                    ),
                ),
                'google_analytics' => array(
                    'title' => 'Google Analytics',
                    'input' => array(
                        'type' => 'textarea',                        
                    ),
                    'validation' => array(
                    ),
                ),
                'map_coordinates' => array(
                    'title' => 'Map Coordinates',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                    ),
                ),
                'mobile' => array(
                    'title' => 'Mobile',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'validation' => array(
                    ),
                ),
                'fax' => array(
                    'title' => 'Fax',
                    'input' => array(
                        'type' => 'text',
                    ),
                    'nosave' => true,
                    'validation' => array(
                        'numeric' => array(
                            'rule' => array('numeric'),
                            'message' => 'Invalid number!'
                        ),
                    ),
                ),
                'checked' => array(
                    'title' => 'Checked?',
                    'input' => array(
                        'type' => 'checkbox',
                    ),
                    'validation' => array(
                        'boolean' => array(
                            'rule' => 'boolean',
                            'message' => 'invalid value!'
                        ),
                    ),
                ),
                'file' => array(
                    'title' => 'PDF',
                    'actsAs' => array('File' => array()),
                    'input' => array(
                        'type' => 'file',
                    ),
                    'validation' => array(
                    ),
                ),
            )
        ),
    );

    public function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        $this->validator()->remove('value');
        foreach ($this->vars as $configGroup) {
            if (!empty($configGroup['vars'][$this->data[$this->alias]['setting']]['validation'])) {
                $this->validator()->add('value', $configGroup['vars'][$this->data[$this->alias]['setting']]['validation']);
            }
            if (!empty($configGroup['vars'][$this->data[$this->alias]['setting']]['actsAs'])) {
                foreach($configGroup['vars'][$this->data[$this->alias]['setting']]['actsAs'] as $behavior => $options){
                    $this->Behaviors->attach($behavior, array('value' => $options));
                }
            }
        }
    }

    public function beforeSave($options = array()) {
        if (in_array(strtolower($this->data[$this->alias]['setting']), array('password', 'passwd'))) {
            if (!empty($this->data[$this->alias]['value']) || (isset($this->data[$this->alias]['value']) && is_numeric($this->data[$this->alias]['value']))) {
                $this->data[$this->alias]['value'] = Security::hash($this->data[$this->alias]['value']);
            } else {
                unset($this->data[$this->alias]['value']);
            }
        }
        return true;
    }

    public function getStructure($var = null) {
        $ret = array();

        if (!empty($var) && strpos($var, '.') !== false) {
            $ex = explode('.', $var);
            $gid = $ex[0];
            $varid = $ex[1];
            return $this->vars[$gid]['vars'][$varid];
        }
        foreach ($this->vars as $gid => $group) {
            foreach ($group['vars'] as $varid => $var) {
                $ret[$gid . '.' . $varid] = $var;
            }
        }
        return $ret;
    }

    public function decode_var($var) {
        if (strpos($var, '.') === false)
            return false;
        $object = new stdClass();
        $ex = explode('.', $var);
        $object->group = $ex[0];
        $object->setting = $ex[1];
        return $object;
    }

    public function get($var = null) {
        $conditions = array();
        if (!empty($var)) {
            $ex = explode('.', $var);
            $gid = '';
            $dot_var = $var;
            if (count($ex) == 2 && !empty($ex[0])) {
                $gid = $ex[0];
                $var = $ex[1];
            }
            if (!empty($gid)) {
                $conditions['SiteConfig.group'] = $gid;
            }
            $conditions['SiteConfig.setting'] = $var;
        }

        $list = $this->find('list', array('conditions' => $conditions, 'fields' => array('var', 'value')));
        if (!empty($var) && count($list) == 1) {
            $list = reset($list);
            return $list[$dot_var];
        } else {
            return $list;
        }
    }

}
