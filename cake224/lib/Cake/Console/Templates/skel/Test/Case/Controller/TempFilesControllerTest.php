<?php
App::uses('TempFilesController', 'Controller');

/**
 * TempFilesController Test Case
 *
 */
class TempFilesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.temp_file'
	);

}
