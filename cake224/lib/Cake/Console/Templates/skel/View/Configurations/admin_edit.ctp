<h1>Configurations</h1>
<div class='FormExtended'>
    <form action='<?= Router::url(array('controller' => 'configurations', 'action' => 'admin_edit')) ?>' method="post">
        <?php
        foreach ($config as $key => $value) {

            $ext = substr($key, 0, strpos($key, '.'));
            $name = substr($key, strpos($key, '.') + 1);
            if ($ext == 'pwd') {
                $value = '';
            }
            if ($ext == 'map') {
                /* @var $googleMap GoogleMapHelper */
                $map = explode(',', $config['map.map_coordiate']);
//                echo '<div class="input text">';
//                echo $googleMap->embed('Configurations', 'map_map_coordiate1', 'Map coordinates', 'latitude', 'longitude', 'zoom', false, $map[0], $map[1], $map[2]);
//                echo '</div>';
                
        if(count($map)!=3)	
//	 $map = explode(',', $config['map.map_coordiate']);
//	 else
	 $map = explode(',', "31.10665029999999,29.76791090000006,13");
	 
      echo '<div class="input text">';
      echo $this->GoogleMap->map_selector('Configurations', 'map_map_coordiate', 'Map coordinates', $map[0], $map[1], $map[2]);
	  echo '</div>';
                
                
            } else {
                ?>
        <div class="input <?=in_array($key, array('txt.keywords', 'txt.description' , 'txt.google_analytics'))?"textarea":"text"?>">
                <label for="<?= $key ?>"><?= Inflector::humanize($name) ?></label>
                <?php
                if (in_array($key, array('txt.keywords', 'txt.description' , 'txt.google_analytics'))) {
                    ?>
                    <textarea class='INPUT' rows="6" cols="30"  name="<?= $key ?>" id='<?= $key ?>'><?= $value ?></textarea>
                    <?php
                } else {
                    ?>
                    <input id='<?= $key ?>' type='<?= $extensions[$ext] ?>' name="<?= $key ?>" value="<?= $value ?>" class='INPUT' />
                    <?php
                }
                ?></div><?php
            }
        }
        ?>
        <div class="submit">
        <input type="submit" class="Submit" value='Submit' />
        </div>
    </form>
</div>