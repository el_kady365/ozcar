<div class="file_properties file-upload">
    <p class="note">File formats (<?php echo implode(',', $info['extensions']); ?>), Max file size:<?php echo $info['max_file_size'] ;?>   </p>

    <? if ($id && $base_name && !is_array($base_name)) { ?>
        <span class="file-name"><?php echo $base_name; ?></span><a target="_blank" class="button-secondary" href="<?php echo Router::url('/' . $info['folder'] . $base_name); ?>"><?php echo __('Download'); ?></a><?
    echo $this->Html->link(__('Delete', true), array('action' => 'delete_field', 'file', $id, $field), array('class' => 'button-secondary'), __('Are you sure you want to delete this image?', true));
}
    ?></div>

