<div class="logo_image file-upload input">
    <?php if ($info['width'] > 0 && $info['height'] > 0) { ?>
        <p class="note image_desc">Image size <?php echo $info['width']; ?>px X <?php echo $info['height']; ?>px   </p>
    <? } ?>
    <? if (isset($id) && isset($base_name) && !is_array($base_name) && !empty($base_name)) { ?>
        <span class="image_base_name file-name"><?php echo $base_name; ?></span>
        <a target="_blank" class="button-secondary" href="<?php echo Router::url('/' . $info['folder'] . $base_name); ?>"><?php echo __('Preview'); ?></a><?
    echo $this->Html->link(__('Delete', true), array('action' => 'delete_field', 'image', $id, $field), array('class' => 'button-secondary'), __('Are you sure you want to delete this image?', true));
}
    ?>
</div>

