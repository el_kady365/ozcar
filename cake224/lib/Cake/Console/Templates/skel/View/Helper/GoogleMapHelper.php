<?php

class GoogleMapHelper extends AppHelper {

    public $name = "GoogleMap";
    public $helpers = array('Js', 'Html', 'Form');
    public $count;

    /**
     * Google Map latitude , longitude  fitcher
     * @param string $model model name
     * @param string $field the field to save data to
     * @param string $label the label to give the input
     * @param string $prefix to sepeate between maps when morethan one are used
     * @param double $latitude the latitude to start with  , Default = -21.391705
     * @param double $longitude the longitude to start with , Default = 133.330078
     * @param string $google_key  default =''
     * @param integer $zoom the zoom to start with , Default = 4
     * @return string
     */
    function embed($model, $field, $label, $latfield, $longfield, $zoomfield, $prefix='', $latitude = false, $longitude = false, $zoom = false, $google_key = '') {
        if ($latitude === false)
            $latitude = empty($this->data[$model][$latfield]) ? 0 : $this->data[$model][$latfield];
        if ($longitude === false)
            $longitude = empty($this->data[$model][$longfield]) ? 0 : $this->data[$model][$longfield];
        if ($zoom === false)
            $zoom = empty($this->data[$model][$zoomfield]) ? 0 : $this->data[$model][$zoomfield];

        $this->count++;
        if ($prefix == '') {
            if ($this->count == 1)
                $prefix = '_1st';
            else if ($this->count == 2)
                $prefix = '_2nd';
            else if ($this->count == 3)
                $prefix = '_3rd';
            else
                $prefix = '_' . $this->count . 'th';
        }
        else
            $prefix = '_' . $prefix;

        $config = parse_ini_file(APP . DS . 'app_config.ini');


        $this->Html->script('http://maps.google.com/maps/api/js?sensor=false', false); //key='.$google_key,false);
        $this->Html->scriptBlock('
			var ' . $prefix . '_map = false;
			function ' . $prefix . '_initialize() {
				' . $prefix . '_map = new google.maps.Map(document.getElementById("' . $prefix . '_map_canvas"), {mapTypeId: google.maps.MapTypeId.ROADMAP, center: new google.maps.LatLng(' . $latitude . ', ' . $longitude . '),zoom: ' . $zoom . '});
				google.maps.event.addListener(' . $prefix . '_map, "click", function(event) {
					var latlng = event.latLng;
					document.getElementById("' . $field . '").value = latlng.lat()+","+latlng.lng()+","+' . $prefix . '_map.getZoom();
				});
			}


			$(function(){
			$(".' . $prefix . '_map").hide();
				$("#' . $prefix . '_browseMap").click(function(){
					$(".' . $prefix . '_map").toggle();
					if (!' . $prefix . '_map){
						' . $prefix . '_initialize();
					}
				});
				$("#' . $prefix . '_close").click(function(){
						$(".' . $prefix . '_map").hide();
				});
			});
			', array('inline' => false));
        $output = '
				<div class="Map_box map-selector" id="Map_Block">
					<label for="' . $prefix . '_Glatlng">' . $label . '</label>
					<p class="note">Single click on the location you want it to be the center of the map in the front end</p>
					<input class="left" type="text" name="data[' . $model . '][' . $field . ']" id="' . $field . '" value="' . $latitude . ',' . $longitude . ',' . $zoom . '" />
					<button class="button-secondary left" type="button" id="' . $prefix . '_browseMap">Browse Map</button>
					<div class="clear"></div>
<div class="' . $prefix . '_map Map_box map-popup"  style="position:absolute; z-index:400;" >
<button  class="button-secondary" type="button" id="' . $prefix . '_close" >Close</button>
					<div id="' . $prefix . '_map_canvas" style="width: 650px; height: 300px;">
					</div>
				   </div>
				</div>';
        return $output;
    }

    //-------------------------------------
    /*
     * $googleMap->create_map(30.216355,30.928574,10,array('height'=>240,'width'=>600,'direction'=>true));
     *
     */



    function map_selector($model, $field, $label, $latitude = false, $longitude = false, $zoom = false, $name = '') {

        $longitude = empty($longitude) ? 0 : $longitude;
        $latitude = empty($longitude) ? 0 : $latitude;
        $zoom = empty($longitude) ? 5 : $zoom;
        $prefix = "";
        $this->count++;
        if ($prefix == '') {
            if ($this->count == 1)
                $prefix = '_1st';
            else if ($this->count == 2)
                $prefix = '_2nd';
            else if ($this->count == 3)
                $prefix = '_3rd';
            else
                $prefix = '_' . $this->count . 'th';
        }
        else
            $prefix = '_' . $prefix;



        $this->Html->css('reveal', null, array('inline' => false));
        $this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places', false); //key='.$google_key,false);
       // $this->Html->script('http://code.jquery.com/jquery-1.6.min.js', false); //key='.$google_key,false);
        $this->Html->script('jquery.reveal.js', array('inline' => false));

        $this->Html->scriptBlock('
        
      function initialize() {
	  var myLatlng = new google.maps.LatLng(' . $latitude . ',' . $longitude . ');

        var mapOptions = {
          center: new google.maps.LatLng(' . $latitude . ',' . $longitude . '),
          zoom: ' . $zoom . ',
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var ' . $prefix . '_map = new google.maps.Map(document.getElementById(\'' . $prefix . 'map_canvas\'),
          mapOptions);

        var input = document.getElementById(\'' . $prefix . 'searchTextField\');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo(\'bounds\', ' . $prefix . '_map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: ' . $prefix . '_map,
		  position: myLatlng,
          title:"silvertrees"

        });

					marker.setAnimation(google.maps.Animation.BOUNCE);
			        marker.setMap(' . $prefix . '_map);
		
					google.maps.event.addListener(' . $prefix . '_map, \'click\', function(event) {
				   
					marker.setPosition(event.latLng);
				    document.getElementById(\'' . $field . '\').value=""+event.latLng.lat()+","+event.latLng.lng()+","+' . $prefix . '_map.getZoom();
				
			  });
			  
        google.maps.event.addListener(autocomplete, \'place_changed\', function() {
          infowindow.close();
          var place = autocomplete.getPlace();
          if (place.geometry.viewport) {
		    ' . $prefix . '_map.fitBounds(place.geometry.viewport);
          } else {
            ' . $prefix . '_map.setCenter(place.geometry.location);
            ' . $prefix . '_map.setZoom(17);  // Why 17? Because it looks good.
          }
	

          var image = new google.maps.MarkerImage(
              place.icon,
              new google.maps.Size(71, 71),
              new google.maps.Point(0, 0),
              new google.maps.Point(17, 34),
              new google.maps.Size(35, 35));
          //marker.setIcon(image);
            marker.setPosition(place.geometry.location);
		    document.getElementById(\'' . $field . '\').value=""+place.geometry.location.lat()+","+place.geometry.location.lng()+","+' . $prefix . '_map.getZoom();

          var address = \'\';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || \'\'),
              (place.address_components[1] && place.address_components[1].short_name || \'\'),
              (place.address_components[2] && place.address_components[2].short_name || \'\')
            ].join(\' \');
          }

        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
      
      }
      google.maps.event.addDomListener(window, \'load\', initialize);

      var OldData="";
      
$(\'#' . $prefix . 'myModal\').reveal({
     animation: \'fadeAndPop\',                   //fade, fadeAndPop, none
     animationspeed: 300,                       //how fast animtions are
     closeonbackgroundclick: false,              //if you click background will modal close?
     dismissmodalclass: \'close-reveal-modal\'    //the class of a button or element that will close an open modal
});
           
			  function setOldData(id,type)
			{
			   
			    if(type==0)
			    $("#"+id).val(OldData);
			    else
				OldData=$("#"+id).val();
				
			}
			
			
			
			function disableEnterKey(e)
			{
			     var key;     
			     if(window.event)
			          key = window.event.keyCode; //IE
			     else
			          key = e.which; //firefox     
			
			     return (key != 13);
			 }
		
			', array('inline' => false));
        if (isset($name) && $name != "")
            $field_name = $name;
        else
            $field_name = "data[$model][$field]";
        $output = '

          <label for="searchTextField">' . $label . '</label>
            <input  readonly=readonly type="text" class="INPUT"  name="' . $field_name . '" id="' . $field . '" value="' . $latitude . ',' . $longitude . ',' . $zoom . '" size=35>
               <a href="#" class="big-link button-secondary"  onclick="setOldData(\'' . $field . '\',1)" data-reveal-id="' . $prefix . 'myModal">
			            Change Map
		        </a>
		    <div id="' . $prefix . 'myModal" class="reveal-modal">
            <label for="' . $prefix . 'searchTextField">' . $label . '</label>
            <input  id="' . $prefix . 'searchTextField" class="INPUT"  onKeyPress="return disableEnterKey(event)" type="text" size="50">		   
		    <div id="' . $prefix . 'map_canvas" class="map-box" style="width: 500px; height:300px;"></div>
			<a href="javascript:void(0)" id="submitdata"  class="close-reveal-modal button-primary">Save Coordinates</a>
			<a href="javascript:void(0)" id="canceldata"  onclick="setOldData(\'' . $field . '\',0)" class="close-reveal-modal" >&nbsp;&nbsp;&nbsp;<strong>Or </strong>Cancel</a> 
		    </div>
           ';
        return $output;
    }

    function create_map($latitude = 0, $longitude = 0, $zoom = 10, $options = array()) {
        $default = array('width' => 646, 'height' => 240, 'direction' => false, 'country' => 'Australia');
        $options = array_merge($default, $options);

        echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false');
        echo $this->Html->scriptBlock("
                var map;
                var gdir;
                var geocoder = null;
                var center=null;
                var addressMarker;
                var directionDisplay;
                var directionsService  = new google.maps.DirectionsService();
                var latlng;
                function initialize() {
                    latlng = new google.maps.LatLng($latitude,$longitude);
                    directionsDisplay = new google.maps.DirectionsRenderer();
                    var settings = {
                        zoom: $zoom,
                        center: latlng,
                        mapTypeControl: true,
                        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                        navigationControl: true,
                        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById('map_canvas'), settings);
                    var companyPos = new google.maps.LatLng($latitude,$longitude);
                    var companyMarker = new google.maps.Marker({
                        position: companyPos,
                        map: map
                    });
                    directionsDisplay.setMap(map);
                    directionsDisplay.setPanel(document.getElementById('map_directions'));

                }
                $(document).ready(function(){
                    initialize();
                    $('#DirectionsForm').submit(function(){
                        calcRoute($('#txtAddress').val());
                        return false;
                    });
                });
                
                function calcRoute(address) {
                    var country = '{$options["country"]}';
                    var start = address + ','+country;
                    var end = latlng;
                    var request = {
                        origin:start,
                        destination:end,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    };
                    directionsService.route(request, function(response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(response);
                        }else{
                            alert('No Result Found.');
                        }
                    });
                }

            ");

        $output = "<div class='Map' id='map_canvas' style='width: {$options['width']}px; height: {$options['height']}px;'></div>";
        if ($options['direction']) {
            $output.="<form action='' id='DirectionsForm'>
                    <div class='directions_input'>
                        <label>Starting from:</label>
                        <input type='text' name='address' id='txtAddress'/>
                        <input type='submit' class='getdirections blackbutton' value='Get Directions' />
                    </div>
                </form>
                <div class='direction' id='map_directions' style='vertical-align: top;'></div>";
        }
        return $output;
    }

    /*
     * <img src='$googleMap->create_static_map(30.216355,30.928574,12,array('width'=>500,'height'=>240));' />
     * 
     */

    function create_static_map($latitude = 0, $longitude = 0, $zoom = 11, $options = array()) {
        $default = array('width' => 646, 'height' => 240);
        $options = array_merge($default, $options);
        $this->Html->script('http://maps.google.com/maps/api/js?sensor=false', false);
        $output = "
            http://maps.google.com/maps/api/staticmap?center=$latitude,$longitude&zoom=$zoom&size={$options['width']}x{$options['height']}&maptype=roadmap
            &markers=color:blue|label:S|$latitude,$longitude&sensor=false
        ";

        return $output;
    }

}

?>
