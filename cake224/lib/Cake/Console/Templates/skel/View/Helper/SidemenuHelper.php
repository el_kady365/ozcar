<?php

class SidemenuHelper extends AppHelper {

    public $helpers = array('Html');

    function outputAdminMenu($selected = null) {

        $adminMenu = array(
            __('Configurations', true) => array(
                'edit' => array('title' => __('Edit Configurations', true), 'url' => array('controller' => 'configurations', 'action' => 'edit')
                )
            ),
//            __('Enquiries', true) => array(
//                'list' => array('title' => __('List Enquiries', true), 'url' => array('controller' => 'contacts', 'action' => 'index')),
//                'list Orders' => array('title' => __('List Orders', true), 'url' => array('controller' => 'orders', 'action' => 'index'))
//            ),



            __('Items', true) => array(
                'list' => array('title' => __('List Items', true), 'url' => array('controller' => 'items', 'action' => 'index')),
                'add' => array('title' => __('Add Item', true), 'url' => array('controller' => 'items', 'action' => 'add')),
            ),
            __('Pages', true) => array(
                'list' => array('title' => __('List Pages', true), 'url' => array('controller' => 'pages', 'action' => 'index')),
                'add' => array('title' => __('Add Page', true), 'url' => array('controller' => 'pages', 'action' => 'add')),
                'list_snippets' => array('title' => __('List Snippets', true), 'url' => array('controller' => 'snippets', 'action' => 'index'))
            ),
//            __('Templates', true) => array(
//                'list' => array('title' => __('List Templates', true), 'url' => array('controller' => 'templates', 'action' => 'index')),
//                'add' => array('title' => __('Add Template', true), 'url' => array('controller' => 'templates', 'action' => 'add'))
//            ),

            __('System Emails', true) => array(
                'Edit system emails' => array(
                    'title' => 'Edit system emails',
                    'url' => array('plugin' => 'system_emails', 'controller' => 'system_emails', 'action' => 'index')
                )
            ),
            __('SEO', true) => array(
                'list' => array('title' => __('List SEO rules', true), 'url' => array('controller' => 'seo', 'action' => 'index')),
                'add' => array('title' => __('Add SEO rule', true), 'url' => array('controller' => 'seo', 'action' => 'add'))
            ),
            __('Misc', true) => array(
                'list-tempfiles' => array('title' => __('Temp Files', true), 'url' => array('controller' => 'temp_files', 'action' => 'index')),
            ),
//            __('Users', true) => array(
//                'list' => array('title' => __('List Users', true), 'url' => array('controller' => 'users', 'action' => 'index')),
//            ),
        );

        return $this->__outputMenu($adminMenu, $selected);
    }

    function outputSubAdminMenu($selected) {
        $subAdminMenu = array(
        );
        return $this->__outputMenu($subAdminMenu);
    }

    function __outputMenu($menu, $selected) {
        $output = '';
        $selected = empty($selected) ? strtolower($this->params['controller']) : $selected;
        foreach ($menu as $title => $content) {
            $class = (strtolower($title) == strtolower($selected)) ? 'current' : 'menu_options';

            $output .= "<li class='options'>";
            $output .= $this->Html->link($title, 'javascript: void(0)', array('class' => '%CLASS%', 'onclick' => "javascript: \$('#" . Inflector::slug($title) . "').slideToggle('fast'); return 0;"));
            $output .= "<ul id='" . Inflector::slug($title) . "' class='%CLASS%'>";

            foreach ($content as $item) {
                if ($item['url']["controller"] == $this->params['controller'])
                    $class = 'current';

                $output .= "<li>" . $this->Html->link($item['title'], $item['url']) . "</li>";
            }

            $output = str_replace('%CLASS%', $class, $output);
            $output .= '</ul>';
            $output .= '</li>';
        }
        return $output;
    }

}
