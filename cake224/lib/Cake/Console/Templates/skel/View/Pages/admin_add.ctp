<?php echo $this->Html->script('menu'); ?>

<div class="FormExtended">
    <?php echo $this->Form->create('Page'); ?>
    <?php
    if (isset($this->data['Page']['category_id'])) {
        echo $this->Form->input('category_id', array('class' => 'INPUT', 'type' => 'hidden'));
    }
    echo $this->Form->input('id', array('class' => 'INPUT'));
    echo $this->Form->input('title', array('class' => 'INPUT'));
    //  if (empty($this->data['Page']['default'])) {
    echo $this->Form->input('permalink', array('class' => 'INPUT'));
    //}
    ?>
    <div class="PageSelector">
        <h4>Page will act as: </h4>
        <div>
            <div class="tab-bar">
                <ul>
                    <li><a href="#" id="OpenPage" >Content Page</a> </li>
                    <li><a href="#" id="OpenLink" >Just a Link</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="LinkContainer" class="tab-content">
        <?php echo $this->Form->input('url', array('class' => 'INPUT', 'label' => 'URL')); ?>
        <?php echo $this->Form->hidden('is_url', array('class' => 'INPUT', 'id' => 'is_url', 'label' => __('Is URL', true))); ?>
    </div>
    <div id="PageContainer" class="tab-content">
        <?php /* 
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td valign="top" width="300">
                    <?php
                    echo $this->Form->label('Use saved template');
                    $template_images = array();
                    echo $this->element('uboard_left', array('templates' => $templates, 'prefix' => 'templates', 'template_id' => 'UseTemplate', 'current_template' => $this->data['Page']['template_id']));
                    echo "<span class='Hints'>Note: if you select a template,it will be delete all the content you have added for this page </span>";
                    echo "<div style='display:none;'>";
                    echo $this->Form->input('template_id', array(
                        'class' => 'INPUT',
                        'id' => 'UseTemplate',
                        'after' => $this->Html->link('Preview selected template', '#', array('id' => 'TemplatePreviewLink', 'class' => 'Preview'))));
                    echo "</div>";
                    ?>
                </td>
            </tr>
        </table>
         */ ?>
         
        <?php
        echo $this->Form->input('content', array('class' => 'INPUT editor'));
        ?>

        <?php
        echo $this->Form->input('keywords', array('class' => 'INPUT', 'label' => __("Meta Keywords", true)));
        echo $this->Form->input('description', array('class' => 'INPUT', 'label' => __("Meta description", true)));
        ?>	
    </div>
    
    <?php /* // we dont need attachments 
    <div class="attachments">
        <?php echo $this->Form->label('Attachments'); ?>
        <div>
            <ul id="menu">
                <li><a class="Acc_Head" href="#DocumentsTab">Documents</a>
                    <ul id="pdfs_list" >
                        <li>
                            <div class="RelatedItems">
                                <div id="DocumentsTab" class="attachment-tab input text">
                                    <label>Please select the attached Documents</label>
                                    <?php
                                    $selected_document = array();
                                    if ($this->data['Page']['attachments']['documents']) {
                                        $selected_document = $this->data['Page']['attachments']['documents'];
                                    }
                                    //echo $checkbox->checkboxMultiple('documents', $documents, empty($document) ? null : $document, null, array('class' => 'Input', 'name' => 'data[Page][attachments][documents]'));
                                    ?>
                                    <select name="data[Page][attachments][documents][]" data-placeholder="Choose a Document..." class="chzn-select" id="documents-select" multiple="multiple" style="width:350px;" tabindex="1">
                                        <?php foreach ($documents as $id => $document): ?>
                                            <option value="<?= $id ?>" <?= in_array($id, $selected_document) ? "selected" : "" ?>><?= $document ?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>

                <li><a class="Acc_Head" href="#LinksTab">Links</a>
                    <ul id="links_list" >
                        <li>
                            <div class="RelatedItems">
                                <div id="LinksTab" class="attachment-tab input text">
                                    <label>Please select the attached Links</label>

                                    <?php
                                    $selected_link = array();
                                    if ($this->data['Page']['attachments']['links']) {
                                        $selected_link = $this->data['Page']['attachments']['links'];
                                    }
                                    //echo $checkbox->checkboxMultiple('links', $links, empty($link) ? null : $link, null, array('class' => 'Input', 'name' => 'data[Page][attachments][links]'));
                                    ?>
                                    <select name="data[Page][attachments][links][]" data-placeholder="Choose a Link..." class="chzn-select" multiple='multiple' id="links-select"  style="width:350px;" tabindex="2">
                                        <?php foreach ($links as $id => $link): ?>
                                            <option value="<?= $id ?>" <?= in_array($id, $selected_link) ? "selected" : "" ?>><?= $link ?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a class="Acc_Head" href="#VideosTab">Videos</a>
                    <ul id="videos_list" >
                        <li>
                            <div class="RelatedItems">
                                <div id="VideosTab" class="attachment-tab input text">
                                    <label>Please select the attached Videos</label>

                                    <?php
                                    $selected_video = array();
                                    if ($this->data['Page']['attachments']['videos']) {
                                        $selected_video = $this->data['Page']['attachments']['videos'];
                                    }
                                    //echo $checkbox->checkboxMultiple('video', $videos, empty($video) ? null : $video, null, array('class' => 'Input', 'name' => 'data[Page][attachments][videos]'));
                                    ?>
                                    <select name="data[Page][attachments][videos][]" data-placeholder="Choose a Video..." class="chzn-select" multiple='multiple' id="videos-select"  style="width:350px;" tabindex="3">
                                        <?php foreach ($videos as $id => $video): ?>
                                            <option value="<?= $id ?>" <?= in_array($id, $selected_video) ? "selected" : "" ?>><?= $video ?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    
    */ ?>
    <?php
    //if (empty($this->data['Page']['default'])) {
    //echo $this->Form->input('access', array('class' => 'INPUT'));
    //}

    echo $this->Form->input('add_to_main_menu', array('id' => 'add-to-main', 'class' => 'INPUT', 'label' => 'Add To Menu?'));
    echo "<div id='MenuList' style='display:none;'>";
    echo $this->Form->input('menu_id', array('id' => 'SelectMenu', 'class' => 'INPUT', 'empty' => 'No Parent'));

        echo $this->Form->input('submenu_id', array('options' => array(), 'class' => 'INPUT', 'empty' => 'No Submenu'));
    echo "</div>";
    
    
 if(!empty($this->params['named']['dce']) && $this->params['named']['dce'] == 'yes'){ //to enable/disable dynamic children
    echo $this->Form->input('has_dynamic_children', array('id' => 'has_dc', 'class' => 'INPUT', 'label' => 'Dynamic Children: Does this menu have dynamic children? ( e.g. parent of a list from the database, such as categories, products etc.. )', 'style' => 'display:'));
    
    echo '<div id="children-options" style="display: ; ">'; 
    
    echo $this->Form->input('dc_model', array('id' => 'dc_model', 'class' => 'INPUT', 'label' => 'Children Model Name e.g. Category'));
    echo $this->Form->input('dc_condition', array('id' => 'dc_condition', 'class' => 'INPUT', 'label' => 'Condition e.g. active = \'1\''));
    echo $this->Form->input('dc_limit', array('id' => 'dc_limit', 'class' => 'INPUT', 'label' => 'Limit (max items)'));
    echo $this->Form->input('dc_order', array('id' => 'dc_order', 'class' => 'INPUT', 'label' => 'order eg. display_order asc, published desc'));
    echo $this->Form->input('dc_controller', array('id' => 'dc_controller', 'class' => 'INPUT', 'label' => 'Controller e.g. categories'));
    echo $this->Form->input('dc_action', array('id' => 'dc_action', 'class' => 'INPUT', 'label' => 'action e.g. view'));
    echo $this->Form->input('dc_title_field', array('id' => 'dc_title_field', 'class' => 'INPUT', 'label' => 'Title field (the field that will be used as title for menu items'));
    echo $this->Form->input('dc_uri_field', array('id' => 'dc_uri_field', 'class' => 'INPUT', 'label' => 'Url field (the field that will be used as url for menu items e.g. permalink or id'));
    
    echo "</div>" ; 
 }

    echo $this->Form->input('display_order', array('class' => 'INPUT'));
    //echo $this->Form->input('active', array('class' => 'INPUT'));
    // if (empty($this->data['Page']['default'])) {
    echo $this->Form->input('active', array('class' => 'INPUT'));
    // }
    ?>
    <?php echo $this->Form->enableEditors(); ?>
    <?php echo $this->Form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (!empty($this->data['Page']['id'])) {
      //  echo $this->element('save_as_new', array('model' => 'Page'));
    }
    ?>
    <?php echo $this->Form->end(); ?>
</div>
<?php
//if (empty($this->data['Page']['default'])) {
echo $this->element('permalink');
//}

echo $this->Html->script('chosen.jquery.min');
echo $this->Html->css('chosen');
?>
<script type="text/javascript"> $(".chzn-select").chosen(); </script>
<script type="text/javascript">
 <?php /* //   var  email_templates=<?= $this->Js->object($templates); ?>; */ ?> 
    
    
    
    function loadTemplate(id)
    {
        var oEditor = FCKeditorAPI.GetInstance('data[Page][content]') ;
        var OldText = oEditor.GetHTML();
        var NewText = (id)?OldText:"";
        if(OldText){
            if(confirm('Are you sure you want to update page content?')){
                NewText = (id)?email_templates[id].html : "" ;
                console.log(id);
                oEditor.SetHTML(NewText);
            }
        }else{
            NewText = email_templates[id].html ; 
            oEditor.SetHTML(NewText);
        }
        
    }
    
</script>

<script type="text/javascript">
    function check_menu_list(){
        if($('#add-to-main').is(':checked')){
            $('#MenuList').show();
            $('#has-dc').show
        }else{
            $('#MenuList').hide();
        }
    }
    $(function(){
		
        check_menu_list();
        $('#add-to-main').click(function(){
            check_menu_list();
        });
	
        $('#has-dc').click(function(){
            if($('#has-dc').is(':checked')){
                $('#children-options').show
            }
            else $('#children-options').hide(); 
        });


        $('#LinkConatiner,#PageContainer').hide();
		
        $('#OpenLink').bind('click', function(){
            $('#LinkContainer').show();
            $('#is_url').val(1);
            $('#PageContainer').hide();
            return false;
        });

        $('#OpenPage').bind('click', function(){
            $('#PageContainer').show();
            $('#LinkContainer').hide();
            return false;
        });

<?php if (!empty($this->data['Page']['url'])): ?>
            $('#OpenLink').click();
<?php else: ?>
            $('#OpenPage').click();
<?php endif; ?>
			
    });
</script>

<script type="text/javascript">//<![CDATA[
    $(function(){
        //		$('a', '.breadcrumb').click(function(){
        //			var href = $(this).attr('href');
        //
        //			$('.attachment-tab').hide();
        //			$(href).show();
        //			console.log(href);
        //			$('.active', '.breadcrumb').removeClass('active');
        //			$(this).addClass('active');
        //
        //			window.location.hash = 'tab:' + href.substr(1);
        //			return false;
        //		});
        //		
        //		
        //		var activeSelector = ':first';
        //		var rx = new RegExp('#tab:');
        //		if (window.location.hash != '' && rx.test(window.location.hash)){
        //			var hash = window.location.hash;
        //			activeSelector = hash.replace('tab:', '');
        //		}
        //		
        //		$('.attachment-tab').not(activeSelector).hide();
    });
    //]]></script>

<script type="text/javascript">
    $('document').ready(function(){
        $("#PageSubmenuId").attr("disabled", true);
        change_submenu($('#SelectMenu').val());
        if (!$('#PageSubmenuId').val())
        {
            document.getElementById('PageSubmenuId').disabled = true;
            $('#PageSubmenuId').html('<option></option>');
        }
        $('#SelectMenu').change(function(){
            change_submenu($(this).val());
        });
    });
    // Function to handle ajax.
    function change_submenu(menu_id)
    {
        if(!menu_id){
            return false;
        }
        $.ajax({
            async: true,
            type: "GET",
            url: "<?php echo Router::url('/admin/pages/get_submenus/'); ?>"+menu_id+"/"+<?php echo empty($this->data['Page']['id']) ? 0 : $this->data['Page']['id']; ?>,
            dataType: "json",
            success: function(data){
                $('#PageSubmenuId').html('<option value=0>No Submenu</option>');

                for(var i=0; i < data.submenus.length; i++)
                {
                    var submenu = data.submenus[i];
                    var is_Selected="";
                    var selected_submenu="<?php echo  !empty($this->data['Page']['submenu_id']) ? $this->data['Page']['submenu_id'] : 0 ; ?>";
                    //alert(submenu.Submenu.id);
                    if(submenu.Page.id==selected_submenu)
                    {
                        is_Selected="selected=selected";
                    }
                    $('#PageSubmenuId').append('<option value="'+submenu.Page.id+'"'+is_Selected+'>' +submenu.Page.title+'</option>');

                }
                document.getElementById('PageSubmenuId').disabled = false;
                return false;
            }
			

        });

    }
</script>