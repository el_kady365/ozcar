<?php echo $form->create('Page',array('action'=>$this->action)); ?>
<div class="rounded-item config-msg">
    <b class="bl-corners"></b><b class="br-corners"></b>
    <div class="FormExtended confirm-message">
	<h3><?=__('Confirmation Message !',true)?></h3>
		<?php echo __('Are you sure you want to delete?'); ?>
	<div class='delete-items'>
	    <?php foreach($pages as $i=>$page):?>
		<input type="hidden" value='<?=$page['Page']['id']?>' name='ids[]' />
		<span class='one-item'><?=$html->link($page['Page']['title'],array('action'=>'edit',$page['Page']['id']),array('target'=>'_blank'))?></span>
		<?php if(!empty ($page[$i+1])) {?>
		    <span class="seprated-item">,</span>
		<?php } ?>
	    <?php endforeach;?>
	</div>	<div class="clear"></div>
    </div>
</div>

<div class="confirm-action">
    <button class="green-button" type="submit" name="submit_btn" value="yes" ><?=__('Yes',true)?></button>
    <button class="red-button" type="submit" name="submit_btn" value="no" ><?=__('No',true)?></button>
</div>

<?php echo $form->end(); ?>