-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 29, 2013 at 08:59 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sitefactory`
--

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `keywords` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `template_id` int(11) DEFAULT '0',
  `menu_id` int(11) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT '0',
  `add_to_main_menu` tinyint(1) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_url` tinyint(1) NOT NULL,
  `access` int(11) DEFAULT '0' COMMENT '0=>Public,1=>members only,2=>Both',
  `attachments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` int(11) DEFAULT NULL,
  `default` tinyint(1) NOT NULL,
  `has_dynamic_children` tinyint(1) NOT NULL DEFAULT '0',
  `dynamic_children` mediumtext COLLATE utf8_unicode_ci,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_on_table` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `perma_link` (`permalink`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `site_id`, `title`, `permalink`, `content`, `keywords`, `description`, `template_id`, `menu_id`, `submenu_id`, `add_to_main_menu`, `url`, `is_url`, `access`, `attachments`, `active`, `display_order`, `default`, `has_dynamic_children`, `dynamic_children`, `created`, `modified`, `type`, `id_on_table`) VALUES
(1, 0, 'first page', 'first-page', 'some content', '', '', 0, NULL, 0, 1, '', 1, 0, NULL, 1, NULL, 0, 0, NULL, '2013-01-07 15:47:27', '2013-01-07 15:56:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_configs`
--

CREATE TABLE IF NOT EXISTS `site_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(255) NOT NULL,
  `setting` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_configs`
--

INSERT INTO `site_configs` (`id`, `group`, `setting`, `value`, `active`, `created`, `modified`) VALUES
(1, 'group1', 'site_name', 'Site 1', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(2,  'group1', 'user_name', 'admin', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(3,  'group1', 'password', '7c222fb2927d828af22f592134e8932480637c0d', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(4, 'group1', 'admin_email', 'amrelsaqqa@hotmail.com', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(5, 'group1', 'send_email_from', 'email@email.com', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(6, 'group1', 'keywords', '', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(7,  'group1', 'description', '', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(8,  'group1', 'robots', '', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(9,  'group1', 'currency_symbol', '$', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(10, 'group1', 'google_analytics', '', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(11,  'group1', 'map_coordinates', '', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(12,  'group1', 'mobile', '', 1, '2013-01-24 10:32:55', '2013-01-24 11:06:34'),
(14,  'group1', 'checked', '', 1, '2013-01-24 11:04:43', '2013-01-24 11:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `temp_files`
--

CREATE TABLE IF NOT EXISTS `temp_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `behavior` varchar(255) DEFAULT NULL,
  `uploaded_for` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
