
$(document).ready(function(){ 
    $('ul.action a.Delete').click(function(){
        var elem  = this ;

        $.confirm({
            'title': 'Delete Confirmation',
            'message' : 'Really delete #' + $(elem).parents('tr').find('td').eq(1).html() + ' ? ' ,
            'buttons' :{
                'Yes': {
                    'class' : 'red',
                    'fade' : false,
                    'action' : function(){
                        $.ajax({
                            url: $(elem).attr('href'),
                            async: false,
                            cache: false,
                            type: 'POST',
                            dataType : 'json',
                            success: function(data){
                                console.log(data);
                                if(data.status == 'success'){
                                    $(elem).parents('tr').fadeOut(800, function(){
                                        $(this).remove()
                                    }); 
                                }else{
                                    alert('error');
                                }
            
                            }
                        });
                    }
                },
                'No':{
                    'class': 'gray'
                }
            }
        });
        return false;
    });


});