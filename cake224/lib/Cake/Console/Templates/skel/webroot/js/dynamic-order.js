
$(document).ready(function(){ 
    $("#Table tbody").sortable({
        helper: function(e, ui) {
            ui.parents('table').find('thead tr th').each(function(){
                $(this).width($(this).width())
                });
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle: 'span.sortHandle',
        containment: 'form' ,
        cursor: "move",
        revert: true ,
        start: function(event, ui){
            //alert($('tr.nextPageBreak', $(ui.item).parent()).index());
            //alert(ui.item.index());
            if(ui.item.index() < $('tr.nextPageBreak', $(ui.item).parent()).index()) 
                var nextoffset = 1 ;
            else var nextoffset = 0; 
            
            if(ui.item.index() < $('tr.prevPageBreak', $(ui.item).parent()).index()) 
                var prevoffset = 1 ;
            else var prevoffset = 0; 
            
            $(ui.item).attr('prev-index', $('tr.prevPageBreak', $(ui.item).parent()).index() - prevoffset);
            $(ui.item).attr('next-index', $('tr.nextPageBreak', $(ui.item).parent()).index() - nextoffset);
        },
        stop: function(event, ui){
            var minorder = parseInt($('input[rel="display_order"]:first', ui.item).val(), 10) ; 
            if(isNaN(minorder)) minorder = 0 ;
            $('input[rel="display_order"]',  ui.item.parents('tbody')).each(function(){
                if(parseInt($(this).val(), 10) < minorder  )
                    minorder = parseInt($(this).val(), 10) ;
            });
            $('input[rel="display_order"]', ui.item.parents('tbody')).val(function(index, val){
                return (index + minorder) ;
            });
            
            $('tr.prevPageBreak', $(ui.item).parent()).removeClass( 'prevPageBreak');
            $('tr.nextPageBreak', $(ui.item).parent()).removeClass( 'nextPageBreak');
                
            if($(ui.item).attr('prev-index')> -1) $('tr' , $(ui.item).parent()).eq($(ui.item).attr('prev-index')).addClass( 'prevPageBreak');
            if($(ui.item).attr('next-index') > -1) $('tr' , $(ui.item).parent()).eq($(ui.item).attr('next-index')).addClass( 'nextPageBreak');
            
            $(ui.elem).removeAttr('prev-index').removeAttr('next-index');
            if($('a.dynamicPage', $(ui.item)).length){
                $('a.dynamicPage[rel="prev"]', $(ui.item)).insertAfter($('tr:first td input[rel="display_order"]', $(ui.item).parent()));
                $('a.dynamicPage[rel="next"]', $(ui.item)).insertAfter($('tr:last td input[rel="display_order"]', $(ui.item).parent()));

            }           
            
            $('.AdditionOption').trigger('change'); 
        }
    }); //.disableSelection(); 

    if($('.paging a[rel="prev"]').length) $('table#Table tbody tr:first input[rel="display_order"]').after('<a href="' + $('.paging a[rel="prev"]').attr('href') + '" class="dynamicPage" rel="prev">Load Previous Page</a>');
    if($('.paging a[rel="next"]').length) $('table#Table tbody tr:last input[rel="display_order"]').after('<a href="' + $('.paging a[rel="next"]').attr('href') + '" class="dynamicPage" rel="next">Load Next Page</a>');


    $('a.dynamicPage').click(function(){
        var elem = this
        $.ajax({
            url: $(elem).attr('href'),
            cache: false,
            async: false,
            success: function(data){
                if(!$('table#Table tbody tr').length) return false; 
                
                if($(elem).attr('rel') == 'next') {
                    $('table#Table tbody tr', data).each(function(){
                        $(this).appendTo('table#Table tbody');
                    });
                    $(elem).parents('tr').addClass('nextPageBreak');
                } else if($(elem).attr('rel') == 'prev') {
                    $('table#Table tbody tr', data).prependTo('table#Table tbody');
                    $(elem).parents('tr').addClass('prevPageBreak');
                }
                
                
                //$(elem).fadeOut(700,function(){$(this).remove(); });
                $(elem).removeAttr('href').css('visibility', 'hidden');
                
            }
        });
        return false; 
    });
    

});