$(document).ready(function(){
    $(':file[name^=data]').each(function(){
        $(this)
        .after($('<div class="uploadBox"></div>')
            .append(
                $('<input type="hidden" class="hiddenHash" />')
                .attr('id', $(this).attr('id'))
                .attr('name', $(this).attr('name')))
            .append($('<div class="uploader"></div>')))
        .remove();
    });
    
    
    $('form input.hiddenHash').each(function(){
        var md5 = /^[a-f0-9]{32}$/;
        var filename = $('div.file-upload span.file-name', $(this).parent().parent());
        if(filename.size() && md5.test(filename.html())){
            $(this).val(filename.html());
            filename.html('<i>using last submitted file...</i>');
            $('div.file-upload a').remove();
        }
    });
        
    $('form div.uploadBox div.uploader').each(function(){
        $(this).fineUploader({
            request: {
                endpoint: BASE_URL + 'temp_files/upload' ,
                inputName: $('input.hiddenHash', $(this).parent()).attr('name')
            }, 
            multiple: false,
            failedUploadTextDisplay:{
                mode: 'custom',
                responseProperty: 'error'
            }
                
        }).on('complete', function(e, id, filename, response){
            $('input.hiddenHash' , $(this).parent()).val(response.hash);
                
        });  
    });

});