(function(c) {
	c("a[data-reveal-id]").live("click", function(a) {
		a.preventDefault();
		a = c(this).attr("data-reveal-id");
		c("#" + a).reveal(c(this).data())
	});
	c.fn.reveal = function(a) {
		a = c.extend({}, {
			animation: "fadeAndPop",
			animationspeed: 300,
			closeonbackgroundclick: true,
			dismissmodalclass: "close-reveal-modal"
		}, a);
		return this.each(function() {
			function e() {
				locked = false
			}
			var b = c(this), d = (document.documentElement.clientHeight - b.height()) / 2;
		        topOffset = b.height() + d;
			locked = false;
			modalBG = c(".reveal-modal-bg");
			modalBG.length == 0 && (modalBG = c('<div class="reveal-modal-bg" />').insertAfter(b));
			b.bind("reveal:open", function() {
				modalBG.unbind("click.modalEvent");
				c("." + a.dismissmodalclass).unbind("click.modalEvent");
				locked || (locked = true, a.animation == "fadeAndPop" && (b.css({
					top: c(document).scrollTop() - topOffset,
					opacity: 0,
					visibility: "visible",
					marginLeft: (- b.width() / 2) + 'px'
				}), modalBG.fadeIn(a.animationspeed / 2), b.delay(a.animationspeed / 2).animate({
					top: c(document).scrollTop() + d + "px",
					opacity: 1
				}, a.animationspeed, e())), a.animation == "fade" && (b.css({
					opacity: 0,
					visibility: "visible",
					top: c(document).scrollTop() + d,
					marginLeft: (- b.width() / 2) + 'px'
				}), modalBG.fadeIn(a.animationspeed / 2), b.delay(a.animationspeed / 2).animate({
					opacity: 1
				}, a.animationspeed, e())), a.animation == "none" && (b.css({
					visibility: "visible",
					top: c(document).scrollTop() + d,
					marginLeft: (- b.width() / 2) + 'px'
				}), modalBG.css({
					display: "block"
				}), e()));
				b.unbind("reveal:open")
			});
			b.bind("reveal:close", function() {
				locked || (locked = true, a.animation == "fadeAndPop" && (modalBG.delay(a.animationspeed).fadeOut(a.animationspeed), b.animate({
					top: c(document).scrollTop() - topOffset + "px",
					opacity: 0
				}, a.animationspeed / 2, function() {
					b.css({
						top: d,
						opacity: 1,
						visibility: "hidden"
					});
					e()
				})), a.animation == "fade" && (modalBG.delay(a.animationspeed).fadeOut(a.animationspeed), b.animate({
					opacity: 0
				}, a.animationspeed, function() {
					b.css({
						opacity: 1,
						visibility: "hidden",
						top: d
					});
					e()
				})), a.animation == "none" && (b.css({
					visibility: "hidden",
					top: d
				}), modalBG.css({
					display: "none"
				})));
				b.unbind("reveal:close")
			});
			b.trigger("reveal:open");
			c("." + a.dismissmodalclass).bind("click.modalEvent", function() {
				b.trigger("reveal:close")
			});
			a.closeonbackgroundclick && (modalBG.css({
				cursor: "pointer"
			}), modalBG.bind("click.modalEvent", function() {
				b.trigger("reveal:close")
			}));
			c("body").keyup(function(a) {
				a.which === 27 && b.trigger("reveal:close")
			})
		})
	}
})(jQuery);
