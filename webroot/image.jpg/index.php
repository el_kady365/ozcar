<?php

ini_set('display_errors', false);
header('Content-Type: image/jpeg');
session_name('CAKEPHP');
session_start();
$bgcolor = false;


function generateCode($characters) {
    /* list all possible characters, similar looking characters and vowels have been removed */
    $possible = '2345789ABCDEFGHKMNPQRSTUVWXZY';
    $code = '';
    $i = 0;
    while ($i < $characters) {
        $code .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
        $i++;
    }
    return $code;
}

function CaptchaSecurityImages($width = '270', $height = '40', $characters = '4', $bgcolor = false) {
    $font = './Helvetica_Greek.ttf';
    $code = generateCode($characters);
    /* font size will be 75% of the image height */
    $font_size = 20;
    $image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
    /* set the colours */


    $background_color = imagecolorallocate($image, 245, 255, 255);
    $text_color = imagecolorallocate($image, 0, 90, 171);


    //$line_color = imagecolorallocate($image, 20, 40, 100);
    //$noise_color = imagecolorallocate($image, 100, 180, 255);
    /* generate random dots in background */
    for ($i = 0; $i < (0.3 * $width * $height) / 3; $i++) {
        imagefilledellipse($image, mt_rand(0, $width), mt_rand(0, $height), 1, 1, $noise_color);
    }
    /* generate random lines in background 
      for( $i=0; $i<(0.3* $width*$height)/150; $i++ ) {
      imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $line_color);
      } */
    /* create textbox and add text */

    $angel = rand(-8, 8);
    $textbox = imagettfbbox($font_size, $angel, $font, $code) or die('Error in imagettfbbox function');
    $x = ($width - $textbox[4]) / 2;
    $y = ($height - $textbox[5]) / 2;
    imagettftext($image, $font_size, $angel, $x, $y, $text_color, $font, $code) or die('Error in imagettftext function');
    /* output captcha image to browser */
    //header('Content-Type: image/jpeg');
    imagejpeg($image);
    imagedestroy($image);
    $_SESSION['security_code'] = $code;
    session_register('security_code', $code);
}

CaptchaSecurityImages('120', '30', '6', $bgcolor);
?>