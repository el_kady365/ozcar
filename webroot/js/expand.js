$(function() {
	$('.staff-actions a.btn').click(function() {
		if ($('span.arrow', $(this)).hasClass('arrow-down')) {
			$(this).html(function(i, h) {
				return h.replace(/EXPAND/i, 'REDUCE')
			});
			$('span.arrow', $(this)).removeClass('arrow-down');
			$('span.arrow', $(this)).addClass('arrow-up');
			$('.staff-details', $(this).parents('.quick-info')).slideDown();
		} else {
			$(this).html(function(i, h) {
				return h.replace(/REDUCE/i, 'EXPAND')
			});
			$('span.arrow', $(this)).addClass('arrow-down');
			$('span.arrow', $(this)).removeClass('arrow-up');
			$('.staff-details', $(this).parents('.quick-info')).slideUp();

		}
		
		return false;

	});


});
