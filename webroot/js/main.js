/*
 * Image preview script 
 * powered by jQuery (http://www.jquery.com)
 * 
 * written by Alen Grakalic (http://cssglobe.com)
 * 
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 *
 */
 
this.imagePreview = function(){	
    /* CONFIG */
		
    xOffset = 10;
    yOffset = 30;
		
    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result
		
    /* END CONFIG */
    $("a.preview").hover(function(e){
        this.t = this.title;
        this.title = "";
        var c = (this.t != "") ? "<br/>" + this.t : "";
        $("body").append("<p id='preview' style='z-index:150'><img src='"+ this.href +"' alt='Image preview' />"+ c +"</p>");
        var l=e.clientX+15;
        var t=e.clientY+$(window).scrollTop();
        l=l || $('#preview').position().left;
        t=t || $('#preview').position().top;
        if(t+$('#preview').height()>$(window).scrollTop()+$(window).height()-50)
            t=$(window).scrollTop()+$(window).height()-$('#preview').height()-50;
        if(t<$(window).scrollTop()) t=$(window).scrollTop();
        
        $("#preview")
        .css("top",(t)+"px")
        .css("left",(l)+"px")
        .fadeIn("fast");
    },
    function(){
        this.title = this.t;
        $("#preview").remove();
    });	
    $("a.preview").mousemove(function(e){
        var l=e.clientX+15;
        var t=e.clientY+$(window).scrollTop();
        l=l || $('#preview').position().left;
        t=t || $('#preview').position().top;
        if(t+$('#preview').height()>$(window).scrollTop()+$(window).height()-50)
            t=$(window).scrollTop()+$(window).height()-$('#preview').height()-50;
        if(t<$(window).scrollTop()) t=$(window).scrollTop();

        $("#preview")
        .css("top",(t)+ "px")
        .css("left",(l) + "px")
    });
};


// starting the script on page load
$(document).ready(function(){
    imagePreview();
});