
$(document).on('click', 'li.record a.delete, li.record a.delete-item', function () {
    var elem = this;
    $(elem).closest('li.record').addClass('selected');
    $.confirm({
        'title': 'Delete Confirmation',
        'message': 'Really delete ' + $(elem).closest('li.record').find('input[name^=data]:first').attr('name').replace(/data\[(.*?)\].*/, "$1") + " #" + $(elem).closest('li.record').find('input[name^=data][name$=\\[id\\]]:first').val() + ' ? ',
        'buttons': {
            'Yes': {
                'class': 'red',
                'fade': false,
                'action': function () {
                    $(elem).closest('li.record').fadeTo(200, 0.5, function () {
                        $(this).slideUp(200, function () {
                            $(this).trigger('deleted');
                            $(this).remove();
                        });
                    });
                    if ($(elem).closest('ul.many').children().length == 1) { // it was the last item
                        $(elem).closest('.many-container').find('.add').html(function (i, h) {
                            return h.replace(/^Add Another/i, 'Add New');
                        });
                    }
                }
            },
            'No': {
                'class': 'gray',
                'action': function () {
                    $(elem).closest('li.record').removeClass('selected');

                }
            }
        }
    });
    return false;
});


$(document).on('updated', function () {
    $('ul.many').each(function () {
        if ($(this).data('counter') != null)
            return;
        $(this).data('counter', $(this).children().length)
        $(this).data('template',
                $('li:first', $(this))
                .clone(true)
                .find(':input')
                .removeAttr('value')
                .end()

                );

        if ($(this).children('li').length == 1) {
            var $ul = $(this);

            var hasdata = false;
            $(this).find('li.record :input').each(function () {
                if ($(this).val().length && !$(this).is(':hidden')) {
                    hasdata = true;
                    return false;
                }
            });
            if (!hasdata && (!$(this).find('li.record input[name^=data\\[][name$=\\[id\\]]:first').length || $(this).find('li.record input[name^=data\\[][name$=\\[id\\]]:first').val().length == 0)) {
//                $('li:first', $(this)).remove();
//                $(this).hide();

            }

        }

    });
});
$(document).trigger('updated');

$(document).on('click', '.many-container a.add', function () {
    var link = this;
    $('ul.many', $(link).closest('div.many-container')).show();
    $(this).html(function (i, h) {
        return h.replace(/^Add New/i, 'Add Another').replace(/Attach a /i, 'Attach Another ');
    });
    var counter = $('ul.many', $(link).closest('div.many-container')).data('counter');
    $('ul.many', $(link).closest('div.many-container')).data('counter', counter + 1);

    $('ul.many', $(link).closest('div.many-container')).data('template')
            .clone(true).find('.file-upload').remove().end()
            .find('.upload-box').removeAttr('style').end()
            .find('h5.dealership-title').text(function (name, value) {
        return value.replace(/\d+/, counter + 1);
    }).end()
            .appendTo($('ul.many', $(this).closest('div.many-container')))
            .hide()
            .find('*')
            .attr('for', function (index, attr) {
                if (!attr)
                    return;
                return attr.replace(/(.*?)(\d+)(.*?)/, "$1" + counter + "$3");
            })
            .attr('id', function (index, attr) {
                if (!attr)
                    return;
                return attr.replace(/(.*?)(\d+)(.*?)/, "$1" + counter + "$3");
            })
            .attr('name', function (index, attr) {
                if (!attr)
                    return;
                return attr.replace(/data\[(.*?)\]\[(\d+)\]\[(.*?)\]/, "data[$1][" + counter + "][$3]");
            })
            .attr('rel', function (index, attr) {

                if (!attr)
                    return;

                return attr.replace(/(.*?)(\d+)(.*?)/, "$1" + counter + "$3");
            })
            .end().trigger('duplicated')
            .slideDown(300);


    return false;
});



