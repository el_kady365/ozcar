$(document).ready(function(){
    
    $('li.record a.delete').click(function(){
        var elem  = this ;
        $(elem).parents('li.record').addClass('selected');
        $.confirm({
            'title': 'Delete Confirmation',
            'message' : 'Really delete ' + $(elem).parents('li.record').find('input[name^=data]:first').attr('name').replace(/data\[(.*?)\].*/, "$1") + " #" + $(elem).parents('li.record').find('input[name^=data][name$=\\[id\\]]:first').val() +  ' ? ' ,
            'buttons' :{
                'Yes': {
                    'class' : 'red',
                    'fade' : false,
                    'action' : function(){
                            $(elem).parents('li.record').fadeTo(200, 0.5,  function(){$(this).slideUp(200, function(){$(this).remove();});});
                            if($(elem).parents('ul.many').children().length == 1){ // it was the last item
                                $(elem).parents('.many-container').find('.add').html(function(i,h){ return h.replace(/^Add Another/i, 'Add New');});
                            }
                    }
                },
                'No':{
                    'class': 'gray',
                    'action' : function(){
                                $(elem).parents('li.record').removeClass('selected');

                    }
                }
            }
        });
        return false;
    });


    $('ul.many').each(function(){
        $(this).data('counter', $(this).children().length)
        $(this).data('template', 
            $('li:first', $(this))
            .clone(true)
            .find(':input')
                .removeAttr('value')
            .end()

         );
             
        if($(this).children('li').length == 1){
          if($(this).find('li.record input[name^=data\\[][name$=\\[id\\]]:first').val().length == 0){
            $('li:first', $(this)).remove();  
          }
        }
             
    });
    
    $('.many-container a.add').click(function(){
        var link = this;
        $(this).html(function(i, h){ return h.replace(/^Add New/i, 'Add Another');});
        var counter = $('ul.many', $(link).parents('div.many-container')).data('counter');
        $('ul.many', $(link).parents('div.many-container')).data('counter', counter + 1);
        
         $('ul.many', $(link).parents('div.many-container')).data('template')
            .clone(true)
            .appendTo($('ul.many', $(this).parents('div.many-container')))
            .hide()
            .find('*')
                .attr('for', function(index, attr){
                    if(!attr) return ;
                    return attr.replace(/(.*?)(\d+)(.*?)/, "$1" +  counter  + "$3") ; 
                })
                .attr('id', function(index, attr){
                    if(!attr) return ;
                    return attr.replace(/(.*?)(\d+)(.*?)/, "$1" + counter + "$3") ; 
                })
                .attr('name', function(index, attr){
                    if(!attr) return ;
                    return attr.replace(/data\[(.*?)\]\[(\d+)\]\[(.*?)\]/, "data[$1][" +  counter  + "][$3]") ; 
                })
            .end()
            .slideDown(300)

        
        return false;
    });
    
    

});