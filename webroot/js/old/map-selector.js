// requires : 
    // reveal.css
    // https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places
    // jquery.reveal.js


$(function(){
    
    var modalContainer = '<div id="googleMapModal" class="reveal-modal">  <form onSubmit="return false;"> <label for="_1stsearchTextField">Map coordinates</label>    <input  id="_1stsearchTextField" class="INPUT"   type="text" size="50"/> </form>		       <div id="_1stmap_canvas" class="map-box" style="width: 500px; height:300px;"></div>    <a href="javascript:void(0)" id="map-submitdata"  class="close-reveal-modal button-primary">Save Coordinates</a>    <a href="javascript:void(0)" id="map-canceldata"  class="close-reveal-modal" >&nbsp;&nbsp;&nbsp;<strong>Or </strong>Cancel</a> </div>';        
    $(modalContainer).appendTo('body');
    var mapOptions = {
        center: new google.maps.LatLng(0,0),
        zoom: 1,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var globalGoogleMap = new google.maps.Map(document.getElementById('_1stmap_canvas'),
        mapOptions);


    var input = document.getElementById('_1stsearchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', globalGoogleMap);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: globalGoogleMap
    });

    marker.setAnimation(google.maps.Animation.BOUNCE);
    marker.setMap(globalGoogleMap);
    
    var map_clicked =  function (event) {				   
        marker.setPosition(event.latLng);
        var input = $('input.map-selector', $(globalGoogleMap.originator).parent());
        input.val("" + event.latLng.lat() + "," + event.latLng.lng()+ "," + globalGoogleMap.getZoom());
        
        input.data('map-latlng',globalGoogleMap.getCenter());
        input.data('map-zoom',globalGoogleMap.getZoom());
        input.data('map-marker',marker.getPosition());
            
            				
    };
    var map_dragged =  function (event) {
        var input = $('input.map-selector', $(globalGoogleMap.originator).parent());

        input.data('map-latlng',this.getCenter());
        input.data('map-zoom',this.getZoom());            
            				
    };
        
    google.maps.event.addListener(globalGoogleMap, 'click', map_clicked);
    google.maps.event.addListener(globalGoogleMap, 'drag', map_dragged);
    google.maps.event.addListener(globalGoogleMap, 'zoom_changed', map_dragged);
			  
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        var place = autocomplete.getPlace();
        if(typeof place.geometry  == 'undefined') return false;
        
        if (place.geometry.viewport) {
            globalGoogleMap.fitBounds(place.geometry.viewport);
        } else {
            globalGoogleMap.setCenter(place.geometry.location);
            globalGoogleMap.setZoom(17);  // Why 17? Because it looks good.
        }
	

        marker.setPosition(place.geometry.location);
        var input = $('input.map-selector', $(globalGoogleMap.originator).parent());
        input.val("" + place.geometry.location.lat() + "," + place.geometry.location.lng()+ "," + globalGoogleMap.getZoom());
            
            
        input.data('map-latlng',globalGoogleMap.getCenter());
        input.data('map-zoom',globalGoogleMap.getZoom());
        input.data('map-marker' , marker.getPosition());
            
        var address = '';
        if (place.address_components) {
            address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

    });
    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
      
 
    $('input.map-selector').each(function(){
        $(this).after('<a data-reveal-id="googleMapModal" class="big-link button-secondary map-selector-btn" href="#">Change Map</a>')               
        var latlngzoom = $(this).val().split(',');
        var latlng = new google.maps.LatLng(latlngzoom[0] ? parseFloat(latlngzoom[0], 10) : 0 , latlngzoom[1] ? parseFloat(latlngzoom[1], 10) : 0);
        $(this).data('map-latlng', latlng);

        $(this).data('map-marker', latlng);               
        $(this).data('map-zoom', latlngzoom[2] ? parseFloat(latlngzoom[2],10) : 1);
    });
           
    $('a.map-selector-btn').click(function(){
        globalGoogleMap.originator = this;
        var input = $('input.map-selector' , $(this).parent());
        input.data('old-value', input.val());
        globalGoogleMap.setCenter(input.data('map-latlng'));
        globalGoogleMap.setZoom(input.data('map-zoom'));
        marker.setPosition(input.data('map-marker'));
        

    });

    $('a#map-canceldata').click(function(){
        var input = $('input.map-selector', $(globalGoogleMap.originator).parent());
        input.val(input.data('old-value'));
        var latlngzoom = input.data('old-value').split(',');
        var latlng = new google.maps.LatLng(latlngzoom[0] ? parseFloat(latlngzoom[0], 10) : 0 , latlngzoom[1] ? parseFloat(latlngzoom[1], 10) : 0);
        input.data('map-marker', latlng);             
        input.data('map-latlng', latlng);        
        input.data('map-zoom', latlngzoom[2] ? parseFloat(latlngzoom[2],10) : 1);
             
    });
                
           
});
 