$(document).ready(function () {

    "use strict";

    ////////////////////// Radio and Checkboxs  ////////////////////// 
    $('input[type="radio"]').wrap('<div class="radio-btn"><i></i></div>');
    $('input[type="radio"]').each(function () {
        if ($(this).prop('checked')) {
            $(this).closest('.radio-btn').addClass('checkedRadio');
        }
    });
    $(".radio-btn").on('click', function () {
        var _this = $(this),
                block = _this.parent().parent().parent();

        block.find('input:radio').prop('checked', false);
        block.find(".radio-btn").removeClass('checkedRadio');
        _this.addClass('checkedRadio');
        _this.find('input:radio').prop('checked', 'checked');
        _this.find('input:radio').trigger('change');


    });
    $('input[type="checkbox"]').wrap('<div class="check-box"><i class="fa fa-check"></i></div>');
    $.fn.toggleCheckbox = function () {
        this.prop('checked', !this.prop("checked"));
    }
    $('input[type="checkbox"]').each(function () {
        if ($(this).prop('checked')) {
            $(this).closest('.check-box').addClass('checkedBox');
        }
    });
    $('.check-box').on('click', function () {
        $(this).find('input:checkbox').toggleCheckbox();
        $(this).toggleClass('checkedBox');
        $(this).find('input:checkbox').trigger('change');
//        alert($(this).find(':checkbox').prop('name'));
    });

    ////////////////////// Selectboxs  ////////////////////// 
    $(".select select").wrap("<div class='select-style'></div>");

    ////////////////////// Circle Chart   ////////////////////// 

    if ($('#profile-state').length) {
        $('#profile-state').circliful();
    }



    ////////////////////// Dual List   ////////////////////// 
    if ($('#source').length) {
        $('#source, #destination').listswap({
            height: 180,
            is_scroll: true,
            label_add: 'ADD TO LIST',
            label_remove: 'REMOVE FROM LIST',
        });
    }



    if ($('#source2').length) {
        $('#source2, #destination2').listswap({
            height: 180,
            is_scroll: true,
            label_add: 'ADD TO LIST',
            label_remove: 'REMOVE FROM LIST',
        });
    }

    ////////////////////// Dropdown toggle   ////////////////////// 
    if ($('.dropdown-toggle').length) {
        $('.dropdown-toggle').click(function () {
            $(this).next('.dropdown').toggle();
            return false;
        });
    }

//Accordion
    $('.toggle-btn').on('click', function () {
        var target = $($(this).next('.content-box'));
        $(this).toggleClass('active');
        $(target).slideToggle(300);
        $(target).parents('.accordion-panel').toggleClass('active-box');
        return false;
    });

    if ($('.type-ico').length) {
        $(function () {
            var select = $('select[name="body[]"]');

            select.each(function () {
                var elements = $(this).get(0).options;
                for (var i = 0; i < elements.length; i++) {
                    if ($(elements[i]).prop('selected')) {

                        $(this).closest('form').find('a[rel="' + elements[i].value + '"]').addClass('active');
                    }
                }
            });

        });



        //type-ico
        $('.type-ico').on('click', function () {
            var href = $(this).prop('rel');

            var select = $(this).closest('form').find('select[name="body[]"]').get(0);
            var elements = select.options;
            console.log(elements);
            for (var i = 0; i < elements.length; i++) {
                if (elements[i].value == href) {
                    if (elements[i].selected == true) {
                        elements[i].selected = false;
                    } else {
                        elements[i].selected = true;
                    }
                }
            }
            $(select).change();

            $(this).toggleClass('active');
            return false;
        });

    }


    if ($('.car-preview-thumb').length) {
        $(".car-preview-thumb").owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            transitionStyle: "backSlide"
        });
    }



    if ($('.popular-dealerships').length) {
        var owl = $(".popular-dealerships-slider");
        owl.owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            pagination: false
        });
        // Custom Navigation Events
        $(".next").click(function () {
            owl.trigger('owl.next');
        })
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        })
    }


    if ($('.testimonials-slider').length) {
        $(".testimonials-slider").owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            navigation: true,
            singleItem: true,
            transitionStyle: "backSlide"
        });

    }

    if ($('.featured-cars').length) {
        $(".featured-cars").owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            navigation: true,
            items: 4,
            paginationNumbers: false,
            transitionStyle: "backSlide"
        });

    }


    if ($('.random-cars-slider').length) {
        $(".random-cars-slider").owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            navigation: false,
            paginationNumbers: false,
            transitionStyle: "backSlide"
        });

    }






// Slider Referance -  http://ionden.com/a/plugins/ion.rangeSlider/en.html 
    $(function () {
        if ($('#Price').length) {
            $("#Price").ionRangeSlider({
                type: "double",
                min: 1000,
                max: 100000,
                step: 500,
                prefix: '$ ',
                onChange: function (data) {
                    $('input[name="price_from"]').val(data.from);
                    $('input[name="price_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="price_from"]').val('');
                        $('input[name="price_to"]').val('');
                    }
                }
            });
        }


        if ($('#Kilometers').length) {
            $("#Kilometers").ionRangeSlider({
                type: "double",
                min: 10000,
                max: 150000,
                step: 1000,
                postfix: 'Km ',
                onChange: function (data) {
                    $('input[name="kms_from"]').val(data.from);
                    $('input[name="kms_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="kms_from"]').val('');
                        $('input[name="kms_to"]').val('');
                    }
                }
            });
        }
        if ($('#Power').length) {
            $("#Power").ionRangeSlider({
                type: "double",
                min: 80,
                max: 250,
                step: 20,
                postfix: ' Kw',
                onChange: function (data) {
                    $('input[name="power_from"]').val(data.from);
                    $('input[name="power_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="power_from"]').val('');
                        $('input[name="power_to"]').val('');
                    }
                }
            });
        }
        if ($('#EngineSize').length) {
            $("#EngineSize").ionRangeSlider({
                type: "double",
                min: 1,
                max: 7,
                step: 0.2,
                postfix: ' L',
                onChange: function (data) {
                    $('input[name="engine_from"]').val(data.from);
                    $('input[name="engine_to"]').val(data.to);
                    if (data.from == data.min && data.to == data.max) {
                        $('input[name="engine_from"]').val('');
                        $('input[name="engine_to"]').val('');
                    }
                }
            });
        }




        if ($('.all-search-tabs li a').length) {
            $('#CarHomeSearch input[name="type_id"]').val($('.all-search-tabs li a:first').prop('rel'));
            $('#CarAdvancedSearch input[name="type_id"]').val($('.all-search-tabs li a:first').prop('rel'));
        }

        $('.all-search-tabs li a').on('click', function () {
            var $type = $(this).prop('rel');
            $('#CarHomeSearch input[name="type_id"]').val($type);
            $('#CarAdvancedSearch input[name="type_id"]').val($type);
        });


    });


    if ($('#flashMessage').length) {
        var $type = '';

        if ($('#flashMessage').hasClass('Sucmessage')) {
            $type = "success";

        } else if ($('#flashMessage').hasClass('Erroressage')) {
            $type = "error";

        } else if ($('#flashMessage').hasClass('Notemessage')) {
            $type = "warning";
        }
        notification($type, $('#flashMessage').text());
        $('#flashMessage').remove();


    }



});

function notification(type, text)
{
    $icon = '';
    if (type == 'success') {
        $icon = '<i class="fa fa-check"></i> ';
    } else if (type == 'error') {
        $icon = '<i class="fa fa-close"></i> ';
    }

    noty({
        text: $icon + text,
        type: type,
        layout: 'topRight',
        theme: 'relax',
        killer: true,
        animation: {
            open: 'animated fadeInLeft',
            close: 'animated fadeOutLeft',
            easing: 'swing',
            speed: 500
        }
    });
}



$(window).load(function () {
    // The slider being synced must be initialized first
    if ($('#videocarousel').length) {
        $('#videocarousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 108,
            asNavFor: '#videoslider'
        });
    }
    if ($('#videoslider').length) {
        $('#videoslider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#videocarousel"
        });
    }
});


$(document).on('click', '.car-compare, .save-dealership', function () {
    $url = $(this).prop('href');
    $.ajax({
        url: $url,
        dataType: 'json'
    }).done(function (data) {

        notification(data.status, data.message);
    }).always(function (jqXHR) {
        if (jqXHR.status == 403) {
            self.location = $url;
        }

    });
    return false;
});









function activate_tabs() {
//tabs
    $(document).ready(function () {
        if ($('.tabs').length) {
//Tabs
            $('.tabs .tab-btn').on('click', function () {
                var target = $(this).data('id');
                $(this).closest('.tab-buttons').find('.tab-btn').removeClass('active');
                $(this).addClass('active');
                $(this).closest('.tabs').find('.tabs-box .tab').fadeOut(0);
                $(this).closest('.tabs').find('.tabs-box .tab').removeClass('current');
                $(this).closest('.tabs').find('.tabs-box ' + target).fadeIn(300);
                $(this).closest('.tabs').find('.tabs-box ' + target).addClass('current');
                var windowWidth = $(window).width();
                if (windowWidth <= 700) {
                    $('html, body').animate({
                        scrollTop: $('.tabs-box').offset().top
                    }, 1000);
                }
                return false;
            });
        }

    });

}
activate_tabs();