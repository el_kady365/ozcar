 /*******  Tabs Code  ******/

    $('.tab-links ul li:first').addClass('current');
    $(".tab-editable").hide();
    $('.tab-contents .tab-editable:first').css('display','block');
    $('.tab-links ul li a').click(function(){
        
        href=$(this).attr('href');
        $(this).parents('ul').find(' > li').removeClass('current');   
        $(this).parents('li').addClass('current');
        $(this).parents('.tabs').find('.tab-editable').css('display','none');
        $('div.tab-editable#'+href).css('display','block');
        return false;
    });

/*******  Tabs Code  ******/