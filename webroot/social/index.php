<?php

@session_name("CAKEPHP");
@session_start();
define('CONF_FILE', dirname(__FILE__) . '/' . 'opauth.conf.php');
define('OPAUTH_LIB_DIR', dirname(__FILE__) . '/lib/Opauth/');
require CONF_FILE;
$config = array_merge($config, $config_2);
require OPAUTH_LIB_DIR . 'Opauth.php';
$Opauth = new Opauth($config);
die();


?>