<?php

/**
 * Opauth basic configuration file to quickly get you started
 * ==========================================================
 * To use: rename to opauth.conf.php and tweak as you like
 * If you require advanced configuration options, refer to opauth.conf.php.advanced
 */
$config_2 = array(
    /**
     * Path where Opauth is accessed.
     *  - Begins and ends with /
     *  - eg. if Opauth is reached via http://example.org/auth/, path is '/auth/'
     *  - if Opauth is reached via http://auth.example.org/, path is '/'
     */
    /**
     * Callback URL: redirected to after authentication, successful or otherwise
     */
    'callback_url' => '{path}callback',
    /**
     * A random string used for signing of $auth response.
     * 
     * NOTE: PLEASE CHANGE THIS INTO SOME OTHER RANDOM STRING
     */
    'security_salt' => 'ZZFmiilYf8Fyw5W10rx4W1KsVrieQCnpBzzpTBWA5vJidQKDx8pMJbmw28R1C4m',
    'security_timeout' => '2 minutes',
    /**
     * Strategy
     * Refer to individual strategy's documentation on configuration requirements.
     * 
     * eg.
     * 'Strategy' => array(
     * 
     *   'Facebook' => array(
     *      'app_id' => 'APP ID',
     *      'app_secret' => 'APP_SECRET'
     *    ),
     * 
     * )
     *
     */
    'Strategy' => array(
        // Define strategies and their respective configs here

        'Facebook' => array(
            'app_id' => '380720198785126',
            'app_secret' => '6600388c60ed7fd2f62b050a8606bbe0'
            //'app_id' => '361124483965786',
            //'app_secret' => '008496b12be3009b92cc04de47cba78a'
        ),
        'Google' => array(
            'client_id' => '410910500913-fe92936m35lob8qu82ojbkj1ag4659cg.apps.googleusercontent.com',
            'client_secret' => 'onG6lljSF_5u7TCVzjw5pAbX',
            
        ),
        'Twitter' => array(
            'key' => 'xGIc6wbOAP6wmN6VvYdpxMwtK',
            'secret' => 'KEO7SP9DVebFb9ODAHgvvnzqEdrCZijx2iqBrTgWOcW5jhW7VI'
        ),
        'LinkedIn' => array(
            'api_key' => '75z2fxtqacvtlm',
            'secret_key' => 'tAN3klONNFiHjsqt'
        ),
    ),
);

